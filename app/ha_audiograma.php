<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ha_audiograma extends Model
{
    protected $table = "ha_audiograma";
    protected $fillable = [
        "oidoderecho",
        "oidoizquierdo",
        "nivel_id",
        "historiaAudiometria_id"
    ];
    public function historiaAudiometria() {
		return $this->belongsTo('App\HistoriaAudiometria');
	}
}
