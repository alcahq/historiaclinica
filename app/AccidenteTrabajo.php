<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccidenteTrabajo extends Model
{
    protected $table = 'hc_accidente';

    protected $fillable = [
        'lesion',
        'empresa',
        'dias_incapacidad',
        'secuelas',
        'fecha',
        'paciente_id',
        'historiaclinica_id',
    ];

    public function historiaClinica() {
        return $this->belongsTo('App\HistoriaClinica');
    }

    public function paciente() {
        return $this->belongsTo('App\Patient');
    }
}
