<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Esta clase es usaba para Visiometria y optometria
 *
 */
class RiesgoCargoEvaluar extends Model
{
    protected $table = "riesgoCargoEvaluar";
    
    public function historiaoptometria()
    {
        return $this->belongsTo('App\HistoriaOptometria');
    }
}
