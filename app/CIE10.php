<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CIE10 extends Model
{
    protected $table = "hc_cie10";

	protected $fillable = [
		"codigo",
		"descripcion",
	];

	public function impresiondiagnostica() {
		return $this->hasMany('App\ImpresionDiagnostica', 'cie10_id');
	}
}
