<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NuevaValoracion extends Model
{
    protected $table = 'hc_nuevavaloracion';

    protected $fillable = [
        'nota_valoracion_medica',
        'nota_recomendacion_medica',
        'nota_recomendacion_ingreso',
        'nota_recomendacion_ocupacional',
        'nota_recomendacion_habito',
        'nota_complementaria',
        'nota_complementaria_otro',
        'nota_restriccion',
        'nota_recomendacion_adicional',
        'historiaclinica_id'
    ];
}
