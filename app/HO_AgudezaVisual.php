<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HO_AgudezaVisual extends Model
{
    protected $table = 'ho_agudezaVisual';

    public function historiaoptometria()
    {
        return $this->belongsTo('App\HistoriaOptometria');
    }
}
