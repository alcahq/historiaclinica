<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriaVisiometria extends Model
{
    protected $table = "historiavisiometria";

	public function agudezavisual()
    {
        return $this->hasOne('App\HV_AgudezaVisual', 'historiavisiometria_id', 'id');
    }
    public function antecedente()
    {
        return $this->hasOne('App\HO_Antecedentes', 'historiavisiometria_id', 'id');
    }
    public function recomendacion()
    {
        return $this->hasOne('App\HO_Recomendaciones', 'historiavisiometria_id', 'id');
    }
    public function sintoma()
    {
        return $this->hasOne('App\Sintomas', 'historiavisiometria_id', 'id');
    }
    public function riesgocargoevaluar()
    {
        return $this->hasOne('App\RiesgoCargoEvaluar', 'historiavisiometria_id', 'id');
    }
    public function paciente(){
        return $this->belongsTo('App\Patient');
    }

    public function medicoapertura() {
        return $this->belongsTo('App\User','medicoApertura_id');
    }

    public function medicocierre() {
        return $this->belongsTo('App\User','medicoCierre_id');
    }

    public function medicoanula() {
        return $this->belongsTo('App\User','medicoAnula_id');
    }

    public function empresa(){
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }
}
