<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_name', 'type_document',
        'document', 'role_id', 'registro_medico', 'licencia', 'nombre_completo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Instancia del modelo role que pertenece a la clase User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function historiaClinicaA(){
        return $this->hasMany('App\HistoriaClinica', 'medicoApertura_id');
    }

    public function historiaClinicaC(){
        return $this->hasMany('App\HistoriaClinica', 'medicoCierre_id');
    }

    public function historiaClinicaAn(){
        return $this->hasMany('App\HistoriaClinica', 'medicoAnula_id');
    }

    public function historiaOptometriaA(){
        return $this->hasMany('App\HistoriaClinica', 'medicoapertura_id');
    }

    public function historiaOptometriaC(){
        return $this->hasMany('App\HistoriaClinica', 'medicocierre_id');
    }

    public function historiaOptometriaAn(){
        return $this->hasMany('App\HistoriaClinica', 'medicoAnula_id');
    }
    /**
     * Método que permite verificar el rol de un usuario
     * @param array $roles
     * @return bool, true si el rol del usuario coincide con alguno de los roles enviados
     * por parametro; false si no coincide
     */
    public function hasRoles(array $roles){
        foreach ($roles as $role){
            if ($this->role->nombre === $role){
                return true;
            }
        }
        return false;
    }
}
