<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImpresionDiagnostica extends Model
{
    protected $table = "hc_impresionDiagnostica";

	protected $fillable = [
		"diagnostico",
		"sospechaorigen",
		"tipodiagnostico",
        "id",
        "historiaClinica_id",
        "cie10_id"
	];
	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
    }
    public function cie10() {
		return $this->belongsTo('App\CIE10', 'cie10_id');
	}
}
