<?php

namespace App\Exports;

use App\HistoriaAudiometria;
use Excel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AudiometriaExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize, WithChunkReading, ShouldQueue
{
    use Exportable, Queueable;
    private $fecha_inicio;
    private $fecha_fin;

    public function __construct($fecha_inicio, $fecha_fin){
        $this->fecha_inicio = $fecha_inicio;
        $this->fecha_fin = $fecha_fin;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return HistoriaAudiometria::with([])->where("estado", "<>","Anulada")
            ->Where("created_at",">=", $this->fecha_inicio)
            ->Where("created_at","<=", $this->fecha_fin);
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $audiometria = [
            $row->motivoevaluacion,
            $row->cargoEvaluar,
            $row->fecha,
            $row->motivoevaluacion_otros,
            $row->paciente_edad,
            $row->paciente,
            $row->paciente_cedula,
            $row->estado,
            $row->empresa->nombre,
            $row->medicoapertura->nombre_completo,
            $row->antecedentes->extralaboral_audifonos,
            $row->antecedentes->extralaboral_cuales,
            $row->antecedentes->extralaboral_moto,
            $row->antecedentes->extralaboral_musica,
            $row->antecedentes->extralaboral_otros,
            $row->antecedentes->extralaboral_serviciomilitar,
            $row->antecedentes->extralaboral_tejo,
            $row->antecedentes->familiares_otologicos,
            $row->antecedentes->ocupacionales_horario,
            $row->antecedentes->ocupacionales_jornadalaboral,
            $row->antecedentes->ocupacionales_laboresdesempena,
            $row->antecedentes->ocupacionales_ocupacionactual,
            $row->antecedentes->ocupacionales_ocupacionactual_tiempo,
            $row->antecedentes->ocupacionales_ocupacionanterior,
            $row->antecedentes->ocupacionales_ocupacionanterior_tiempo,
            $row->antecedentes->ocupacionales_tiempoexposicion,
            $row->antecedentes->ocupacionales_utilizaproteccion,
            $row->antecedentes->ocupacionales_utilizaproteccion_tiempo,
            $row->antecedentes->ocupacionales_utilizaproteccion_tipo,
            $row->antecedentes->otologicos_cuales,
            $row->antecedentes->otologicos_otalgia,
            $row->antecedentes->otologicos_otitis,
            $row->antecedentes->otologicos_otorrea,
            $row->antecedentes->otologicos_otros,
            $row->antecedentes->otologicos_prurito,
            $row->antecedentes->otologicos_sensacionoido,
            $row->antecedentes->otologicos_tinitus,
            $row->antecedentes->otologicos_vertigo,
            $row->antecedentes->patologico_cuales,
            $row->antecedentes->patologico_diabetes,
            $row->antecedentes->patologico_hipertension,
            $row->antecedentes->patologico_otros,
            $row->antecedentes->patologico_parotiditis,
            $row->antecedentes->patologico_rinitis,
            $row->antecedentes->patologico_rubeola,
            $row->antecedentes->patologico_sarampion,
            $row->antecedentes->quirurgicos_cirugiaoido,
            $row->antecedentes->quirurgicos_cuales,
            $row->antecedentes->quirurgicos_otros,
            $row->antecedentes->quirurgicos_timpanoplastia,
            $row->antecedentes->toxicosnervio_farmacos,
            $row->antecedentes->toxicosnervio_industriales,
            $row->antecedentes->traumaticos_acustico,
            $row->antecedentes->traumaticos_craneo,
            $row->antecedentes->traumaticos_cuales,
            $row->antecedentes->traumaticos_otros,
            $row->otoscopia->membranatimpanica_otros,
            $row->otoscopia->cae_tapontotal,
            $row->otoscopia->cae_taponparcial,
            $row->otoscopia->cae_normal,
            $row->otoscopia->pabellonauricular_otros,
            $row->otoscopia->membranatimpanica_hiperemica,
            $row->otoscopia->cae_otros,
            $row->otoscopia->pabellonauricular_atresia,
            $row->otoscopia->pabellonauricular_normal,
            $row->otoscopia->membranatimpanica_perforada,
            $row->otoscopia->pabellonauricular_cicatriz,
            $row->otoscopia->membranatimpanica_normal,
            $row->otoscopia->membranatimpanica_placacalcarea,
            $row->otoscopia->membranatimpanica_abultada,
            $row->otoscopia->pabellonauricular_agenesia,
            $row->otoscopia->membranatimpanica_nosevisualiza,
            $row->otoscopia->membranatimpanica_opaca,
            $row->recomendacion->requierevaloracion_obs,
            $row->recomendacion->requiereremision_obs,
            $row->recomendacion->pacientecompatible,
            $row->recomendacion->requiereremision,
            $row->recomendacion->impresiondiagnostica,
            $row->recomendacion->recomendaciones,
            $row->recomendacion->pacientecompatible_obs,
            $row->recomendacion->requierevaloracion,

        ];
        $audiograma = $row->audiograma;
        for ($i = 0; $i < count($audiograma); $i++) {
            array_push($audiometria, $audiograma[$i]->oidoderecho);
            array_push($audiometria, $audiograma[$i]->oidoizquierdo);
        }
        return $audiometria;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        $array = [
            "motivoevaluacion",
            "cargoEvaluar",
            "fecha",
            "motivoevaluacion_otros",
            "paciente_edad",
            "paciente",
            "paciente_cedula",
            "estado",
            "empresa",
            "Medico",
            "antecedentes: extralaboral_audifonos",
            "antecedentes: extralaboral_cuales",
            "antecedentes: extralaboral_moto",
            "antecedentes: extralaboral_musica",
            "antecedentes: extralaboral_otros",
            "antecedentes: extralaboral_serviciomilitar",
            "antecedentes: extralaboral_tejo",
            "antecedentes: familiares_otologicos",
            "antecedentes: ocupacionales_horario",
            "antecedentes: ocupacionales_jornadalaboral",
            "antecedentes: ocupacionales_laboresdesempena",
            "antecedentes: ocupacionales_ocupacionactual",
            "antecedentes: ocupacionales_ocupacionactual_tiempo",
            "antecedentes: ocupacionales_ocupacionanterior",
            "antecedentes: ocupacionales_ocupacionanterior_tiempo",
            "antecedentes: ocupacionales_tiempoexposicion",
            "antecedentes: ocupacionales_utilizaproteccion",
            "antecedentes: ocupacionales_utilizaproteccion_tiempo",
            "antecedentes: ocupacionales_utilizaproteccion_tipo",
            "antecedentes: otologicos_cuales",
            "antecedentes: otologicos_otalgia",
            "antecedentes: otologicos_otitis",
            "antecedentes: otologicos_otorrea",
            "antecedentes: otologicos_otros",
            "antecedentes: otologicos_prurito",
            "antecedentes: otologicos_sensacionoido",
            "antecedentes: otologicos_tinitus",
            "antecedentes: otologicos_vertigo",
            "antecedentes: patologico_cuales",
            "antecedentes: patologico_diabetes",
            "antecedentes: patologico_hipertension",
            "antecedentes: patologico_otros",
            "antecedentes: patologico_parotiditis",
            "antecedentes: patologico_rinitis",
            "antecedentes: patologico_rubeola",
            "antecedentes: patologico_sarampion",
            "antecedentes: quirurgicos_cirugiaoido",
            "antecedentes: quirurgicos_cuales",
            "antecedentes: quirurgicos_otros",
            "antecedentes: quirurgicos_timpanoplastia",
            "antecedentes: toxicosnervio_farmacos",
            "antecedentes: toxicosnervio_industriales",
            "antecedentes: traumaticos_acustico",
            "antecedentes: traumaticos_craneo",
            "antecedentes: traumaticos_cuales",
            "antecedentes: traumaticos_otros",
            "Otoscopia: membranatimpanica_otros",
            "Otoscopia: cae_tapontotal",
            "Otoscopia: cae_taponparcial",
            "Otoscopia: cae_normal",
            "Otoscopia: pabellonauricular_otros",
            "Otoscopia: membranatimpanica_hiperemica",
            "Otoscopia: cae_otros",
            "Otoscopia: pabellonauricular_atresia",
            "Otoscopia: pabellonauricular_normal",
            "Otoscopia: membranatimpanica_perforada",
            "Otoscopia: pabellonauricular_cicatriz",
            "Otoscopia: membranatimpanica_normal",
            "Otoscopia: membranatimpanica_placacalcarea",
            "Otoscopia: membranatimpanica_abultada",
            "Otoscopia: pabellonauricular_agenesia",
            "Otoscopia: membranatimpanica_nosevisualiza",
            "Otoscopia: membranatimpanica_opaca",
            "Recomendacion: requierevaloracion_obs",
            "Recomendacion: requiereremision_obs",
            "Recomendacion: pacientecompatible",
            "Recomendacion: requiereremision",
            "Recomendacion: impresiondiagnostica",
            "Recomendacion: recomendaciones",
            "Recomendacion: pacientecompatible_obs",
            "Recomendacion: requierevaloracion",
            "Audiograma 250 Hz: Oido derecho",
            "Audiograma 250 Hz: Oido izquierdo",
            "Audiograma 500 Hz: Oido derecho",
            "Audiograma 500 Hz: Oido izquierdo",
            "Audiograma 1000 Hz: Oido derecho",
            "Audiograma 1000 Hz: Oido izquierdo",
            "Audiograma 2000 Hz: Oido derecho",
            "Audiograma 2000 Hz: Oido izquierdo",
            "Audiograma 3000 Hz: Oido derecho",
            "Audiograma 3000 Hz: Oido izquierdo",
            "Audiograma 4000 Hz: Oido derecho",
            "Audiograma 4000 Hz: Oido izquierdo",
            "Audiograma 6000 Hz: Oido derecho",
            "Audiograma 6000 Hz: Oido izquierdo",
            "Audiograma 8000 Hz: Oido derecho",
            "Audiograma 8000 Hz: Oido izquierdo",
        ];
        // $array->
        return $array;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        // TODO: Implement chunkSize() method.
        return 10000;
    }
}
