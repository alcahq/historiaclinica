<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaEvolucion extends Model
{
    protected $table = 'hc_notaevolucion';

    protected $fillable = [
        'fecha_nota',
        'observacion_nota',
        'historiaclinica_id'
    ];
}
