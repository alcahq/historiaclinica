<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionPorSistema extends Model {
	protected $table = "hc_revisionporsistema";

	protected $fillable = [
		"neurologico",
		"cardiovascular",
		"genitourinario",
		"digestivo",
		"dermatologico",
		"hematologico",
		"respiratorio",
		"psiquiatrico",
		"osteomuscular",
        "observaciones",
        "historiaClinica_id",
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
}
