<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ha_recomendaciones extends Model
{
    protected $table = "ha_recomendacion";
    protected $fillable = [
        "requierevaloracion_obs",
        "requiereremision_obs",
        "pacientecompatible",
        "requiereremision",
        "impresiondiagnostica",
        "recomendaciones",
        "pacientecompatible_obs",
        "requierevaloracion",
        "historiaAudiometria_id"
    ];
    public function historiaAudiometria() {
		return $this->belongsTo('App\HistoriaAudiometria');
	}
}
