<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Habitos extends Model {

	protected $table = "hc_habito";
	protected $fillable = [
		"alcohol",
		"alcohol_frecuencia",
		"alcohol_cantidad",
		"deporte",
		"deporte_cual",
		"deporte_frecuencia",
		"deporte_lesion",
		"deporte_lesion_cual",
		"sedentarismo",
		"sustancias",
		"sustancias_cual",
		"sustancias_frecuencia",
		"tabaquismo",
		"tabaquismo_exfumador",
		"tabaquismo_cantidad",
		"tabaquismo_tiempo",
        "tabaquismo_tiempoMedida",
        "historiaClinica_id"
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}

}
