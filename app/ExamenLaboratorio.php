<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamenLaboratorio extends Model
{
    protected $table = "hc_examenlaboratorio";
}
