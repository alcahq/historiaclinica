<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ha_otoscopia extends Model
{
    protected $table = "ha_otoscopia";
    protected $fillable = [
        "membranatimpanica_otros",
        "cae_tapontotal",
        "cae_taponparcial",
        "cae_normal",
        "pabellonauricular_otros",
        "membranatimpanica_hiperemica",
        "cae_otros",
        "pabellonauricular_atresia",
        "pabellonauricular_normal",
        "membranatimpanica_perforada",
        "pabellonauricular_cicatriz",
        "membranatimpanica_normal",
        "membranatimpanica_placacalcarea",
        "membranatimpanica_abultada",
        "pabellonauricular_agenesia",
        "membranatimpanica_nosevisualiza",
        "membranatimpanica_opaca",
        "historiaAudiometria_id"
    ];
    public function historiaAudiometria() {
		return $this->belongsTo('App\HistoriaAudiometria');
	}
}
