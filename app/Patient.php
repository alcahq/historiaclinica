<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = "paciente";

    protected $fillable = ['documento', 'tipodocumento', 'nombre', 'apellido', 'genero',
        'fechaNacimiento', 'lugarNacimiento', 'direccionDomicilio', 'ciudadDomicilio',
        'telefono', 'celular','cargo', 'antiguedad', 'correoElectronico', 'escolaridad',
        'estadoCivil', 'eps','arl', 'afp', 'grupoSanguineo', 'rh', 'nombreAcompaniante', 'edad',
        'apellidoAcompaniante', 'parentescoAcompaniante', 'celularAcompaniante', 'nombre_completo',
        'firma', 'foto'];

    public function historiaclinica(){
        return $this->hasMany('App\HistoriaClinica','paciente_id');
    }
    public function historiaoptometria()
    {
        return $this->hasMany('App\HistoriaOptometria', 'paciente_id');
    }
    public function historiavisiometria()
    {
        return $this->hasMany('App\HistoriaVisiometria', 'paciente_id');
    }
    public function historiaaudiometria()
    {
        return $this->hasMany('App\HistoriaAudiometria', 'paciente_id');
    }
    public function accidenteTrabajo(){
        return $this->hasMany('App\AccidenteTrabajo','paciente_id');
    }
    public function enfermedadProfesional(){
        return $this->hasMany('App\EnfermedadProfesional','paciente_id');
    }
}

