<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sintomas extends Model
{
    protected $table = "sintoma";

    public function historiaoptometria()
    {
        return $this->belongsTo('App\HistoriaOptometria');
    }
}
