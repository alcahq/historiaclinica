<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $fillable = ['nit','nombre','contacto','direccion','telefono'];

    public function historiaclinica(){
        return $this->hasMany('App\HistoriaClinica','empresa_id');
    }
}
