<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 21/03/2019
 * Time: 9:37 AM
 */

function setActive($routeName){
    return request()->is($routeName) ? 'active' : '';
}
