<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document' => 'required|regex:/^[0-9]{7,15}$/|unique:users,document,'.decrypt($this->route('id')),
            'type_document' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role_id' => 'required'
        ];
    }

    function messages()
    {
        return [
            'document.regex' => 'El campo número de documento es inválido.',
        ];
    }

    function attributes()
    {
        return [
            'role_id' => 'rol usuario',
            'type_document' => 'tipo de documento',
            'document' => 'número de documento',
            'email' => 'nombre de usuario',
        ];
    }
}
