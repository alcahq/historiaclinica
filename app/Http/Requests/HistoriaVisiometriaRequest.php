<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HistoriaVisiometriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documento' => 'required|exists:paciente,documento',
            'empresa' => 'required|exists:empresa,nombre',
            'motivoevaluacion' => 'required',
            'cargo' => 'required|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+$/',
            'lensometria_ojoderecho' => 'required',
            'lensometria_ojoizquierdo' => 'required',
            'segmentoanterior_visioncolor' => 'required',
            'segmentoanterior_visionprofundidad' => 'required',
            'segmentoanterior_impresiondiagnostica_ojoderecho' => 'required',
            'segmentoanterior_impresiondiagnostica_ojoizquierdo' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'documento.required' => 'El campo número de documento es obligatorio',
            'documento.exists' => 'El documento ingresado no corresponde a un paciente registrado',
            'empresa.exists' => 'La empresa ingresada no se encuentra registrada',
            'empresa.required' => 'El campo empresa es obligatorio',
            'motivoevaluacion.required' => 'El campo motivo de la evaluación es obligatorio',
            'cargo.required' => 'El campo cargo a evaluar es obligatorio',
            'cargo.regex' => 'El campo cargo a evaluar no permite caracteres especiales',
            'lensometria_ojoderecho.required' => 'El campo lensometria ojo derecho es obligatorio',
            'lensometria_ojoizquierdo.required' => 'El campo lensometria ojo izquierdo es obligatorio',
            'segmentoanterior_visioncolor.required' => 'El campo visión color es obligatorio',
            'segmentoanterior_visionprofundidad.required' => 'El campo visión profundidad es obligatorio',
            'segmentoanterior_impresiondiagnostica_ojoderecho.required' => 'El campo ojo derecho de impresión diagnóstica es obligatorio',
            'segmentoanterior_impresiondiagnostica_ojoizquierdo.required' => 'El campo ojo izquierdo de impresión diagnóstica es obligatorio',
        ];
    }
}
