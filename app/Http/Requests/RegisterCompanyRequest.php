<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nit' => 'required|regex:/[0-9]/|unique:empresa',
            'nombre' => 'required|regex:/[A-Za-z]/',
            'telefono' => 'regex:/[0-9]{7,10}/'
        ];
    }

    public function messages()
    {
        return [
            'nit.regex' => 'El campo :attribute solo permite números.',
            'telefono.regex' => 'El campo :attribute es invalido.',
            'nombre.regex' => 'El campo nombre es inválido'
        ];
    }
}
