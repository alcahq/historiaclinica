<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterHistoriaCRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //---------------Datos iniciales----------------
            'documento' => 'required|exists:paciente,documento',
            'motivoevaluacion' => 'required',
            'cargoEvaluar' => 'required|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+$/',

           //---------------Ocupacionales----------------

            'cargos.antiguedad' => 'regex:/^[0-9]+$/|nullable',
            'cargos.area' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+$/',
            'cargos.empresa' => 'required|exists:empresa,nombre',
            'cargos.horas' => 'regex:/^[0-9]+$/|nullable',
            'factores.*.cargo' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+$/',
            'factores.*.tiempoexposicion' => 'regex:/^[0-9]+$/|nullable',

            //---------------Laboral----------------

            'accidentes.*.empresa' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+$/',
            'accidentes.*.lesion' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+$/',
            'accidentes.*.dias_incapacidad' => 'regex:/^[0-9]+$/|nullable',
            'enfermedades.*.empresa' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+$/',
            'enfermedades.*.diagnostico' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+$/',
            'enfermedades.*.arl' => 'nullable|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1]+$/',

            //---------------Habitos----------------

            'habitos.tabaquismo_cantidad' => 'regex:/^[0-9]+$/|nullable',
            'habitos.tabaquismo_tiempo' => 'regex:/^[0-9]+$/|nullable',

            //---------------Antecedentes----------------

            'antecedentes.ginecoobstetricos_menarquia' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_ciclos_1' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_ciclos_2' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_G' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_P' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_A' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_C' => 'regex:/^[0-9]+$/|nullable',
            'antecedentes.ginecoobstetricos_E' => 'regex:/^[0-9]+$/|nullable',

            //---------------Examen fisico----------------

            'examenfisico.tenArt1' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.tenArt2' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.fc' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.fr' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.t' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.perimetroabdominal' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.talla' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.peso' => 'numeric|nullable',

            'idiagnostica' => 'required',
//            'idiagnostica.*.codigo' => 'required|exists:hc_cie10,codigo',
            'idiagnostica.*.diagnostico' => 'nullable|exists:hc_cie10,descripcion',
            'paraclinicos.*.examenLaboratorio_id' => 'exists:hc_examenlaboratorio,id'


        ];
    }

    public function messages()
    {
        return [
            //---------------Datos iniciales----------------

            'documento.required' => 'El campo número de documento es obligatorio',
            'documento.exists' => 'El documento ingresado no corresponde a un paciente registrado',
            'motivoevaluacion.required' => 'El campo motivo de la evaluación es obligatorio',
            'cargoEvaluar.required' => 'El campo cargo a evaluar es obligatorio',
            'cargoEvaluar.regex' => 'El campo cargo a evaluar no permite caracteres especiales',

            //---------------Ocupacionales----------------

            'cargos.antiguedad.regex' => 'El campo antiguedad solo permite valores numéricos',
            'cargos.area.regex' => 'El campo cargo a evaluar no permite caracteres especiales',
            'cargos.empresa.exists' => 'La empresa ingresada no se encuentra registrada.',
            'cargos.empresa.required' => 'El campo empresa es obligatorio',
            'cargos.horas.regex' => 'El campo horas trabajadas solo permite valores numéricos',
            'factores.*.empresa.regex' => 'El campo empresa de factor de riesgo no permite caracteres especiales',
            'factores.*.cargo.regex' => 'El campo cargo de factor de riesgo no permite caracteres especiales',
            'factores.*.tiempoexposicion.regex' => 'El campo tiempo de exposición solo permite valores numéricos',

            //---------------Laboral----------------

            'accidentes.*.empresa.regex' => 'El campo empresa de accidente de trabajo no permite caracteres especiales',
            'accidentes.*.lesion.regex' => 'El campo lesión de accidente de trabajo no permite caracteres especiales',
            'accidentes.*.dias_incapacidad.regex' => 'El campo dias incapacidad de accidente de trabajo solo permite valores numericos',
            'accidentes.*.secuelas.regex' => 'El campo secuelas de accidente de trabajo no permite caracteres especiales',
            'enfermedades.*.empresa.regex' => 'El campo empresa de enfermedad profesional no permite caracteres especiales',
            'enfermedades.*.diagnostico.regex' => 'El campo diagnostico de enfermedad profesional no permite caracteres especiales',
            'enfermedades.*.arl.regex' => 'El campo arl de enfermedad profesional no permite caracteres especiales',
            'enfermedades.*.reubicacion.regex' => 'El campo reubicación de enfermedad profesional no permite caracteres especiales',

            //---------------Habitos----------------

            'habitos.sustancias_cual.regex' => 'El campo cual sustancias psicoactivas no permite caracteres especiales',
            'habitos.tabaquismo_cantidad.regex' => 'El campo cantidad cigarrillos dia solo permite valores numéricos',
            'habitos.tabaquismo_tiempo.regex' => 'El campo cuanto tiempo fumó solo permite valores numéricos',
            'habitos.deporte_lesion_cual.regex' => 'El campo cual lesion deportiva no permite caracteres especiales',
            'habitos.deporte_cual.regex' => 'El campo cual deporte no permite caracteres especiales',

            //---------------Antecedentes----------------

            'antecedentes.ginecoobstetricos_menarquia.regex' => 'El campo menarquia solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_ciclos_1.regex' => 'El campo ciclos solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_ciclos_2.regex' => 'El campo ciclos solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_G.regex' => 'El campo F. obstétrica G solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_P.regex' => 'El campo F. obstétrica P solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_A.regex' => 'El campo F. obstétrica A solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_C.regex' => 'El campo F. obstétrica C solo permite valores numéricos',
            'antecedentes.ginecoobstetricos_E.regex' => 'El campo F. obstétrica E solo permite valores numéricos',

            'idiagnostica.*.codigo.exists' => 'No existe un diagnostico asociada al codigo ingresado',
            'idiagnostica.*.codigo.requires' => 'El campo código de impresión diagnóstica es obligatorio',
            'idiagnostica.*.diagnostico.exists' => 'No existe un diagnostico asociado al nombre ingresado',
            'idiagnostica.required' => 'Debe ingresar una impresión diagnóstica',
            'paraclinicos.*.examenLaboratorio_id.exists' => 'Uno de los examenes ingresados no corresponde a los registrados en el sistema.',

            //-------------------Examen fisico-----------------------------------

            'examenfisico.tenArt1' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.tenArt2' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.fc' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.fr' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.t' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.perimetroabdominal' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.talla' => 'regex:/^[0-9]+$/|nullable',
            'examenfisico.peso' => 'numeric|nullable',

            'examenfisico.tenArt1.regex' => 'El campo tension arterial sistólica no permite valores decimales',
            'examenfisico.tenArt2.regex' => 'El campo tension arterial diastólica no permite valores decimales',
            'examenfisico.fc.regex' => 'El campo frecuencia cardiaca no permite valores decimales',
            'examenfisico.fr.regex' => 'El campo frecuencia respiratoria no permite valores decimales',
            'examenfisico.t.regex' => 'El campo temperatura no permite valores decimales',
            'examenfisico.perimetroabdominal.regex' => 'El perímetro abdominal no permite valores decimales',
            'examenfisico.talla.regex' => 'El campo talla no permite valores decimales',


        ];
    }
}
