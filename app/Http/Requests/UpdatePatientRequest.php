<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipodocumento' => 'required',
            'documento' => 'regex:/^[0-9]{5,15}$/|required|unique:paciente,documento,'.decrypt($this->route('id')),
            'nombre' => 'required',
            'apellido' => 'required',
            'genero' => 'required',
            'telefono' => 'nullable|regex:/^[0-9]{7,10}$/',
            'celular' => 'nullable|regex:/^[0-9]{10}$/',
            'celularAcompaniante' => 'nullable|Integer',
            'fechaNacimiento' => 'required',
            'antiguedad' => 'required',
            'escolaridad' => 'required',
            'estadoCivil' => 'required',
            'correoElectronico' => 'nullable|regex:/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/|unique:paciente,correoElectronico,'.decrypt($this->route('id')),
        ];
    }

    public function messages()
    {
        return [
            'documento.unique' => 'Ya existe un paciente asociado a este número de documento.',
            'correoElectronico.unique' => 'Ya existe un paciente asociado a este :attribute.',
            'documento.regex' => 'El campo n° de documento es inválido.',
            'telefono.regex' => 'El campo teléfono de domicilio debe ser numérico y contener entre siete y diez dígitos.',
            'celular.regex' => 'El campo teléfono celular debe ser numérico y contener diez dígitos.',
            'celularAcompaniante.integer' => 'El campo teléfono celular de acompañante debe ser numérico.',
        ];
    }

    public function attributes()
    {
        return [
            'tipodocumento' => 'tipo de documento',
            'documento' => 'n° de documento',
            'correoElectronico' => 'Correo electrónico'
        ];
    }
}
