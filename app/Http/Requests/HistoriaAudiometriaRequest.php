<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HistoriaAudiometriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documento' => 'required|exists:paciente,documento',
            'empresa' => 'required|exists:empresa,nombre',
            'motivoevaluacion' => 'required',
            'cargoEvaluar' => 'required|regex:/^[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+(\s*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]*)*[0-9a-zA-ZÀ-ÿ\u00f1\u00d1\u0022]+$/',
            'audiograma.*.oidoderecho' => 'regex:/^[0-9]+$/|nullable',
            'audiograma.*.oidoizquierdo' => 'regex:/^[0-9]+$/|nullable',
        ];
    }

    public function messages()
    {
        return [
            'documento.required' => 'El campo número de documento es obligatorio',
            'documento.exists' => 'El documento ingresado no corresponde a un paciente registrado',
            'empresa.exists' => 'La empresa ingresada no se encuentra registrada',
            'empresa.required' => 'El campo empresa es obligatorio',
            'motivoevaluacion.required' => 'El campo motivo de la evaluación es obligatorio',
            'cargoEvaluar.required' => 'El campo cargo a evaluar es obligatorio',
            'cargoEvaluar.regex' => 'El campo cargo a evaluar no permite caracteres especiales',
            'audiograma.*.oidoderecho.regex' => 'El campo oido derecho de audiograma solo acepta valores numericos',
            'audiograma.*.oidoizquierdo.regex' => 'El campo oido izquierdo de audiograma solo acepta valores numericos'
        ];
    }
}
