<?php

namespace App\Http\Controllers;

use App\HistoriaClinica;
use File;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class CertificadoHistoriaClinicaController extends Controller
{
    public function generateCertificate(Request $request, $id){
        $historia = HistoriaClinica::find($id);
        $template = new TemplateProcessor(public_path()."/historiaclinica.docx");
        $resultado = $historia->resultado;
        $valoracion = $historia->nuevavaloracion;
        $photoPatient = $historia->pacienteHC->foto;
        $otrosME = $historia->motivoevaluacion_otros;
        if ($otrosME != "") {
            $otrosME = str_replace(",", ", ", ($historia->motivoevaluacion_otros) . ".");
        }
        //------------------------------------MOTIVO D
        //E EVALUACION--------------------------------
        $motivo = new TextRun();
        $motivo->addText($historia->motivoevaluacion,array('name'=>'Calibri'));
        $template->setValue('motivoevaluacion', $historia->motivoevaluacion);

        $motivo = new TextRun();
        $motivo->addText($historia->motivoevaluacion_otros);
        $template->setValue('motivoevaluacion_otros', $otrosME);

        if (isset($photoPatient)){
            $image = str_replace('data:image/png;base64,', '', $photoPatient);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            // Load the image
            $source = @imagecreatefromjpeg(public_path() . '/temp/img/' . $imagePatient);
            if (!$source){

            }else{
                $rotate = imagerotate($source, -90, 0);

                imagejpeg($rotate, public_path() . '/temp/img/' . $imagePatient);
                $template->setImageValue("foto",array("path" => public_path('/temp/img/' . $imagePatient),"width"=>240,"height"=>158));
            }

        }else{
            $template->setValue("foto","");

        }

        //--------------------------------------DATOS DEL PACIENTE--------------------------------
        $paciente = $historia->pacienteHC;

        $template->setValue('nombre_completo', $paciente->nombre_completo);
        $template->setValue('genero', $paciente->genero);
        $template->setValue('documento', $paciente->documento);
        $template->setValue('direccion', $paciente->direccionDomicilio);
        $template->setValue('fechanacimiento', $paciente->fechaNacimiento);
        $template->setValue('edad', $historia->paciente_edad);
        $template->setValue('lugarnacimiento', $paciente->lugarNacimiento);
        $template->setValue('correoelectronico', $paciente->correoElectronico);
        $template->setValue('telefono', $paciente->direccionDomicilio);
        $template->setValue('celular', $paciente->direccionDomicilio);
        $template->setValue('escolaridad', $paciente->direccionDomicilio);
        $template->setValue('estadoCivil', $paciente->direccionDomicilio);
        $template->setValue('eps', $paciente->eps);
        $template->setValue('afp', $paciente->afp);
        $template->setValue('arl', $paciente->arl);
        $template->setValue('nombre_acompanante', $paciente->nombreAcompaniante." ".$paciente->apellidoAcompaniante);
        $template->setValue('telefonoacompanante', $paciente->celularAcompaniante);

        //--------------------------------IDENTIFICACION DE LA EMPRESA ------------------------

        $template->setValue('cargoevaluar', $historia->cargoEvaluar);
        $template->setValue('empresa', htmlspecialchars($historia->empresa->nombre));
        //--------------------------------PRUEBAS COMPLEMENTARIAS ------------------------


        if ($historia->nuevaValoracion == 0){
            $fecha = new TextRun();
            $fecha->addText($historia->fecha,array('name'=>'Calibri'));
            $template->setValue('fecha', $historia->fecha);

            //--------------------------------PRUEBAS COMPLEMENTARIAS ------------------------
            $template->setValue('pruebascomplementarias', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_pruebacomplementaria)) . "."));
            $template->setValue('otros', $resultado->recomendacion_pruebacomplementaria_otros);

            //--------------------------------RECOMENDACIONES PARA EL TRABAJADOR ------------------------
            $template->setValue('medicas', str_replace(",", ", ", ($resultado->recomendacion_medica) . "."));
            $template->setValue('ingresosve', str_replace(",", ", ", ($resultado->recomendacion_ingresosve) . "."));
            $template->setValue('habitosyestilosdevida', str_replace(",", ", ", ($resultado->recomendacion_habitos) . "."));
            $template->setValue('ocupacionales', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_ocupacional)) . "."));

            //--------------------------------CONCEPTO-------------------------------------------
            $template->setValue('concepto', htmlspecialchars(str_replace(",", ", ", ($resultado->resultadoValoracionMedica)) . "."));
            $template->setValue('recomendaciones',htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacionesAdicionales)) . "."));
            $template->setValue('restricciones', htmlspecialchars(str_replace(",", ", ", ($resultado->restricciones)) . "."));
        }else{
            $fecha = new TextRun();
            $fecha->addText($historia->fecha,array('name'=>'Calibri'));
            $template->setValue('fecha', $valoracion->created_at);

            $template->setValue('pruebascomplementarias', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_complementaria)) . "."));
            $template->setValue('otros', htmlspecialchars($valoracion->nota_complementaria_otro));

            $template->setValue('medicas', str_replace(",", ", ", ($valoracion->nota_recomendacion_medica) . "."));
            $template->setValue('ingresosve', str_replace(",", ", ", ($valoracion->nota_recomendacion_ingreso) . "."));
            $template->setValue('habitosyestilosdevida', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_habito)) . "."));
            $template->setValue('ocupacionales', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_ocupacional)) . "."));

            $template->setValue('concepto', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_valoracion_medica)) . "."));
            $template->setValue('recomendaciones', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_adicional)) . "."));
            $template->setValue('restricciones', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_restriccion)) . "."));
        }

        //--------------------------------ELEMENTOS DE PROTECCIÓN PERSONAL------------------------
        $cargoactual = $historia->cargoactual;
        $template->setValue('elementosproteccion', str_replace(",", ", ", ($cargoactual->elementos) . "."));

        $template->setValue('nombremedico', str_replace(",", ", ", ($historia->medicocierre->nombre_completo) . "."));
        $template->setValue('cocumentomedico', str_replace(",", ", ", ($historia->medicocierre->document) . "."));
        $template->setValue('licencia', str_replace(",", ", ", ($historia->medicocierre->licencia) . "."));

        if (isset($historia->firmaMedico)){
            $image = str_replace('data:image/png;base64,', '', $historia->firmaMedico);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmamedico", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmamedico","");
        }

        if (isset($historia->firmaPaciente)){
            $image = str_replace('data:image/png;base64,', '', $historia->firmaPaciente);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmapaciente", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmapaciente","");
        }
        $template->saveAs(public_path() . '/generados_hc/' . $paciente->documento . '-' . $historia->id .'.docx');
        $process = new Process("export HOME=/tmp && libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $paciente->documento . '-' . $historia->id . '.docx');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        //exec("libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $paciente->documento . '.docx');

        return response()->download(public_path($paciente->documento.'-'.$historia->id.'.pdf'));

    }

    public function generateCertificateUnificado(Request $request, $id)
    {
        $historia = HistoriaClinica::find($id);
        $template = new TemplateProcessor(public_path() . "/historiaclinica_completo.docx");
        $resultado = $historia->resultado;
        $valoracion = $historia->nuevavaloracion;
        $photoPatient = $historia->pacienteHC->foto;
        $otrosME = $historia->motivoevaluacion_otros;
        if ($otrosME != "") {
            $otrosME = str_replace(",", ", ", ($historia->motivoevaluacion_otros) . ".");
        }
        //------------------------------------MOTIVO DE EVALUACION--------------------------------
        $motivo = new TextRun();
        $motivo->addText($historia->motivoevaluacion, array('name' => 'Calibri'));
        $template->setValue('motivoevaluacion', $historia->motivoevaluacion);


        $motivo = new TextRun();
        $motivo->addText($historia->motivoevaluacion_otros);
        $template->setValue('motivoevaluacion_otros', htmlspecialchars($otrosME));

        if (isset($photoPatient)) {
            $image = str_replace('data:image/png;base64,', '', $photoPatient);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            // Load the image
            $source = @imagecreatefromjpeg(public_path() . '/temp/img/' . $imagePatient);
            if (!$source){

            }else{
                $rotate = imagerotate($source, -90, 0);

                imagejpeg($rotate, public_path() . '/temp/img/' . $imagePatient);
                $template->setImageValue("foto",array("path" => public_path('/temp/img/' . $imagePatient),"width"=>240,"height"=>158));
            }


        } else {
            $template->setValue("foto", "");
        }

        //--------------------------------------DATOS DEL PACIENTE--------------------------------
        $paciente = $historia->pacienteHC;

        $template->setValue('nombre_completo', $paciente->nombre_completo);
        $template->setValue('genero', $paciente->genero);
        $template->setValue('documento', $paciente->documento);
        $template->setValue('direccion', $paciente->direccionDomicilio);
        $template->setValue('fechanacimiento', $paciente->fechaNacimiento);
        $template->setValue('edad', $historia->paciente_edad);
        $template->setValue('lugarnacimiento', $paciente->lugarNacimiento);
        $template->setValue('correoelectronico', $paciente->correoElectronico);
        $template->setValue('telefono', $paciente->telefono);
        $template->setValue('celular', $paciente->celular);
        $template->setValue('escolaridad', $paciente->escolaridad);
        $template->setValue('estadocivil', $paciente->estadoCivil);
        $template->setValue('eps', $paciente->eps);
        $template->setValue('afp', $paciente->arl);
        $template->setValue('arl', $paciente->afp);
        $template->setValue('nombre_acompanante', $paciente->nombreAcompaniante." ".$paciente->apellidoAcompaniante );
        $template->setValue('telefonoacompanante', $paciente->celularAcompaniante);
        $template->setValue('parentescoacompanante', $paciente->parentescoAcompaniante);

        //--------------------------------IDENTIFICACION DE LA EMPRESA ------------------------

        $template->setValue('cargoevaluar', $historia->cargoEvaluar);
        $template->setValue('empresa', htmlspecialchars($historia->empresa->nombre));
        $template->setValue('nit', $historia->empresa->nit);

        //-------------------------------CARGO ACTUAL------------------------------------------
        $cargoactual = $historia->cargoactual;
        $template->setValue('cargoevaluar', $historia->cargoEvaluar);
        $template->setValue('cargoactual_fechaingreso', $cargoactual->fecha);
        $template->setValue('cargoactual_antiguedad', $cargoactual->antiguedad . " " . $cargoactual->antiguedadMedida);
        $template->setValue('cargoactual_jornadatrabajo', $cargoactual->jornada);
        $template->setValue('cargoactual_horastrabajadas', $cargoactual->horas);

        $factores = $historia->cargoanterior[0];
        $template->setValue('factores_fisicos', $factores->fisicos);
        $template->setValue('factores_quimicos', $factores->quimicos);
        $template->setValue('factores_ergonomicos', $factores->ergonomicos);
        $template->setValue('factores_biologicos', $factores->biologicos);
        $template->setValue('factores_psicosociales', $factores->psicosociales);
        $template->setValue('factores_seguridad', $factores->seguridad);
        $template->setValue('factores_otros', htmlspecialchars($factores->otros));
        $template->setValue('factores_usoelemento', $cargoactual->elementos);

        //-------------------------------ACCIDENTES DE TRABAJO------------------------------------------

        $template->setValue('accidentes', $historia->accidente);
        $accidentes = $historia->accidentes;
        $examenes = "";
        for($i = 0; $i<count($accidentes);$i++) {
            $examenes .= "FECHA: ".$accidentes[$i]->fecha."</w:t><w:br/><w:t> 
            EMPRESA: " . htmlspecialchars($accidentes[$i]->empresa) . "</w:t><w:br/><w:t> 
            LESIÓN: ".$accidentes[$i]->lesion."</w:t><w:br/><w:t> 
            DIAS DE INCAPACIDAD: ".$accidentes[$i]->dias_incapacidad."</w:t><w:br/><w:t> 
            SECUELAS: ".$accidentes[$i]->secuelas."</w:t><w:br/><w:t></w:t><w:br/><w:t>";
        }
        $template->setValue('accidentetrabajo', $examenes);

        //-------------------------------ENFERMEDAD LABORAL------------------------------------------

        $template->setValue('enfermedad', $historia->enfermedad);
        $accidentes = $historia->enfermedades;
        $examenes = "";
        for($i = 0; $i<count($accidentes);$i++) {
            $examenes .= "FECHA: ".$accidentes[$i]->fecha."</w:t><w:br/><w:t> 
            EMPRESA: " . htmlspecialchars($accidentes[$i]->empresa) . "</w:t><w:br/><w:t> 
            DIAGNOSTICO: ".$accidentes[$i]->diagnostico."</w:t><w:br/><w:t> 
            ARL: ".$accidentes[$i]->arl."</w:t><w:br/><w:t> 
            REUBICACIÓN: ".$accidentes[$i]->reubicacion."</w:t><w:br/><w:t></w:t><w:br/><w:t>";
        }
        $template->setValue('enfermedades', $examenes);

        //-------------------------------HABITOS------------------------------------------

        $habitos = $historia->habitos;
        $template->setValue('habitos_alcohol', $habitos->alcohol);
        $template->setValue('habitos_alcohol_frecuencia', $habitos->alcohol_frecuencia);
        $template->setValue('habitos_alcohol_cantidad', $habitos->alcohol_cantidad);
        $template->setValue('habitos_sedentarismo', $habitos->sedentarismo);
        $template->setValue('habitos_sustancias', $habitos->sustancias);
        $template->setValue('habitos_sustancias_frecuencia', $habitos->sustancias_frecuencia);
        $template->setValue('habitos_sustancias_cual', $habitos->sustancias_cual);
        $template->setValue('habitos_fumador', $habitos->tabaquismo);
        $template->setValue('habitos_fumador_cantidad', $habitos->tabaquismo_cantidad);
        $template->setValue('habitos_exfumador', $habitos->tabaquismo_exfumador);
        $template->setValue('habitos_exfumador_tiempo', $habitos->tabaquismo_tiempo);
        $template->setValue('tiempo_medida', $habitos->tabaquismo_tiempoMedida);
        $template->setValue('habitos_deporte', $habitos->deporte);
        $template->setValue('habitos_deporte_cual', htmlspecialchars($habitos->deporte_cual));
        $template->setValue('habitos_deporte_frecuencia', $habitos->deporte_frecuencia);
        $template->setValue('habitos_lesiones', $habitos->deporte_lesion);
        $template->setValue('habitos_lesiones_cual', htmlspecialchars($habitos->deporte_lesion_cual));

        //-------------------------------ANTECEDENTES INMUNOLOGICOS------------------------------------------
        $antecedentes = $historia->antecedentes;
        $template->setValue('inmunologico_carnetvacunacion', $antecedentes->inmunologicos_carnetVacunacion);
        $template->setValue('inmunologico_hepatitisa', $antecedentes->inmunologicos_hepatitisA);
        $template->setValue('inmunologico_hepatitisb', $antecedentes->inmunologicos_hepatitisB);
        $template->setValue('inmunologico_tripleviral', $antecedentes->inmunologicos_tripleViral);
        $template->setValue('inmunologico_tetano', $antecedentes->inmunologicos_tetano);
        $template->setValue('inmunologico_varicela', $antecedentes->inmunologicos_varicela);
        $template->setValue('inmunologico_influenza', $antecedentes->inmunologicos_influenza);
        $template->setValue('inmunologico_fiebreamarilla', $antecedentes->inmunologicos_fiebreAmarilla);
        $template->setValue('inmunologico_otros', $antecedentes->inmunologicos_otros);
        $template->setValue('inmuno_observaciones', htmlspecialchars($antecedentes->inmunologicos_otros_obs));

        //-------------------------------ANTECEDENTES PERSONALES------------------------------------------

        $template->setValue('antecedentes_patologicos', $antecedentes->personales_patologicos);
        $template->setValue('antecedentes_quirurgicos', $antecedentes->personales_quirurgicos);
        $template->setValue('antecedentes_traumaticos', $antecedentes->personales_traumaticos);
        $template->setValue('antecedentes_farmacologicos', $antecedentes->personales_farmacologicos);
        $template->setValue('antecedentes_transfuncionales', $antecedentes->personales_transfusionales);
        $template->setValue('antecedentes_otros', htmlspecialchars($antecedentes->personales_otros));

        $template->setValue('antecedentes_patologicos_obs', $antecedentes->personales_patologicos_parentesco);
        $template->setValue('antecedentes_quirurgicos_obs', $antecedentes->personales_quirurgicos_parentesco);
        $template->setValue('antecedentes_traumaticos_obs', $antecedentes->personales_traumaticos_parentesco);
        $template->setValue('antecedentes_farmacologicos_obs', $antecedentes->personales_farmacologicos_parentesco);
        $template->setValue('antecedentes_transfuncionales_obs', $antecedentes->personales_transfusionales_parentesco);
        $template->setValue('antecedentes_otros_obs', $antecedentes->personales_otros_parentesco);


        //-------------------------------ANTECEDENTES PERSONALES------------------------------------------

        $template->setValue('familiares_hta', $antecedentes->familiares_hta);
        $template->setValue('familiares_eap', $antecedentes->familiares_eap);
        $template->setValue('familiares_ecv', $antecedentes->familiares_ecv);
        $template->setValue('familiares_tbc', $antecedentes->familiares_tbc);
        $template->setValue('familiares_asma', $antecedentes->familiares_asma);
        $template->setValue('familiares_alergias', $antecedentes->familiares_alergia);
        $template->setValue('familiares_artritis', $antecedentes->familiares_artritis);
        $template->setValue('familiares_varices', $antecedentes->familiares_varices);
        $template->setValue('familiares_cancer', $antecedentes->familiares_cancer);
        $template->setValue('familiares_diabetes', $antecedentes->familiares_diabetes);
        $template->setValue('familiares_enfermedadrenal', $antecedentes->familiares_enfermedadRenal);
        $template->setValue('familiares_dislipidemia', $antecedentes->familiares_displipidemia);
        $template->setValue('familiares_enfermedadcoronaria', $antecedentes->familiares_enfermedadCoronaria);
        $template->setValue('familiares_enfermedadcolagenosis', $antecedentes->familiares_enfermedadColagenosis);
        $template->setValue('familiares_enfermedadtiroidea', $antecedentes->familiares_enfermedadTiroidea);
        $template->setValue('familiares_otra', $antecedentes->familiares_otra);

        $template->setValue('familiares_hta_parentesco', $antecedentes->familiares_hta_parentesco);
        $template->setValue('familiares_eap_parentesco', $antecedentes->familiares_eap_parentesco);
        $template->setValue('familiares_ecv_parentesco', $antecedentes->familiares_ecv_parentesco);
        $template->setValue('familiares_tbc_parentesco', $antecedentes->familiares_tbc_parentesco);
        $template->setValue('familiares_asma_parentesco', $antecedentes->familiares_asma_parentesco);
        $template->setValue('familiares_alergias_parentesco', $antecedentes->familiares_alergia_parentesco);
        $template->setValue('familiares_artritis_parentesco', $antecedentes->familiares_artritis_parentesco);
        $template->setValue('familiares_varices_parentesco', $antecedentes->familiares_varices_parentesco);
        $template->setValue('familiares_cancer_parentesco', $antecedentes->familiares_cancer_parentesco);
        $template->setValue('familiares_diabetes_parentesco', $antecedentes->familiares_diabetes_parentesco);
        $template->setValue('familiares_enfermedadrenal_parentesco', $antecedentes->familiares_enfermedadRenal_parentesco);
        $template->setValue('familiares_dislipidemia_parentesco', $antecedentes->familiares_displipidemia_parentesco);
        $template->setValue('familiares_enfermedadcoronaria_parentesco', $antecedentes->familiares_enfermedadCoronaria_parentesco);
        $template->setValue('familiares_enfermedadcolagenosis_parentesco', $antecedentes->familiares_enfermedadColagenosis_parentesco);
        $template->setValue('familiares_enfermedadtiroidea_parentesco', $antecedentes->familiares_enfermedadTiroidea_parentesco);
        $template->setValue('familiares_otra_parentesco', $antecedentes->familiares_otra_parentesco);

        //-------------------------------REVISION POR SISTEMAS------------------------------------------

        $revisionporsistemas = $historia->revisionporsistema;

        $template->setValue('revisionporsistemas_neurologico', $revisionporsistemas->neurologico);
        $template->setValue('revisionporsistemas_cardiovascular', $revisionporsistemas->cardiovascular);
        $template->setValue('revisionporsistemas_genitourinario', $revisionporsistemas->genitourinario);
        $template->setValue('revisionporsistemas_digestivo', $revisionporsistemas->digestivo);
        $template->setValue('revisionporsistemas_dermatologico', $revisionporsistemas->dermatologico);
        $template->setValue('revisionporsistemas_hematologico', $revisionporsistemas->hematologico);
        $template->setValue('revisionporsistemas_respiratorio', $revisionporsistemas->respiratorio);
        $template->setValue('revisionporsistemas_psiquiatrico', $revisionporsistemas->psiquiatrico);
        $template->setValue('revisionporsistemas_osteomuscular', $revisionporsistemas->osteomuscular);
        $template->setValue('revisionporsistemas_observaciones', htmlspecialchars($revisionporsistemas->observaciones));

        //-------------------------------EXAMEN FISICO------------------------------------------

        $examengeneral = $historia->examenfisico;

        $template->setValue('estadogeneral', $examengeneral->estadogeneral);
        $template->setValue('ta', $examengeneral->tas." / ".$examengeneral->tad);
        $template->setValue('t', $examengeneral->t);
        $template->setValue('imc', $examengeneral->imc);
        $template->setValue('perimetro', $examengeneral->perimetroabdominal);
        $template->setValue('fc', $examengeneral->fc);
        $template->setValue('peso', $examengeneral->peso);
        $template->setValue('dominancia', $examengeneral->dominancia);
        $template->setValue('fr', $examengeneral->fr);
        $template->setValue('talla', $examengeneral->talla);

        $template->setValue('cabeza', $examengeneral->cabezacuello);
        $template->setValue('ojos', $examengeneral->ojos);
        $template->setValue('oido', $examengeneral->oido);
        $template->setValue('abdomen', $examengeneral->abdomen);
        $template->setValue('piel', $examengeneral->piel);
        $template->setValue('torax', $examengeneral->torax);
        $template->setValue('cabeza_obs', $examengeneral->cabezacuello_obs);
        $template->setValue('ojos_obs', $examengeneral->ojos_obs);
        $template->setValue('ojos_ojoderecho', $examengeneral->ojos_ojoderecho);
        $template->setValue('ojos_ojoizquierdo', $examengeneral->ojos_ojoizquierdo);
        $template->setValue('oido_obs', $examengeneral->oido_obs);
        $template->setValue('abdomen_obs', $examengeneral->abdomen_obs);
        $template->setValue('piel_obs', $examengeneral->piel_obs);
        $template->setValue('torax_obs', $examengeneral->torax_obs);
        $template->setValue('torax_rscs', $examengeneral->torax_rscs);
        $template->setValue('torax_rscs_obs', $examengeneral->torax_rscs_obs);
        $template->setValue('torax_rsrs', $examengeneral->torax_rsrs);
        $template->setValue('torax_senos', $examengeneral->torax_senos);

        $template->setValue('extremidads', $examengeneral->extremidadsuperior);
        $template->setValue('tineld', $examengeneral->extremidadsuperior_derechotinel);
        $template->setValue('phaneld', $examengeneral->extremidadsuperior_derechophanel);
        $template->setValue('finkelsteind', $examengeneral->extremidadsuperior_derechofinkelstein);
        $template->setValue('extremidadsd', $examengeneral->extremidadsuperior_descripcion);
        $template->setValue('tineli', $examengeneral->extremidadsuperior_izquierdotinel);
        $template->setValue('phaneli', $examengeneral->extremidadsuperior_izquierdophanel);
        $template->setValue('finkelsteini', $examengeneral->extremidadsuperior_izquierdofinkelstein);

        $template->setValue('extremidadi', $examengeneral->extremidadinferior);
        $template->setValue('varices', $examengeneral->extremidadinferior_varices);
        $template->setValue('juanetes', $examengeneral->extremidadinferior_juanetes);
        $template->setValue('extremidadid', $examengeneral->extremidadinferior_obs);
        $template->setValue('varices_obs', $examengeneral->extremidadinferior_varices_obs);
        $template->setValue('juanetes_obs', $examengeneral->extremidadinferior_juanetes_obs);

        $template->setValue('neurologico', $examengeneral->neurologico);
        $template->setValue('neurologico_parescraneanos', $examengeneral->neurologico_parescraneanos);
        $template->setValue('neurologico_sensibilidad', $examengeneral->neurologico_sensibilidad);
        $template->setValue('neurologico_marchaenlinea', $examengeneral->neurologico_marchaenlinea);
        $template->setValue('neurologico_reflejos', $examengeneral->neurologico_reflejos);
        $template->setValue('neurologico_coordinacionromber', $examengeneral->neurologico_coordinacionromber);
        $template->setValue('neurologico_esferamental', $examengeneral->neurologico_esferamental);
        $template->setValue('neurologico_obs', $examengeneral->neurologico_obs);

        $template->setValue('columna', $examengeneral->columna);
        $template->setValue('columna_descripcion', $examengeneral->columna_descripcion);
        $template->setValue('columna_inspeccion_simetria', $examengeneral->columna_inspeccion_simetria);
        $template->setValue('columna_inspeccion_simetria_obs', $examengeneral->columna_inspeccion_simetria_obs);
        $template->setValue('columna_inspeccion_curvatura', $examengeneral->columna_inspeccion_curvatura);
        $template->setValue('columna_inspeccion_curvatura_obs', $examengeneral->columna_inspeccion_curvatura_obs);
        $template->setValue('columna_palpacion_dolor', $examengeneral->columna_palpacion_dolor);
        $template->setValue('columna_palpacion_dolor_obs', $examengeneral->columna_palpacion_dolor_obs);
        $template->setValue('columna_palpacion_espasmo', $examengeneral->columna_palpacion_espasmo);
        $template->setValue('columna_palpacion_espasmo_obs', $examengeneral->columna_palpacion_espasmo_obs);
        $template->setValue('columna_test_wells', $examengeneral->columna_test_wells);
        $template->setValue('columna_test_wells_obs', $examengeneral->columna_test_wells_obs);
        $template->setValue('columna_test_shober', $examengeneral->columna_test_shober);
        $template->setValue('columna_test_shober_obs', $examengeneral->columna_test_shober_obs);
        $template->setValue('columna_movilidad_flexion', $examengeneral->columna_movilidad_flexion);
        $template->setValue('columna_movilidad_flexion_obs', $examengeneral->columna_movilidad_flexion_obs);
        $template->setValue('columna_movilidad_extension', $examengeneral->columna_movilidad_extension);
        $template->setValue('columna_movilidad_extension_obs', $examengeneral->columna_movilidad_extension_obs);
        $template->setValue('columna_movilidad_flexionlateral', $examengeneral->columna_movilidad_flexionlateral);
        $template->setValue('columna_movilidad_flexionlateral_obs', $examengeneral->columna_movilidad_flexionlateral_obs);
        $template->setValue('columna_movilidad_rotacion', $examengeneral->columna_movilidad_rotacion);
        $template->setValue('columna_movilidad_rotacion_obs', $examengeneral->columna_movilidad_rotacion_obs);
        $template->setValue('columna_marcha_puntas', $examengeneral->columna_marcha_puntas);
        $template->setValue('columna_marcha_puntas_obs', $examengeneral->columna_marcha_puntas_obs);
        $template->setValue('columna_marcha_talones', $examengeneral->columna_marcha_talones);
        $template->setValue('columna_marcha_talones_obs', $examengeneral->columna_marcha_talones_obs);
        $template->setValue('columna_trofismomuscular_trofismo', $examengeneral->columna_trofismomuscular_trofismo);
        $template->setValue('columna_trofismomuscular_trofismo_obs', $examengeneral->columna_trofismomuscular_trofismo_obs);
        $template->setValue('balancemuscular_cinturaescapular', $examengeneral->balancemuscular_cinturaescapular);
        $template->setValue('balancemuscular_cinturaescapular_obs', $examengeneral->balancemuscular_cinturaescapular_obs);
        $template->setValue('balancemuscular_pectoral', $examengeneral->balancemuscular_pectoral);
        $template->setValue('balancemuscular_pectoral_obs', $examengeneral->balancemuscular_pectoral_obs);
        $template->setValue('balancemuscular_brazo', $examengeneral->balancemuscular_brazo);
        $template->setValue('balancemuscular_brazo_obs', $examengeneral->balancemuscular_brazo_obs);
        $template->setValue('balancemuscular_antebrazo', $examengeneral->balancemuscular_antebrazo);
        $template->setValue('balancemuscular_antebrazo_obs', $examengeneral->balancemuscular_antebrazo_obs);
        $template->setValue('balancemuscular_caderagluteo', $examengeneral->balancemuscular_caderagluteo);
        $template->setValue('balancemuscular_caderagluteo_obs', $examengeneral->balancemuscular_caderagluteo_obs);
        $template->setValue('balancemuscular_muslos', $examengeneral->balancemuscular_muslos);
        $template->setValue('balancemuscular_muslos_obs', $examengeneral->balancemuscular_muslos_obs);
        $template->setValue('balancemuscular_piernas', $examengeneral->balancemuscular_piernas);
        $template->setValue('balancemuscular_piernas_obs', $examengeneral->balancemuscular_piernas_obs);
        $template->setValue('balancemuscular_explicacion', $examengeneral->balancemuscular_explicacion);
        $template->setValue('reflejososteotendinosos_bicipital_derecho', $examengeneral->reflejososteotendinosos_bicipital_derecho);
        $template->setValue('reflejososteotendinosos_bicipital_izquierdo', $examengeneral->reflejososteotendinosos_bicipital_izquierdo);
        $template->setValue('reflejososteotendinosos_radial_derecho', $examengeneral->reflejososteotendinosos_radial_derecho);
        $template->setValue('reflejososteotendinosos_radial_izquierdo', $examengeneral->reflejososteotendinosos_radial_izquierdo);
        $template->setValue('reflejososteotendinosos_rotuliano_derecho', $examengeneral->reflejososteotendinosos_rotuliano_derecho);
        $template->setValue('reflejososteotendinosos_rotuliano_izquierdo', $examengeneral->reflejososteotendinosos_rotuliano_izquierdo);
        $template->setValue('reflejososteotendinosos_alquiliano_derecho', $examengeneral->reflejososteotendinosos_alquiliano_derecho);
        $template->setValue('reflejososteotendinosos_alquiliano_izquierdo', $examengeneral->reflejososteotendinosos_alquiliano_izquierdo);
        $template->setValue('fuerza_miembrosuperior_derecho', $examengeneral->fuerza_miembrosuperior_derecho);
        $template->setValue('fuerza_miembrosuperior_izquierda', $examengeneral->fuerza_miembrosuperior_izquierda);
        $template->setValue('fuerza_miembroinferior_derecho', $examengeneral->fuerza_miembroinferior_derecho);
        $template->setValue('fuerza_miembroinferior_izquierda', $examengeneral->fuerza_miembroinferior_izquierda);

        //-------------------------------MANIPULACION DE ALIMENTOS------------------------------------------

        $manipulacion  = $historia->manipulacionalimentos;
        $template->setValue('solicitado', $manipulacion->solicitado);
        $template->setValue('respiratorio_cumple', $manipulacion->respiratorio_cumple);
        $template->setValue('respiratorio_inspeccion', $manipulacion->respiratorio_inspeccion);
        $template->setValue('respiratorio_ausculacion', $manipulacion->respiratorio_ausculacion);
        $template->setValue('dermatologico_prurito', $manipulacion->dermatologico_prurito);
        $template->setValue('dermatologico_medicamento', $manipulacion->dermatologico_medicamento);
        $template->setValue('dermatologico_cual', htmlspecialchars($manipulacion->dermatologico_cual));
        $template->setValue('dermatologico_lesiones', $manipulacion->dermatologico_lesiones);
        $template->setValue('manipulacion_observacion', htmlspecialchars($manipulacion->observacion));

        //-------------------------------MANIPULACION DE ALIMENTOS------------------------------------------

        $manipulacion  = $historia->manipulacionalimentos;
        $template->setValue('solicitado', $manipulacion->solicitado);
        $template->setValue('respiratorio_cumple', $manipulacion->respiratorio_cumple);
        $template->setValue('respiratorio_inspeccion', $manipulacion->respiratorio_inspeccion);
        $template->setValue('respiratorio_ausculacion', $manipulacion->respiratorio_ausculacion);
        $template->setValue('dermatologico_prurito', $manipulacion->dermatologico_prurito);
        $template->setValue('dermatologico_medicamento', $manipulacion->dermatologico_medicamento);
        $template->setValue('dermatologico_cual', htmlspecialchars($manipulacion->dermatologico_cual));
        $template->setValue('dermatologico_lesiones', $manipulacion->dermatologico_lesiones);
        $template->setValue('manipulacion_observacion', $manipulacion->observacion);

        //--------------------------------IMPRESION DIAGNOSTICA ------------------------
        $impresion = $historia->impresiondiagnostica;
        $examenes = "";
        for($i = 0; $i<count($impresion);$i++) {
            $examenes .= "DIAGNOSTICO: ".$impresion[$i]->diagnostico."</w:t><w:br/><w:t> SOSPECHA DE ORIGEN: ".$impresion[$i]->sospechaOrigen."</w:t><w:br/><w:t> TIPO DE DIAGNOSTICO: ".$impresion[$i]->tipodiagnostico."</w:t><w:br/><w:t></w:t><w:br/><w:t>";
        }
        $template->setValue('impresion', $examenes);

        //--------------------------------PARACLINICOS ------------------------

        $datosparaclinicos = $historia->paraclinicosdatos;
        $template->setValue('optometria', $datosparaclinicos->optometria);
        $template->setValue('optometria_alteracioncorregida', $datosparaclinicos->optometria_alteracioncorregida);
        $template->setValue('optometria_alteracioncorregida_obs', htmlspecialchars($datosparaclinicos->optometria_alteracioncorregida_obs));
        $template->setValue('visiometria', $datosparaclinicos->visiometria);
        $template->setValue('visiometria_alteracioncorregida', $datosparaclinicos->visiometria_alteracioncorregida);
        $template->setValue('visiometria_alteracioncorregida_obs', htmlspecialchars($datosparaclinicos->visiometria_alteracioncorregida_obs));
        $template->setValue('espirometria', $datosparaclinicos->espirometria);
        $template->setValue('espirometria_resultado', htmlspecialchars($datosparaclinicos->espirometria_resultado));
        $template->setValue('espirometria_obs', htmlspecialchars($datosparaclinicos->espirometria_obs));
        $template->setValue('audiometria', $datosparaclinicos->audiometria);
        $template->setValue('audiometria_resultado', $datosparaclinicos->audiometria_resultado);
        $template->setValue('audiometria_obs', htmlspecialchars($datosparaclinicos->audiometria_obs));
        $template->setValue('psicometrico', $datosparaclinicos->psicometrico);
        $template->setValue('psicometrico_obs', htmlspecialchars($datosparaclinicos->psicometrico_obs));
        $template->setValue('psicologico', $datosparaclinicos->psicologico);
        $template->setValue('psicologico_obs', htmlspecialchars($datosparaclinicos->psicologico_obs));
        $template->setValue('pruebavestibular', $datosparaclinicos->pruebavestibular);
        $template->setValue('pruebavestibular_obs', htmlspecialchars($datosparaclinicos->pruebavestibular_obs));

        $paraclinicos = $historia->paraclinicos;
        $examenes = "";
        for($i = 0; $i<count($paraclinicos);$i++) {
            if (isset($paraclinicos[$i]->examenlab)){
                $examenes .= "PARACLINICO: ".$paraclinicos[$i]->examenlab->descripcion."</w:t><w:br/><w:t> RESULTADO: ".$paraclinicos[$i]->observacion."</w:t><w:br/><w:t> OBSERVACION: ".$paraclinicos[$i]->diagnostico."</w:t><w:br/><w:t></w:t><w:br/><w:t>";
            }
        }
        $template->setValue('examen', $examenes);

        //--------------------------------ELEMENTOS DE PROTECCIÓN PERSONAL------------------------
        $cargoactual = $historia->cargoactual;
        $template->setValue('elementosproteccion', str_replace(",", ", ", ($cargoactual->elementos) . "."));

        if ($historia->nuevaValoracion == 0){
            $fecha = new TextRun();
            $fecha->addText($historia->fecha, array('name' => 'Calibri'));
            $template->setValue('fecha', $historia->fecha);
            //--------------------------------PRUEBAS COMPLEMENTARIAS ------------------------
            $template->setValue('pruebascomplementarias', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_pruebacomplementaria)) . "."));
            $template->setValue('otros', $resultado->recomendacion_pruebacomplementaria_otros);

            //--------------------------------CONCEPTO-------------------------------------------
            $template->setValue('concepto', htmlspecialchars(str_replace(",", ", ", ($resultado->resultadoValoracionMedica)) . "."));
            $template->setValue('recomendaciones', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacionesAdicionales)) . "."));
            $template->setValue('restricciones', htmlspecialchars(str_replace(",", ", ", ($resultado->restricciones)) . "."));

            //--------------------------------RECOMENDACIONES PARA EL TRABAJADOR ------------------------
            $template->setValue('medicas', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_medica)) . "."));
            $template->setValue('ingresosve',htmlspecialchars( str_replace(",", ", ", ($resultado->recomendacion_ingresosve)) . "."));
            $template->setValue('habitosyestilosdevida', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_habitos)) . "."));
            $template->setValue('ocupacionales', htmlspecialchars(str_replace(",", ", ", ($resultado->recomendacion_ocupacional)) . "."));

        }else{
            $fecha = new TextRun();
            $fecha->addText($historia->fecha, array('name' => 'Calibri'));
            $template->setValue('fecha', $valoracion->created_at);
            //--------------------------------PRUEBAS COMPLEMENTARIAS ------------------------
            $template->setValue('pruebascomplementarias', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_complementaria)) . "."));
            $template->setValue('otros', $valoracion->nota_complementaria_otro);

            //--------------------------------CONCEPTO-------------------------------------------
            $template->setValue('concepto', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_valoracion_medica)) . "."));
            $template->setValue('recomendaciones', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_adicional)) . "."));
            $template->setValue('restricciones', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_restriccion)) . "."));

            //--------------------------------RECOMENDACIONES PARA EL TRABAJADOR ------------------------
            $template->setValue('medicas', str_replace(",", ", ", ($valoracion->nota_recomendacion_medica) . "."));
            $template->setValue('ingresosve', str_replace(",", ", ", ($valoracion->nota_recomendacion_ingreso) . "."));
            $template->setValue('habitosyestilosdevida', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_habito)) . "."));
            $template->setValue('ocupacionales', htmlspecialchars(str_replace(",", ", ", ($valoracion->nota_recomendacion_ocupacional)) . "."));
        }
        if ($historia->nota == 1){
            if (isset($historia->notaEvolucion)){
                $notaEvolucion = $historia->notaEvolucion;
                $notas = "";
                for($i = 0; $i<count($notaEvolucion);$i++) {
                    $notas .= "Fecha: ".$notaEvolucion[$i]->fecha_nota."</w:t><w:br/><w:t> Nota: ".$notaEvolucion[$i]->observacion_nota."</w:t><w:br/><w:t></w:t><w:br/><w:t>";
                }
                $template->setValue('nota_evolucion', $notas);
            }
        }

        $template->setValue('nombremedico', str_replace(",", ", ", ($historia->medicoapertura->nombre_completo) . "."));
        $template->setValue('cocumentomedico', str_replace(",", ", ", ($historia->medicoapertura->document) . "."));
        $template->setValue('licencia', str_replace(",", ", ", ($historia->medicoapertura->licencia) . "."));

        //--------------------------------CONCEPTO-------------------------------------------



        if (isset($historia->firmaMedico)){
            $image = str_replace('data:image/png;base64,', '', $historia->medicoapertura->firma);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmamedico", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmamedico","");
        }

        if (isset($historia->firmaPaciente)){
            $image = str_replace('data:image/png;base64,', '', $historia->firmaPaciente);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmapaciente", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmapaciente","");
        }

        $template->saveAs(public_path() . '/generados_hc/' . $paciente->documento . '-' . $historia->id .'inf.docx');

        $process = new Process("export HOME=/tmp && libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $paciente->documento . '-' . $historia->id . 'inf.docx ');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
	//        return response()->file(public_path('hc.pdf'));
        return response()->download(public_path($paciente->documento . '-' . $historia->id . 'inf.pdf'));
    }

}
