<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WSHistoriaOptometriaController extends Controller {
	function registerOptometria(Request $request) {
		$agudezavisual = $request->input("agudezavisual");

		$lensometria = $agudezavisual['lensometria'];
		$retinoscopia = $agudezavisual['retinoscopia'];
		$segmentoanterior = $agudezavisual['segmentoanterior'];

		$lensometria_add = $lensometria['add'];
		$lensometria_ojoderecho = $lensometria['ojoderecho'];
		$lensometria_ojoizquierdo = $lensometria['ojoizquierdo'];
		$lensometria_tipolente = $lensometria['tipolente'];

		$retinoscopia_add = $retinoscopia['add'];
		$retinoscopia_ojoderecho = $retinoscopia['ojoderecho'];
		$retinoscopia_ojoizquierdo = $retinoscopia['ojoizquierdo'];

		$segmentoanterior_camaraanterior = $segmentoanterior['camaraanterior'];
		$segmentoanterior_camaraanterior_obs = $segmentoanterior['camaraanterior_obs'];
		$segmentoanterior_cejas = $segmentoanterior['cejas'];
		$segmentoanterior_cejas_obs = $segmentoanterior['cejas_obs'];
		$segmentoanterior_conjuntiva = $segmentoanterior['conjuntiva'];
		$segmentoanterior_conjuntiva_obs = $segmentoanterior['conjuntiva_obs'];
		$segmentoanterior_cornea = $segmentoanterior['cornea'];
		$segmentoanterior_cornea_obs = $segmentoanterior['cornea_obs'];
		$segmentoanterior_esclerotica = $segmentoanterior['esclerotica'];
		$segmentoanterior_esclerotica_obs = $segmentoanterior['esclerotica_obs'];
		$segmentoanterior_fondoojoderecho = $segmentoanterior['fondoojoderecho'];
		$segmentoanterior_fondoojoderecho_obs = $segmentoanterior['fondoojoderecho_obs'];
		$segmentoanterior_fondoojoizquierdo = $segmentoanterior['fondoojoizquierdo'];
		$segmentoanterior_fondoojoizquierdo_obs = $segmentoanterior['fondoojoizquierdo_obs'];
		$segmentoanterior_iris = $segmentoanterior['iris'];
		$segmentoanterior_iris_obs = $segmentoanterior['iris_obs'];
		$segmentoanterior_motilidadocular = $segmentoanterior['motilidadocular'];
		$segmentoanterior_motilidadocular_obs = $segmentoanterior['motilidadocular_obs'];
		$segmentoanterior_parpados = $segmentoanterior['parpados'];
		$segmentoanterior_parpados_obs = $segmentoanterior['parpados_obs'];
		$segmentoanterior_pestanas = $segmentoanterior['pestanas'];
		$segmentoanterior_pestanas_obs = $segmentoanterior['pestanas_obs'];
		$segmentoanterior_pupilas = $segmentoanterior['pupilas'];
		$segmentoanterior_pupilas_obs = $segmentoanterior['pupilas_obs'];
		$segmentoanterior_viaslagrimales = $segmentoanterior['viaslagrimales'];
		$segmentoanterior_viaslagrimales_obs = $segmentoanterior['viaslagrimales_obs'];
		$segmentoanterior_visioncolor = $segmentoanterior['visioncolor'];
		$segmentoanterior_visioncolor_obs = $segmentoanterior['visioncolor_obs'];
		$segmentoanterior_visionprofundidad = $segmentoanterior['visionprofundidad'];
		$segmentoanterior_visionprofundidad_obs = $segmentoanterior['visionprofundidad_obs'];

		$agudezavisual_db = $this->ho_agudezavisual($agudezavisual);

		$historiaOptometria = new HistoriaOptometria();
		$historiaOptometria->lensometria_tipolente = $lensometria_tipolente;
		$historiaOptometria->lensometria_ojoderecho = $lensometria_ojoderecho;
		$historiaOptometria->lensometria_ojoizquierdo = $lensometria_ojoizquierdo;
		$historiaOptometria->lensometria_add = $lensometria_add;

		$historiaOptometria->retinoscopia_ojoderecho = $retinoscopia_ojoderecho;
		$historiaOptometria->retinoscopia_ojoizquierdo = $retinoscopia_ojoizquierdo;
		$historiaOptometria->retinoscopia_add = $retinoscopia_add;

		$historiaOptometria->segmentoanterior_camaraanterior = $segmentoanterior_camaraanterior;
		$historiaOptometria->segmentoanterior_camaraanterior_obs = $segmentoanterior_camaraanterior_obs;
		$historiaOptometria->segmentoanterior_cejas = $segmentoanterior_cejas;
		$historiaOptometria->segmentoanterior_cejas_obs = $segmentoanterior_cejas_obs;
		$historiaOptometria->segmentoanterior_conjuntiva = $segmentoanterior_conjuntiva;
		$historiaOptometria->segmentoanterior_conjuntiva_obs = $segmentoanterior_conjuntiva_obs;
		$historiaOptometria->segmentoanterior_cornea = $segmentoanterior_cornea;
		$historiaOptometria->segmentoanterior_cornea_obs = $segmentoanterior_cornea_obs;
		$historiaOptometria->segmentoanterior_esclerotica = $segmentoanterior_esclerotica;
		$historiaOptometria->segmentoanterior_esclerotica_obs = $segmentoanterior_esclerotica_obs;
		$historiaOptometria->segmentoanterior_fondoojoderecho = $segmentoanterior_fondoojoderecho;
		$historiaOptometria->segmentoanterior_fondoojoderecho_obs = $segmentoanterior_fondoojoderecho_obs;
		$historiaOptometria->segmentoanterior_fondoojoizquierdo = $segmentoanterior_fondoojoizquierdo;
		$historiaOptometria->segmentoanterior_fondoojoizquierdo_obs = $segmentoanterior_fondoojoizquierdo_obs;
		$historiaOptometria->segmentoanterior_iris = $segmentoanterior_iris;
		$historiaOptometria->segmentoanterior_iris_obs = $segmentoanterior_iris_obs;
		$historiaOptometria->segmentoanterior_motilidadocular = $segmentoanterior_motilidadocular;
		$historiaOptometria->segmentoanterior_motilidadocular_obs = $segmentoanterior_motilidadocular_obs;
		$historiaOptometria->segmentoanterior_parpados = $segmentoanterior_parpados;
		$historiaOptometria->segmentoanterior_parpados_obs = $segmentoanterior_parpados_obs;
		$historiaOptometria->segmentoanterior_pestanas = $segmentoanterior_pestanas;
		$historiaOptometria->segmentoanterior_pestanas_obs = $segmentoanterior_pestanas_obs;
		$historiaOptometria->segmentoanterior_pupilas = $segmentoanterior_pupilas;
		$historiaOptometria->segmentoanterior_pupilas_obs = $segmentoanterior_pupilas_obs;
		$historiaOptometria->segmentoanterior_viaslagrimales = $segmentoanterior_viaslagrimales;
		$historiaOptometria->segmentoanterior_viaslagrimales_obs = $segmentoanterior_viaslagrimales_obs;
		$historiaOptometria->segmentoanterior_visioncolor = $segmentoanterior_visioncolor;
		$historiaOptometria->segmentoanterior_visioncolor_obs = $segmentoanterior_visioncolor_obs;
		$historiaOptometria->segmentoanterior_visionprofundidad = $segmentoanterior_visionprofundidad;
		$historiaOptometria->segmentoanterior_visionprofundidad_obs = $segmentoanterior_visionprofundidad_obs;

		return $historiaOptometria;
	}

	function ho_agudezavisual($agudezavisual) {
		$lejana_concorreccion_ojobinocular = $agudezavisual['lejana_concorreccion_ojobinocular'];
		$lejana_concorreccion_ojobinocular_obs = $agudezavisual['lejana_concorreccion_ojobinocular_obs'];
		$lejana_concorreccion_ojoderecho = $agudezavisual['lejana_concorreccion_ojoderecho'];
		$lejana_concorreccion_ojoderecho_obs = $agudezavisual['lejana_concorreccion_ojoderecho_obs'];
		$lejana_concorreccion_ojoizquierdo = $agudezavisual['lejana_concorreccion_ojoizquierdo'];
		$lejana_concorreccion_ojoizquierdo_obs = $agudezavisual['lejana_concorreccion_ojoizquierdo_obs'];
		$lejana_sincorreccion_ojobinocular = $agudezavisual['lejana_sincorreccion_ojobinocular'];
		$lejana_sincorreccion_ojobinocular_obs = $agudezavisual['lejana_sincorreccion_ojobinocular_obs'];
		$lejana_sincorreccion_ojoderecho = $agudezavisual['lejana_sincorreccion_ojoderecho'];
		$lejana_sincorreccion_ojoderecho_obs = $agudezavisual['lejana_sincorreccion_ojoderecho_obs'];
		$lejana_sincorreccion_ojoizquierdo = $agudezavisual['lejana_sincorreccion_ojoizquierdo'];
		$lejana_sincorreccion_ojoizquierdo_obs = $agudezavisual['lejana_sincorreccion_ojoizquierdo_obs'];
		$cercana_concorreccion_ojobinocular = $agudezavisual['cercana_concorreccion_ojobinocular'];
		$cercana_concorreccion_ojobinocular_obs = $agudezavisual['cercana_concorreccion_ojobinocular_obs'];
		$cercana_concorreccion_ojoderecho = $agudezavisual['cercana_concorreccion_ojoderecho'];
		$cercana_concorreccion_ojoderecho_obs = $agudezavisual['cercana_concorreccion_ojoderecho_obs'];
		$cercana_concorreccion_ojoizquierdo = $agudezavisual['cercana_concorreccion_ojoizquierdo'];
		$cercana_concorreccion_ojoizquierdo_obs = $agudezavisual['cercana_concorreccion_ojoizquierdo_obs'];
		$cercana_sincorreccion_ojobinocular = $agudezavisual['cercana_sincorreccion_ojobinocular'];
		$cercana_sincorreccion_ojobinocular_obs = $agudezavisual['cercana_sincorreccion_ojobinocular_obs'];
		$cercana_sincorreccion_ojoderecho = $agudezavisual['cercana_sincorreccion_ojoderecho'];
		$cercana_sincorreccion_ojoderecho_obs = $agudezavisual['cercana_sincorreccion_ojoderecho_obs'];
		$cercana_sincorreccion_ojoizquierdo = $agudezavisual['cercana_sincorreccion_ojoizquierdo'];
		$cercana_sincorreccion_ojoizquierdo_obs = $agudezavisual['cercana_sincorreccion_ojoizquierdo_obs'];

		$agudezavisual_db = new HO_AgudezaVisual();
		$agudezavisual_db->ls_od = $lejana_sincorreccion_ojoderecho;
		$agudezavisual_db->ls_odobservacion = $lejana_sincorreccion_ojoderecho_obs;
		$agudezavisual_db->ls_oi = $lejana_sincorreccion_ojoizquierdo;
		$agudezavisual_db->ls_oiobservacion = $lejana_sincorreccion_ojoizquierdo_obs;
		$agudezavisual_db->ls_ojobinocular = $lejana_sincorreccion_ojobinocular;
		$agudezavisual_db->ls_bobservacion = $lejana_sincorreccion_ojobinocular_obs;
		$agudezavisual_db->lc_od = $lejana_concorreccion_ojoderecho;
		$agudezavisual_db->lc_odobservacion = $lejana_concorreccion_ojoderecho_obs;
		$agudezavisual_db->lc_oi = $lejana_concorreccion_ojoizquierdo;
		$agudezavisual_db->lc_oiobservacion = $lejana_concorreccion_ojoizquierdo_obs;
		$agudezavisual_db->lc_ojobinocular = $lejana_concorreccion_ojobinocular;
		$agudezavisual_db->lc_bobservacion = $lejana_concorreccion_ojobinocular_obs;
		$agudezavisual_db->cs_od = $cercana_sincorreccion_ojoderecho;
		$agudezavisual_db->cs_odobservacion = $cercana_sincorreccion_ojoderecho_obs;
		$agudezavisual_db->cs_oi = $cercana_sincorreccion_ojoizquierdo;
		$agudezavisual_db->cs_oiobservacion = $cercana_sincorreccion_ojoizquierdo_obs;
		$agudezavisual_db->cs_ojobinocular = $cercana_sincorreccion_ojobinocular;
		$agudezavisual_db->cs_bobservacion = $cercana_sincorreccion_ojobinocular_obs;
		$agudezavisual_db->cc_od = $cercana_concorreccion_ojoderecho;
		$agudezavisual_db->cc_odobservacion = $cercana_concorreccion_ojoderecho_obs;
		$agudezavisual_db->cc_oi = $cercana_concorreccion_ojoizquierdo;
		$agudezavisual_db->cc_oiobservacion = $cercana_concorreccion_ojoizquierdo_obs;
		$agudezavisual_db->cc_ojobinocular = $cercana_concorreccion_ojobinocular;
		$agudezavisual_db->cc_bobservacion = $cercana_concorreccion_ojobinocular_obs;
		return $agudezavisual_db;
	}
}
