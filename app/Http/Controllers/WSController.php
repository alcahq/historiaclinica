<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterPatientRequest;
use App\Patient;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WSController extends Controller {

	public function pacienteRegistro(RegisterPatientRequest $request) {
		$documento = $request->input('documento');

		if (Patient::where('documento', $documento)->count() == 0) {
            $requestData = $request->all();

            $requestData['fechaNacimiento'] = date('Y-m-d', strtotime($requestData['fechaNacimiento']));
            $requestData['edad'] = Carbon::parse($requestData['fechaNacimiento'])->age;
            $requestData['nombre_completo'] = $requestData['nombre'].' '.$requestData['apellido'];
            $tipodocumento = $request->input('tipodocumento');
			$nombre = $request->input('nombre');
			$apellido = $request->input('apellido');
			$genero = $request->input('genero');
			$fechaNacimiento = date('Y-m-d', strtotime($request->input('fechaNacimiento')));
			$lugarNacimiento = $request->input('lugarNacimiento');
			$direccionDomicilio = $request->input('direccionDomicilio');
			$ciudadDomicilio = $request->input('ciudadDomicilio');
			$telefonoDomicilio = $request->input('telefonoDomicilio');
			$telefonoCelular = $request->input('telefonoCelular');
			$correoElectronico = $request->input('correoElectronico');
			$nivelEscolaridad = $request->input('escolaridad');
			$estadoCivil = $request->input('estadoCivil');
			$cargo = $request->input('cargo');
			$antiguedad = $request->input('antiguedad');
			$edad = $request->input('edad');
			$eps = $request->input('eps');
			$arl = $request->input('arl');
			$afp = $request->input('afp');
			$grupoSanguineo = $request->input('grupoSanguineo');
			$rh = $request->input('rh');
			$foto = $request->input('foto');
			$firma = $request->input('firma');
			$avisoEmergenciaNombres = $request->input('acompanantenombre');
			$avisoEmergenciaApellidos = $request->input('acompananteapellido');
			$avisoEmergenciaTelefono = $request->input('acompanantetelefono');
			$avisoEmergenciaParentesco = $request->input('acompananteparentesco');

			$paciente = new Patient();
			$paciente->tipodocumento = $tipodocumento;
			$paciente->documento = $documento;
			$paciente->nombre = $nombre;
            $paciente->apellido = $apellido;
			$paciente->genero = $genero;
			$paciente->fechaNacimiento = $fechaNacimiento;
			$paciente->lugarNacimiento = $lugarNacimiento;
			$paciente->direccionDomicilio = $direccionDomicilio;
			$paciente->ciudadDomicilio = $ciudadDomicilio;
			$paciente->telefono = $telefonoDomicilio;
			$paciente->celular = $telefonoCelular;
			$paciente->correoElectronico = $correoElectronico;
			$paciente->escolaridad = $nivelEscolaridad;
			$paciente->estadoCivil = $estadoCivil;
			$paciente->cargo = $cargo;
			$paciente->antiguedad = $antiguedad;
			$paciente->edad = $edad;
			$paciente->eps = $eps;
			$paciente->arl = $arl;
			$paciente->afp = $afp;
			$paciente->grupoSanguineo = $grupoSanguineo;
			$paciente->rh = $rh;
			$paciente->foto = $foto;
			$paciente->firma = $firma;
			$paciente->nombreAcompaniante = $avisoEmergenciaNombres;
			$paciente->apellidoAcompaniante = $avisoEmergenciaApellidos;
			$paciente->celularAcompaniante = $avisoEmergenciaTelefono;
			$paciente->parentescoAcompaniante = $avisoEmergenciaParentesco;
			$requestData = $request->all();
			$requestData['nombre_completo'] = $requestData['nombre'] . ' ' . $requestData['apellido'];
			$paciente->nombre_completo = $nombre . ' ' . $apellido;
			$paciente->save();
			$json = array('mensaje' => "Se ha creado correctamente el paciente", "apciente" => $request->all());
			return json_encode($json);
		} else {
			$json = array('mensaje' => "Ya existe un paciente registrado con este numero de documento");
			return json_encode($json);
		}
	}

    public function pacienteUpdate(Request $request) {

        $documento = $request->input('documento');
        if (Patient::where('documento', $documento)->count() > 0) {
            $tipodocumento = $request->input('tipodocumento');
            $nombre = $request->input('nombre');
            $apellido = $request->input('apellido');
            $genero = $request->input('genero');
            $fechaNacimiento = date('Y-m-d', strtotime($request->input('fechaNacimiento')));
            $edad = $request->input('edad');
            $lugarNacimiento = $request->input('lugarNacimiento');
            $direccionDomicilio = $request->input('direccionDomicilio');
            $ciudadDomicilio = $request->input('ciudadDomicilio');
            $telefonoDomicilio = $request->input('telefonoDomicilio');
            $telefonoCelular = $request->input('telefonoCelular');
            $correoElectronico = $request->input('correoElectronico');
            $nivelEscolaridad = $request->input('escolaridad');
            $estadoCivil = $request->input('estadoCivil');
            $cargo = $request->input('cargo');
            $antiguedad = $request->input('antiguedad');
            $eps = $request->input('eps');
            $arl = $request->input('arl');
            $afp = $request->input('afp');
            $grupoSanguineo = $request->input('grupoSanguineo');
            $rh = $request->input('rh');
            $foto = $request->input('foto');
            $firma = $request->input('firma');
            $acompanante = $request->input('acompanante');
            $avisoEmergenciaNombres = $request->input('acompanantenombre');
            $avisoEmergenciaApellidos = $request->input('acompananteapellido');
            $avisoEmergenciaTelefono = $request->input('acompanantetelefono');
            $avisoEmergenciaParentesco = $request->input('acompananteparentesco');

            $paciente = Patient::where('documento', $documento)->first();
            $paciente->tipodocumento = $tipodocumento;
            $paciente->nombre = $nombre;
            $paciente->apellido = $apellido;
            $paciente->genero = $genero;
            if($fechaNacimiento != "1969-12-31") {
                $paciente->fechaNacimiento = $fechaNacimiento;
                $paciente->edad = $edad;
            }
            $paciente->lugarNacimiento = $lugarNacimiento;
            $paciente->direccionDomicilio = $direccionDomicilio;
            $paciente->ciudadDomicilio = $ciudadDomicilio;
            $paciente->telefono = $telefonoDomicilio;
            $paciente->celular = $telefonoCelular;
            $paciente->correoElectronico = $correoElectronico;
            $paciente->escolaridad = $nivelEscolaridad;
            $paciente->estadoCivil = $estadoCivil;
            $paciente->cargo = $cargo;
            $paciente->antiguedad = $antiguedad;
            $paciente->eps = $eps;
            $paciente->arl = $arl;
            $paciente->afp = $afp;
            $paciente->grupoSanguineo = $grupoSanguineo;
            $paciente->rh = $rh;
            $paciente->foto = $foto;
            $paciente->firma = $firma;
            $paciente->nombreAcompaniante = $avisoEmergenciaNombres;
            $paciente->apellidoAcompaniante = $avisoEmergenciaApellidos;
            $paciente->celularAcompaniante = $avisoEmergenciaTelefono;
            $paciente->parentescoAcompaniante = $avisoEmergenciaParentesco;
            $requestData = $request->all();
            $requestData['nombre_completo'] = $requestData['nombre'] . ' ' . $requestData['apellido'];
            $paciente->nombre_completo = $nombre . ' ' . $apellido;
            $paciente->save();
            $json = array('mensaje' => "Se ha actualizado correctamente el paciente " );
            return ($json);
        } else {
            $json = array('mensaje' => "No existe un paciente registrado con este numero de documento");
            return $json;
        }
    }

    public function pacienteUpdateFotoFirma(Request $request) {

        $documento = $request->input('documento');
        if (Patient::where('documento', $documento)->count() > 0) {

            $foto = $request->input('foto');
            $firma = $request->input('firma');

            $paciente = Patient::where('documento', $documento)->first();

            $paciente->foto = $foto;
            $paciente->firma = $firma;
            $paciente->save();
            $json = array('mensaje' => "Se ha actualizado correctamente el paciente");
            return ($json);
        } else {
            $json = array('mensaje' => "No existe un paciente registrado con este numero de documento");
            return $json;
        }
    }

	public function pacienteSync(Request $request) {

		$documento = $request->input('documento');
		$tipodocumento = $request->input('tipodocumento');
		$nombre = $request->input('nombre');
		$apellido = $request->input('apellido');
		$genero = $request->input('genero');
		$input = strtotime($request->input('fechaNacimiento'));
		$fechaNacimiento = date('Y-m-d', strtotime($request->input('fechaNacimiento')));
		$lugarNacimiento = $request->input('lugarNacimiento');
		$direccionDomicilio = $request->input('direccionDomicilio');
		$ciudadDomicilio = $request->input('ciudadDomicilio');
		$telefonoDomicilio = $request->input('telefonoDomicilio');
		$telefonoCelular = $request->input('telefonoCelular');
		$correoElectronico = $request->input('correoElectronico');
		$nivelEscolaridad = $request->input('escolaridad');
		$estadoCivil = $request->input('estadoCivil');
		$cargo = $request->input('cargo');
		$antiguedad = $request->input('antiguedad');
		$edad = $request->input('edad');
		$eps = $request->input('eps');
		$arl = $request->input('arl');
		$afp = $request->input('afp');
		$grupoSanguineo = $request->input('grupoSanguineo');
		$rh = $request->input('rh');
		$foto = $request->input('foto');
		$firma = $request->input('firma');
		$acompanante = $request->input('acompanante');
		$avisoEmergenciaNombres = $request->input('nombreAcompaniante');
		$avisoEmergenciaApellidos = $request->input('apellidoAcompaniante');
		$avisoEmergenciaTelefono = $request->input('celularAcompaniante');
		$avisoEmergenciaParentesco = $request->input('parentescoAcompaniante');
		$mensaje = "Se ha actualizado correctamente el paciente";
		$paciente = Patient::where('documento', $documento)->first();
		if (!isset($paciente)) {
			$paciente = new Patient();
			$paciente->documento = $documento;
			$mensaje = "Se ha registrado correctamente el paciente";
		}
		$paciente->tipodocumento = $tipodocumento;
		$paciente->nombre = $nombre;
		$paciente->apellido = $apellido;
		$paciente->genero = $genero;
		$paciente->fechaNacimiento = $fechaNacimiento;
		$paciente->lugarNacimiento = $lugarNacimiento;
		$paciente->direccionDomicilio = $direccionDomicilio;
		$paciente->ciudadDomicilio = $ciudadDomicilio;
		$paciente->telefono = $telefonoDomicilio;
		$paciente->celular = $telefonoCelular;
		$paciente->correoElectronico = $correoElectronico;
		$paciente->escolaridad = $nivelEscolaridad;
		$paciente->estadoCivil = $estadoCivil;
		$paciente->cargo = $cargo;
		$paciente->antiguedad = $antiguedad;
		$paciente->edad = $edad;
		$paciente->eps = $eps;
		$paciente->arl = $arl;
		$paciente->afp = $afp;
		$paciente->grupoSanguineo = $grupoSanguineo;
		$paciente->rh = $rh;
		$paciente->foto = $foto;
		$paciente->firma = $firma;
		$paciente->nombreAcompaniante = $avisoEmergenciaNombres;
		$paciente->apellidoAcompaniante = $avisoEmergenciaApellidos;
		$paciente->celularAcompaniante = $avisoEmergenciaTelefono;
		$paciente->parentescoAcompaniante = $avisoEmergenciaParentesco;
        $paciente->nombre_completo = $nombre . ' ' . $apellido;
		$paciente->save();
		$json = array('mensaje' => $mensaje);
		return ($json);

	}

	function pacienteSearch(Request $request) {
		$documento = $request->input('documento');
		$paciente = Patient::where('documento', $documento)->first();
		if ($paciente != null) {
			return ($paciente);
		} else {
			$json = array('mensaje' => "No existe un paciente registrado con este numero de documento");
			return json_encode($json);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request) {
		$usuario = $request->all();
		$usuario['nombre_completo'] = $usuario['name'] . ' ' . $usuario['last_name'];
		$user = User::where("document", $usuario["document"])->update($usuario);

		return ['mensaje' => 'Usuario actualizado exitosamente', "usre" => $usuario];
	}

}
