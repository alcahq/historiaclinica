<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\HistoriaClinica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Style\Language;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validarCampos(Request $request){
        $request->validate([
            'empresa_id' => 'required',
            'fechaDesde' => 'required',
            'fechaHasta' => 'required',
            'motivoEvaluacion' => 'required',
        ],[
            'empresa_id.required' => 'El campo empresa es obligatorio',
            'fechaDesde.required' => 'El campo fecha desde es obligatorio',
            'fechaHasta.required' => 'El campo fecha hasta es obligatorio',
            'motivoEvaluacion.required' => 'El campo motivo evaluacion es obligatorio',
        ]);
    }

    public function generateReport(Request $request)
    {
        $this->validarCampos($request);
        //Captura de datos de la vista grafica
        $fechaDesde = date('Y-m-d H:i:s', strtotime($request->input('fechaDesde').' 00:00:00'));
        $fechaHasta = date('Y-m-d H:i:s', strtotime($request->input('fechaHasta').' 23:59:59'));
        $motivoE = $request->input('motivoEvaluacion');
        $empresa_id = $request->input('empresa_id');
        $empresa = Empresa::find($empresa_id);
        $nombreEmpresa = $empresa->nombre;


        $cantidadPacientes = $this->cantPacientesByFechaEmpresa($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        if ($cantidadPacientes == 0){
            return back()->with('busquedaReporte','No se encuentran resultados asociados a la busqueda.');
        }

        //Se crea el documento
        $documento = new \PhpOffice\PhpWord\PhpWord();
        $documento->getSettings()->setThemeFontLang(new Language(Language::ES_ES));

        //Creacion de una vineta
        $documento->addNumberingStyle(
            'multilevel',
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'bullet', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                ),
            )
        );

        $documento->addNumberingStyle(
            'multilevel-general',
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'decimal', 'text' => '%1.%2.', 'left' => 720, 'hanging' => 600, 'tabPos' => 720),
                    array('format' => 'decimal', 'text' => '%1.%2.%3.', 'left' => 600, 'hanging' => 300, 'tabPos' => 720),
                    array('format' => 'bullet', 'text' => '%4.', 'left' => 360, 'hanging' => 300, 'tabPos' => 720),

                ),
            )
        );

        $documento->addNumberingStyle(
            'multilevel-bibliografia',
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'decimal', 'text' => '%1.%2.', 'left' => 720, 'hanging' => 600, 'tabPos' => 720),
                    array('format' => 'decimal', 'text' => '%1.%2.%3.', 'left' => 600, 'hanging' => 300, 'tabPos' => 720),
                    array('format' => 'bullet', 'text' => '%4.', 'left' => 360, 'hanging' => 300, 'tabPos' => 720),

                ),
            )
        );

        //Se agrega al documento un estilo de alineado
        $documento->addParagraphStyle('PSCentrado',array("alignment" => "center", 'lineHeight' => 1.5,'spaceBefore' => 0, 'spaceAfter' => 0));
        $documento->addParagraphStyle('PSIzquierda',array("alignment" => "left", 'lineHeight' => 1.5,'spaceBefore' => 0, 'spaceAfter' => 0));
        $documento->addParagraphStyle('PSJustificado',array("alignment" => "both", 'lineHeight' => 1.5,'spaceBefore' => 0, 'spaceAfter' => 0));
        $documento->addParagraphStyle('PSTablas',array("alignment" => "both", 'lineHeight' => 1.5,'spaceBefore' => 0, 'spaceAfter' => 0));
        $documento->addParagraphStyle('PSPiePag',array("alignment" => "both", 'lineHeight' => 1,'spaceBefore' => 0, 'spaceAfter' => 0));

        //Se crean los estulos de fuente como tipo de letra, tamano, etc.

        $fontStyle = array('name' => 'Arial', 'bold' => true, 'size' => '12');
        $fontStyle2 = array('name' => 'Arial', 'bold' => true, 'size' => '14');
        $fontStyle3 = array('name' => 'Arial', 'bold' => true, 'size' => '18');
        $fontStyle4 = array('name' => 'Arial', 'bold' => true, 'size' => '8');
        $fontStyle5 = array('name' => 'Arial', 'bold' => false, 'size' => '11');
        $fontStyle6 = array('name' => 'Arial', 'bold' => true, 'size' => '11');
        $fontStyleTable1 = array('name' => 'Calibri', 'bold' => true, 'size' => '10');
        $fontStyleTable2 = array('name' => 'Calibri', 'bold' => false, 'size' => '10', 'align' => 'center');
        $fontStyleTableIE = array('name' => 'Arial', 'bold' => false, 'size' => '11', 'align' => 'center');
        $fsPieTable = array('name' => 'Arial', 'bold' => false, 'size' => '8', 'align' => 'center');

        //Estilo para un titulo
        $documento->addTitleStyle(0, $fontStyle6, array("alignment" => "center",'spacing' => 100));

        //---------------------------------------------------Primera pagina----------------------------------------------------------

        $pag1 = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));

        $currentYear = date('Y');
        $currentMonth = date('M');
        $tittle = "DIAGNÓSTICO CONDICIONES DE SALUD";
        $subTittle = "EXÁMENES MÉDICOS OCUPACIONALES " . $currentYear;
        $nameCompany = strtoupper($nombreEmpresa);
        $text1 = "ASESORÍA TÉCNICA Y METODOLÓGICA";
        $logoSO = public_path("img/logoSO.jpg");
        $text2 = "ELABORADO MEDIANTE CONTRATO DE PRESTACIÓN DE SERVICIOS POR ANALIZAR SALUD OCUPACIONAL "."<w:br/>". " LICENCIA S.O. No. 2328 DEL 25 DE FEBRERO DE 2015";
        $text3 = "BOGOTÁ, D.C."."<w:br/>" . $currentMonth . " " . $currentYear;

        //Se agregan los componentes a la pagina (textos, imagenes, etc.)
        $pag1->addText($tittle, $fontStyle2)->setParagraphStyle(array("alignment" => "center"));
        $pag1->addText($subTittle, $fontStyle)->setParagraphStyle(array("alignment" => "center"));
        $pag1->addTextBreak(2, $fontStyle3, 'PSCenter');
        $pag1->addText($nameCompany, $fontStyle)->setParagraphStyle(array("alignment" => "center"));
        $pag1->addTextBreak(2, $fontStyle3, 'PSCenter');
        $pag1->addText($text1, $fontStyle)->setParagraphStyle(array("alignment" => "center"));
        $pag1->addTextBreak(1, $fontStyle3, 'PSCenter');
        $pag1->addImage($logoSO, array(
            'width'         => 250,
            'height'        => 100,
            'marginTop'     => -1,
            'marginLeft'    => -1,
            'wrappingStyle' => 'behind',
            'alignment'     => 'center'
        ));
        $pag1->addText($text2, $fontStyle)->setParagraphStyle(array("alignment" => "center"));
        $pag1->addTextBreak(5, $fontStyle3, 'PSCenter');
        $pag1->addText($text3, $fontStyle)->setParagraphStyle(array("alignment" => "center"));

        //---------------------------------------------------Segunda pagina----------------------------------------------------------

        $pag2 = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698));
        $pag2->addTextBreak(13, $fontStyle3, 'PSIzquierda');
        $pag2->addImage(public_path("img/logoSO.jpg"), array(
            'width'         => 200,
            'height'        => 80,
            'marginTop'     => -1,
            'marginLeft'    => -1,
            'wrappingStyle' => 'behind',
            'alignment'     => 'left'
        ));
        $pag2->addText("Elaborado por:", $fontStyle5, 'PSIzquierda');
        $pag2->addText(htmlspecialchars(auth()->user()->name." ".auth()->user()->last_name), $fontStyle6, 'PSIzquierda');
        $pag2->addText("Médico Especialista en medicina del trabajo", $fontStyle5, 'PSIzquierda');
        $pag2->addText("Lic. S.O. ".auth()->user()->licencia." de S.D.S", $fontStyle5, 'PSIzquierda');
        $pag2->addText("R.M ".auth()->user()->registro_medico, $fontStyle5, 'PSIzquierda');

        //---------------------------------------------------Tercera pagina----------------------------------------------------------

        $pag3 = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698));
        $pag3->addTitle("TABLA DE CONTENIDO");

        //---------------------------------------------------Cuarta pagina----------------------------------------------------------

        $pag4 = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698));
        $pag4->addTitle("CONTENIDO DE TABLAS");
        $pag4->addPageBreak();

        //---------------------------------------------------Quinta pagina----------------------------------------------------------

        $pag5 = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pag5->addTextBreak(4, "PSJustificado");
        $pag5->addTitle("GLOSARIO", 0);
        $pag5->addTextBreak(1, $fontStyle6, "PSJustificado");
        $pag5->addText($this->addTextArial11("ACCIDENTE DE TRABAJO (AT): ","Todo suceso repentino que sobrevenga por causa o con ocasión del trabajo, y que produzca en el trabajador una lesión orgánica, una perturbación funcional o psiquiátrica, una invalidez o la muerte. Es también accidente de trabajo aquel que se produce durante la ejecución de órdenes del empleador o contratante, o en la ejecución de una labor bajo su autoridad, aún fuera del lugar y horas de trabajo. Igualmente se considera accidente de trabajo el que se produzca durante el traslado de los trabajadores o contratistas, desde su residencia al lugar de trabajo o viceversa cuando el transporte lo suministre el empleador. También se considera accidente de trabajo el sufrido durante la ejecución de la función sindical, en ejecución de actividades recreativas, deportivas o culturales cuando se actúe por cuenta o en representación del empleador o contratante. (Ley 1562 de 2012)\")"), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6, "PSJustificado");
        $pag5->addText($this->addTextArial11("ADMINISTRADORAS DE RIESGOS LABORALES (ARL): ","Entidades encargadas del recaudo y la administración de los aportes que realizan las empresas para cobertura de riesgos laborales (accidentes o enfermedad profesionales)."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1,$fontStyle6, "PSJustificado");
        $pag5->addText($this->addTextArial11("AGUDEZA VISUAL (AV): ","Es la capacidad del sistema visual para percibir, detectar o identificar objetos."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6, "PSJustificado");
        $pag5->addText($this->addTextArial11("AMETROPIA: ","Cuando la luz entra al sistema óptico y se focaliza en uno o varios planos diferentes al plano de la retina. Estos defectos de refracción se dividen en 4 clases: miopía, hipermetropía, astigmatismo, presbicia."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6, "PSJustificado");
        $pag5->addListItem($this->addTextArial11("Miopía: ", "Se caracteriza por mala visión de lejos. Por lo general no se presenta ningún otro síntoma. El miope tiende a acercarse mucho al objeto que quiere observar y cuando mira a lo lejos, entrecierra los párpados para tratar de ver mejor."), 1, null, 'multilevel','PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addListItem($this->addTextArial11("Hipermetropía: ", "El hipermétrope trata de ver bien encogiendo el cristalino permanentemente, lo que provoca cansancio, enrojecimiento, lagrimeo y dolor de cabeza, en especial al mirar algo cercano.  La visión no siempre se verá afectada."), 1, null, 'multilevel','PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addListItem($this->addTextArial11("Astigmatismo: ", "Se presenta cuando la córnea es ovalada en lugar de ser esférica. Al igual que la hipermetropía, la disminución de la visión no se percibe en defectos bajos pero producirá síntomas similares a los descritos.  Si el astigmatismo es medio o alto provocará gran distorsión en la visión a todas las distancias."), 1, null, 'multilevel', 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addListItem($this->addTextArial11("Presbicia: ", "Cuando la persona como consecuencia de la pérdida de elasticidad del cristalino, que se presenta a medida que se va avanzando en edad, presenta dificultad para enfocar nítidamente las imágenes que están ubicadas cerca (30 a 50 cm)."), 1, null, 'multilevel', 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("DESLUMBRAMIENTO: ","Es cualquier brillo que produce molestia, interferencia con la visión o fatiga visual."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("DESORDEN MUSCULO ESQUELETICO (DME): ","Son lesiones o trastornos de los músculos, nervios, tendones, articulaciones, cartílagos y discos vertebrales. Incluye aquellas lesiones que se deben a una reacción corporal (movimientos repetitivos, esfuerzo físico, posturas forzadas exposición a vibración.  No incluyen trastornos causados por caídas, tropezones, resbalones, o incidentes con vehículos automotores o similares."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("DISCROMATOPSIA: ","Trastornos en la percepción de los colores, alteración en la visión cromática, particularmente la dificultad de reconocer matices o gamas de colores."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("DOLOR LUMBAR: ","Es la sensación de dolor o molestia localizada entre el límite inferior de las costillas y el límite inferior de los glúteos, su intensidad varía en según las posturas y la actividad física.  Generalmente el dolor se presenta con el movimiento.  El lumbago es inespecífico cuando el dolor no es originado por traumatismos, enfermedades sistémicas, ni hay compresión de las raíces nerviosas (radiculopatía)."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("DOLOR RADICULAR: ","Es el dolor que se origina en los nervios espinales, cuando hay compresión de las raíces nerviosas."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("ENFERMEDAD LABORAL: ","(Ley 1562 de 2012 – Artículo 4): Es la enfermedad contraída como resultado de exposición a factores de riesgo inherentes a la actividad laboral o del medio en el que el trabajador se ha visto obligado a trabajar. El gobierno nacional, determinará, en forma periódica, las enfermedades que se consideran como laborales, y en los casos en que una enfermedad no figure en la tabla de enfermedades laborales, pero se demuestre la relación de causalidad con los factores de riesgo ocupacionales, será reconocida como enfermedad laboral, conforme lo establecido en las normas legales vigentes."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("ENFERMEDAD COMÚN O DE ORIGEN GENERAL: ","Cualquier estado patológico producto de factores como la herencia y su relación con el medio ambiente biológico, psicológico y social.  Existen casos de enfermedad común que pueden ser incrementados en su severidad y/o frecuencia de aparición por el ambiente laboral."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("EVALUACIÓN FUNCIONAL DE LA VOZ - FONIATRIA: ","La evaluación funcional de la voz es el proceso por el cual se valora cada uno de los parámetros de la voz: Intensidad, tono, timbre y duración; se evalúan en gran parte a través de la percepción auditiva, palpación y observación visual, lo que convierte a este tipo de evaluación en subjetiva. Como resultado de este proceso de obtiene una visión del estado de la voz del paciente.  "), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("EMPRESAS PROMOTORAS DE SALUD (EPS): ","Entidad encargada del recaudo y administración de dinero del régimen contributivo, destinado a la atención de la salud de los afiliados y sus familiares."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("ESTEREOPSIS – VISION DE PROFUNDIDAD: ","la agudeza visual estereoscópica es la mínima disparidad binocular que da lugar a la sensación de profundidad.  El umbral de discriminación de profundidad es el menor intervalo espacial en profundidad entre 2 objetos que una persona es capaz de percibir.  Hay alteración en la esteropsis, es decir en la percepción de la profundidad, por causas como: estrabismos (endotropías, exotropías, microtropías), ametropías altas no corregidas y ambliopías."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("ESTILO DE VIDA SALUDABLE: ","Conjunto de comportamientos beneficiosos que desarrolla una persona, como por ejemplo alimentación balanceada, actividad física regular, autoexamen de mama y testículo, control por citología de cuello uterino, etc., con el fin de gozar de buena salud."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("EXAMEN MEDICO OCUPACIONAL: ","Acto médico mediante el cual se interroga y examina a un trabajador, con el fin de monitorear la exposición a factores de riesgo y determinar la existencia de consecuencias en la persona por dicha exposición."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("EXPOSICIÓN: ","Frecuencia en la que una persona o estructura entran en contacto con el factor de riesgo, la cual puede ser continúa, frecuente, ocasional, esporádica y remota."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("HISTORIA CLÍNICA: ","Documento privado, obligatorio y sometido a reserva, en el cual se registran cronológicamente las condiciones de salud del paciente, los actos médicos y los demás procedimientos ejecutados por el equipo de salud que interviene en su atención. Dicho documento únicamente puede ser conocido por terceros previa autorización del paciente o en los casos previstos por la ley."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("HIPERTENSIÓN ARTERIAL: ","Elevación mantenida de la presión arterial por encima de los límites normales. El séptimo informe del Joint National Comittee (JNC VII) establece niveles de normalidad, pre-hipertensión e hipertensión estadio 1 y estadio 2 así:"), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("TABLA 1. ","Clasificación cifras presión arterial según Joint National Comittee (JNC VII)"), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);

        //Propiedades de una tabla
        $styleTable = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 25, 'valign' => 'center', 'width' => 50 * 50, 'unit' => 'pct', 'align' => 'center');
        $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
        $cellHCentered = array('align' => 'center', 'spaceBefore' => 0, 'spaceAfter' => 0);
        $cellHJustify = array('align' => 'both', 'spaceBefore' => 0, 'spaceAfter' => 0);

        //Tabla 1
        $documento->addTableStyle('table1', $styleTable);
        $table1 = $pag5->addTable( 'table1');
        $table1->addRow();
        $table1->addCell(2000, $cellRowSpan)->addText("CLASIFICACIÓN", $fontStyleTable1, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText("PRESIÓN ARTERIAL SISTÓLICA", $fontStyleTable1, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText("PRESIÓN ARTERIAL DIASTÓLICA", $fontStyleTable1, $cellHCentered);

        $table1->addRow();
        $table1->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable2, $cellHJustify);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("< 120 mmHg"), $fontStyleTable2, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("< 80 mmHg"), $fontStyleTable2, $cellHCentered);

        $table1->addRow();
        $table1->addCell(2000, $cellRowSpan)->addText("PREHIPERTENSIÓN", $fontStyleTable2, $cellHJustify);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("120 -139 mmHg"), $fontStyleTable2, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("80 -89 mmHg"), $fontStyleTable2, $cellHCentered);

        $table1->addRow();
        $table1->addCell(2000, $cellRowSpan)->addText("HTA ESTADIO 1", $fontStyleTable2, $cellHJustify);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("140 -159 mmHg"), $fontStyleTable2, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("90- 99 mmHg"), $fontStyleTable2, $cellHCentered);

        $table1->addRow();
        $table1->addCell(2000, $cellRowSpan)->addText("HTA ESTADIO 2", $fontStyleTable2, $cellHJustify);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars(">160 mmHg"), $fontStyleTable2, $cellHCentered);
        $table1->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("> 100 mmHg"), $fontStyleTable2, $cellHCentered);

        $pag5->addTextBreak(1);
        $pag5->addText($this->addTextArial11("HIPOACUSIA INDUCIDA POR RUIDO: ","es la pérdida auditiva que se deriva de la incapacidad del oído interno de servir como transductor, siendo incapaz de convertir el estímulo físico en potencial nervioso y por lo tanto éste no llega al cerebro para ser interpretado.  Su lesión, como su nombre lo indica, es debido a la exposición continuada a niveles peligrosos de ruido."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("INSUFICIENCIA VENOSA: ","Es la disminución en el flujo sanguíneo venoso secundario a la incompetencia de las válvulas, obstrucción de las venas, o a la reducción en la eficacia de la acción de bombeo de los músculos circulantes.  La disminución del flujo sanguíneo aumenta la presión venosa que lleva a la dilatación progresiva de las venas (varices)."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("MEDICINA PREVENTIVA Y DEL TRABAJO: ","Es el área de la salud que previene los efectos nocivos de un evento laboral sobre la salud de los funcionarios.  Además, actúa en la curación y rehabilitación cuando ya se presentan las consecuencias de esos eventos."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("PATOLOGÍA: ","Se usa como sinónimo de enfermedad, aunque etimológicamente no signifique lo mismo."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("PAUSAS ACTIVAS: ","Es una actividad física realizada en un breve espacio de tiempo en la jornada laboral, orientada a que las personas recuperen energías para un desempeño eficiente de trabajo, a través, de ejercicios que compensen las tareas desempeñadas, revirtiendo de esta manera la fatiga muscular y el cansancio generado por el trabajo."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("POSTURA: ","Alineación refinada con arreglo relativo de las partes del cuerpo en un estado de equilibrio, que protege las estructuras de soporte contra lesiones o deformidades progresivas."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("PROGRAMA DE VIGILANCIA EPIDEMIOLOGICA (PVE): ","Serie de actividades, orientadas a hacer seguimiento de una enfermedad, evitar su aparición, disminuir su incidencia o sus secuelas, interviniendo los riesgos que favorecen su aparición."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("PTERIGIO: ","Es el crecimiento anormal y no canceroso de la conjuntiva que aparece como una mancha carnosa de color blancuzco y con vasos sanguíneos; el crecimiento del pterigio puede avanzar a la córnea y si es muy grande puede producir visión borrosa por distorsión de la superficie corneal."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGO LABORAL: ","Probabilidad de ocurrencia de un accidente o enfermedad que se produce como consecuencia directa del trabajo o labor desempeñada."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGOS BIOLÓGICOS: ","Se refiere a microorganismos patógenos y a los residuos, que por sus características, pueden ser nocivos para las personas que entren en contacto con ellos, desencadenando enfermedades infectocontagiosas, reacciones alérgicas, o intoxicaciones."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGOS BIOMECÁNICOS: ","Son todos aquellos objetos, puestos de trabajo y herramientas, que por el peso, tamaño, forma o diseño, encierran la capacidad potencial de producir fatiga física o desórdenes músculo-esqueléticos, por obligar al trabajador a realizar sobreesfuerzos, movimientos repetitivos y/o posturas inadecuadas."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGO FÍSICOS: ","Son todos aquellos factores ambientales de naturaleza física que al ser percibidos por las personas, pueden provocar efectos adversos a la salud según sea la intensidad, la exposición y concentración de los mismos."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGO PSICOSOCIALES: ","Se refiere a la interacción de los aspectos propios de las personas (edad, patrimonio genético, estructura sociológica, historia, vida familiar, cultura, etc.) con las modalidades de gestión administrativa y demás aspectos organizacionales inherentes al tipo de proceso productivo."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("RIESGO QUÍMICOS: ","Se refiere los elementos o sustancias orgánicas e inorgánicas que pueden ingresar al organismo por inhalación, absorción o ingestión y dependiendo de su concentración de exposición, pueden generar lesiones sistémicas, intoxicaciones o quemaduras."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("SALUD: ","El más completo estado de bienestar físico, mental y social y no-solo la ausencia de enfermedad (Organización Mundial de la Salud OMS)."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("SEGURIDAD Y SALUD EN EL TRABAJO: ","Promoción y mantenimiento del bienestar físico, biológico, mental y social de los trabajadores en su entorno laboral."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("SISTEMA DE GESTIÓN DE LA SEGURIDAD Y SALUD EN EL TRABAJO SG-SST: ","(Ley 1562 de 2012 y del decreto 1072 de 2015). Este Sistema consiste en el desarrollo de un proceso lógico y por etapas, basado en la mejora continua y que incluye la política, la organización, la planificación, la aplicación, la evaluación, la auditoría y las acciones de mejora con el objetivo de anticipar, reconocer, evaluar y controlar los riesgos que puedan afectar la seguridad y salud en el trabajo. Este sistema de gestión debe permitir ubicar al trabajador en un puesto de trabajo de acuerdo a sus condiciones."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("EL ÍNDICE DE MASA CORPORAL (IMC): ","es un indicador simple de la relación entre el peso y la talla que se utiliza frecuentemente para identificar el sobrepeso y la obesidad en los adultos.  Se calcula dividiendo el peso de una persona en kilos por el cuadrado de su talla en metros (kg/m2)."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("TABLA 2. ","Clasificación del índice de masa corporal"), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);

        //Tabla 2
        $documento->addTableStyle('table2', $styleTable);
        $table2 = $pag5->addTable( 'table2');
        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("CLASIFICACIÓN", $fontStyleTable1, $cellHCentered);
        $table2->addCell(1200, $cellRowSpan)->addText("IMC", $fontStyleTable1, $cellHCentered);
        $table2->addCell(1500, $cellRowSpan)->addText("RIESGO", $fontStyleTable1, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("BAJO PESO", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("< 18.5"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("BAJO"), $fontStyleTable2, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("18.5  – 24.9"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PROMEDIO"), $fontStyleTable2, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("SOBREPESO", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars(">25 - 29.9"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("AUMENTADO"), $fontStyleTable2, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("OBESIDAD GI", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars(">30 - 34.9"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("MODERADO"), $fontStyleTable2, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("OBESIDAD GII", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("35 - 39.9"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("GRAVE"), $fontStyleTable2, $cellHCentered);

        $table2->addRow();
        $table2->addCell(2000, $cellRowSpan)->addText("OBESIDAD GIII", $fontStyleTable2, $cellHJustify);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars(">40"), $fontStyleTable2, $cellHCentered);
        $table2->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("GRAVE"), $fontStyleTable2, $cellHCentered);

        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("SOBREPESO Y OBESIDAD: ","Se definen como una acumulación anormal o excesiva de grasa que puede ser perjudicial para la salud.  La causa fundamental del sobrepeso y la obesidad es un desequilibrio energético entre calorías consumidas y gastadas.  En el mundo, se ha producido un aumento en la ingesta de alimentos hipercalóricos que son ricos en grasa, sal y azúcares, pero pobres en vitaminas, minerales y otros micronutrientes, y un descenso en la actividad física como resultado de la naturaleza cada vez más sedentaria de muchas formas de trabajo, de los nuevos modos de desplazamiento y de una creciente urbanización."), $fontStyle5, 'PSJustificado');
        $pag5->addTextBreak(1, $fontStyle6);
        $pag5->addText($this->addTextArial11("SEDENTARISMO: ","Es la falta de actividad física adecuada.  Algunos autores han definido como “sedentarios” a quienes gastan en actividades de tiempo libre menos del 10% de la energía total empleada en la actividad física diaria.  Cuando no se cumple la meta de realizar actividad física fuera del horario de trabajo, equivalente mínimo a 30 minutos 3 veces por semana.  El sedentarismo es un factor de riesgo para enfermedad coronaria que suele asociarse a otros factores de riesgo como hiperlipidemia, sobrepeso - obesidad, hipertensión arterial, diabetes."), $fontStyle5, 'PSJustificado');
        $pag5->addPageBreak();

        //---------------------------------------------------Informacion empresa----------------------------------------------------------

        $pagInfoEmpresa = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagInfoEmpresa->addTextBreak(4, $fontStyle5);
        $pagInfoEmpresa->addListItem("INFORMACIÓN DE LA EMPRESA", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagInfoEmpresa->addTextBreak(2, $fontStyle5);

        //Tabla informacion de la empresa
        $styleTableIE = array('borderSize' => 6, 'borderColor' => 'FFFFFF', 'cellMargin' => 200, 'valign' => 'center');
        $documento->addTableStyle('informacion empresa', $styleTableIE);
        $tableIE = $pagInfoEmpresa->addTable( 'informacion empresa');
        $tableIE->addRow();
        $tableIE->addCell(3000, $cellRowSpan)->addText("NOMBRE:", $fontStyleTableIE, 'PSJustificado');
        $tableIE->addCell(6000, $cellRowSpan)->addText(htmlspecialchars(strtoupper($nombreEmpresa)), $fontStyleTableIE, 'PSJustificado');

        $tableIE->addRow();
        $tableIE->addCell(3000, $cellRowSpan)->addText("DIRECCIÓN:", 'PSJustificado');
        $tableIE->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($empresa->direccion), $fontStyleTableIE, 'PSJustificado');

        $tableIE->addRow();
        $tableIE->addCell(3000, $cellRowSpan)->addText("TELÉFONO:", 'PSJustificado');
        $tableIE->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($empresa->telefono), $fontStyleTableIE, 'PSJustificado');

        $tableIE->addRow();
        $tableIE->addCell(3000, $cellRowSpan)->addText("CONTACTO:", 'PSJustificado');
        $tableIE->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($empresa->contacto), $fontStyleTableIE, 'PSJustificado');

        $tableIE->addRow();
        $tableIE->addCell(3000, $cellRowSpan)->addText("TRABAJADORES EVALUADOS:", $fontStyleTableIE);
        $tableIE->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($this->convertir($cantidadPacientes). " (".$cantidadPacientes.")"), $fontStyleTableIE, 'PSJustificado');

        $pagInfoEmpresa->addPageBreak();

        //---------------------------------------------------Introduccion----------------------------------------------------------

        $pagIntroduccion = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagIntroduccion->addTextBreak(4, $fontStyle6);
        $pagIntroduccion->addListItem("INTRODUCCIÓN", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagIntroduccion->addTextBreak(1, $fontStyle5);

        $pagIntroduccion->addText(htmlspecialchars("Con el objetivo de promover las políticas de la empresa en cuanto a la protección de sus trabajadores frente a los factores de riesgo ocupacionales, fomentar los estilos de vida saludables, proporcionar ambientes de trabajo más seguros y dar cumplimiento a la reglamentación legal vigente en materia de Salud Ocupacional, ".htmlspecialchars(strtoupper($nombreEmpresa)).", programó los exámenes médicos ocupacionales de ".htmlspecialchars($this->motivosSeleccionados($motivoE))." para su población trabajadora."), $fontStyle5, 'PSJustificado');
        $pagIntroduccion->addTextBreak(1, $fontStyle6);
        $pagIntroduccion->addText(htmlspecialchars("Este documento contiene información y análisis sobre las condiciones de salud de los ".$cantidadPacientes." trabajadores valorados en los exámenes ocupacionales del "."[AQUI VA EL BIMESTRE EN QUE SE REALIZAN LAS PRUEBAS]".". La unidad de salud ocupacional -ANALI.S.O- de Analizar Laboratorio Clínico, [AQUI VAN DIAS Y LUGAR DONDE SE HICIERON LAS PRUEBAS] "), $fontStyle5, 'PSJustificado');
        $pagIntroduccion->addTextBreak(1, $fontStyle6);
        $pagIntroduccion->addText(htmlspecialchars("Este informe se elaboró con los parámetros establecidos en la Resolución 2346 de 2007, en él se encuentra el análisis estadístico de la información relevante sobre las variables socio-demográficas, ocupacionales y médicas de la población evaluada, conclusiones y recomendaciones de forma global, teniendo en cuenta el artículo 18 de la resolución 2346 de 2007, este diagnóstico no podrá contener datos médicos personales ni individualizados."), $fontStyle5, 'PSJustificado');
        $pagIntroduccion->addPageBreak();

        //---------------------------------------------------Objetivos----------------------------------------------------------

        $pagObjetivos = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagObjetivos->addListItem("OBJETIVOS", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagObjetivos->addTextBreak(1, $fontStyle5);
        $pagObjetivos->addListItem("OBJETIVO GENERAL", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle5);
        $pagObjetivos->addText(htmlspecialchars("Identificar las condiciones de salud, de los trabajadores de ".htmlspecialchars(strtoupper($nombreEmpresa))."[UBICACION DE LA EMPRESA], ocasionadas por la exposición del trabajador a los factores de riesgo propios de la labor o del ambiente de trabajo, así mismo detectar enfermedades de origen común, con el fin de que se inicien las intervenciones pertinentes a través del personal encargado del Sistema de Gestión en Seguridad y Salud en el Trabajo, y si es el caso de condiciones de origen común se canalice través de la EPS."), $fontStyle5, 'PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle5);
        $pagObjetivos->addListItem("OBJETIVOS ESPECÍFICOS", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle6);
        $pagObjetivos->addListItem("Evaluar a los trabajadores de ".htmlspecialchars(strtoupper($nombreEmpresa)).", mediante la aplicación de la Historia Clínica Ocupacional, para emitir recomendaciones específicas y generales de la población evaluada, de acuerdo a los hallazgos de patologías comunes, enfermedades laborales, o sintomatología referida por los trabajadores.", 3, $fontStyle5, 'multilevel-general','PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle6);
        $pagObjetivos->addListItem("Identificar que personal requiere intervención prioritaria dentro de los sistemas de vigilancia de la empresa, según los riesgos y las condiciones de trabajo.", 3, $fontStyle5, 'multilevel-general','PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle6);
        $pagObjetivos->addListItem("Recomendar actividades orientadas a promover la salud de los trabajadores, prevenir enfermedades y controlar las ya existentes, tanto a nivel individual como grupal.", 3, $fontStyle5, 'multilevel-general','PSJustificado');
        $pagObjetivos->addTextBreak(1, $fontStyle6);
        $pagObjetivos->addListItem("Orientar a cada trabajador respecto al manejo de la red de su EPS, la adopción de medidas de promoción y prevención de enfermedades de alto impacto en Salud Pública, recomendaciones de realización de pruebas complementarias en la EPS si así lo amerita, y motivarlo a continuar sus controles médicos de patologías crónicas.", 3, $fontStyle5, 'multilevel-general','PSJustificado');
        $pagObjetivos->addPageBreak();

        //---------------------------------------------------Materiales y metodos----------------------------------------------------------

        $pagMateriales = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagMateriales->addListItem("MATERIALES Y MÉTODOS", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagMateriales->addTextBreak(1, $fontStyle5, 'PSJustificado');
        $pagMateriales->addListItem("FUENTES DE INFORMACIÓN", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagMateriales->addTextBreak(1, $fontStyle5, 'PSJustificado');
        $pagMateriales->addText(htmlspecialchars("La fuente de información utilizada para realizar el diagnóstico de salud, fueron las historias clínicas de los exámenes ocupacionales del [AQUI VA EL BIMESTRE DEL AÑO].  Se atendieron en la consulta médica ocupacional ".$cantidadPacientes."trabajadores de ".$nombreEmpresa.". [REQUERIMIENTOS DE LA EMPRESA]"), $fontStyle5, 'PSJustificado');
        $pagMateriales->addTextBreak(1, $fontStyle5,'PSJustificado');
        $pagMateriales->addText($this->addTextArial11('Tabla 3. ', 'Frecuencia y tipo de valoración ocupacional realizada'));

        $cantidadPruebasMotivo = $this->pruebasMotivo($fechaDesde, $fechaHasta, $empresa_id,$motivoE);

        $pagMateriales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('pruebas', $styleTable);
        $tableCantPruebas = $pagMateriales->addTable( 'pruebas');

        $tableCantPruebas->addRow();
        $tableCantPruebas->addCell(4000, $cellRowSpan)->addText("TIPOS DE EXÁMENES REALIZADOS", $fontStyleTable1, $cellHCentered);
        $tableCantPruebas->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        for ($i = 0; $i < count($cantidadPruebasMotivo); $i++){
            $tableCantPruebas->addRow();
            $tableCantPruebas->addCell(4000, $cellRowSpan)->addText("EXAMEN OCUPACIONAL ".$cantidadPruebasMotivo[$i]->motivoevaluacion, $fontStyleTable2, $cellHJustify);
            $tableCantPruebas->addCell(2000, $cellRowSpan)->addText($cantidadPruebasMotivo[$i]->cantidad, $fontStyleTable2, $cellHCentered);
        }

        $pagMateriales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagMateriales->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagMateriales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //Tabla pruebas complementarias

        $cantPruebasComplement = $this->pruebasComplementarias($fechaDesde, $fechaHasta, $empresa_id,$motivoE);
        $pruebasCom = array('OPTOMETRIA','VISIOMETRIA','AUDIOMETRIA','ESPIROMETRIA', 'PSICOLOGICO', 'PSICOMETRICO', 'PRUEBA VESTIBULAR');
        $pagMateriales->addText($this->addTextArial11('Tabla 4. ', 'Frecuencia y tipo de valoración ocupacional realizada pruebas complementarias'));
        $pagMateriales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        $documento->addTableStyle('pruebasComp', $styleTable);
        $tableCantPruebasComplementarias = $pagMateriales->addTable( 'pruebasComp');
        $examenesLaboratorio = $this->cantidadExamenesComplementarios($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        $tableCantPruebasComplementarias->addRow();
        $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText("PRUEBAS COMPLEMENTARIAS", $fontStyleTable1, $cellHCentered);
        $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        if ($cantPruebasComplement[0]->rango1 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[0], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango1, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango2 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[1], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango2, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango3 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[2], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango3, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango4 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[3], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango4, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango5 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[4], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango5, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango6 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[5], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango6, $fontStyleTable2, $cellHCentered);
        }

        if ($cantPruebasComplement[0]->rango7 > 0) {
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(6000, $cellRowSpan)->addText($pruebasCom[6], $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($cantPruebasComplement[0]->rango7, $fontStyleTable2, $cellHCentered);
        }

        for ($i = 0; $i < count($examenesLaboratorio); $i++){
            $tableCantPruebasComplementarias->addRow();
            $tableCantPruebasComplementarias->addCell(4000, $cellRowSpan)->addText($examenesLaboratorio[$i]->descripcion, $fontStyleTable2, $cellHJustify);
            $tableCantPruebasComplementarias->addCell(2000, $cellRowSpan)->addText($examenesLaboratorio[$i]->cantidad, $fontStyleTable2, $cellHCentered);
        }

        $pagMateriales->addTextBreak(1, $fsPieTable,'PSPiePag');
        $pagMateriales->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagMateriales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagMateriales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagMateriales->addPageBreak();

        //---------------------------------------------------Resultados----------------------------------------------------------

        $pagResultados = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagResultados->addListItem("RESULTADOS", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("A continuación, se presentan los datos estadísticos en términos de frecuencias, y porcentajes."), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addListItem("CARACTERÍSTICAS SOCIODEMOGRÁFICAS DE LA POBLACIÓN", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addListItem("DISTRIBUCION POR GÉNERO", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantidadPacientesGenero = $this->cantidadPorGenero($fechaDesde, $fechaHasta, $empresa_id,$motivoE);

        if (isset($cantidadPacientesGenero[0]->cantidad)){
            $porcentajeM = number_format(($cantidadPacientesGenero[0]->cantidad/$cantidadPacientes)*100, '1');
            $cantidadM = $cantidadPacientesGenero[0]->cantidad;
        }else{
            $porcentajeM = 0;
            $cantidadM = 0;
        }
        if (isset($cantidadPacientesGenero[1]->cantidad)){
            $porcentajeF = number_format(($cantidadPacientesGenero[1]->cantidad/$cantidadPacientes)*100, '1');
            $cantidadF = $cantidadPacientesGenero[1]->cantidad;
        }else{
            $porcentajeF = 0;
            $cantidadF = 0;
        }
        $pagResultados->addText($this->addTextArial11('Tabla 5. ', 'Distribución por género y área'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('cantidad genero', $styleTable);
        $tableCantGenero = $pagResultados->addTable( 'cantidad genero');
        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("GÉNERO:", $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("MASCULINO", $fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($cantidadM, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentajeM.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("FEMENINO",$fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($cantidadF, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentajeF.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($cantidadPacientes, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeF + $porcentajeM).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addPageBreak();
        $pagResultados->addText($this->addTextArial11('GRÁFICA 1. ', 'Distribución por género'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------
        $categories = array('MASCULINO', 'FEMENINO');
        $series = array($porcentajeM/100, $porcentajeF/100);
        $style = array('showAxisLabels' => true, 'gridX' => true);
        $chart = $pagResultados->addChart('column', $categories, $series, $style);
        $chart->getStyle()->setWidth(Converter::inchToEmu(3))->setHeight(Converter::inchToEmu(3));
        $chart->getStyle()->setColors(array('4F81BD','F79646'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        //------------------Fin grafica-----------------------

        $pagResultados->addTextBreak(1,$fontStyle5);
        $pagResultados->addListItem("DISTRIBUCION POR GRUPO ETARIO", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('Tabla 6. ', 'Distribución por grupo etario'));
        $grupoEtario = $this->cantidadGrupoEtario($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalEtario = $grupoEtario[0]->rango1 + $grupoEtario[0]->rango2 + $grupoEtario[0]->rango3 + $grupoEtario[0]->rango4 + $grupoEtario[0]->rango5;
        $porcentaje20_31 = number_format(($grupoEtario[0]->rango1/$totalEtario)*100, '1');
        $porcentaje31_40 = number_format(($grupoEtario[0]->rango2/$totalEtario)*100, '1');
        $porcentaje41_50 = number_format(($grupoEtario[0]->rango3/$totalEtario)*100, '1');
        $porcentaje51_60 = number_format(($grupoEtario[0]->rango4/$totalEtario)*100, '1');
        $porcentaje60 = number_format(($grupoEtario[0]->rango5/$totalEtario)*100, '1');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $porcentajeEtario = $porcentaje20_31 + $porcentaje31_40 + $porcentaje41_50 + $porcentaje51_60 + $porcentaje60;

        $documento->addTableStyle('grupo etario', $styleTable);
        $tableCantGenero = $pagResultados->addTable( 'grupo etario');
        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("EDAD /AÑOS"), $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("20-31"), $fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($grupoEtario[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentaje20_31.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("31-40"),$fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($grupoEtario[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentaje31_40.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("41-50"), $fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($grupoEtario[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentaje41_50.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("51-60"),$fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($grupoEtario[0]->rango4, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentaje51_60.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("> 60"),$fontStyleTable2, $cellHJustify);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($grupoEtario[0]->rango5, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($porcentaje60.'%', $fontStyleTable2, $cellHCentered);

        $tableCantGenero->addRow();
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText($totalEtario, $fontStyleTable2, $cellHCentered);
        $tableCantGenero->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEtario).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addPageBreak();
        $pagResultados->addText($this->addTextArial11('GRÁFICA 2. ', 'Distribución por grupo etario'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------
        $categoriesGE = array(htmlspecialchars('20-30'), htmlspecialchars('31-40'), htmlspecialchars('41-50'), htmlspecialchars('51-60'), htmlspecialchars('> 60'));
        $seriesGE = array($porcentaje20_31/100, $porcentaje31_40/100, $porcentaje41_50/100, $porcentaje51_60/100, $porcentaje60/100);
        //$style = array('showAxisLabels' => true, 'gridX' => true);
        $chartGE = $pagResultados->addChart('column', $categoriesGE, $seriesGE, $style);
        $chartGE->getStyle()->setWidth(Converter::inchToEmu(5))->setHeight(Converter::inchToEmu(3));
        $chartGE->getStyle()->setColors(array('4F81BD'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        //------------------Fin grafica-----------------------

        //------------------Distribucion escolaridad-----------------------

        $pagResultados->addTextBreak(1,$fontStyle5, 'PSPiePag');
        $pagResultados->addListItem("DISTRIBUCIÓN POR ESCOLARIDAD", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('Tabla 7. ', 'Distribución por escolaridad'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantidadEscolaridad = $this->cantidadEscolaridad($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalEscolaridad = $cantidadEscolaridad[0]->rango1 + $cantidadEscolaridad[0]->rango2 + $cantidadEscolaridad[0]->rango3 + $cantidadEscolaridad[0]->rango4 +
            $cantidadEscolaridad[0]->rango5 + $cantidadEscolaridad[0]->rango6 + $cantidadEscolaridad[0]->rango7 + $cantidadEscolaridad[0]->rango8 + $cantidadEscolaridad[0]->rango9;
        $analfabeta = number_format(($cantidadEscolaridad[0]->rango1/$totalEscolaridad)*100, '1');
        $primaria = number_format(($cantidadEscolaridad[0]->rango2/$totalEscolaridad)*100, '1');
        $secundaria = number_format(($cantidadEscolaridad[0]->rango3/$totalEscolaridad)*100, '1');
        $tecnico = number_format(($cantidadEscolaridad[0]->rango4/$totalEscolaridad)*100, '1');
        $tecnologo = number_format(($cantidadEscolaridad[0]->rango5/$totalEscolaridad)*100, '1');
        $universidad = number_format(($cantidadEscolaridad[0]->rango6/$totalEscolaridad)*100, '1');
        $especia = number_format(($cantidadEscolaridad[0]->rango7/$totalEscolaridad)*100, '1');
        $maestria = number_format(($cantidadEscolaridad[0]->rango8/$totalEscolaridad)*100, '1');
        $doctorado = number_format(($cantidadEscolaridad[0]->rango9/$totalEscolaridad)*100, '1');
        $porcentajeEscolaridad = $analfabeta + $primaria + $secundaria + $tecnico + $tecnologo + $universidad + $especia + $maestria + $doctorado;

        $documento->addTableStyle('escolaridad', $styleTable);
        $tableCantEscolaridad = $pagResultados->addTable( 'escolaridad');
        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ESCOLARIDAD"), $fontStyleTable1, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('ANALFABETA', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($analfabeta.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('PRIMARIA', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($primaria.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('SECUNDARIA', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($secundaria.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('TÉCNICO', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango4, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($tecnico.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('TECNÓLOGO', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango5, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($tecnologo.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('UNIVERSITARIO', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango6, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($universidad.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('ESPECIALIZACIÓN', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango7, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($especia.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('MAESTRIA', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango8, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($maestria.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText('DOCTORADO', $fontStyleTable2, $cellHJustify);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($cantidadEscolaridad[0]->rango9, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($doctorado.'%', $fontStyleTable2, $cellHCentered);

        $tableCantEscolaridad->addRow();
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText($totalEscolaridad, $fontStyleTable2, $cellHCentered);
        $tableCantEscolaridad->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEscolaridad).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('GRÁFICA 3. ', 'Distribución por escolaridad'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------
        $categoriesES = array(htmlspecialchars('ANALFABETA'), htmlspecialchars('PRIMARIA'), htmlspecialchars('SECUNDARIA'), htmlspecialchars('TÉCNICO'),
            htmlspecialchars('TECNÓLOGO'), htmlspecialchars('UNIVERSITARIO'), htmlspecialchars('ESPECIALIZACIÓN'), htmlspecialchars('MAESTRIA'), htmlspecialchars('DOCTORADO'));
        $seriesES = array($analfabeta/100, $primaria/100, $secundaria/100, $tecnico/100, $tecnologo/100, $universidad/100, $especia/100, $maestria/100, $doctorado/100);
        $chartES = $pagResultados->addChart('column', $categoriesES, $seriesES, $style);
        $chartES->getStyle()->setWidth(Converter::inchToEmu(5))->setHeight(Converter::inchToEmu(3));
        $chartES->getStyle()->setColors(array('4F81BD'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        //------------------Fin grafica-----------------------

        //-----------------Distribucion estado civil----------------
        $pagResultados->addTextBreak(1,$fontStyle5, 'PSPiePag');
        $pagResultados->addListItem("DISTRIBUCIÓN POR ESTADO CIVIL", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('Tabla 8. ', 'Distribución por estado civil'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantEstadoCivil = $this->cantidadEstadoCivil($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalEstadoCivil = $cantEstadoCivil[0]->rango1 + $cantEstadoCivil[0]->rango2 + $cantEstadoCivil[0]->rango3 + $cantEstadoCivil[0]->rango4 + $cantEstadoCivil[0]->rango5;
        $soltero = number_format(($cantEstadoCivil[0]->rango1/$totalEstadoCivil)*100, '1');
        $casado = number_format(($cantEstadoCivil[0]->rango2/$totalEstadoCivil)*100, '1');
        $union = number_format(($cantEstadoCivil[0]->rango3/$totalEstadoCivil)*100, '1');
        $separado = number_format(($cantEstadoCivil[0]->rango4/$totalEstadoCivil)*100, '1');
        $viudo = number_format(($cantEstadoCivil[0]->rango5/$totalEstadoCivil)*100, '1');
        $porcentajeEstadoCivil = $soltero + $casado + $union + $separado + $viudo;

        $documento->addTableStyle('estado civil', $styleTable);
        $tableCantEstadoC = $pagResultados->addTable( 'estado civil');
        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ESTADO CIVIL"), $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SOLTERO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantEstadoCivil[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($soltero."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("CASADO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantEstadoCivil[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($casado."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("UNIÓN LIBRE"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantEstadoCivil[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($union."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SEPARADO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantEstadoCivil[0]->rango4, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($separado."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("VIUDO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantEstadoCivil[0]->rango5, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($viudo."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($totalEstadoCivil, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEstadoCivil).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('GRÁFICA 4. ', 'Distribución por estado civil'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------
        $categoriesEC = array(htmlspecialchars('SOLTERO'), htmlspecialchars('CASADO'), htmlspecialchars('UNION LIBRE'), htmlspecialchars('SEPARADO'),
            htmlspecialchars('VIUDO'));
        $seriesEC = array($soltero/100, $casado/100, $union/100, $separado/100, $viudo/100);
        $chartEC = $pagResultados->addChart('column', $categoriesEC, $seriesEC, $style);
        $chartEC->getStyle()->setWidth(Converter::inchToEmu(5))->setHeight(Converter::inchToEmu(3));
        $chartEC->getStyle()->setColors(array('4F81BD'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        //------------------Fin grafica-----------------------


        //-------------------Distribucion tabaquismo-------------------------

        $pagResultados->addTextBreak(1,$fontStyle5, 'PSPiePag');
        $pagResultados->addListItem("DISTRIBUCIÓN POR HÁBITOS", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('Tabla 9. ', 'Distribución por hábitos tabáquicos'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantTabaquismo = $this->cantidadTabaquismo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalPacientesTabaquismo = $cantTabaquismo[0]->rango1 + $cantTabaquismo[0]->rango2 + $cantTabaquismo[0]->rango3;
        $noFumador = number_format(($cantTabaquismo[0]->rango1/$totalPacientesTabaquismo)*100, '1');
        $exFumador = number_format(($cantTabaquismo[0]->rango2/$totalPacientesTabaquismo)*100, '1');
        $fumador = number_format(($cantTabaquismo[0]->rango3/$totalPacientesTabaquismo)*100, '1');
        $porcentajeTabaquismo = $noFumador + $exFumador + $fumador;

        $documento->addTableStyle('tabaquismo', $styleTable);
        $tableCantTabaquismo = $pagResultados->addTable( 'tabaquismo');
        $tableCantTabaquismo->addRow();
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TABAQUISMO"), $fontStyleTable1, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantTabaquismo->addRow();
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO FUMADOR"), $fontStyleTable2, $cellHJustify);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($cantTabaquismo[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($noFumador."%", $fontStyleTable2, $cellHCentered);

        $tableCantTabaquismo->addRow();
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("EX FUMADOR"), $fontStyleTable2, $cellHJustify);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($cantTabaquismo[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($exFumador."%", $fontStyleTable2, $cellHCentered);

        $tableCantTabaquismo->addRow();
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("FUMADOR"), $fontStyleTable2, $cellHJustify);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($cantTabaquismo[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($fumador."%", $fontStyleTable2, $cellHCentered);

        $tableCantTabaquismo->addRow();
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText($totalPacientesTabaquismo, $fontStyleTable2, $cellHCentered);
        $tableCantTabaquismo->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeTabaquismo).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------------Ingesta de licor----------------------

        $pagResultados->addText($this->addTextArial11('Tabla 10. ', 'Distribución por ingesta de licor'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantAlcohol = $this->cantidadAlcohol($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalPacientesAlcohol = $cantAlcohol[0]->rango1 + $cantAlcohol[0]->rango2;
        $siAlcohol = number_format(($cantAlcohol[0]->rango1/$totalPacientesAlcohol)*100, '1');
        $noAlcohol = number_format(($cantAlcohol[0]->rango2/$totalPacientesAlcohol)*100, '1');
        $porcentajeLicor = $siAlcohol + $noAlcohol;

        $documento->addTableStyle('alcohol', $styleTable);
        $tableCantAlcohol = $pagResultados->addTable( 'alcohol');
        $tableCantAlcohol->addRow();
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("INGESTAN DE LICOR"), $fontStyleTable1, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantAlcohol->addRow();
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SI"), $fontStyleTable2, $cellHJustify);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText($cantAlcohol[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText($siAlcohol."%", $fontStyleTable2, $cellHCentered);

        $tableCantAlcohol->addRow();
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO"), $fontStyleTable2, $cellHJustify);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText($cantAlcohol[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText($noAlcohol."%", $fontStyleTable2, $cellHCentered);

        $tableCantAlcohol->addRow();
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText($totalPacientesAlcohol, $fontStyleTable2, $cellHCentered);
        $tableCantAlcohol->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeLicor).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------------Distribucion practica deportiva-----------------------

        $pagResultados->addText($this->addTextArial11('Tabla 11. ', 'Distribución por práctica deportiva'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $cantDeportiva = $this->cantidadDeportiva($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalPacientesDepor = $cantDeportiva[0]->rango1 + $cantDeportiva[0]->rango2;
        $siDepor = number_format(($cantDeportiva[0]->rango1/$totalPacientesDepor)*100, '1');
        $noDepor = number_format(($cantDeportiva[0]->rango2/$totalPacientesDepor)*100, '1');
        $porcentajeDeportes = $siDepor + $noDepor;

        $documento->addTableStyle('deportiva', $styleTable);
        $tableCantDepor = $pagResultados->addTable( 'deportiva');
        $tableCantDepor->addRow();
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PRÁCTICA DEPORTIVA"), $fontStyleTable1, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantDepor->addRow();
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SI"), $fontStyleTable2, $cellHJustify);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText($cantDeportiva[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText($siDepor."%", $fontStyleTable2, $cellHCentered);

        $tableCantDepor->addRow();
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO"), $fontStyleTable2, $cellHJustify);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText($cantDeportiva[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText($noDepor."%", $fontStyleTable2, $cellHCentered);

        $tableCantDepor->addRow();
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText($totalPacientesDepor, $fontStyleTable2, $cellHCentered);
        $tableCantDepor->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeDeportes).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------------Distribucion Sedentarismo-----------------------

        $pagResultados->addListItem("DISTRIBUCIÓN POR SEDENTARISMO", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText($this->addTextArial11('Tabla 12. ', 'Distribución por sedentarismo'));
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantSedentarismo = $this->cantidadSedentarismo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalPacientesSedent = $cantSedentarismo[0]->rango1 + $cantSedentarismo[0]->rango2;
        $siSedent = number_format(($cantSedentarismo[0]->rango1/$totalPacientesSedent)*100, '1');
        $noSedent = number_format(($cantSedentarismo[0]->rango2/$totalPacientesSedent)*100, '1');
        $porcentajeSedentarismo = $siSedent + $noSedent;

        $documento->addTableStyle('sedentarismo', $styleTable);
        $tableCantSedent = $pagResultados->addTable( 'sedentarismo');
        $tableCantSedent->addRow();
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SEDENTARISMO"), $fontStyleTable1, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantSedent->addRow();
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SI"), $fontStyleTable2, $cellHJustify);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText($cantSedentarismo[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText($siSedent."%", $fontStyleTable2, $cellHCentered);

        $tableCantSedent->addRow();
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO"), $fontStyleTable2, $cellHJustify);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText($cantSedentarismo[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText($noSedent."%", $fontStyleTable2, $cellHCentered);

        $tableCantSedent->addRow();
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText($totalPacientesSedent, $fontStyleTable2, $cellHCentered);
        $tableCantSedent->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeSedentarismo).'%', $fontStyleTable2, $cellHCentered);

        $pagResultados->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagResultados->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagResultados->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagResultados->addPageBreak();

        //---------------------------------------------------Caracteristicas ocupacionales----------------------------------------------------------

        $pagOcupacionales = $documento->addSection(array('paperSize' => 'Letter','marginLeft' => 1698, 'marginRight' => 1890,
            'marginTop' => 2264, 'marginBottom' => 1698,'pageNumberingStart' => 1));
        $pagOcupacionales->addListItem("CARACTERISTICAS OCUPACIONALES", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("DISTRIBUCIÓN POR CARGOS", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 13. ', 'Distribución por cargos'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantCargos = $this->cantidadCargos($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        $documento->addTableStyle('cargos', $styleTable);
        $tableCantCargos = $pagOcupacionales->addTable( 'cargos');
        $tableCantCargos->addRow();
        $tableCantCargos->addCell(4000, $cellRowSpan)->addText("CARGOS", $fontStyleTable1, $cellHCentered);
        $tableCantCargos->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        for ($i = 0; $i < count($cantCargos); $i++){
            $tableCantCargos->addRow();
            $tableCantCargos->addCell(4000, $cellRowSpan)->addText(strtoupper($cantCargos[$i]->cargoEvaluar), $fontStyleTable2, $cellHJustify);
            $tableCantCargos->addCell(2000, $cellRowSpan)->addText($cantCargos[$i]->cantidad, $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son calculadas sobre el total de la población evaluada y lo referido por cada trabajador"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //----------------Distribucion antiguedad-----------------------

        $pagOcupacionales->addListItem("DISTRIBUCIÓN POR ANTIGÜEDAD EN LA EMPRESA", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 14. ', 'Distribución por antigüedad en la empresa'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantAntiguedad = $this->cantidadAntiguedad($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalAntiguedad = $cantAntiguedad[0]->rango1 + $cantAntiguedad[0]->rango2 + $cantAntiguedad[0]->rango3 + $cantAntiguedad[0]->rango4;
        $menora1 = number_format(($cantAntiguedad[0]->rango1/$totalAntiguedad)*100, '1');
        $unoCinco = number_format(($cantAntiguedad[0]->rango2/$totalAntiguedad)*100, '1');
        $cincoDiez = number_format(($cantAntiguedad[0]->rango3/$totalAntiguedad)*100, '1');
        $mayorDiez = number_format(($cantAntiguedad[0]->rango4/$totalAntiguedad)*100, '1');
        $porcentajeAntiguedad = number_format($menora1 + $unoCinco + $cincoDiez + $mayorDiez);

        $documento->addTableStyle('antiguedad', $styleTable);
        $tableCantEstadoC = $pagOcupacionales->addTable( 'antiguedad');
        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ANTIGÜEDAD"), $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("< 1 año"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantAntiguedad[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($menora1."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("1 - 5 años"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantAntiguedad[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($unoCinco."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("5 - 10 años"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantAntiguedad[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cincoDiez."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("> 10 años"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantAntiguedad[0]->rango4, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($mayorDiez."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($totalAntiguedad, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($porcentajeAntiguedad.'%', $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada como examen periódico"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('GRÁFICA 5. ', 'Distribución por antigüedad'));
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------
        $categoriesAnt = array(htmlspecialchars('< 1 año'), htmlspecialchars('1 - 5 años'), htmlspecialchars('5 - 10 años'), htmlspecialchars('> 10 años'));
        $seriesAnt = array($menora1/100, $unoCinco/100, $cincoDiez/100, $mayorDiez/100);
        $chartAnt = $pagOcupacionales->addChart('column', $categoriesAnt, $seriesAnt, $style);
        $chartAnt->getStyle()->setWidth(Converter::inchToEmu(5))->setHeight(Converter::inchToEmu(3));
        $chartAnt->getStyle()->setColors(array('4F81BD'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        //------------------Fin grafica-----------------------

        //------------------Distribucion factores de riesgo---------------------------
        $pagOcupacionales->addListItem("DISTRIBUCIÓN POR PERCEPCIÓN DE EXPOSICIÓN A FACTORES DE RIESGO", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA INTRODUCCION A LA SECCION DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 15. ', 'Distribución por exposición a factores de riesgo'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantFactorR = $this->cantidadFactoresRiesgo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $fisicos = number_format(($cantFactorR[0]->rango1/$cantidadPacientes)*100);
        $biologicos = number_format(($cantFactorR[0]->rango2/$cantidadPacientes)*100);
        $psicosocial = number_format(($cantFactorR[0]->rango3/$cantidadPacientes)*100);
        $seguridad = number_format(($cantFactorR[0]->rango4/$cantidadPacientes)*100);
        $quimicos = number_format(($cantFactorR[0]->rango5/$cantidadPacientes)*100);
        $ergono = number_format(($cantFactorR[0]->rango6/$cantidadPacientes)*100);

        $documento->addTableStyle('factores cantidad', $styleTable);
        $tableCantEstadoC = $pagOcupacionales->addTable( 'factores cantidad');
        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("FACTOR DE RIESGO"), $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("FISICO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($fisicos."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("BIOLÓGICO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($biologicos."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PSICOSOCIAL"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango3, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($psicosocial."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SEGURIDAD"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango4, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($seguridad."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("QUÍMICO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango5, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($quimicos."%", $fontStyleTable2, $cellHCentered);

        $tableCantEstadoC->addRow();
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ERGONÓMICO"), $fontStyleTable2, $cellHJustify);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($cantFactorR[0]->rango6, $fontStyleTable2, $cellHCentered);
        $tableCantEstadoC->addCell(2000, $cellRowSpan)->addText($ergono."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados sobre el total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText($this->addTextArial11('Tabla 16. ', 'Condiciones de exposición a factores de riesgo'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA LA TABLA 15]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------Distribucion por accidente----------------

        $pagOcupacionales->addListItem("DISTRIBUCION POR ANTECEDENTE DE ACCIDENTE LABORAL (A.L.) Y/O ENFERMEDAD LABORAL (E.L.)", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText("Esta información es subjetiva, depende de la remembranza que tiene cada trabajador, respecto a los accidentes laborales ocurridos durante su vida laboral y reportados ante la A.R.L.; y respecto a las enfermedades laborales calificadas  por la A.R.L.",$fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 17. ', 'Distribución por antecedente de accidente de trabajo'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantAccidentes = $this->cantidadAccidenteTRabajo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalAccidentes = $cantAccidentes[0]->rango1 + $cantAccidentes[0]->rango2;
        $siAccidente = number_format(($cantAccidentes[0]->rango1/$totalAccidentes)*100, '1');
        $noAccidente = number_format(($cantAccidentes[0]->rango2/$totalAccidentes)*100, '1');
        $porcentajeAccidentes = number_format($siAccidente + $noAccidente);

        $documento->addTableStyle('accidentes', $styleTable);
        $tableCantAccidente = $pagOcupacionales->addTable( 'accidentes');
        $tableCantAccidente->addRow();
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ACCIDENTES DE TRABAJO"), $fontStyleTable1, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantAccidente->addRow();
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SI"), $fontStyleTable2, $cellHJustify);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($cantAccidentes[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($siAccidente."%", $fontStyleTable2, $cellHCentered);

        $tableCantAccidente->addRow();
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO"), $fontStyleTable2, $cellHJustify);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($cantAccidentes[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($noAccidente."%", $fontStyleTable2, $cellHCentered);

        $tableCantAccidente->addRow();
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($totalAccidentes, $fontStyleTable2, $cellHCentered);
        $tableCantAccidente->addCell(2000, $cellRowSpan)->addText($porcentajeAccidentes.'%', $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados de lo referido por la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------Distribucion por accidente - accidentes----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 18. ', 'Detalles de antecedente de accidente de trabajo'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $accidentesTrabajo = $this->accidentesTrabajo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $documento->addTableStyle('accidentes trabajo', $styleTable);

        $tableAccidentes = $pagOcupacionales->addTable( 'accidentes trabajo');
        $tableAccidentes->addRow();
        $tableAccidentes->addCell(1500, $cellRowSpan)->addText(htmlspecialchars("FECHA"), $fontStyleTable1, $cellHCentered);
        $tableAccidentes->addCell(3000, $cellRowSpan)->addText("TIPO DE LESIÓN", $fontStyleTable1, $cellHCentered);
        $tableAccidentes->addCell(3000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        for ($i = 0; $i < count($accidentesTrabajo); $i++){
            $tableAccidentes->addRow();
            $tableAccidentes->addCell(1500, $cellRowSpan)->addText(htmlspecialchars($accidentesTrabajo[$i]->fecha), $fontStyleTable2, $cellHCentered);
            $tableAccidentes->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($accidentesTrabajo[$i]->lesion), $fontStyleTable2, $cellHJustify);
            $tableAccidentes->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($accidentesTrabajo[$i]->cargoEvaluar), $fontStyleTable2, $cellHJustify);
        }

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Referido por la población evaluada que manifestó antecedente de accidente de trabajo"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------Distribucion por enfermedad----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 19. ', 'Distribución por antecedente de enfermedad laboral'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $cantEnfermedades = $this->cantidadEnfermedadTrabajo($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $siEnfermedad = number_format(($cantEnfermedades[0]->rango1/$cantidadPacientes)*100);
        $noEnfermedad = number_format(($cantEnfermedades[0]->rango2/$cantidadPacientes)*100);

        $documento->addTableStyle('enfermedades cantidad', $styleTable);
        $tableCantEnferm = $pagOcupacionales->addTable( 'enfermedades cantidad');
        $tableCantEnferm->addRow();
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ENFERMEDAD LABORAL"), $fontStyleTable1, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableCantEnferm->addRow();
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("SI"), $fontStyleTable2, $cellHJustify);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText($cantEnfermedades[0]->rango1, $fontStyleTable2, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText($siEnfermedad."%", $fontStyleTable2, $cellHCentered);

        $tableCantEnferm->addRow();
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("NO"), $fontStyleTable2, $cellHJustify);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText($cantEnfermedades[0]->rango2, $fontStyleTable2, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText($noEnfermedad."%", $fontStyleTable2, $cellHCentered);

        $tableCantEnferm->addRow();
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("TOTAL:"), $fontStyleTable1, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText($cantidadPacientes, $fontStyleTable2, $cellHCentered);
        $tableCantEnferm->addCell(2000, $cellRowSpan)->addText('100%', $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados de lo referido por la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------Distribucion por enfermedad - enfermedades----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 20. ', 'Detalles de antecedente de enfermedad laboral'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $enfermedadesLaborales = $this->enfermedadesLaborales($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $documento->addTableStyle('enfermedades', $styleTable);

        $tableEnfermedades = $pagOcupacionales->addTable( 'enfermedades');
        $tableEnfermedades->addRow();
        $tableEnfermedades->addCell(1500, $cellRowSpan)->addText(htmlspecialchars("FECHA"), $fontStyleTable1, $cellHCentered);
        $tableEnfermedades->addCell(3000, $cellRowSpan)->addText("DIAGNOSTICO", $fontStyleTable1, $cellHCentered);
        $tableEnfermedades->addCell(3000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        for ($i = 0; $i < count($enfermedadesLaborales); $i++){
            $tableEnfermedades->addRow();
            $tableEnfermedades->addCell(1500, $cellRowSpan)->addText(htmlspecialchars($enfermedadesLaborales[$i]->fecha), $fontStyleTable2, $cellHCentered);
            $tableEnfermedades->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($enfermedadesLaborales[$i]->diagnostico), $fontStyleTable2, $cellHJustify);
            $tableEnfermedades->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($enfermedadesLaborales[$i]->cargoEvaluar), $fontStyleTable2, $cellHJustify);
        }

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Referido por la población evaluada que manifestó antecedente de enfermedad laboral"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addListItem("RESULTADOS DE PRUEBAS DE LABORATORIO Y COMPLEMENTARIAS", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("A continuación se muestran resultados de los exámenes de laboratorio clínico, y los resultados de las pruebas complementarias de optometría, audiometría, prueba vestibular, evaluación funcional de la voz (foniatría), y prueba psicosensométrica."), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addListItem("DISTRIBUCION RESULTADOS PRUEBAS DE LABORATORIO", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Examenes laboratorio rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 21. ', 'Distribución resultado pruebas de laboratorio'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $examLabRango = $this->pruebasLaboratorio($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $documento->addTableStyle('ExamenesLabRango', $styleTable);
        $tableExamLabRango = $pagOcupacionales->addTable( 'ExamenesLabRango');
        $tableExamLabRango->addRow();
        $tableExamLabRango->addCell(7000, $cellRowSpan)->addText(htmlspecialchars("EXAMENES DE LABORATORIO"), $fontStyleTable1, $cellHCentered);
        $tableExamLabRango->addCell(2500, $cellRowSpan)->addText("DENTRO DEL RANGO NORMAL", $fontStyleTable1, $cellHCentered);
        $tableExamLabRango->addCell(2500, $cellRowSpan)->addText("FUERA DEL RANGO NORMAL", $fontStyleTable1, $cellHCentered);
        $tableExamLabRango->addCell(3000, $cellRowSpan)->addText("TOTAL PRUEBAS REALIZADAS", $fontStyleTable1, $cellHCentered);
//        dd($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
       // dd($examLabRango);
        if (count($examLabRango) == 0){
            $tableExamLabRango->addRow();
            $tableExamLabRango->addCell(5000, $cellRowSpan)->addText("", $fontStyleTable2, $cellHJustify);
            $tableExamLabRango->addCell(3000, $cellRowSpan)->addText(0, $fontStyleTable2, $cellHCentered);
            $tableExamLabRango->addCell(2800, $cellRowSpan)->addText(0, $fontStyleTable2, $cellHCentered);
            $tableExamLabRango->addCell(3000, $cellRowSpan)->addText(0, $fontStyleTable2, $cellHCentered);
        } else{
            for ($i = 0; $i < count($examLabRango); $i++){
                $tableExamLabRango->addRow();
                $tableExamLabRango->addCell(5000, $cellRowSpan)->addText(htmlspecialchars($examLabRango[$i]->descripcion), $fontStyleTable2, $cellHJustify);
                $tableExamLabRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($examLabRango[$i]->Normal), $fontStyleTable2, $cellHCentered);
                $tableExamLabRango->addCell(2800, $cellRowSpan)->addText(htmlspecialchars($examLabRango[$i]->Anormal), $fontStyleTable2, $cellHCentered);
                $tableExamLabRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($examLabRango[$i]->Normal + $examLabRango[$i]->Anormal), $fontStyleTable2, $cellHCentered);
            }
        }

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son calculadas del total de la población evaluada para cada prueba de laboratorio"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addListItem("DISTRIBUCION RESULTADOS PRUEBAS COMPLEMENTARIAS", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 22. ', 'Distribución resultado optometría'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        $complementariosRango = $this->pruebasComplementariasRango($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $totalOptometria = $complementariosRango[0]->optoAnormal+ $complementariosRango[0]->optoNormal;

        if ($totalOptometria == 0){
            $totalPOptometria = 0;
            $porcentajeOptoNormal = 0;
            $porcentajeOptoAnormal = 0;
        }else{
            $porcentajeOptoAnormal = number_format(($complementariosRango[0]->optoAnormal/$totalOptometria)*100, '1');
            $porcentajeOptoNormal = number_format(($complementariosRango[0]->optoNormal/$totalOptometria)*100, '1');
            $totalPOptometria = number_format($porcentajeOptoNormal + $porcentajeOptoAnormal);
        }

        $documento->addTableStyle('optometriaRango', $styleTable);
        $tableOptometriaRango = $pagOcupacionales->addTable( 'optometriaRango');
        $tableOptometriaRango->addRow();
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("OPTOMETRÍA"), $fontStyleTable1, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableOptometriaRango->addRow();
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->optoNormal, $fontStyleTable2, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeOptoNormal."%", $fontStyleTable2, $cellHCentered);

        $tableOptometriaRango->addRow();
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->optoAnormal, $fontStyleTable2, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeOptoAnormal."%", $fontStyleTable2, $cellHCentered);

        $tableOptometriaRango->addRow();
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($totalOptometria, $fontStyleTable2, $cellHCentered);
        $tableOptometriaRango->addCell(2000, $cellRowSpan)->addText($totalPOptometria."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en optometría"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Audiometria rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 23. ', 'Distribución resultado audiometría'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $totalAudiometria = $complementariosRango[0]->audioAnormal + $complementariosRango[0]->audioNormal;
        if ($totalAudiometria == 0){
            $totalPAudiometria = 0;
            $porcentajeAudioNormal = 0;
            $porcentajeAudioAnormal = 0;
        }else{
            $porcentajeAudioNormal = number_format(($complementariosRango[0]->audioNormal/$totalAudiometria)*100, '1');
            $porcentajeAudioAnormal = number_format(($complementariosRango[0]->audioAnormal/$totalAudiometria)*100, '1');
            $totalPAudiometria = number_format($porcentajeAudioNormal + $porcentajeAudioAnormal);
        }

        $documento->addTableStyle('audiometriaRango', $styleTable);
        $tableAudiometriaRango = $pagOcupacionales->addTable( 'audiometriaRango');
        $tableAudiometriaRango->addRow();
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("AUDIOMETRÍA"), $fontStyleTable1, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableAudiometriaRango->addRow();
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->audioNormal, $fontStyleTable2, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeAudioNormal."%", $fontStyleTable2, $cellHCentered);

        $tableAudiometriaRango->addRow();
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->audioAnormal, $fontStyleTable2, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeAudioAnormal."%", $fontStyleTable2, $cellHCentered);

        $tableAudiometriaRango->addRow();
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($totalAudiometria, $fontStyleTable2, $cellHCentered);
        $tableAudiometriaRango->addCell(2000, $cellRowSpan)->addText($totalPAudiometria."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en audiometría"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Visiometria rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 24. ', 'Distribución resultado visiometría'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $totalVisiometria = $complementariosRango[0]->visioAnormal + $complementariosRango[0]->visioNormal;
        if ($totalVisiometria == 0){
            $totalPVisiometria = 0;
        }else{
            $porcentajeVicioNormal = number_format(($complementariosRango[0]->visioNormal/$totalVisiometria)*100, '1');
            $porcentajeVicioAnormal = number_format(($complementariosRango[0]->visioAnormal/$totalVisiometria)*100, '1');
            $totalPVisiometria = number_format($porcentajeVicioNormal + $porcentajeVicioAnormal);
        }

        $documento->addTableStyle('visiometriaRango', $styleTable);
        $tablevisiometriaRango = $pagOcupacionales->addTable( 'visiometriaRango');
        $tablevisiometriaRango->addRow();
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("VISIOMETRÍA"), $fontStyleTable1, $cellHCentered);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tablevisiometriaRango->addRow();
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->visioNormal, $fontStyleTable2, $cellHCentered);
        if ($totalVisiometria == 0){
            $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("0%", $fontStyleTable2, $cellHCentered);
        }else{
            $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeVicioNormal."%", $fontStyleTable2, $cellHCentered);
        }

        $tablevisiometriaRango->addRow();
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->visioAnormal, $fontStyleTable2, $cellHCentered);

        if ($totalVisiometria == 0){
            $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("0%", $fontStyleTable2, $cellHCentered);
        }else{
            $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($porcentajeVicioAnormal."%", $fontStyleTable2, $cellHCentered);
        }
        $tablevisiometriaRango->addRow();
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($totalVisiometria, $fontStyleTable2, $cellHCentered);
        $tablevisiometriaRango->addCell(2000, $cellRowSpan)->addText($totalPVisiometria."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en visiometría"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Espirometria rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 25. ', 'Distribución resultado espirometría'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $totalCEspiro = $complementariosRango[0]->espiroPM + $complementariosRango[0]->espiroPO + $complementariosRango[0]->espiroPR + $complementariosRango[0]->espiroNormal;

        if($totalCEspiro == 0){
            $totalPEspiro = 0;
            $porcentajeEspiroNormal = 0;
            $porcentajeEspiroPR = 0;
            $porcentajeEspiroPO = 0;
            $porcentajeEspiroPM = 0;
        }else{
            $porcentajeEspiroNormal = ($complementariosRango[0]->espiroNormal/$totalCEspiro)*100;
            $porcentajeEspiroPR = ($complementariosRango[0]->espiroPR/$totalCEspiro)*100;
            $porcentajeEspiroPO = ($complementariosRango[0]->espiroPO/$totalCEspiro)*100;
            $porcentajeEspiroPM = ($complementariosRango[0]->espiroPM/$totalCEspiro)*100;
            $totalPEspiro = number_format($porcentajeEspiroPR+ ($porcentajeEspiroPO) + ($porcentajeEspiroPM) + ($porcentajeEspiroNormal));
        }

        $documento->addTableStyle('espirometriaRango', $styleTable);
        $tableespirometriaRango = $pagOcupacionales->addTable( 'espirometriaRango');
        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("ESPIROMETRÍA"), $fontStyleTable1, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->espiroNormal, $fontStyleTable2, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEspiroNormal)."%", $fontStyleTable2, $cellHCentered);

        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("PÁTRON RESTRICTIVO", $fontStyleTable1, $cellHJustify);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->espiroPR, $fontStyleTable2, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEspiroPR)."%", $fontStyleTable2, $cellHCentered);

        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("PÁTRON OBSTRUCTIVO", $fontStyleTable1, $cellHJustify);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->espiroPO, $fontStyleTable2, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEspiroPO)."%", $fontStyleTable2, $cellHCentered);

        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("PÁTRON MIXTO", $fontStyleTable1, $cellHJustify);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->espiroPM, $fontStyleTable2, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeEspiroPM)."%", $fontStyleTable2, $cellHCentered);

        $tableespirometriaRango->addRow();
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($totalCEspiro, $fontStyleTable2, $cellHCentered);
        $tableespirometriaRango->addCell(2000, $cellRowSpan)->addText($totalPEspiro."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en espirometría"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Psicologico rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 26. ', 'Distribución resultado psicológico'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $totalPsicologico = $complementariosRango[0]->psicoNormal + $complementariosRango[0]->psicoAnormal;
        if ($totalPsicologico == 0){
            $porcentajePsicologico = 0;
            $porcentajePsicoNormal = 0;
            $porcentajePsicoAnormal = 0;
        }else {
            $porcentajePsicoNormal = ($complementariosRango[0]->psicoNormal/$totalPsicologico)*100;
            $porcentajePsicoAnormal = ($complementariosRango[0]->psicoAnormal/$totalPsicologico)*100;
            $porcentajePsicologico = number_format($porcentajePsicoNormal + $porcentajePsicoAnormal);
        }

        $documento->addTableStyle('psicologicoRango', $styleTable);
        $tablepsicologicoRango = $pagOcupacionales->addTable( 'psicologicoRango');
        $tablepsicologicoRango->addRow();
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PSICOLÓGICO"), $fontStyleTable1, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tablepsicologicoRango->addRow();
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->psicoNormal, $fontStyleTable2, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePsicoNormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepsicologicoRango->addRow();
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->psicoAnormal, $fontStyleTable2, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePsicoAnormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepsicologicoRango->addRow();
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText($totalPsicologico, $fontStyleTable2, $cellHCentered);
        $tablepsicologicoRango->addCell(2000, $cellRowSpan)->addText($porcentajePsicologico."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en psicológico"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- Psicometricos rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 27. ', 'Distribución resultado psicométrico'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $totalPsicometrico = $complementariosRango[0]->psicomeNormal + $complementariosRango[0]->psicomeAnormal;
        if ($totalPsicometrico == 0){
            $porcentajePsicometrico = 0;
            $porcentajePsicomNormal = 0;
            $porcentajePsicomAnormal = 0;
        }else{
            $porcentajePsicomNormal = ($complementariosRango[0]->psicomeNormal/$totalPsicometrico)*100;
            $porcentajePsicomAnormal = ($complementariosRango[0]->psicomeAnormal/$totalPsicometrico)*100;
            $porcentajePsicometrico = number_format($porcentajePsicomNormal + $porcentajePsicomAnormal);
        }

        $documento->addTableStyle('psicometricoRango', $styleTable);
        $tablepsicometricoRango = $pagOcupacionales->addTable( 'psicometricoRango');
        $tablepsicometricoRango->addRow();
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PSICOMÉTRICO"), $fontStyleTable1, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tablepsicometricoRango->addRow();
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->psicomeNormal, $fontStyleTable2, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePsicomNormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepsicometricoRango->addRow();
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->psicomeAnormal, $fontStyleTable2, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePsicomAnormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepsicometricoRango->addRow();
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText($totalPsicometrico, $fontStyleTable2, $cellHCentered);
        $tablepsicometricoRango->addCell(2000, $cellRowSpan)->addText($porcentajePsicometrico."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en psicométrico"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //----------- pruebaVestibulars rango------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 28. ', 'Distribución resultado prueba vestibular'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $totalPruebaVest = $complementariosRango[0]->vestibularNormal + $complementariosRango[0]->vestibularAnormal;
        if($totalPruebaVest == 0){
            $porcentajePruebaVest = 0;
            $porcentajePruebaVestNormal = 0;
            $porcentajePruebaVestAnormal = 0;
        }else{
            $porcentajePruebaVestAnormal = ($complementariosRango[0]->vestibularAnormal/$totalPruebaVest)*100;
            $porcentajePruebaVestNormal = ($complementariosRango[0]->vestibularNormal/$totalPruebaVest)*100;
            $porcentajePruebaVest = number_format($porcentajePruebaVestAnormal + $porcentajePruebaVestNormal);
        }

        $documento->addTableStyle('pruebaVestibularRango', $styleTable);
        $tablepruebaVestibularRango = $pagOcupacionales->addTable( 'pruebaVestibularRango');
        $tablepruebaVestibularRango->addRow();
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText(htmlspecialchars("PRUEBA VESTIBULAR"), $fontStyleTable1, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tablepruebaVestibularRango->addRow();
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHJustify);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->vestibularNormal, $fontStyleTable2, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePruebaVestNormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepruebaVestibularRango->addRow();
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText("ANORMAL", $fontStyleTable1, $cellHJustify);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText($complementariosRango[0]->vestibularAnormal, $fontStyleTable2, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajePruebaVestAnormal)."%", $fontStyleTable2, $cellHCentered);

        $tablepruebaVestibularRango->addRow();
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText("TOTAL", $fontStyleTable1, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText($totalPruebaVest, $fontStyleTable2, $cellHCentered);
        $tablepruebaVestibularRango->addCell(2000, $cellRowSpan)->addText($porcentajePruebaVest."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada en prueba vestibular"), $fsPieTable, 'PSCentrado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-----------------VARIABLES DE SALUD--------------------

        $pagOcupacionales->addListItem("VARIABLES DE SALUD", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addListItem("DISTRIBUCIÓN HALLAZGOS EN LA PRESIÓN ARTERIAL", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Durante el examen clínico ocupacional, se les midió la presión arterial a los ".$cantidadPacientes." trabajadores; se tuvo en cuenta los parámetros del Joint National Comittee (JNC VII)."), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText($this->addTextArial11('Tabla 29. ', 'Distribución hallazgo de la medición de la Presión Arterial'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $distribucionPresionArt = $this->distribucionPresionArterial($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $TANormal = $distribucionPresionArt[0]->normal;
        $TAPrehip = $distribucionPresionArt[0]->prehipertension;
        $TAEst1 = $distribucionPresionArt[0]->htaEst1;
        $TAEst2 = $distribucionPresionArt[0]->htaEst2;
        $totalPresionArt = $TANormal + $TAPrehip + $TAEst1 + $TAEst2;
        $porcentajePresionArt = ($TANormal/$totalPresionArt)*100 + ($TAPrehip/$totalPresionArt)*100 + ($TAEst1/$totalPresionArt)*100 + ($TAEst2/$totalPresionArt)*100;

        $documento->addTableStyle('presionArterialRango', $styleTable);
        $tablepresionArterialRango = $pagOcupacionales->addTable( 'presionArterialRango');
        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("MEDICIÓN PRESIÓN ARTERIAL"), $fontStyleTable1, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("NORMAL"), $fontStyleTable1, $cellHJustify);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText($TANormal, $fontStyleTable2, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText(number_format(($TANormal/$totalPresionArt)*100)."%", $fontStyleTable2, $cellHCentered);

        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("PREHIPERTENSIÓN"), $fontStyleTable1, $cellHJustify);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText($TAPrehip, $fontStyleTable2, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText(number_format(($TAPrehip/$totalPresionArt)*100)."%", $fontStyleTable2, $cellHCentered);

        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("HTA ESTADIO 1"), $fontStyleTable1, $cellHJustify);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText($TAEst1, $fontStyleTable2, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText(number_format(($TAEst1/$totalPresionArt)*100)."%", $fontStyleTable2, $cellHCentered);

        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("HTA ESTADIO 2"), $fontStyleTable1, $cellHJustify);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText($TAEst2, $fontStyleTable2, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText(number_format(($TAEst2/$totalPresionArt)*100)."%", $fontStyleTable2, $cellHCentered);

        $tablepresionArterialRango->addRow();
        $tablepresionArterialRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("TOTAL"), $fontStyleTable1, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText($totalPresionArt, $fontStyleTable2, $cellHCentered);
        $tablepresionArterialRango->addCell(2000, $cellRowSpan)->addText(number_format(($porcentajePresionArt))."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------------INDICE DE MASA CORPORAL-------------

        $pagOcupacionales->addListItem("DISTRIBUCIÓN HALLAZGOS INDICE DE MASA CORPORAL", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 30. ', 'Distribución hallazgos Índice de masa corporal (IMC)'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $distribucionIMC = $this->distribucionIMC($fechaDesde, $fechaHasta, $empresa_id, $motivoE);


        $totalBP = $distribucionIMC[0]->bajoPeso;
        $totalNormal = $distribucionIMC[0]->normal;
        $totalSP = $distribucionIMC[0]->sobrePeso;
        $totalObesi = $distribucionIMC[0]->obesidad;
        $totalCIMC = $totalBP + $totalNormal + $totalSP + $totalObesi;

        $porcentajeBP = ($distribucionIMC[0]->bajoPeso/$totalCIMC)*100;
        $porcentajeNormal = ($distribucionIMC[0]->normal/$totalCIMC)*100;
        $porcentajeSP = ($distribucionIMC[0]->sobrePeso/$totalCIMC)*100;
        $porcentajeObesi = ($distribucionIMC[0]->obesidad/$totalCIMC)*100;
        $totalPIMC = number_format($porcentajeBP + $porcentajeNormal + $porcentajeSP + $porcentajeObesi);

        $documento->addTableStyle('imcRango', $styleTable);
        $tableimcRango = $pagOcupacionales->addTable( 'imcRango');
        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("INDICE DE MASA CORPORAL"), $fontStyleTable1, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText("PORCENTAJE", $fontStyleTable1, $cellHCentered);

        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("BAJO PESO"), $fontStyleTable1, $cellHJustify);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalBP, $fontStyleTable2, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeBP)."%", $fontStyleTable2, $cellHCentered);

        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("NORMAL"), $fontStyleTable1, $cellHJustify);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalNormal, $fontStyleTable2, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeNormal)."%", $fontStyleTable2, $cellHCentered);

        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("SOBREPESO"), $fontStyleTable1, $cellHJustify);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalSP, $fontStyleTable2, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeSP)."%", $fontStyleTable2, $cellHCentered);

        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("OBESIDAD"), $fontStyleTable1, $cellHJustify);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalObesi, $fontStyleTable2, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText(number_format($porcentajeObesi)."%", $fontStyleTable2, $cellHCentered);

        $tableimcRango->addRow();
        $tableimcRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("TOTAL"), $fontStyleTable1, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalCIMC, $fontStyleTable2, $cellHCentered);
        $tableimcRango->addCell(2000, $cellRowSpan)->addText($totalPIMC."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText($this->addTextArial11('GRÁFICA 6. ', 'Distribución hallazgos índice de masa corporal (IMC)'));
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //------------------Grafica---------------------------

        $categories = array('BAJO PESO', 'NORMAL', 'SOBREPESO', 'OBESIDAD');
        $series = array(number_format($porcentajeBP/100,1), number_format($porcentajeNormal/100,1), number_format($porcentajeSP/100,1), number_format($porcentajeObesi/100,1));
        $style = array('showAxisLabels' => true, 'gridX' => true);
        $chart = $pagOcupacionales->addChart('column', $categories, $series, $style);
        $chart->getStyle()->setWidth(Converter::inchToEmu(3.5))->setHeight(Converter::inchToEmu(3));
        $chart->getStyle()->setColors(array('4F81BD','4F81BD'))->setDataLabelOptions(array('showPercent' => true, 'showCatName' => false));
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-----------------Examen osteomuscular------------------

        $pagOcupacionales->addListItem("DISTRIBUCIÓN HALLAZGOS EXAMEN OSTEOMUSCULAR", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 31. ', 'Distribución hallazgos examen osteomuscular por segmento anatómico y área.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $distribucionOsteomuscular = $this->distribucionExamenOsteomuscular($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        $osteoColumnaNormal = $distribucionOsteomuscular[0]->columnaNormal;
        $osteoColumnaAnormal = $distribucionOsteomuscular[0]->columnaAnormal;
        $osteoExtInfNormal = $distribucionOsteomuscular[0]->extremidadInfNormal;
        $osteoExtInfAnormal = $distribucionOsteomuscular[0]->extremidadInfAnormal;
        $osteoExtSupNormal = $distribucionOsteomuscular[0]->extremidadSupNormal;
        $osteoExtSupAnormal = $distribucionOsteomuscular[0]->extremidadSupAnormal;

        $osteoColumnaTotal = $osteoColumnaNormal + $osteoColumnaAnormal;
        $osteoExtInfTotal = $osteoExtInfNormal + $osteoExtInfAnormal;
        $osteoExtSupTotal = $osteoExtSupNormal + $osteoExtSupAnormal;

        if ($osteoColumnaTotal == 0){
            $osteoColumnaNormalPorc = 0;
            $osteoColumnaAnormalPor = 0;
        }else{
            $osteoColumnaNormalPorc = number_format(($distribucionOsteomuscular[0]->columnaNormal/($osteoColumnaNormal+$osteoColumnaAnormal))*100);
            $osteoColumnaAnormalPor = number_format(($distribucionOsteomuscular[0]->columnaAnormal/($osteoColumnaNormal+$osteoColumnaAnormal))*100);

        }

        if($osteoExtInfTotal){
            $osteoExtInfNormalPor = 0;
            $osteoExtInfAnormalPor = 0;

        }else{
            $osteoExtInfNormalPor = number_format(($distribucionOsteomuscular[0]->extremidadInfNormal/($osteoExtInfNormal+$osteoExtInfAnormal))*100);
            $osteoExtInfAnormalPor = number_format(($distribucionOsteomuscular[0]->extremidadInfAnormal/($osteoExtInfNormal+$osteoExtInfAnormal))*100);
        }

        if($osteoExtSupTotal == 0){
            $osteoExtSupNormalPor = 0;
            $osteoExtSupAnormalPor = 0;
        }else{
            $osteoExtSupNormalPor = number_format(($distribucionOsteomuscular[0]->extremidadSupNormal/($osteoExtSupNormal+$osteoExtSupAnormal))*100);
            $osteoExtSupAnormalPor = number_format(($distribucionOsteomuscular[0]->extremidadSupAnormal/($osteoExtSupNormal+$osteoExtSupAnormal))*100);
        }

        $documento->addTableStyle('osteomuscularRango', $styleTable);
        $tableosteomuscularRango = $pagOcupacionales->addTable( 'osteomuscularRango');
        $tableosteomuscularRango->addRow();
        $tableosteomuscularRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("EXAMEN OSTEOMUSCULAR"), $fontStyleTable1, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText("NORMAL", $fontStyleTable1, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText("ALTERADO", $fontStyleTable1, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText("% NORMAL", $fontStyleTable1, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText("% ANORMAL", $fontStyleTable1, $cellHCentered);

        $tableosteomuscularRango->addRow();
        $tableosteomuscularRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("COLUMNA VERTEBRAL"), $fontStyleTable2, $cellHJustify);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoColumnaNormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoColumnaAnormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoColumnaNormalPorc."%", $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoColumnaAnormalPor."%", $fontStyleTable2, $cellHCentered);

        $tableosteomuscularRango->addRow();
        $tableosteomuscularRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("MIEMBROS SUPERIORES"), $fontStyleTable2, $cellHJustify);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtSupNormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtSupAnormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtSupNormalPor."%", $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtSupAnormalPor."%", $fontStyleTable2, $cellHCentered);

        $tableosteomuscularRango->addRow();
        $tableosteomuscularRango->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("MIEMBROS INFERIORES"), $fontStyleTable2, $cellHJustify);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtInfNormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtInfAnormal, $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtInfNormalPor."%", $fontStyleTable2, $cellHCentered);
        $tableosteomuscularRango->addCell(2000, $cellRowSpan)->addText($osteoExtInfAnormalPor."%", $fontStyleTable2, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Los porcentajes son calculados del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL ANALISIS DE LOS RESULTADOS]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //-------------------DISTRIBUCIÓN DE PATOLOGIAS ENCONTRADAS POR SISTEMAS---------------------

        $pagOcupacionales->addListItem("DISTRIBUCIÓN DE PATOLOGIAS ENCONTRADAS POR SISTEMAS", 2, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Para determinar los diagnósticos de los trabajadores, el médico evaluador tuvo en cuenta, los antecedentes patológicos referidos por ellos, los hallazgos clínicos encontrados al examen físico, los síntomas mencionados en la revisión por sistemas, los resultados de las pruebas de laboratorio y pruebas complementarias. Un mismo trabajador puede presentar más de un diagnóstico en un mismo sistema. Se distribuirán las patologías o diagnósticos por sistemas y se analizarán las patologías más frecuentes."), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText($this->addTextArial11('Tabla 32. ', 'Distribución frecuencia de patología por sistemas.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA TEXTO]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $this->crearTablaPatologias($documento, $pagOcupacionales, $styleTable, $fechaDesde, $fechaHasta, $empresa_id, $motivoE,
            $fontStyleTable1, $cellHCentered, $fontStyleTable2, $cellHJustify, $cellRowSpan);
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son determinadas por el total de patologías encontradas en las personas evaluadas."), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');


        $pagOcupacionales->addPageBreak();

        //----------------------Conclusiones-------------------------

        $pagOcupacionales->addTextBreak(4, $fontStyle6);
        $pagOcupacionales->addListItem("CONSLUSIONES", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VAN LAS CONCLUSIONES]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addPageBreak();

        //----------------------Recomendaciones-------------------------

        $pagOcupacionales->addTextBreak(4, $fontStyle6);
        $pagOcupacionales->addListItem("RECOMENDACIONES", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("RECOMENDACIONES GENERALES", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //---------------RECOMENDAICONES INDIVIDUALES---------------

        $pagOcupacionales->addListItem("RECOMENDACIONES INDIVIDUALES", 1, $fontStyle6, 'multilevel-general','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL TEXTO]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $pagOcupacionales->addText($this->addTextArial11('Tabla 33. ', 'Distribución frecuencia de recomendaciones médicas.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $recomendacionesMedicas = $this->pacientesRecomendacionMedica($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        $documento->addTableStyle('recomendacionMedica', $styleTable);
        $tablerecomendacionMedica = $pagOcupacionales->addTable( 'recomendacionMedica');

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("RECOMENDACIÓN MÉDICA"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Remisión a EPS", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->remisionEPS), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Continuar manejo médico", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->manejoMedico), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Remisión ARL", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->remisionARL), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Seguimiento caso por ARL", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->seguimientoARL), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Citología cervico - Vaginal", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->citlogia), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Tamizaje prostático", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->tamizaje), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Esquema de Vacunación (Adulto)", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->esquemaV), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Control Audiologico periódico", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->controlAudio), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Control Anual optométrico", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->controlOpto), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3000, $cellRowSpan)->addText("Control odontológico periódico", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesMedicas[0]->controlOdont), $fontStyleTable2, $cellHCentered);

        $totalRecomendacionMedica = $recomendacionesMedicas[0]->controlOdont + $recomendacionesMedicas[0]->controlOpto + $recomendacionesMedicas[0]->controlAudio + $recomendacionesMedicas[0]->esquemaV +
            $recomendacionesMedicas[0]->tamizaje + $recomendacionesMedicas[0]->citlogia + $recomendacionesMedicas[0]->seguimientoARL + $recomendacionesMedicas[0]->remisionARL +
            $recomendacionesMedicas[0]->manejoMedico + $recomendacionesMedicas[0]->remisionEPS;

        $tablerecomendacionMedica->addRow();
        $tablerecomendacionMedica->addCell(3500, $cellRowSpan)->addText(htmlspecialchars(" TOTAL"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionMedica->addCell(2000, $cellRowSpan)->addText($totalRecomendacionMedica, $fontStyleTable1, $cellHCentered);

        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son calculadas del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        //---------------Recomendaciones ocupacionales-----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 34. ', 'Distribución frecuencia de recomendaciones ocupacionales.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $recomendacionesOcupacionales = $this->pacientesRecomendacionOcupacional($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        $documento->addTableStyle('recomendacionOcupacional', $styleTable);
        $tablerecomendacionOcupacional = $pagOcupacionales->addTable( 'recomendacionOcupacional');

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("RECOMENDACIÓN OCUPACIONAL"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Uso de EPP", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->usoEPP), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Higiene Postural", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->higienePost), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Pausas Activas", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->pausasActivas), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Distribucion de fuerzas y cargas", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->distFuerzas), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Pausas activas musculo esqueléticas", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->pausasActivasMusc), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3000, $cellRowSpan)->addText("Pausas activas visuales", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesOcupacionales[0]->pausasActivasVisuales), $fontStyleTable2, $cellHCentered);

        $totalRecomendacionOcupacional = $recomendacionesOcupacionales[0]->usoEPP + $recomendacionesOcupacionales[0]->higienePost + $recomendacionesOcupacionales[0]->pausasActivas + $recomendacionesOcupacionales[0]->distFuerzas +
            $recomendacionesOcupacionales[0]->pausasActivasMusc + $recomendacionesOcupacionales[0]->pausasActivasVisuales ;

        $tablerecomendacionOcupacional->addRow();
        $tablerecomendacionOcupacional->addCell(3500, $cellRowSpan)->addText(htmlspecialchars(" TOTAL"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionOcupacional->addCell(2000, $cellRowSpan)->addText($totalRecomendacionOcupacional, $fontStyleTable1, $cellHCentered);
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son calculadas del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        //---------------Recomendaciones HABITOS-----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 35. ', 'Distribución frecuencia de recomendaciones en hábitos y estilos de vida saludable.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $recomendacionesHabitoses = $this->pacientesRecomendacionHabitos($fechaDesde, $fechaHasta, $empresa_id, $motivoE);

        $documento->addTableStyle('recomendacionHabitos', $styleTable);
        $tablerecomendacionHabitos = $pagOcupacionales->addTable( 'recomendacionHabitos');

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("RECOMENDACIÓN OCUPACIONAL"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText("NÚMERO", $fontStyleTable1, $cellHCentered);

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText("Inicio de actividad Física", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesHabitoses[0]->inicioAF), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText("Dejar de Fumar", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesHabitoses[0]->dejarF), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText("Reducir Consumo de Alcohol", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesHabitoses[0]->reducirA), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText("Control Nutricional y de Peso", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesHabitoses[0]->controlNutri), $fontStyleTable2, $cellHCentered);

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3000, $cellRowSpan)->addText("Continuar con actividad física", $fontStyleTable2, $cellHJustify);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($recomendacionesHabitoses[0]->continuarAF), $fontStyleTable2, $cellHCentered);

        $totalRecomendacionHabitos = $recomendacionesHabitoses[0]->inicioAF + $recomendacionesHabitoses[0]->dejarF + $recomendacionesHabitoses[0]->reducirA
            + $recomendacionesHabitoses[0]->controlNutri + $recomendacionesHabitoses[0]->continuarAF ;

        $tablerecomendacionHabitos->addRow();
        $tablerecomendacionHabitos->addCell(3500, $cellRowSpan)->addText(htmlspecialchars(" TOTAL"), $fontStyleTable1, $cellHCentered);
        $tablerecomendacionHabitos->addCell(2000, $cellRowSpan)->addText($totalRecomendacionHabitos, $fontStyleTable1, $cellHCentered);
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("Las frecuencias son calculadas del total de la población evaluada"), $fsPieTable, 'PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');

        //---------------SISTEMAS DE VIGILANCIA---------------

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("TRABAJADORES PARA SISTEMAS DE VIGILANCIA ", 1, $fontStyle6, 'multilevel-general','PSJustificado');

        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addText(htmlspecialchars("[AQUI VA EL TEXTO]"), $fontStyle5, 'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        //---------------SVE Ergonomico----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 36. ', 'Pacientes para vigilancia riesgo biomecánico - ergonómico.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaBiomecanico', $styleTable);
        $tableostevigilaciaBiomecanico = $pagOcupacionales->addTable( 'ostevigilaciaBiomecanico');

        $tableostevigilaciaBiomecanico->addRow();
        $tableostevigilaciaBiomecanico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaBiomecanico->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEErgonomico = $this->pacientesConSVEErgonomico($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEErgonomico); $i++){
            $tableostevigilaciaBiomecanico->addRow();
            $tableostevigilaciaBiomecanico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEErgonomico[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaBiomecanico->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEErgonomico[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fsPieTable, 'PSPiePag');


        //---------------SVE ruido----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 37. ', 'Pacientes para vigilancia riesgo biomecánico - Ruido.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaRuido', $styleTable);
        $tableostevigilaciaRuido = $pagOcupacionales->addTable( 'ostevigilaciaRuido');

        $tableostevigilaciaRuido->addRow();
        $tableostevigilaciaRuido->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaRuido->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVERuido = $this->pacientesConSVRuido($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVERuido); $i++){
            $tableostevigilaciaRuido->addRow();
            $tableostevigilaciaRuido->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVERuido[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaRuido->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVERuido[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Biologico----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 38. ', 'Pacientes para vigilancia riesgo biomecánico - Biológico.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaBiologico', $styleTable);
        $tableostevigilaciaBiologico = $pagOcupacionales->addTable( 'ostevigilaciaBiologico');

        $tableostevigilaciaBiologico->addRow();
        $tableostevigilaciaBiologico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaBiologico->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEBiologico = $this->pacientesConSVEBiologico($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEBiologico); $i++){
            $tableostevigilaciaBiologico->addRow();
            $tableostevigilaciaBiologico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEBiologico[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaBiologico->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEBiologico[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Visual----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 39. ', 'Pacientes para vigilancia riesgo biomecánico - Visual.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaVisual', $styleTable);
        $tableostevigilaciaVisual = $pagOcupacionales->addTable( 'ostevigilaciaVisual');

        $tableostevigilaciaVisual->addRow();
        $tableostevigilaciaVisual->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaVisual->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEVisual = $this->pacientesConSVEVisual($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEVisual); $i++){
            $tableostevigilaciaVisual->addRow();
            $tableostevigilaciaVisual->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEVisual[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaVisual->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEVisual[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Psicolaboral----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 40. ', 'Pacientes para vigilancia riesgo biomecánico - Psicolaboral.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaPsicolaboral', $styleTable);
        $tableostevigilaciaPsicolaboral = $pagOcupacionales->addTable( 'ostevigilaciaPsicolaboral');

        $tableostevigilaciaPsicolaboral->addRow();
        $tableostevigilaciaPsicolaboral->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaPsicolaboral->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEPsicolaboral = $this->pacientesConSVEPsicolaboral($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEPsicolaboral); $i++){
            $tableostevigilaciaPsicolaboral->addRow();
            $tableostevigilaciaPsicolaboral->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEPsicolaboral[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaPsicolaboral->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEPsicolaboral[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Cardiovascular----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 41. ', 'Pacientes para vigilancia riesgo biomecánico - Cardiovascular.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaCardiovascular', $styleTable);
        $tableostevigilaciaCardiovascular = $pagOcupacionales->addTable( 'ostevigilaciaCardiovascular');

        $tableostevigilaciaCardiovascular->addRow();
        $tableostevigilaciaCardiovascular->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaCardiovascular->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVECardiovascular = $this->pacientesConSVECardiovascular($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVECardiovascular); $i++){
            $tableostevigilaciaCardiovascular->addRow();
            $tableostevigilaciaCardiovascular->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVECardiovascular[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaCardiovascular->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVECardiovascular[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Metabolico----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 42. ', 'Pacientes para vigilancia riesgo biomecánico - Metabólico.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaMetabolico', $styleTable);
        $tableostevigilaciaMetabolico = $pagOcupacionales->addTable( 'ostevigilaciaMetabolico');

        $tableostevigilaciaMetabolico->addRow();
        $tableostevigilaciaMetabolico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaMetabolico->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEMetabolico = $this->pacientesConSVEMetabolico($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEMetabolico); $i++){
            $tableostevigilaciaMetabolico->addRow();
            $tableostevigilaciaMetabolico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEMetabolico[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaMetabolico->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEMetabolico[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        //---------------SVE Quimico----------------

        $pagOcupacionales->addText($this->addTextArial11('Tabla 43. ', 'Pacientes para vigilancia riesgo biomecánico - Químico.'), $fontStyle5,'PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');

        $documento->addTableStyle('ostevigilaciaQuimico', $styleTable);
        $tableostevigilaciaQuimico = $pagOcupacionales->addTable( 'ostevigilaciaQuimico');

        $tableostevigilaciaQuimico->addRow();
        $tableostevigilaciaQuimico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars("CÉDULA"), $fontStyleTable1, $cellHCentered);
        $tableostevigilaciaQuimico->addCell(2000, $cellRowSpan)->addText("CARGO", $fontStyleTable1, $cellHCentered);

        $SVEQuimico = $this->pacientesConSVEQuimico($fechaDesde, $fechaHasta, $empresa_id, $motivoE);
        for($i = 0; $i < count($SVEQuimico); $i++){
            $tableostevigilaciaQuimico->addRow();
            $tableostevigilaciaQuimico->addCell(3000, $cellRowSpan)->addText(htmlspecialchars($SVEQuimico[$i]->paciente_cedula), $fontStyleTable2, $cellHCentered);
            $tableostevigilaciaQuimico->addCell(2000, $cellRowSpan)->addText(htmlspecialchars($SVEQuimico[$i]->cargoEvaluar), $fontStyleTable2, $cellHCentered);
        }
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');


        $pagOcupacionales->addPageBreak();

        //----------------------BIBLIOGRAFIA-------------------------

        $pagOcupacionales->addTextBreak(4, $fontStyle6);
        $pagOcupacionales->addListItem("CONSLUSIONES", 0, $fontStyle6, 'multilevel-general','PSCentrado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("COLOMBIA. MINISTERIO DEL TRABAJO. Decreto 1072 (26 de Mayo de 2015). Por medio del cual se expide el Decreto Único Reglamentario del Sector Trabajo. Capítulo 6, Sistema de Gestión de la seguridad y Salud en el trabajo.", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("COLOMBIA. MINISTERIO DEL TRABAJO. Decreto 1443 (31 de Julio de 2014).  Por medio del cual se dictan disposiciones para la implementación del Sistema de Gestión para la Seguridad y Salud en el trabajo.", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("COLOMBIA. MINISTERIO DE LA PROTECCION SOCIAL. Resolución 2346 (11 de Julio de 2007). Por el cual se regula la práctica de las evaluaciones médicas ocupacionales y el manejo y contenido de las historias clínicas ocupacionales. ", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("COLOMBIA. MINISTERIO DE LA PROTECCION SOCIAL. Resolución 1918 (5 de Junio de 2009).  Por el cual se modifican los artículos 11 y 17 de la resolución 2346 de 2007 y se dictan otras disposiciones.", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("GUIA TECNICA COLOMBIANA, GTC 45. Guía Técnica para la identificación de los peligros y la valoración de los riesgos en seguridad y salud ocupacional.", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("COLOMBIA. MINISTERIO DEL TRABAJO. IETS, Instituto de evaluación tecnológica en salud. Guías de Atención Integral en seguridad y salud en el trabajo – GATISST: Desordenes musculo esqueléticos de miembros superiores, Dolor lumbar inespecífico y enfermedad discal de origen ocupacional.", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');
        $pagOcupacionales->addTextBreak(1, $fontStyle5, 'PSPiePag');
        $pagOcupacionales->addListItem("Organización Mundial de la Salud. Prevención de las enfermedades no transmisibles en el lugar de trabajo.  [Citado en] http://www.who.int/dietphysicalactivity/workplace-report-spanish.pdf", 0, $fontStyle5, 'multilevel-bibliografia','PSJustificado');

        //------------------------Se limpian los errores del archivo, se genera y se exporta en formato docx-----------------------

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($documento, 'Word2007');
        $fileName = "reporteCondicionesDeSalud.docx";
        ob_clean();
        $documento->save($fileName, 'Word2007');
        header("Content-Disposition: attachment; filename=$fileName"); // Vamos a dar la opcion para descargar el archivo
        readfile($fileName);  // leemos el archivo para que se "descargue"
        unlink($fileName);
        exit();

        return response()->download(storage_path('reporteCondicionesDeSalud.docx'));
    }

    /**
     * Funcion que arma el formato de parrafo textoNegrilla: texto normal
     * @param $textNegrilla primera parte del texto que va en negrilla
     * @param $text texto con el mismo formato a excepcion de la negrilla
     * @return string texto con el formato definido
     */
    public function addTextArial11($textNegrilla, $text){
        return "<w:r><w:rPr><w:sz w:val=\"22\" /><w:b/></w:rPr><w:t xml:space=\"preserve\">".$textNegrilla."</w:t></w:r><w:r><w:rPr><w:sz w:val=\"22\" /></w:rPr><w:t xml:space=\"preserve\">".$text."</w:t></w:r>";
    }

    /**
     * Metodo que redirecciona a la vista grafica de reporte diagnostico
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View vista
     */
    public function view(){
        $empresas = Empresa::all();
        return view('reportes.reporteDiagnostico', compact('empresas'));
    }

    /**
     * @param $motivos arreglo que contiene los motivos seleccionados en el filtro
     * @return string cadena con los motivos de evaluacion que son seleccionado
     */
    public  function motivosSeleccionados($motivos){
        $motivoCadena = "";
        if(count($motivos) == 4){
            return "INGRESO, PERIÓDICO, RETIRO y POSTINCAPACIDAD";
        }if(count($motivos) == 1){
            return $motivos[0];
        }else{
            for($i = 0; $i < count($motivos); $i++){
                $motivoCadena .= $motivos[$i];

                if ($i == count($motivos)-2){
                    $motivoCadena .= " y ";
                }else if($i != count($motivos)-1){
                    $motivoCadena .= ", ";
                }
            }
            return $motivoCadena;
        }

    }

    /**
     * @param $motivo
     * @return string
     */
    public function motivosFormatoSQL($motivo){

        $motivoE = "(";
        for ($i = 0; $i < count($motivo); $i++){
            $motivoE .= "'".$motivo[$i] ;
            if ($i == count($motivo)-1){
                $motivoE .= "')";
            }else{
                $motivoE .= "',";
            }
        }

        return $motivoE;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return mixed
     */
    function cantPacientesByFechaEmpresa($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $count = HistoriaClinica::select('paciente_id')
            ->where('empresa_id', $empresa_id)
            ->where('estado','<>','Anulada')
            ->whereBetween('created_at',[$fechaInicio, $fechaFin])
            ->whereIn('motivoevaluacion', $motivo)
            ->count();

        return $count;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function cantidadPorGenero($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $cantidad = DB::table('paciente as p')
            ->select(DB::raw('p.genero, count(hc.paciente_id) as cantidad'))
            ->join('historiaclinica as hc','p.id','hc.paciente_id')
            ->where('hc.empresa_id', $empresa_id)
            ->where('hc.estado','<>','Anulada')
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->groupBy('p.genero')->orderBy('p.genero', 'desc')
            ->get();
        if (count($cantidad) == 1){
            $cantidad[1] = array('p.genero' => 'FEMENINO', 'cantidad' => 0);
        }

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function cantidadExamenesComplementarios($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw('el.descripcion, count(el.descripcion) as cantidad'))
            ->join('hc_paraclinico as p','hc.id','p.historiaclinica_id')
            ->join('hc_examenlaboratorio as el','el.id','p.examenLaboratorio_id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->groupBy('el.descripcion')
            ->get();

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function cantidadGrupoEtario($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $grupoEtario = DB::select("select SUM(IF(paciente_edad between 20 and 30,1,0)) as \"rango1\",
                                            SUM(IF(paciente_edad between 31 and 40,1,0)) as \"rango2\",
                                            SUM(IF(paciente_edad between 41 and 50,1,0)) as \"rango3\",
                                            SUM(IF(paciente_edad between 51 and 60,1,0)) as \"rango4\",
                                            SUM(IF(paciente_edad > 60,1,0)) as \"rango5\" from historiaclinica h  
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $grupoEtario;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadEscolaridad($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $escolaridad = DB::select("select SUM(IF(paciente_escolaridad like 'ANALFABETA',1,0)) as \"rango1\",
                                            SUM(IF(paciente_escolaridad like 'PRIMARIA',1,0)) as \"rango2\",
                                            SUM(IF(paciente_escolaridad like 'SECUNDARIA',1,0)) as \"rango3\",
                                            SUM(IF(paciente_escolaridad like 'TÉCNICO',1,0)) as \"rango4\",
                                            SUM(IF(paciente_escolaridad like 'TECNÓLOGO',1,0)) as \"rango5\",
                                            SUM(IF(paciente_escolaridad like 'UNIVERSITARIO',1,0)) as \"rango6\",
                                            SUM(IF(paciente_escolaridad like 'ESPECIALIZACIÓN',1,0)) as \"rango7\",
                                            SUM(IF(paciente_escolaridad like 'MAESTRIA',1,0)) as \"rango8\",
                                            SUM(IF(paciente_escolaridad like 'DOCTORADO',1,0)) as \"rango9\" from historiaclinica h  
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $escolaridad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadEstadoCivil($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $estadoCivil = DB::select("select SUM(IF(paciente_estadoCivil like 'SOLTERO',1,0)) as \"rango1\",
                                            SUM(IF(paciente_estadoCivil like 'CASADO',1,0)) as \"rango2\",
                                            SUM(IF(paciente_estadoCivil like 'UNIÓN LIBRE',1,0)) as \"rango3\",
                                            SUM(IF(paciente_estadoCivil like 'SEPARADO',1,0)) as \"rango4\",
                                            SUM(IF(paciente_estadoCivil like 'VIUDO',1,0)) as \"rango5\" from historiaclinica h  
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $estadoCivil;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadTabaquismo($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $habitos = DB::select("select SUM(IF(tabaquismo like 'NO',1,0)) as \"rango1\",
                                            SUM(IF(tabaquismo_exfumador like 'SI',1,0)) as \"rango2\",
                                            SUM(IF(tabaquismo like 'SI',1,0)) as \"rango3\" from paciente p join historiaclinica h on h.paciente_id = p.id
                                            JOIN hc_habito hab ON h.id = hab.historiaclinica_id
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $habitos;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadAlcohol($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $alcohol = DB::select("select SUM(IF(alcohol like 'SI',1,0)) as \"rango1\",
                                            SUM(IF(alcohol like 'NO',1,0)) as \"rango2\" from paciente p join historiaclinica h on h.paciente_id = p.id
                                            JOIN hc_habito hab ON h.id = hab.historiaclinica_id
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $alcohol;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadDeportiva($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $deportiva = DB::select("select SUM(IF(deporte like 'SI',1,0)) as \"rango1\",
                                            SUM(IF(deporte like 'NO',1,0)) as \"rango2\" from paciente p join historiaclinica h on h.paciente_id = p.id
                                            JOIN hc_habito hab ON h.id = hab.historiaclinica_id
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $deportiva;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadSedentarismo($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $sedentarismo = DB::select("select SUM(IF(sedentarismo like 'SI',1,0)) as \"rango1\",
                                            SUM(IF(sedentarismo like 'NO',1,0)) as \"rango2\" from paciente p join historiaclinica h on h.paciente_id = p.id
                                            JOIN hc_habito hab ON h.id = hab.historiaclinica_id
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $sedentarismo;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function cantidadCargos($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $cargos = DB::table('historiaclinica')
            ->select(DB::raw('cargoEvaluar, count(cargoEvaluar) as cantidad'))
            ->where('estado', '<>', 'Anulada')
            ->where('empresa_id', $empresa_id)
            ->whereBetween('historiaclinica.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('motivoevaluacion', $motivo)
            ->groupBy('cargoEvaluar')->orderBy('cargoEvaluar', 'desc')
            ->get();
        return $cargos;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function cantidadAntiguedad($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $antiguedad = DB::select("select SUM(IF(paciente_antiguedad like '< 1 año',1,0)) as \"rango1\",
                                            SUM(IF(paciente_antiguedad like '1 - 5 años',1,0)) as \"rango2\",
                                            SUM(IF(paciente_antiguedad like '5 - 10 años',1,0)) as \"rango3\",
                                            SUM(IF(paciente_antiguedad like '> 10 años',1,0)) as \"rango4\" from  historiaclinica h
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $antiguedad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function cantidadFactoresRiesgo($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);

        $factoresRiesgo = DB::select("select SUM(IF(fisicos is not null,1,0)) as \"rango1\",
					SUM(IF(biologicos is not null,1,0)) as \"rango2\",
					SUM(IF(psicosociales is not null,1,0)) as \"rango3\",
					SUM(IF(seguridad is not null,1,0)) as \"rango4\",
					SUM(IF(quimicos is not null,1,0)) as \"rango5\",
					SUM(IF(ergonomicos is not null ,1,0)) as \"rango6\" from paciente p 
                    JOIN historiaclinica h on h.paciente_id = p.id
					JOIN hc_cargoactual ca ON h.id = ca.historiaclinica_id
					JOIN hc_cargo fr ON h.id = fr.historiaclinica_id
					WHERE h.estado <> 'Anulada' and ca.cargo = fr.cargo and ca.empresa = fr.empresa and h.created_at between ? and ? and 
					      h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);

        return $factoresRiesgo;

    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadAccidenteTRabajo($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $motivoE = $this->motivosFormatoSQL($motivo);

        $accidentes = DB::select("select SUM(IF(accidente like 'SI',1,0)) as \"rango1\",
                                            SUM(IF(accidente like 'NO',1,0)) as \"rango2\" from  historiaclinica h
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $accidentes;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function accidentesTrabajo($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $motivoE = $this->motivosFormatoSQL($motivo);

        $accidentes = DB::select("SELECT acc.fecha, acc.lesion, h.cargoEvaluar from historiaclinica h
					JOIN hc_accidente acc ON h.id = acc.historiaclinica_id
					WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $accidentes;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function cantidadEnfermedadTrabajo($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $motivoE = $this->motivosFormatoSQL($motivo);

        $enfermedades = DB::select("select SUM(IF(enfermedad like 'SI',1,0)) as \"rango1\",
                                            SUM(IF(enfermedad like 'NO',1,0)) as \"rango2\" from  historiaclinica h
                                            WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $enfermedades;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function enfermedadesLaborales($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $motivoE = $this->motivosFormatoSQL($motivo);

        $enfermedades = DB::select("SELECT enf.fecha, enf.diagnostico, h.cargoEvaluar from historiaclinica h
					JOIN hc_enfermedadProfesional enf ON h.id = enf.historiaclinica_id
					WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $enfermedades;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function pruebasMotivo($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $pruebas = DB::table('historiaclinica')
            ->select(DB::raw("motivoevaluacion, count(id) as cantidad"))
            ->where('estado', '<>','Anulada')
            ->where('empresa_id', $empresa_id)
            ->whereBetween('historiaclinica.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('motivoevaluacion', $motivo)
            ->groupBy('motivoevaluacion')
            ->get();
        return $pruebas;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function pruebasComplementarias($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $motivoE = $this->motivosFormatoSQL($motivo);
        $complementarias = DB::select("select 
					SUM(IF(optometria is not null,1,0)) as \"rango1\",
					SUM(IF(visiometria is not null,1,0)) as \"rango2\",
					SUM(IF(audiometria like 'SI',1,0)) as \"rango3\",
					SUM(IF(espirometria like 'SI',1,0)) as \"rango4\",
					SUM(IF(psicologico is not null,1,0)) as \"rango5\",
					SUM(IF(psicometrico is not null,1,0)) as \"rango6\",
					SUM(IF(pruebavestibular is not null,1,0)) as \"rango7\"
                    FROM historiaclinica h
					LEFT JOIN hc_datosParaclinico dp ON h.id = dp.historiaclinica_id
					WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $complementarias;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pruebasLaboratorio($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("el.descripcion, sum(case when p.diagnostico = 'Normal' then 1 else 0 end) as 'Normal', 
            sum(case when p.diagnostico = 'Anormal' then 1 else 0 end) as 'Anormal' "))
            ->join('hc_paraclinico as p','hc.id','p.historiaclinica_id')
            ->join('hc_examenlaboratorio as el','el.id','p.examenLaboratorio_id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->where(function($query){
                $query->where('p.diagnostico', 'Normal')
                    ->orWhere('p.diagnostico', 'Anormal');
            })
            ->groupBy('el.descripcion')
            ->get();

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return array
     */
    function pruebasComplementariasRango($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $motivoE = $this->motivosFormatoSQL($motivo);
        $complementarias = DB::select("select 
					SUM(IF(optometria like 'Normal',1,0)) as \"optoNormal\",
					SUM(IF(optometria like 'Alterada',1,0)) as \"optoAnormal\",
					SUM(IF(visiometria like 'Normal',1,0)) as \"visioNormal\",
					SUM(IF(visiometria like 'Alterada',1,0)) as \"visioAnormal\",
					SUM(IF(audiometria_resultado like 'Normal' and audiometria like 'Si',1,0)) as \"audioNormal\",
					SUM(IF(audiometria_resultado like 'Alterada' and audiometria like 'Si',1,0)) as \"audioAnormal\",
					SUM(IF(espirometria_resultado like 'Normal' and espirometria like 'Si',1,0)) as \"espiroNormal\",
					SUM(IF(espirometria_resultado like 'Patrón restrictivo' and espirometria like 'Si',1,0)) as \"espiroPR\",
					SUM(IF(espirometria_resultado like 'Patrón obstructivo' and espirometria like 'Si',1,0)) as \"espiroPO\",
					SUM(IF(espirometria_resultado like 'Patrón mixto' and espirometria like 'Si',1,0)) as \"espiroPM\",
					SUM(IF(psicologico like 'Normal',1,0)) as \"psicoNormal\",
					SUM(IF(psicologico like 'Anormal',1,0)) as \"psicoAnormal\",
					SUM(IF(psicometrico like 'Normal',1,0)) as \"psicomeNormal\",
					SUM(IF(psicometrico like 'Anormal',1,0)) as \"psicomeAnormal\",
					SUM(IF(pruebavestibular like 'Normal',1,0)) as \"vestibularNormal\",
					SUM(IF(pruebavestibular like 'Anormal',1,0)) as \"vestibularAnormal\"
                    FROM historiaclinica h
					LEFT JOIN hc_datosParaclinico dp ON h.id = dp.historiaclinica_id
					WHERE h.estado <> 'Anulada' and h.created_at between ? and ? and h.empresa_id = ? and h.motivoevaluacion in ".$motivoE." ",
            [$fechaInicio, $fechaFin, $empresa_id]);
        return $complementarias;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function distribucionPresionArterial($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN tas <= 120 and tad <= 80 then 1 else 0 end) as 'normal',
		   SUM(CASE WHEN (tas > 120 and tas <= 139) and (tad > 80 and tad <= 89) then 1 else 0 end) as 'prehipertension',
		   SUM(CASE WHEN (tas > 139 and tas <= 159) or (tad > 89 and tad <= 99) then 1 else 0 end) as 'htaEst1',
		   SUM(CASE WHEN tas > 159 or tad > 99 then 1 else 0 end) as 'htaEst2'"))
            ->join('hc_examenFisico as ef','ef.historiaclinica_id','hc.id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function distribucionIMC($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN imc < 18.5 then 1 else 0 end) as 'bajoPeso',
		   SUM(CASE WHEN imc >= 18.5 and imc <= 24.9 then 1 else 0 end) as 'normal',
		   SUM(CASE WHEN imc >= 25 and imc <= 29.9 then 1 else 0 end) as 'sobrePeso',
		   SUM(CASE WHEN imc >= 30 then 1 else 0 end) as 'obesidad' "))
            ->join('hc_examenFisico as ef','ef.historiaclinica_id','hc.id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();

        return $cantidad;

    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function distribucionExamenOsteomuscular($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN columna = 'Normal' then 1 else 0 end) as 'columnaNormal',
		   SUM(CASE WHEN columna = 'Anormal' then 1 else 0 end) as 'columnaAnormal',
		   SUM(CASE WHEN extremidadinferior = 'Normal' then 1 else 0 end) as 'extremidadInfNormal',
		   SUM(CASE WHEN extremidadinferior = 'Anormal' then 1 else 0 end) as 'extremidadInfAnormal',
		   SUM(CASE WHEN extremidadsuperior = 'Normal' then 1 else 0 end) as 'extremidadSupNormal',
		   SUM(CASE WHEN extremidadsuperior = 'Anormal' then 1 else 0 end) as 'extremidadSupAnormal'"))
            ->join('hc_examenFisico as ef','ef.historiaclinica_id','hc.id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEErgonomico($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Ergonomico)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();

        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVRuido($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Ruido)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEBiologico($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Biológico)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEVisual($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Visual)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEPsicolaboral($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Psicolaboral)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVECardiovascular($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Cardiovascular)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEMetabolico($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Metabólico)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesConSVEQuimico($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select("paciente_cedula", "cargoEvaluar")
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('recomendacion_ingresosve', 'LIKE', '%SVE (Químico)%')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     */
    function pacientesRecomendacionMedica($fechaInicio, $fechaFin, $empresa_id, $motivo){
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN recomendacion_medica LIKE '%Remisión a EPS%' then 1 else 0 end) as 'remisionEPS',
            SUM(CASE WHEN recomendacion_medica LIKE '%Continuar manejo Médico%' then 1 else 0 end) as 'manejoMedico',
            SUM(CASE WHEN recomendacion_medica LIKE '%Remision a ARL%' then 1 else 0 end) as 'remisionARL',
            SUM(CASE WHEN recomendacion_medica LIKE '%Seguimiento caso por ARL%' then 1 else 0 end) as 'seguimientoARL',
            SUM(CASE WHEN recomendacion_medica LIKE '%Citología Cervico-Vaginal%' then 1 else 0 end) as 'citlogia',
            SUM(CASE WHEN recomendacion_medica LIKE '%Tamizaje Prostatico%' then 1 else 0 end) as 'tamizaje',
            SUM(CASE WHEN recomendacion_medica LIKE '%Esquema de Vacunación (Adulto)%' then 1 else 0 end) as 'esquemaV',
            SUM(CASE WHEN recomendacion_medica LIKE '%Control Audiologico Periodico%' then 1 else 0 end) as 'controlAudio',
            SUM(CASE WHEN recomendacion_medica LIKE '%Control Anual Optometrico%' then 1 else 0 end) as 'controlOpto',
            SUM(CASE WHEN recomendacion_medica LIKE '%Control Odontologico Periodico%' then 1 else 0 end) as 'controlOdont'"))
            ->join('hc_resultado as res','res.historiaclinica_id','hc.id')
            ->where('estado','<>','Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at',[$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function pacientesRecomendacionOcupacional($fechaInicio, $fechaFin, $empresa_id, $motivo)
    {
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN recomendacion_ocupacional LIKE '%Uso de EPP%' then 1 else 0 end) as 'usoEPP',
            SUM(CASE WHEN recomendacion_ocupacional LIKE '%Higiene Postural%' then 1 else 0 end) as 'higienePost',
            SUM(CASE WHEN recomendacion_ocupacional LIKE '%Pausas Activas,%' then 1 else 0 end) as 'pausasActivas',
            SUM(CASE WHEN recomendacion_ocupacional LIKE '%Distribucion de fuerzas y cargas%' then 1 else 0 end) as 'distFuerzas',
            SUM(CASE WHEN recomendacion_ocupacional LIKE '%Pausas activas musculo esqueléticas%' then 1 else 0 end) as 'pausasActivasMusc',
            SUM(CASE WHEN recomendacion_ocupacional LIKE '%Pausas activas visuales%' then 1 else 0 end) as 'pausasActivasVisuales'"))
            ->join('hc_resultado as res', 'res.historiaclinica_id', 'hc.id')
            ->where('estado', '<>', 'Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at', [$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }

    /**
     * @param $fechaInicio
     * @param $fechaFin
     * @param $empresa_id
     * @param $motivo
     * @return \Illuminate\Support\Collection
     */
    function pacientesRecomendacionHabitos($fechaInicio, $fechaFin, $empresa_id, $motivo)
    {
        $cantidad = DB::table('historiaclinica as hc')
            ->select(DB::raw("SUM(CASE WHEN recomendacion_habitos LIKE '%Inicio de actividad Física%' then 1 else 0 end) as 'inicioAF',
            SUM(CASE WHEN recomendacion_habitos LIKE '%Dejar de Fumar%' then 1 else 0 end) as 'dejarF',
            SUM(CASE WHEN recomendacion_habitos LIKE '%Reducir Consumo de Alcohol%' then 1 else 0 end) as 'reducirA',
            SUM(CASE WHEN recomendacion_habitos LIKE '%Control Nutricional y de Peso%' then 1 else 0 end) as 'controlNutri',
            SUM(CASE WHEN recomendacion_habitos LIKE '%Continuar con actividad física%' then 1 else 0 end) as 'continuarAF'"))
            ->join('hc_resultado as res', 'res.historiaclinica_id', 'hc.id')
            ->where('estado', '<>', 'Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at', [$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->get();
        return $cantidad;
    }


    function distribucionPorPatologias($fechaInicio, $fechaFin, $empresa_id, $motivo){

        $subQuery = DB::table('historiaclinica as hc')
            ->select(DB::raw('descripcion, categoria_id, count(descripcion) as cantidad'))
            ->join('hc_impresionDiagnostica as impDiag', 'hc.id', 'impDiag.historiaclinica_id')
            ->join('hc_cie10 as cie10','impDiag.cie10_id','cie10.id')
            ->where('estado', '<>', 'Anulada')
            ->where('hc.empresa_id', $empresa_id)
            ->whereBetween('hc.created_at', [$fechaInicio, $fechaFin])
            ->whereIn('hc.motivoevaluacion', $motivo)
            ->groupBy('descripcion', 'categoria_id');


        $count = DB::table( DB::raw("({$subQuery->toSql()}) as sub") )
            ->mergeBindings($subQuery)  // this is required for selecting from subqueries
            ->select(DB::raw('catcie10.nombre, sub.descripcion, sub.cantidad'))
            ->join('categoria_cie10 as catcie10','sub.categoria_id','catcie10.id')
            ->orderBy('catcie10.nombre')
            ->get();
        return $count;
    }

    function crearTablaPatologias($documento, $pagOcupacionales, $styleTable, $fechaInicio, $fechaFin, $empresa_id, $motivo,
                                  $fontStyleTable1, $cellHCentered, $fontStyleTable2, $cellHJustify, $cellRowSpan){

        $documento->addTableStyle('patologias', $styleTable);
        $tablePatologias = $pagOcupacionales->addTable( 'patologias');
        $patologias = $this->distribucionPorPatologias($fechaInicio, $fechaFin, $empresa_id, $motivo);

        for ($i = 0; $i < count($patologias); $i++){
            $categoria = $patologias[$i]->nombre;
            if ($i == 0){
                $tablePatologias->addRow();
                $tablePatologias->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($categoria), $fontStyleTable1, $cellHCentered);
                $tablePatologias->addCell(1500, $cellRowSpan)->addText(htmlspecialchars(""), $fontStyleTable1, $cellHCentered);

                $tablePatologias->addRow();
                $tablePatologias->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->descripcion), $fontStyleTable2, $cellHJustify);
                $tablePatologias->addCell(1500, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->cantidad), $fontStyleTable2, $cellHJustify);
            }else{
                $catAux = $patologias[$i-1]->nombre;

                if ($catAux == $categoria){
                    $tablePatologias->addRow();
                    $tablePatologias->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->descripcion), $fontStyleTable2, $cellHJustify);
                    $tablePatologias->addCell(1500, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->cantidad), $fontStyleTable2, $cellHJustify);
                }else{
                    $tablePatologias->addRow();
                    $tablePatologias->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($categoria), $fontStyleTable1, $cellHCentered);
                    $tablePatologias->addCell(1500, $cellRowSpan)->addText(htmlspecialchars(""), $fontStyleTable1, $cellHCentered);

                    $tablePatologias->addRow();
                    $tablePatologias->addCell(6000, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->descripcion), $fontStyleTable2, $cellHJustify);
                    $tablePatologias->addCell(1500, $cellRowSpan)->addText(htmlspecialchars($patologias[$i]->cantidad), $fontStyleTable2, $cellHJustify);
                }
            }


        }
    }


    // Conversion de numero a letras

    function unidad($numuero){
        switch ($numuero) {
            case 9:
                {
                    $numu = "NUEVE";
                    break;
                }
            case 8:
                {
                    $numu = "OCHO";
                    break;
                }
            case 7:
                {
                    $numu = "SIETE";
                    break;
                }
            case 6:
                {
                    $numu = "SEIS";
                    break;
                }
            case 5:
                {
                    $numu = "CINCO";
                    break;
                }
            case 4:
                {
                    $numu = "CUATRO";
                    break;
                }
            case 3:
                {
                    $numu = "TRES";
                    break;
                }
            case 2:
                {
                    $numu = "DOS";
                    break;
                }
            case 1:
                {
                    $numu = "UN";
                    break;
                }
            case 0:
                {
                    $numu = "";
                    break;
                }
        }
        return $numu;
    }

    function decena($numdero){

        if ($numdero >= 90 && $numdero <= 99) {
            $numd = "NOVENTA ";
            if ($numdero > 90)
                $numd = $numd."Y ".($this->unidad($numdero - 90));
        } else if ($numdero >= 80 && $numdero <= 89) {
            $numd = "OCHENTA ";
            if ($numdero > 80)
                $numd = $numd."Y ".($this->unidad($numdero - 80));
        } else if ($numdero >= 70 && $numdero <= 79) {
            $numd = "SETENTA ";
            if ($numdero > 70)
                $numd = $numd."Y ".($this->unidad($numdero - 70));
        } else if ($numdero >= 60 && $numdero <= 69) {
            $numd = "SESENTA ";
            if ($numdero > 60)
                $numd = $numd."Y ".($this->unidad($numdero - 60));
        } else if ($numdero >= 50 && $numdero <= 59) {
            $numd = "CINCUENTA ";
            if ($numdero > 50)
                $numd = $numd."Y ".($this->unidad($numdero - 50));
        } else if ($numdero >= 40 && $numdero <= 49) {
            $numd = "CUARENTA ";
            if ($numdero > 40)
                $numd = $numd."Y ".($this->unidad($numdero - 40));
        } else if ($numdero >= 30 && $numdero <= 39) {
            $numd = "TREINTA ";
            if ($numdero > 30)
                $numd = $numd."Y ".($this->unidad($numdero - 30));
        } else if ($numdero >= 20 && $numdero <= 29) {
            if ($numdero == 20)
                $numd = "VEINTE ";
            else
                $numd = "VEINTI".($this->unidad($numdero - 20));
        } else if ($numdero >= 10 && $numdero <= 19) {
            switch ($numdero){
                case 10:
                    {
                        $numd = "DIEZ ";
                        break;
                    }
                case 11:
                    {
                        $numd = "ONCE ";
                        break;
                    }
                case 12:
                    {
                        $numd = "DOCE ";
                        break;
                    }
                case 13:
                    {
                        $numd = "TRECE ";
                        break;
                    }
                case 14:
                    {
                        $numd = "CATORCE ";
                        break;
                    }
                case 15:
                    {
                        $numd = "QUINCE ";
                        break;
                    }
                case 16:
                    {
                        $numd = "DIECISEIS ";
                        break;
                    }
                case 17:
                    {
                        $numd = "DIECISIETE ";
                        break;
                    }
                case 18:
                    {
                        $numd = "DIECIOCHO ";
                        break;
                    }
                case 19:
                    {
                        $numd = "DIECINUEVE ";
                        break;
                    }
            }
        } else
            $numd = $this->unidad($numdero);
        return $numd;
    }

    function centena($numc){
        if ($numc >= 100) {
            if ($numc >= 900 && $numc <= 999) {
                $numce = "NOVECIENTOS ";
                if ($numc > 900)
                    $numce = $numce.($this->decena($numc - 900));
            } else if ($numc >= 800 && $numc <= 899) {
                $numce = "OCHOCIENTOS ";
                if ($numc > 800)
                    $numce = $numce.($this->decena($numc - 800));
            } else if ($numc >= 700 && $numc <= 799) {
                $numce = "SETECIENTOS ";
                if ($numc > 700)
                    $numce = $numce.($this->decena($numc - 700));
            } else if ($numc >= 600 && $numc <= 699) {
                $numce = "SEISCIENTOS ";
                if ($numc > 600)
                    $numce = $numce.($this->decena($numc - 600));
            } else if ($numc >= 500 && $numc <= 599) {
                $numce = "QUINIENTOS ";
                if ($numc > 500)
                    $numce = $numce.($this->decena($numc - 500));
            } else if ($numc >= 400 && $numc <= 499) {
                $numce = "CUATROCIENTOS ";
                if ($numc > 400)
                    $numce = $numce.($this->decena($numc - 400));
            } else if ($numc >= 300 && $numc <= 399) {
                $numce = "TRESCIENTOS ";
                if ($numc > 300)
                    $numce = $numce.($this->decena($numc - 300));
            } else if ($numc >= 200 && $numc <= 299) {
                $numce = "DOSCIENTOS ";
                if ($numc > 200)
                    $numce = $numce.($this->decena($numc - 200));
            } else if ($numc >= 100 && $numc <= 199) {
                if ($numc == 100)
                    $numce = "CIEN ";
                else
                    $numce = "CIENTO ".($this->decena($numc - 100));
            }
        } else
            $numce = $this->decena($numc);

        return $numce;
    }

    function miles($nummero){
        if ($nummero >= 1000 && $nummero < 2000){
            $numm = "MIL ".($this->centena($nummero%1000));
        }
        if ($nummero >= 2000 && $nummero <10000){
            $numm = $this->unidad(Floor($nummero/1000))." MIL ".($this->centena($nummero%1000));
        }
        if ($nummero < 1000)
            $numm = $this->centena($nummero);

        return $numm;
    }

    function decmiles($numdmero){
        if ($numdmero == 10000)
            $numde = "DIEZ MIL";
        if ($numdmero > 10000 && $numdmero <20000){
            $numde = $this->decena(Floor($numdmero/1000))."MIL ".($this->centena($numdmero%1000));
        }
        if ($numdmero >= 20000 && $numdmero <100000){
            $numde = $this->decena(Floor($numdmero/1000))." MIL ".($this->miles($numdmero%1000));
        }
        if ($numdmero < 10000)
            $numde = $this->miles($numdmero);

        return $numde;
    }

    function cienmiles($numcmero){
        if ($numcmero == 100000)
            $num_letracm = "CIEN MIL";
        if ($numcmero >= 100000 && $numcmero <1000000){
            $num_letracm = $this->centena(Floor($numcmero/1000))." MIL ".($this->centena($numcmero%1000));
        }
        if ($numcmero < 100000)
            $num_letracm = $this->decmiles($numcmero);
        return $num_letracm;
    }

    function millon($nummiero){
        if ($nummiero >= 1000000 && $nummiero <2000000){
            $num_letramm = "UN MILLON ".($this->cienmiles($nummiero%1000000));
        }
        if ($nummiero >= 2000000 && $nummiero <10000000){
            $num_letramm = $this->unidad(Floor($nummiero/1000000))." MILLONES ".($this->cienmiles($nummiero%1000000));
        }
        if ($nummiero < 1000000)
            $num_letramm = $this->cienmiles($nummiero);

        return $num_letramm;
    }

    function decmillon($numerodm){
        if ($numerodm == 10000000)
            $num_letradmm = "DIEZ MILLONES";
        if ($numerodm > 10000000 && $numerodm <20000000){
            $num_letradmm = $this->decena(Floor($numerodm/1000000))."MILLONES ".($this->cienmiles($numerodm%1000000));
        }
        if ($numerodm >= 20000000 && $numerodm <100000000){
            $num_letradmm = $this->decena(Floor($numerodm/1000000))." MILLONES ".($this->millon($numerodm%1000000));
        }
        if ($numerodm < 10000000)
            $num_letradmm = $this->millon($numerodm);

        return $num_letradmm;
    }

    function cienmillon($numcmeros){
        if ($numcmeros == 100000000)
            $num_letracms = "CIEN MILLONES";
        if ($numcmeros >= 100000000 && $numcmeros <1000000000){
            $num_letracms = $this->centena(Floor($numcmeros/1000000))." MILLONES ".($this->millon($numcmeros%1000000));
        }
        if ($numcmeros < 100000000)
            $num_letracms = $this->decmillon($numcmeros);
        return $num_letracms;
    }

    function milmillon($nummierod){
        if ($nummierod >= 1000000000 && $nummierod <2000000000){
            $num_letrammd = "MIL ".($this->cienmillon($nummierod%1000000000));
        }
        if ($nummierod >= 2000000000 && $nummierod <10000000000){
            $num_letrammd = $this->unidad(Floor($nummierod/1000000000))." MIL ".(cienmillon($nummierod%1000000000));
        }
        if ($nummierod < 1000000000)
            $num_letrammd = $this->cienmillon($nummierod);

        return $num_letrammd;
    }

    function convertir($numero){
        $numf = $this->milmillon($numero);
        return $numf;
    }
}

