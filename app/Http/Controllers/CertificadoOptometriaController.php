<?php

namespace App\Http\Controllers;

use App\HistoriaOptometria;
use File;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


class CertificadoOptometriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generateCertificate(Request $request, $id){
        $historiaOpto = HistoriaOptometria::find($id);
        $template = new TemplateProcessor(public_path()."/historiaOptometria.docx");

        $template->setValue('fecha', $historiaOpto->created_at);
        $template->setValue('motivoevaluacion', $historiaOpto->motivoevaluacion);

        $otrosME = $historiaOpto->motivoevaluacion_otro;
        if ($otrosME != "") {
            $otrosME = str_replace(",", ", ", substr($historiaOpto->motivoevaluacion_otro, 0, -1) . ".");
        }

        $template->setValue('motivoevaluacion_otros', htmlspecialchars($otrosME));

        // Generacion de la foto
        $photoPatient = $historiaOpto->Paciente->foto;
        if (isset($photoPatient)) {
            $image = str_replace('data:image/png;base64,', '', $photoPatient);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            // Load the image
            $source = @imagecreatefromjpeg(public_path() . '/temp/img/' . $imagePatient);
            if (!$source){

            }else{
                $rotate = imagerotate($source, -90, 0);

                imagejpeg($rotate, public_path() . '/temp/img/' . $imagePatient);
                $template->setImageValue("foto",array("path" => public_path('/temp/img/' . $imagePatient),"width"=>240,"height"=>158));
            }

        }

        //------Identificacion trabajador
        $template->setValue('foto', $photoPatient);
        $template->setValue('nombre_completo', $historiaOpto->paciente);
        $template->setValue('genero', $historiaOpto->Paciente->genero);
        $template->setValue('documento', $historiaOpto->paciente_cedula);
        $template->setValue('direccion', $historiaOpto->Paciente->direccionDomicilio);
        $template->setValue('eps', $historiaOpto->Paciente->eps);
        $template->setValue('afp', $historiaOpto->Paciente->afp);
        $template->setValue('arl', $historiaOpto->Paciente->arl);
        $nombreContactoCasoEmergencia = $historiaOpto->Paciente->nombreAcompaniante . " " . $historiaOpto->Paciente->apellidoAcompaniante;
        $template->setValue('nombre_acompanante', $nombreContactoCasoEmergencia);
        $template->setValue('telefonoacompanante', $historiaOpto->Paciente->celularAcompaniante);

        //-----Identificacion empresa-----
        $template->setValue('nombre_empresa', $historiaOpto->empresa->nombre);
        $template->setValue('nit', $historiaOpto->empresa->nit);

        //-----Antecedentes personales-----
        $template->setValue('diabetes', $historiaOpto->antecedente->diabetes);
        $template->setValue('diabetes_obs', htmlspecialchars($historiaOpto->antecedente->diabetes_obs));
        $template->setValue('hipertension', $historiaOpto->antecedente->hipertension);
        $template->setValue('hipertension_obs', htmlspecialchars($historiaOpto->antecedente->hipertension_obs));
        $template->setValue('alergicos', $historiaOpto->antecedente->alergicos);
        $template->setValue('alergicos_obs', htmlspecialchars($historiaOpto->antecedente->alergicos_obs));
        $template->setValue('tiroides', $historiaOpto->antecedente->alteraciontiroides);
        $template->setValue('tiroides_obs', htmlspecialchars($historiaOpto->antecedente->alteraciontiroides_obs));
        $template->setValue('cardiacos', $historiaOpto->antecedente->problemascardiacos);
        $template->setValue('cardiacos_obs', htmlspecialchars($historiaOpto->antecedente->problemascardiacos_obs));
        $template->setValue('otros_personales', htmlspecialchars($historiaOpto->antecedente->personales_otros));
        $template->setValue('otros_personales_obs', htmlspecialchars($historiaOpto->antecedente->personales_otros_obs));

        //---Antecedentes oculares
        $template->setValue('usu_rx', $historiaOpto->antecedente->usuariosrx);
        $template->setValue('usu_rx_obs', htmlspecialchars($historiaOpto->antecedente->usuariosrx_obs));
        $template->setValue('quirur', $historiaOpto->antecedente->quirurgicos);
        $template->setValue('quirur_obs', htmlspecialchars($historiaOpto->antecedente->quirurgicos_obs));
        $template->setValue('trauma', $historiaOpto->antecedente->trauma);
        $template->setValue('trauma_obs', htmlspecialchars($historiaOpto->antecedente->trauma_obs));
        $template->setValue('esquir', $historiaOpto->antecedente->esquirlas);
        $template->setValue('esquir_obs', htmlspecialchars($historiaOpto->antecedente->esquirlas_obs));
        $template->setValue('quimic', $historiaOpto->antecedente->quimicos);
        $template->setValue('quimic_obs', htmlspecialchars($historiaOpto->antecedente->quimicos_obs));

        $template->setValue('glauco', $historiaOpto->antecedente->glaucoma);
        $template->setValue('glauco_obs', htmlspecialchars($historiaOpto->antecedente->glaucoma_obs));

        $template->setValue('rvisual', $historiaOpto->antecedente->rehabilitacionvisual);
        $template->setValue('rvisual_obs', htmlspecialchars($historiaOpto->antecedente->rehabilitacionvisual_obs));
        $template->setValue('catarata', $historiaOpto->antecedente->catarata);
        $template->setValue('catarata_obs', htmlspecialchars($historiaOpto->antecedente->catarata_obs));
        $template->setValue('ao_otros', htmlspecialchars($historiaOpto->antecedente->oculares_otros));
        $template->setValue('ao_otros_obs', htmlspecialchars($historiaOpto->antecedente->oculares_otros_obs));

        //------Sintomas----------
        $template->setValue('vblej', $historiaOpto->sintoma->visionborrosalejana);
        $template->setValue('vdoble', $historiaOpto->sintoma->visiondoble);
        $template->setValue('ardor', $historiaOpto->sintoma->ardor);
        $template->setValue('vbcer', $historiaOpto->sintoma->visionborrosacercana);
        $template->setValue('cefal', $historiaOpto->sintoma->cefalea);
        $template->setValue('nrefie', $historiaOpto->sintoma->norefiere);
        $template->setValue('fotof', $historiaOpto->sintoma->fotofobia);
        $template->setValue('enroj', $historiaOpto->sintoma->enrojecimiento);
        $template->setValue('docul', $historiaOpto->sintoma->dolorocular);
        $template->setValue('sueno', $historiaOpto->sintoma->suenoleer);

        //------Riesgo del cargo a evaluar------
        $template->setValue('trauma', $historiaOpto->riesgocargoevaluar->trauma);
        $template->setValue('expo_qs', $historiaOpto->riesgocargoevaluar->exposicionquimicossolventes);
        $template->setValue('radiacion', $historiaOpto->riesgocargoevaluar->radiacionesionizantes);
        $template->setValue('ex_gv', $historiaOpto->riesgocargoevaluar->exposiciongasesvapores);
        $template->setValue('exp_mat', $historiaOpto->riesgocargoevaluar->exposicionmaterialparticulado);
        $template->setValue('exp_vt', $historiaOpto->riesgocargoevaluar->exposicionvideoterminales);
        $template->setValue('exp_mp', $historiaOpto->riesgocargoevaluar->exposicionmaterialproyeccion);
        $template->setValue('rad_ni', $historiaOpto->riesgocargoevaluar->radiacionesnoionizantes);
        $template->setValue('ilumina', $historiaOpto->riesgocargoevaluar->iluminacion);
        $template->setValue('otros_rc', htmlspecialchars($historiaOpto->riesgocargoevaluar->otros));

        //------Agudeza visual---------
        $template->setValue('lsc_od', htmlspecialchars($historiaOpto->agudezavisual->ls_od));
        $template->setValue('lsc_oi', htmlspecialchars($historiaOpto->agudezavisual->ls_oi));
        $template->setValue('lsc_binocular', htmlspecialchars($historiaOpto->agudezavisual->ls_ojobinocular));
        $template->setValue('lcc_od', htmlspecialchars($historiaOpto->agudezavisual->lc_od));
        $template->setValue('lcc_oi',htmlspecialchars( $historiaOpto->agudezavisual->lc_oi));
        $template->setValue('lcc_binocular', htmlspecialchars($historiaOpto->agudezavisual->lc_ojobinocular));
        $template->setValue('csc_od', htmlspecialchars($historiaOpto->agudezavisual->cs_od));
        $template->setValue('csc_oi', htmlspecialchars($historiaOpto->agudezavisual->cs_oi));
        $template->setValue('csc_binocular', htmlspecialchars($historiaOpto->agudezavisual->cs_ojobinocular));
        $template->setValue('ccc_od', htmlspecialchars($historiaOpto->agudezavisual->cc_od));
        $template->setValue('ccc_oi',htmlspecialchars($historiaOpto->agudezavisual->cc_oi));
        $template->setValue('ccc_binocular', htmlspecialchars($historiaOpto->agudezavisual->cc_ojobinocular));
        $template->setValue('lenso_od', htmlspecialchars($historiaOpto->lensometria_ojoderecho));
        $template->setValue('lenso_oi', htmlspecialchars($historiaOpto->lensometria_ojoizquierdo));
        $template->setValue('lenso_add', htmlspecialchars($historiaOpto->lensometria_add));
        $template->setValue('lenso_tipolente', htmlspecialchars($historiaOpto->lensometria_tipolente));
        $template->setValue('cejas', htmlspecialchars($historiaOpto->segmentoanterior_cejas));
        $template->setValue('pestanas', htmlspecialchars($historiaOpto->segmentoanterior_pestanas));
        $template->setValue('parpados', htmlspecialchars($historiaOpto->segmentoanterior_parpados));
        $template->setValue('vias_lag', htmlspecialchars($historiaOpto->segmentoanterior_viaslagrimales));
        $template->setValue('conjuntiva', htmlspecialchars($historiaOpto->segmentoanterior_conjuntiva));
        $template->setValue('cornea', htmlspecialchars($historiaOpto->segmentoanterior_cornea));
        $template->setValue('iris', htmlspecialchars($historiaOpto->segmentoanterior_iris));
        $template->setValue('pupilas', htmlspecialchars($historiaOpto->segmentoanterior_pupilas));
        $template->setValue('esclerotica', htmlspecialchars($historiaOpto->segmentoanterior_esclerotica));
        $template->setValue('camara_ant', htmlspecialchars($historiaOpto->segmentoanterior_camaraanterior));
        $template->setValue('motilidad', htmlspecialchars($historiaOpto->segmentoanterior_motilidadocular));
        $template->setValue('vision_color', htmlspecialchars($historiaOpto->segmentoanterior_visioncolor));
        $template->setValue('vision_profundidad', htmlspecialchars($historiaOpto->segmentoanterior_visionprofundidad));
        $template->setValue('fondo_od', htmlspecialchars($historiaOpto->segmentoanterior_fondoojoderecho));
        $template->setValue('fondo_oi', htmlspecialchars($historiaOpto->segmentoanterior_fondoojoizquierdo));
        $template->setValue('retino_od', htmlspecialchars($historiaOpto->retinoscopia_ojoderecho));
        $template->setValue('retino_oi',htmlspecialchars( $historiaOpto->retinoscopia_ojoizquierdo));
        $template->setValue('retino_add', htmlspecialchars($historiaOpto->retinoscopia_add));

        //------Conducta y recomendaciones------
        $template->setValue('ergovisual', htmlspecialchars($historiaOpto->recomendacion->ergonomiavisual));
        $template->setValue('pautashigiene', htmlspecialchars($historiaOpto->recomendacion->pautashigienevisual));
        $template->setValue('higienevisual', htmlspecialchars($historiaOpto->recomendacion->higienevisual));
        $template->setValue('correccionopticaper', htmlspecialchars($historiaOpto->recomendacion->correccionopticapermanente));
        $template->setValue('correccionopticavisionp', htmlspecialchars($historiaOpto->recomendacion->correccionopticavisionprolongada));
        $template->setValue('correccionopticaactual', htmlspecialchars($historiaOpto->recomendacion->correccionopticaactual));
        $template->setValue('correccionopticavisioncercana', htmlspecialchars($historiaOpto->recomendacion->correccionopticavisioncercana));
        $template->setValue('elementosproteccion', htmlspecialchars($historiaOpto->recomendacion->usoelementosproteccionvisual));
        $template->setValue('proteccionvisualcorrecionoptica', htmlspecialchars($historiaOpto->recomendacion->proteccionvisualcorreccionoptica));
        $template->setValue('gafasuv', htmlspecialchars($historiaOpto->recomendacion->usogafasfiltrouv));
        $template->setValue('controlanual', htmlspecialchars($historiaOpto->recomendacion->controlanual));
        $template->setValue('controlopto', htmlspecialchars($historiaOpto->recomendacion->controloptometria));
        $template->setValue('controlofta', htmlspecialchars($historiaOpto->recomendacion->controloftalmologia));
        $template->setValue('nousocorrecionoptica', htmlspecialchars($historiaOpto->recomendacion->norequiereusocorreccionoptica));
        $template->setValue('otrasconductas', htmlspecialchars($historiaOpto->recomendacion->otrasconductas));

        //------Firmas-----------
        $template->setValue('nombremedico', $historiaOpto->medicocierre->nombre_completo);
        $template->setValue('cocumentomedico', $historiaOpto->medicocierre->document);
        $template->setValue('licencia', $historiaOpto->medicocierre->licencia);

        //-----------Exportar pdf-----------
        if (isset($historiaOpto->firmamedico)){
            $image = str_replace('data:image/png;base64,', '', $historiaOpto->firmamedico);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmamedico", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmamedico","");
        }

        if (isset($historiaOpto->firmapaciente)){
            $image = str_replace('data:image/png;base64,', '', $historiaOpto->firmapaciente);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmapaciente", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmapaciente","");
        }
        $template->saveAs(public_path() . '/generados_hc/' . $historiaOpto->Paciente->documento . '-optometria.docx');
        $process = new Process("export HOME=/tmp && libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $historiaOpto->Paciente->documento . '-optometria.docx');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return response()->download(public_path($historiaOpto->Paciente->documento.'-optometria.pdf'));
    }

    public function textosEnter($texto1, $texto2)
    {


        $textFinal = $texto1 . '<w:br/>' . $texto2;

        return $textFinal;
    }

    public function view()
    {

        return view('optometria.certificate');
    }
}
