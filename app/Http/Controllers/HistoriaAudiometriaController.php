<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\ha_antecedente;
use App\ha_audiograma;
use App\ha_otoscopia;
use App\ha_recomendaciones;
use App\HistoriaAudiometria;
use App\Http\Requests\HistoriaAudiometriaRequest;
use App\Patient;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HistoriaAudiometriaController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('audiometria.register');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(HistoriaAudiometriaRequest $request) {
		$json = $request->all();
		$paciente = Patient::where('documento', $json['documento'])->first();
		$empresa = Empresa::select('id')->where('nombre', $json['empresa'])->get();
		$medico = User::select('firma')->where('id', $json['medicoApertura_id'])->first();
		$json['firma_medico'] = $medico->firma;
		$json['estado'] = 'Cerrada';
		$json['empresa_id'] = $empresa[0]->id;
		$json['paciente_id'] = $paciente->id;
		$json['paciente_cedula'] = $paciente->documento;
		$json['paciente'] = $paciente->nombre_completo;
		$json['firma_paciente'] = $paciente->firma;
		$json['paciente_edad'] = $paciente->edad;
		$json['medicoCierre_id'] = $json['medicoApertura_id'];
		if (!isset($json['fecha'])){
            $json['fecha'] = Carbon::now('America/Bogota');
        }
		$audiograma = $json['audiograma'];
		foreach ($audiograma as $item) {
			if ($item['oidoderecho'] == 110 || $item['oidoderecho'] == "110") {
				$item['oidoderecho'] = "";
			}

			if ($item['oidoizquierdo'] == 110 || $item['oidoizquierdo'] == "110") {
				$item['oidoizquierdo'] = "";
			}
		}
		if ($json['audiograma']) {
			$ha = HistoriaAudiometria::create($json);
		}

		$json['antecedentes']['historiaAudiometria_id'] = $ha->id;
		$antecedentes = ha_antecedente::create($json['antecedentes']);
		$ha->antecedentes()->save($antecedentes);
		$json['otoscopia']['historiaAudiometria_id'] = $ha->id;
		$otoscopia = ha_otoscopia::create($json['otoscopia']);
		$ha->otoscopia()->save($otoscopia);
		$json['recomendaciones']['historiaAudiometria_id'] = $ha->id;
		$recomendaciones = ha_recomendaciones::create($json['recomendaciones']);
		$ha->recomendacion()->save($recomendaciones);

		$audiogramaArray = $json['audiograma'];
		for ($i = 0; $i < count($audiogramaArray); $i++) {
			$audiogramaArray[$i]['historiaAudiometria_id'] = $ha->id;
			$impresion = ha_audiograma::create($audiogramaArray[$i]);
			$ha->audiograma()->save($impresion);
		}

		return array("mensaje" => "Se ha registrado la historia audiometria", "historia" => $ha);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		return HistoriaAudiometria::all($id)->first();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function getHA(Request $request) {
		$id = $request->input('id');
		$historia = HistoriaAudiometria::with([
			'antecedentes',
			'otoscopia',
			'recomendacion',
			'audiograma', 'empresa',
		])->find($id);
		if ($historia != null) {
			return ["mensaje" => "Historia encontrada", "historia" => $historia];
		} else {
			return ["mensaje" => "Historia no encontrada"];
		}
	}

	public function getHAWeb($id) {

		$historia = HistoriaAudiometria::with([
			'antecedentes',
			'otoscopia',
			'recomendacion',
			'audiograma', 'empresa',
		])->find(decrypt($id));
		if ($historia != null) {
			return view('audiometria.consultar', compact('historia'));
		}
	}

	public function anular(Request $request) {
		$id = $request->input('id');
		$user_id = $request->input('user_id');
		$historia = HistoriaAudiometria::find($id);
		$historia->estado = "Anulada";
		$historia->medicoAnula_id = $user_id;
		$historia->save();
		return ["mensaje" => "Se ha anulado correctamente"];
	}
}
