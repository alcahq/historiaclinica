<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $tipos_doc = ['CC', 'TI', 'PA', 'CE'];
        return view('usuario.register', compact('roles','tipos_doc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterUserRequest $request)
    {
          $requestData = $request->all();
          $requestData['password'] = bcrypt($requestData['password']);
          $requestData['nombre_completo'] = $requestData['name'].' '.$requestData['last_name'];
          User::create($requestData);
          return back()->with('info2','Usuario registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail(decrypt($id));
        $roles = Role::all();

//        return $usuario;
        return view('usuario.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $usuario = $request->all();
        $usuario['nombre_completo'] = $usuario['name'].' '.$usuario['last_name'];
        User::findOrFail(decrypt($id))->update($usuario);
        return back()->with('infoUpdate', 'Usuario actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function findByDocumentOrName(Request $request){
        $usuario_buscado = $request->input('buscar_usuario');
        if ($request->has("documento")) {
            $usuario_buscado = $request->input("documento");
        }
        if ($usuario_buscado == ""){
            return back();
        }
        $usuarios = User::select('document', 'name', 'last_name', 'role_id', 'id', 'type_document')
            ->where('document', $usuario_buscado)
            ->orWhere('nombre_completo', 'like', '%'.$usuario_buscado.'%')->get();
        if ($request->has("documento")) {
            return $usuarios[0];
        } else {
            return view('usuario.list', compact('usuarios'));

        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewList(){

        return view('usuario.list');
    }

    public function viewRecoverPassword(){
        return view('usuario.recoverPass');
    }

    public function validateRecoverPass(Request $request){
        //dd($request->all());

        $request->validate([
            'document' => 'required|exists:users',
            'password' => 'required',
            'password_confirmation' => 'same:password',
        ],[
            'buscar_documento.required' => 'El campo No de documento es obligatorio',
            'password.required' => 'El campo nueva contraseña es obligatorio',
            'password_confirmation.required' => 'El campo confirmar contraseña es obligatorio',
            'same' => 'Las contraseñas no coinciden.',
            'document.exists' => 'No se encontro un usuario asociado al número de documento.'
        ]);
    }

    public function recoverPassword(Request $request){
        $this->validateRecoverPass($request);
        $documento = $request->input('document');
        $usuario = User::select('id')->where('document', $documento)->get()->first();
        if ($usuario != null){
            $pass2 = $request->input('password_confirmation');
            if ($pass2 != ""){
                $actualizado = User::where('id', $usuario->id)
                    ->update(['password' => bcrypt($pass2)]);
                if ($actualizado == 1){
                    return back()->with('SiUser', 'Se modifico la contraseña correctamente.');
                }
            }
        }
        return back()->with('SiUser', 'No se ha podido restablecer la contraseña, intentelo nuevamente.');
    }
}
