<?php

namespace App\Http\Controllers;

use App\Constantes_agudezavisual;
use App\Empresa;
use App\Exports\VisiometriaExport;
use App\HistoriaOptometria;
use App\HistoriaVisiometria;
use App\HO_Antecedentes;
use App\HO_Recomendaciones;
use App\Http\Requests\HistoriaVisiometriaRequest;
use App\HV_AgudezaVisual;
use App\Patient;
use App\RiesgoCargoEvaluar;
use App\Sintomas;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use PDF;

class HistoriaVisiometriaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function home(){
        return view('home');
    }

    public function registerPatient(){
        return 'Registrando';
    }

    public function registerPatientView(){
        return view('paciente.register');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $constantes_agudezavisual = Constantes_agudezavisual::all();
        $empresas = Empresa::all();
        return view('visiometria.register', compact('empresas', 'constantes_agudezavisual'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('visiometria.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoriaVisiometriaRequest $request) {
        $opto = $request->all();

        $empresa = Empresa::select('id')->where('nombre',$opto['empresa'])->get();
        $motivoevaluacion = isset($opto["motivoevaluacion"]) ? $opto["motivoevaluacion"] : "";
        $motivoevaluacion_otros = isset($opto["motivoevaluacion_otros"]) ? $opto["motivoevaluacion_otros"] : "";
        $medicoapertura_id = isset($opto["medicoApertura_id"]) ? $opto["medicoApertura_id"] : 1;
        $medicocierre_id = isset($opto["medicoApertura_id"]) ? $opto["medicoApertura_id"] : 1;
        $documento = $opto["documento"];
        $paciente_id = Patient::select('id', 'firma', 'nombre_completo', 'edad')->where('documento',$documento)->first();
        $medico = User::select('firma')->find($medicocierre_id);

        $antecedentes = $this->antecedentes($opto);
        $sintomas = $this->sintoma($opto);
        $riesgocargoevaluar = $this->riesgocargoevaluar($opto);

        $agudezavisual_db = $this->agudezavisual($opto);
        $recomendacion = $this->recomendacion($opto);

        $historiaVisiometria = new HistoriaVisiometria();
        $opto['fecha'] = Carbon::now('America/Bogota');
        $historiaVisiometria->fecha = $opto['fecha'];
        $historiaVisiometria->paciente_id = $paciente_id->id;
        $historiaVisiometria->paciente_cedula = $documento;
        $historiaVisiometria->paciente = $paciente_id->nombre_completo;
        $historiaVisiometria->paciente_edad = $paciente_id->edad;
        $historiaVisiometria->firmapaciente = $paciente_id->firma;
        $historiaVisiometria->firmamedico = $medico->firma;
        $historiaVisiometria->medicoapertura_id = $medicoapertura_id;
        $historiaVisiometria->medicocierre_id = $medicoapertura_id;
        $historiaVisiometria->motivoevaluacion = $motivoevaluacion;
        $historiaVisiometria->motivoevaluacion_otro = $motivoevaluacion_otros;
        $historiaVisiometria->cargo = $opto['cargo'];
        $historiaVisiometria->empresa_id = $empresa[0]->id;
        $historiaVisiometria->save();
        $historiaVisiometria->agudezavisual()->save($agudezavisual_db);
        $historiaVisiometria->antecedente()->save($antecedentes);
        $historiaVisiometria->recomendacion()->save($recomendacion);
        $historiaVisiometria->sintoma()->save($sintomas);
        $historiaVisiometria->riesgocargoevaluar()->save($riesgocargoevaluar);
        if($historiaVisiometria->id != null){
            return ["mensaje" => "Se ha registrado correctamente."];
        } else{
            return ["mensaje" => "Se ha encontrado un problema."];
        }
    }
    /**
     *
     */
    function riesgocargoevaluar($opto) {
        $riesgocargoevaluar_trauma = $opto["riesgocargoevaluar_trauma"];
        $riesgocargoevaluar_exposicionmaterialparticulado = $opto["riesgocargoevaluar_exposicionmaterialparticulado"];
        $riesgocargoevaluar_exposicionmaterialproyeccion = $opto["riesgocargoevaluar_exposicionmaterialproyeccion"];
        $riesgocargoevaluar_iluminacion = $opto["riesgocargoevaluar_iluminacion"];
        $riesgocargoevaluar_exposicionvideoterminales = $opto["riesgocargoevaluar_exposicionvideoterminales"];
        $riesgocargoevaluar_exposicionquimicossolventes = $opto["riesgocargoevaluar_exposicionquimicossolventes"];
        $riesgocargoevaluar_exposiciongasesvapores = $opto["riesgocargoevaluar_exposiciongasesvapores"];
        $riesgocargoevaluar_radiacionesionizantes = $opto["riesgocargoevaluar_radiacionesionizantes"];
        $riesgocargoevaluar_radiacionesnoionizantes = $opto["riesgocargoevaluar_radiacionesnoionizantes"];
        $riesgocargoevaluar_otros = $opto["riesgocargoevaluar_otros"];

        $riesgo = new RiesgoCargoEvaluar();
        $riesgo->trauma = $riesgocargoevaluar_trauma;
        $riesgo->exposicionmaterialparticulado = $riesgocargoevaluar_exposicionmaterialparticulado;
        $riesgo->iluminacion = $riesgocargoevaluar_iluminacion;
        $riesgo->exposicionvideoterminales = $riesgocargoevaluar_exposicionvideoterminales;
        $riesgo->exposicionquimicossolventes = $riesgocargoevaluar_exposicionquimicossolventes;
        $riesgo->exposiciongasesvapores = $riesgocargoevaluar_exposiciongasesvapores;
        $riesgo->radiacionesionizantes = $riesgocargoevaluar_radiacionesionizantes;
        $riesgo->radiacionesnoionizantes = $riesgocargoevaluar_radiacionesnoionizantes;
        $riesgo->otros = $riesgocargoevaluar_otros;
        $riesgo->exposicionmaterialproyeccion = $riesgocargoevaluar_exposicionmaterialproyeccion;
        return $riesgo;
    }
    /**
     *
     */
    function sintoma($opto) {
        $sintomas_visionborrosalejana = $opto["sintomas_visionborrosalejana"];
        $sintomas_visionborroscercana = $opto["sintomas_visionborrosacercana"];
        $sintomas_fotofobia = $opto["sintomas_fotofobia"];
        $sintomas_dolorocular = $opto["sintomas_dolorocular"];
        $sintomas_visiondoble = $opto["sintomas_visiondoble"];
        $sintomas_cefalea = $opto["sintomas_cefalea"];
        $sintomas_enrojecimiento = $opto["sintomas_enrojecimiento"];
        $sintomas_suenoleer = $opto["sintomas_suenoleer"];
        $sintomas_ardor = $opto["sintomas_ardor"];
        $sintomas_norefiere = $opto["sintomas_norefiere"];
        $sintomas_otros = $opto["sintomas_otros"];

        $sintomas = new Sintomas();
        $sintomas->visionborrosalejana = $sintomas_visionborrosalejana;
        $sintomas->visionborrosacercana = $sintomas_visionborroscercana;
        $sintomas->fotofobia = $sintomas_fotofobia;
        $sintomas->dolorocular = $sintomas_dolorocular;
        $sintomas->visiondoble = $sintomas_visiondoble;
        $sintomas->cefalea = $sintomas_cefalea;
        $sintomas->enrojecimiento = $sintomas_enrojecimiento;
        $sintomas->suenoleer = $sintomas_suenoleer;
        $sintomas->ardor = $sintomas_ardor;
        $sintomas->norefiere = $sintomas_norefiere;
        $sintomas->otros = $sintomas_otros;
        return $sintomas;
    }
    /**
     *
     */
    function antecedentes($opto) {
        $antecedentes_alergicos = $opto["antecedentes_alergicos"];
        $antecedentes_alergicos_obs = $opto["antecedentes_alergicos_obs"];
        $antecedentes_alteraciontiroides = $opto["antecedentes_alteraciontiroides"];
        $antecedentes_alteraciontiroides_obs = $opto["antecedentes_alteraciontiroides_obs"];
        $antecedentes_catarata = $opto["antecedentes_catarata"];
        $antecedentes_catarata_obs = $opto["antecedentes_catarata_obs"];
        $antecedentes_diabetes = $opto["antecedentes_diabetes"];
        $antecedentes_diabetes_obs = $opto["antecedentes_diabetes_obs"];
        $antecedentes_esquirlas = $opto["antecedentes_esquirlas"];
        $antecedentes_esquirlas_obs = $opto["antecedentes_esquirlas_obs"];
        $antecedentes_glaucoma = $opto["antecedentes_glaucoma"];
        $antecedentes_glaucoma_obs = $opto["antecedentes_glaucoma_obs"];
        $antecedentes_hipertension = $opto["antecedentes_hipertension"];
        $antecedentes_hipertension_obs = $opto["antecedentes_hipertension_obs"];
        $antecedentes_oculares_otros = $opto["antecedentes_oculares_otros"];
        $antecedentes_oculares_otros_obs = $opto["antecedentes_oculares_otros_obs"];
        $antecedentes_personales_otros = $opto["antecedentes_personales_otros"];
        $antecedentes_personales_otros_obs = $opto["antecedentes_personales_otros_obs"];
        $antecedentes_problemascardiacos = $opto["antecedentes_problemascardiacos"];
        $antecedentes_problemascardiacos_obs = $opto["antecedentes_problemascardiacos_obs"];
        $antecedentes_quimicos = $opto["antecedentes_quimicos"];
        $antecedentes_quimicos_obs = $opto["antecedentes_quimicos_obs"];
        $antecedentes_quirurgicos = $opto["antecedentes_quirurgicos"];
        $antecedentes_quirurgicos_obs = $opto["antecedentes_quirurgicos_obs"];
        $antecedentes_rehabilitacionvisual = $opto["antecedentes_rehabilitacionvisual"];
        $antecedentes_rehabilitacionvisual_obs = $opto["antecedentes_rehabilitacionvisual_obs"];
        $antecedentes_trauma = $opto["antecedentes_trauma"];
        $antecedentes_trauma_obs = $opto["antecedentes_trauma_obs"];
        $antecedentes_usuariosrx = $opto["antecedentes_usuariosrx"];
        $antecedentes_usuariosrx_obs = $opto["antecedentes_usuariosrx_obs"];

        $antecedentes = new HO_Antecedentes();
        $antecedentes->diabetes = $antecedentes_diabetes;
        $antecedentes->diabetes_obs = $antecedentes_diabetes_obs;
        $antecedentes->problemascardiacos = $antecedentes_problemascardiacos;
        $antecedentes->problemascardiacos_obs = $antecedentes_problemascardiacos_obs;
        $antecedentes->alergicos = $antecedentes_alergicos;
        $antecedentes->alergicos_obs = $antecedentes_alergicos_obs;
        $antecedentes->hipertension = $antecedentes_hipertension;
        $antecedentes->hipertension_obs = $antecedentes_hipertension_obs;
        $antecedentes->alteraciontiroides = $antecedentes_alteraciontiroides;
        $antecedentes->alteraciontiroides_obs = $antecedentes_alteraciontiroides_obs;
        $antecedentes->personales_otros = $antecedentes_personales_otros;
        $antecedentes->personales_otros_obs = $antecedentes_personales_otros_obs;
        $antecedentes->usuariosrx = $antecedentes_usuariosrx;
        $antecedentes->usuariosrx_obs = $antecedentes_usuariosrx_obs;
        $antecedentes->glaucoma = $antecedentes_glaucoma;
        $antecedentes->glaucoma_obs = $antecedentes_glaucoma_obs;
        $antecedentes->quirurgicos = $antecedentes_quirurgicos;
        $antecedentes->quirurgicos_obs = $antecedentes_quirurgicos_obs;
        $antecedentes->rehabilitacionvisual = $antecedentes_rehabilitacionvisual;
        $antecedentes->rehabilitacionvisual_obs = $antecedentes_rehabilitacionvisual_obs;
        $antecedentes->trauma = $antecedentes_trauma;
        $antecedentes->trauma_obs = $antecedentes_trauma_obs;
        $antecedentes->catarata = $antecedentes_catarata;
        $antecedentes->catarata_obs = $antecedentes_catarata_obs;
        $antecedentes->esquirlas = $antecedentes_esquirlas;
        $antecedentes->esquirlas_obs = $antecedentes_esquirlas_obs;
        $antecedentes->oculares_otros = $antecedentes_oculares_otros;
        $antecedentes->oculares_otros_obs = $antecedentes_oculares_otros_obs;
        $antecedentes->quimicos = $antecedentes_quimicos;
        $antecedentes->quimicos_obs = $antecedentes_quimicos_obs;

        return $antecedentes;
    }
    /**
     *
    */
    function recomendacion($opto) {
        $recomendaciones_ergonomiavisual = isset($opto["recomendaciones_ergonomiavisual"]) ? $opto["recomendaciones_ergonomiavisual"] : "";
        $recomendaciones_ergonomiavisual_obs = $opto["recomendaciones_ergonomiavisual_obs"];
        $recomendaciones_pautashigienevisual = isset($opto["recomendaciones_pautashigienevisual"]) ? $opto["recomendaciones_pautashigienevisual"] : "";
        $recomendaciones_pautashigienevisual_obs = $opto["recomendaciones_pautashigienevisual_obs"];
        $recomendaciones_higienevisual = isset($opto["recomendaciones_higienevisual"]) ? $opto["recomendaciones_higienevisual"] : "";
        $recomendaciones_higienevisual_obs = $opto["recomendaciones_higienevisual_obs"];
        $recomendaciones_correccionopticaactual = isset($opto["recomendaciones_correccionopticaactual"]) ? $opto["recomendaciones_correccionopticaactual"] : "";
        $recomendaciones_correccionopticaactual_obs = $opto["recomendaciones_correccionopticaactual_obs"];
        $recomendaciones_controloftalmologia = isset($opto["recomendaciones_controloftalmologia"]) ? $opto["recomendaciones_controloftalmologia"] : "";
        $recomendaciones_controloftalmologia_obs = $opto["recomendaciones_controloftalmologia_obs"];
        $recomendaciones_otrasconductas = $opto["recomendaciones_otrasconductas"];
        $recomendaciones_otrasconductas_obs = $opto["recomendaciones_otrasconductas_obs"];
        $recomendaciones_usoelementosproteccionvisual = isset($opto["recomendaciones_usoelementosproteccionvisual"]) ? $opto["recomendaciones_usoelementosproteccionvisual"] : "";
        $recomendaciones_usoelementosproteccionvisual_obs = $opto["recomendaciones_usoelementosproteccionvisual_obs"];
        $recomendaciones_proteccionvisualcorreccionoptica = isset($opto["recomendaciones_proteccionvisualcorreccionoptica"]) ? $opto["recomendaciones_proteccionvisualcorreccionoptica"] : "";
        $recomendaciones_proteccionvisualcorreccionoptica_obs = $opto["recomendaciones_proteccionvisualcorreccionoptica_obs"];
        $recomendaciones_usogafasfiltrouv = isset($opto["recomendaciones_usogafasfiltrouv"]) ? $opto["recomendaciones_usogafasfiltrouv"] : "";
        $recomendaciones_usogafasfiltrouv_obs = $opto["recomendaciones_usogafasfiltrouv_obs"];
        $recomendaciones_controloptometria = isset($opto["recomendaciones_controloptometria"]) ? $opto["recomendaciones_controloptometria"] : "";
        $recomendaciones_controloptometria_obs = $opto["recomendaciones_controloptometria_obs"];
        $recomendaciones_pacientecompatible = isset($opto["recomendaciones_pacientecompatible"]) ? $opto["recomendaciones_pacientecompatible"] : "";
        $recomendaciones_requierevaloracion = isset($opto["recomendaciones_requierevaloracion"]) ? $opto["recomendaciones_requierevaloracion"] : "";
        $recomendaciones_requiereremision = isset($opto["recomendaciones_requiereremision"]) ? $opto["recomendaciones_requiereremision"] : "";
        $recomendaciones_pacientecompatible_obs = $opto["recomendaciones_pacientecompatible_obs"];
        $recomendaciones_requierevaloracion_obs = $opto["recomendaciones_requierevaloracion_obs"];
        $recomendaciones_requiereremision_obs = $opto["recomendaciones_requiereremision_obs"];

        $recomendacion = new HO_Recomendaciones();
        $recomendacion->ergonomiavisual = $recomendaciones_ergonomiavisual;
        $recomendacion->ergonomiavisual_obs = $recomendaciones_ergonomiavisual_obs;
        $recomendacion->pautashigienevisual = $recomendaciones_pautashigienevisual;
        $recomendacion->pautashigienevisual_obs = $recomendaciones_pautashigienevisual_obs;
        $recomendacion->higienevisual = $recomendaciones_higienevisual;
        $recomendacion->higienevisual_obs = $recomendaciones_higienevisual_obs;
        $recomendacion->correccionopticaactual = $recomendaciones_correccionopticaactual;
        $recomendacion->correccionopticaactual_obs = $recomendaciones_correccionopticaactual_obs;
        $recomendacion->controloftalmologia = $recomendaciones_controloftalmologia;
        $recomendacion->controloftalmologia_obs = $recomendaciones_controloftalmologia_obs;
        $recomendacion->usoelementosproteccionvisual = $recomendaciones_usoelementosproteccionvisual;
        $recomendacion->usoelementosproteccionvisual_obs = $recomendaciones_usoelementosproteccionvisual_obs;
        $recomendacion->proteccionvisualcorreccionoptica = $recomendaciones_proteccionvisualcorreccionoptica;
        $recomendacion->proteccionvisualcorreccionoptica_obs = $recomendaciones_proteccionvisualcorreccionoptica_obs;
        $recomendacion->usogafasfiltrouv = $recomendaciones_usogafasfiltrouv;
        $recomendacion->usogafasfiltrouv_obs = $recomendaciones_usogafasfiltrouv_obs;
        $recomendacion->controloptometria = $recomendaciones_controloptometria;
        $recomendacion->controloptometria_obs = $recomendaciones_controloptometria_obs;
        $recomendacion->pacientecompatible = $recomendaciones_pacientecompatible;
        $recomendacion->pacientecompatible_obs = $recomendaciones_pacientecompatible_obs;
        $recomendacion->requierevaloracion = $recomendaciones_requierevaloracion;
        $recomendacion->requierevaloracion_obs = $recomendaciones_requierevaloracion_obs;
        $recomendacion->requiereremision = $recomendaciones_requiereremision;
        $recomendacion->requiereremision_obs = $recomendaciones_requiereremision_obs;
        $recomendacion->otrasconductas = $recomendaciones_otrasconductas;
        $recomendacion->otrasconductas_obs = $recomendaciones_otrasconductas_obs;

        return $recomendacion;
    }

    /**
     *
     */
    function agudezavisual($agudezavisual) {
        $lejana_concorreccion_binocular = $agudezavisual['lejana_concorreccion_binocular'];
        $lejana_concorreccion_binocular_obs = $agudezavisual['lejana_concorreccion_binocular_obs'];
        $lejana_concorreccion_ojoderecho = $agudezavisual['lejana_concorreccion_ojoderecho'];
        $lejana_concorreccion_ojoderecho_obs = $agudezavisual['lejana_concorreccion_ojoderecho_obs'];
        $lejana_concorreccion_ojoizquierdo = $agudezavisual['lejana_concorreccion_ojoizquierdo'];
        $lejana_concorreccion_ojoizquierdo_obs = $agudezavisual['lejana_concorreccion_ojoizquierdo_obs'];
        $lejana_sincorreccion_binocular = $agudezavisual['lejana_sincorreccion_binocular'];
        $lejana_sincorreccion_binocular_obs = $agudezavisual['lejana_sincorreccion_binocular_obs'];
        $lejana_sincorreccion_ojoderecho = $agudezavisual['lejana_sincorreccion_ojoderecho'];
        $lejana_sincorreccion_ojoderecho_obs = $agudezavisual['lejana_sincorreccion_ojoderecho_obs'];
        $lejana_sincorreccion_ojoizquierdo = $agudezavisual['lejana_sincorreccion_ojoizquierdo'];
        $lejana_sincorreccion_ojoizquierdo_obs = $agudezavisual['lejana_sincorreccion_ojoizquierdo_obs'];
        $cercana_concorreccion_binocular = $agudezavisual['cercana_concorreccion_binocular'];
        $cercana_concorreccion_binocular_obs = $agudezavisual['cercana_concorreccion_binocular_obs'];
        $cercana_concorreccion_ojoderecho = $agudezavisual['cercana_concorreccion_ojoderecho'];
        $cercana_concorreccion_ojoderecho_obs = $agudezavisual['cercana_concorreccion_ojoderecho_obs'];
        $cercana_concorreccion_ojoizquierdo = $agudezavisual['cercana_concorreccion_ojoizquierdo'];
        $cercana_concorreccion_ojoizquierdo_obs = $agudezavisual['cercana_concorreccion_ojoizquierdo_obs'];
        $cercana_sincorreccion_binocular = $agudezavisual['cercana_sincorreccion_binocular'];
        $cercana_sincorreccion_binocular_obs = $agudezavisual['cercana_sincorreccion_binocular_obs'];
        $cercana_sincorreccion_ojoderecho = $agudezavisual['cercana_sincorreccion_ojoderecho'];
        $cercana_sincorreccion_ojoderecho_obs = $agudezavisual['cercana_sincorreccion_ojoderecho_obs'];
        $cercana_sincorreccion_ojoizquierdo = $agudezavisual['cercana_sincorreccion_ojoizquierdo'];
        $cercana_sincorreccion_ojoizquierdo_obs = $agudezavisual['cercana_sincorreccion_ojoizquierdo_obs'];
        $lensometria_add = $agudezavisual['lensometria_add'];
        $lensometria_ojoderecho = $agudezavisual['lensometria_ojoderecho'];
        $lensometria_ojoizquierdo = $agudezavisual['lensometria_ojoizquierdo'];
        $lensometria_tipolente = $agudezavisual['lensometria_tipolente'];
        $segmentoanterior_campovisual = $agudezavisual['segmentoanterior_campovisual'];
        $segmentoanterior_campovisual_obs = $agudezavisual['segmentoanterior_campovisual_obs'];
        $segmentoanterior_impresiondiagnostica_ojoderecho = $agudezavisual['segmentoanterior_impresiondiagnostica_ojoderecho'];
        $segmentoanterior_impresiondiagnostica_ojoderecho_obs = $agudezavisual['segmentoanterior_impresiondiagnostica_ojoderecho_obs'];
        $segmentoanterior_impresiondiagnostica_ojoizquierdo = $agudezavisual['segmentoanterior_impresiondiagnostica_ojoizquierdo'];
        $segmentoanterior_impresiondiagnostica_ojoizquierdo_obs = $agudezavisual['segmentoanterior_impresiondiagnostica_ojoizquierdo_obs'];
        $segmentoanterior_motilidadocular = $agudezavisual['segmentoanterior_motilidadocular'];
        $segmentoanterior_motilidadocular_obs = $agudezavisual['segmentoanterior_motilidadocular_obs'];
        $segmentoanterior_segmentoanterior = $agudezavisual['segmentoanterior_segmentoanterior'];
        $segmentoanterior_segmentoanterior_obs = $agudezavisual['segmentoanterior_segmentoanterior_obs'];
        $segmentoanterior_visioncolor = $agudezavisual['segmentoanterior_visioncolor'];
        $segmentoanterior_visioncolor_obs = $agudezavisual['segmentoanterior_visioncolor_obs'];
        $segmentoanterior_visionprofundidad = $agudezavisual['segmentoanterior_visionprofundidad'];
        $segmentoanterior_visionprofundidad_obs = $agudezavisual['segmentoanterior_visionprofundidad_obs'];
        $recomendaciones = $agudezavisual['recomendaciones_adicionales'];

        $agudezavisual = new HV_AgudezaVisual();
        $agudezavisual->lejana_concorreccion_binocular = $lejana_concorreccion_binocular;
        $agudezavisual->lejana_concorreccion_binocular_obs = $lejana_concorreccion_binocular_obs;
        $agudezavisual->lejana_concorreccion_ojoderecho = $lejana_concorreccion_ojoderecho;
        $agudezavisual->lejana_concorreccion_ojoderecho_obs = $lejana_concorreccion_ojoderecho_obs;
        $agudezavisual->lejana_concorreccion_ojoizquierdo = $lejana_concorreccion_ojoizquierdo;
        $agudezavisual->lejana_concorreccion_ojoizquierdo_obs = $lejana_concorreccion_ojoizquierdo_obs;
        $agudezavisual->lejana_sincorreccion_binocular = $lejana_sincorreccion_binocular;
        $agudezavisual->lejana_sincorreccion_binocular_obs = $lejana_sincorreccion_binocular_obs;
        $agudezavisual->lejana_sincorreccion_ojoderecho = $lejana_sincorreccion_ojoderecho;
        $agudezavisual->lejana_sincorreccion_ojoderecho_obs = $lejana_sincorreccion_ojoderecho_obs;
        $agudezavisual->lejana_sincorreccion_ojoizquierdo = $lejana_sincorreccion_ojoizquierdo;
        $agudezavisual->lejana_sincorreccion_ojoizquierdo_obs = $lejana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual->cercana_concorreccion_binocular = $cercana_concorreccion_binocular;
        $agudezavisual->cercana_concorreccion_binocular_obs = $cercana_concorreccion_binocular_obs;
        $agudezavisual->cercana_concorreccion_ojoderecho = $cercana_concorreccion_ojoderecho;
        $agudezavisual->cercana_concorreccion_ojoderecho_obs = $cercana_concorreccion_ojoderecho_obs;
        $agudezavisual->cercana_concorreccion_ojoizquierdo = $cercana_concorreccion_ojoizquierdo;
        $agudezavisual->cercana_concorreccion_ojoizquierdo_obs = $cercana_concorreccion_ojoizquierdo_obs;
        $agudezavisual->cercana_sincorreccion_binocular = $cercana_sincorreccion_binocular;
        $agudezavisual->cercana_sincorreccion_binocular_obs = $cercana_sincorreccion_binocular_obs;
        $agudezavisual->cercana_sincorreccion_ojoderecho = $cercana_sincorreccion_ojoderecho;
        $agudezavisual->cercana_sincorreccion_ojoderecho_obs = $cercana_sincorreccion_ojoderecho_obs;
        $agudezavisual->cercana_sincorreccion_ojoizquierdo = $cercana_sincorreccion_ojoizquierdo;
        $agudezavisual->cercana_sincorreccion_ojoizquierdo_obs = $cercana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual->lensometria_add = $lensometria_add;
        $agudezavisual->lensometria_ojoderecho = $lensometria_ojoderecho;
        $agudezavisual->lensometria_ojoizquierdo = $lensometria_ojoizquierdo;
        $agudezavisual->lensometria_tipolente = $lensometria_tipolente;
        $agudezavisual->segmentoanterior_campovisual = $segmentoanterior_campovisual;
        $agudezavisual->segmentoanterior_campovisual_obs = $segmentoanterior_campovisual_obs;
        $agudezavisual->segmentoanterior_impresiondiagnostica_ojoderecho = $segmentoanterior_impresiondiagnostica_ojoderecho;
        $agudezavisual->segmentoanterior_impresiondiagnostica_ojoderecho_obs = $segmentoanterior_impresiondiagnostica_ojoderecho_obs;
        $agudezavisual->segmentoanterior_impresiondiagnostica_ojoizquierdo = $segmentoanterior_impresiondiagnostica_ojoizquierdo;
        $agudezavisual->segmentoanterior_impresiondiagnostica_ojoizquierdo_obs = $segmentoanterior_impresiondiagnostica_ojoizquierdo_obs;
        $agudezavisual->segmentoanterior_motilidadocular = $segmentoanterior_motilidadocular;
        $agudezavisual->segmentoanterior_motilidadocular_obs = $segmentoanterior_motilidadocular_obs;
        $agudezavisual->segmentoanterior_segmentoanterior = $segmentoanterior_segmentoanterior;
        $agudezavisual->segmentoanterior_segmentoanterior_obs = $segmentoanterior_segmentoanterior_obs;
        $agudezavisual->segmentoanterior_visioncolor = $segmentoanterior_visioncolor;
        $agudezavisual->segmentoanterior_visioncolor_obs = $segmentoanterior_visioncolor_obs;
        $agudezavisual->segmentoanterior_visionprofundidad = $segmentoanterior_visionprofundidad;
        $agudezavisual->segmentoanterior_visionprofundidad_obs = $segmentoanterior_visionprofundidad_obs;
        $agudezavisual->recomendaciones = $recomendaciones;
        return $agudezavisual;
    }

    public function print($id){

        $ho = HistoriaOptometria::find($id);
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Montserrat']);
        $pdf = PDF::loadView('optometria.print', compact('ho'));
        return $pdf->stream();
    }
    public function print2($id){

        $ho = HistoriaOptometria::find($id);
        return view('optometria.print', compact('ho'));
        //return view('optometria.print', compact('ho'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        // $ho = HistoriaOptometria::with(['agudezavisual', 'antecedente', 'recomendacion', 'sintoma', 'riesgocargoevaluar', 'paciente'])->findOrFail($id);
        // $constantes_agudezavisual = Constantes_agudezavisual::all();
        // $empresas = Empresa::all();
        // return view('optometria.view', compact('ho', 'empresas', 'constantes_agudezavisual'));
        $historia = HistoriaVisiometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            'paciente',
            ])->find($id);
        if($historia != null){
            return ["mensaje" => "Historia encontrada", "historia" => $historia];
        }else{
            return ["mensaje" => "Historia no encontrada"];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getHV(Request $request){
        $id = $request->input('id');
        $historia = HistoriaVisiometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            ])->find($id);
        if($historia != null){
            return ["mensaje" => "Historia encontrada", "historia" => $historia];
        }else{
            return ["mensaje" => "Historia no encontrada"];
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getHVWeb($id){
        $historia = HistoriaVisiometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            'empresa'
        ])->find(decrypt($id));
        if($historia != null){
            return view('visiometria.consultar', compact('historia'));
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function anular(Request $request){
        $id = $request->input('id');
        $user_id = $request->input('user_id');
        $historia = HistoriaVisiometria::find($id);
        $historia->estado = "Anulada";
        $historia->medicoAnula_id = $user_id;
        $historia->save();
        return ["mensaje" => "Se ha anulado correctamente"];
    }
}
