<?php

namespace App\Http\Controllers;

use App\AccidenteTrabajo;
use App\AntecedentesPersonales;
use App\Cargo;
use App\CargoActual;
use App\CIE10;
use App\Empresa;
use App\EnfermedadProfesional;
use App\ExamenFisico;
use App\ExamenLaboratorio;
use App\Exports\AudiometriaExport;
use App\Exports\HistoriaClinicaExport;
use App\Exports\OptometriaExport;
use App\Exports\VisiometriaExport;
use App\Habitos;
use App\HistoriaAudiometria;
use App\HistoriaClinica;
use App\Http\Requests\RegisterHistoriaCRequest;
use App\ImpresionDiagnostica;
use App\ManipulacionAlimentos;
use App\NotaEvolucion;
use App\NuevaValoracion;
use App\Paraclinico;
use App\ParaclinicoDatos;
use App\Patient;
use App\Resultado;
use App\RevisionPorSistema;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\DB;

class HistoriaClinicaController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	    $examenesLab = ExamenLaboratorio::orderBy("descripcion")->get();
//	    $cie10 = CIE10::all();
        return view('historiaClinica.register', compact('examenesLab'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(RegisterHistoriaCRequest $request) {

        $json = $request->all();

        //------------------Paciente historia clinica-------------------------
        $empresa = Empresa::select('id')->where('nombre',$json['cargos']['empresa'])->get();
        $paciente = Patient::where('documento', $json['documento'])->get();
        $json['paciente_id'] = $paciente[0]->id;
        $json['firmaPaciente'] = $paciente[0]->firma;
        $json['empresa_id'] = $empresa[0]->id;
        $json['genero'] = $paciente[0]->genero;
        $json['paciente_escolaridad'] = $paciente[0]->escolaridad;
        $json['paciente_estadoCivil'] = $paciente[0]->estadoCivil;
        $json['paciente_antiguedad'] = $paciente[0]->antiguedad;
        $fecha_nacimiento = $paciente[0]->fechaNacimiento;
        $json['paciente_edad'] = Carbon::parse($fecha_nacimiento)->age;
        $json['paciente'] = $paciente[0]->nombre_completo;
        $json['paciente_cedula'] = $paciente[0]->documento;
        $paciente[0]->cargo = $json['cargoEvaluar'];
        $paciente[0]->save();

        //--------------------------historia clinica--------------------------
        if(!isset($json['fecha'])){
            $json['fecha'] = Carbon::now('America/Bogota');
        }
        $hc = HistoriaClinica::create($json);
        $json['historiaClinica_id'] = $hc->id;

        //-----------------------------Ocupacionales--------------------------
        $json['cargos']['historiaClinica_id'] = $hc->id;
        if($json['cargos']['fecha'] == ""){
            $json['cargos']['fecha'] = "";
        }else{
            $json['cargos']['fecha'] = date('Y-m-d', strtotime($json['cargos']['fecha']));
        }

        if ($json['cargos']['fecha'] == "31-12-1969"){
            $json['cargos']['fecha'] = "";
        }
        CargoActual::create($json['cargos']);

        $factoresArray = $json['factores'];
        for ($i=0; $i < count($factoresArray); $i++) {
            $factoresArray[$i]['historiaClinica_id'] = $hc->id;
            Cargo::create($factoresArray[$i]);
        }

        //-----------------------------Laboral--------------------------
        if (isset($json['accidentes'])){
            $accidentesArray = $json['accidentes'];
            for ($i=0; $i < count($accidentesArray); $i++) {
                $accidentesArray[$i]['historiaclinica_id'] = $hc->id;
                $accidentesArray[$i]['paciente_id'] = $paciente[0]->id;
                AccidenteTrabajo::create($accidentesArray[$i]);
            }
        }

        if (isset($json['enfermedades'])){
            $enfermedadesArray = $json['enfermedades'];
            for ($i=0; $i < count($enfermedadesArray); $i++) {
                $enfermedadesArray[$i]['historiaclinica_id'] = $hc->id;
                $enfermedadesArray[$i]['paciente_id'] = $paciente[0]->id;
                EnfermedadProfesional::create($enfermedadesArray[$i]);
            }
        }


        //-----------------------------Habitos--------------------------
        $json['habitos']['historiaClinica_id'] = $hc->id;
        Habitos::create($json['habitos']);

        //-----------------------------Antecedentes--------------------------
        $json['antecedentes']['historiaClinica_id'] = $hc->id;
        AntecedentesPersonales::create($json['antecedentes']);

        //-------------------------Revision por sistemas---------------------
        $json['revisionporsistemas']['historiaClinica_id'] = $hc->id;
        RevisionPorSistema::create($json['revisionporsistemas']);

        //-----------------------------Examen fisico--------------------------
        $json['examenfisico']['historiaClinica_id'] = $hc->id;
        ExamenFisico::create($json['examenfisico']);

        //-----------------------------Manipulacion alimentos--------------------------
        $json['manipulacionalimentos']['historiaClinica_id'] = $hc->id;
        ManipulacionAlimentos::create($json['manipulacionalimentos']);

        //-----------------------------Paraclinicos--------------------------
        if (isset($json['paraclinicos'])){
            $paraclinicosArray = $json['paraclinicos'];
            for ($i=0; $i < count($paraclinicosArray); $i++) {
                if (($paraclinicosArray[$i]['examenLaboratorio_id'] == -1)){
                    continue;
                }

                if (isset($paraclinicosArray[$i]['examenLaboratorio']) ){
                    $nombre = $paraclinicosArray[$i]['examenLaboratorio'];
                    $examen = ExamenLaboratorio::where('descripcion', 'like',$nombre)->first();
                    $paraclinicosArray[$i]['examenLaboratorio_id'] = $examen->id;
                }
                $paraclinicosArray[$i]['historiaClinica_id'] = $hc->id;
                Paraclinico::create($paraclinicosArray[$i]);
            }
        }

        $json['paraclinicosdatos']['historiaClinica_id'] = $hc->id;
        ParaclinicoDatos::create($json['paraclinicosdatos']);

        //-----------------------------Impresion diag--------------------------
        if(isset($json['idiagnostica'])){
            $impresionArray = $json['idiagnostica'];
            for ($i=0; $i < count($impresionArray); $i++) {
                $impresionArray[$i]['historiaClinica_id'] = $hc->id;
                $cie10Id = CIE10::select('id')->where('descripcion', $impresionArray[$i]['diagnostico'])->first();
                if ($cie10Id != null){
                    $impresionArray[$i]['cie10_id'] = $cie10Id['id'];
                }
                ImpresionDiagnostica::create($impresionArray[$i]);
            }
        }


        //-----------------------------Resultado--------------------------
        $json['resultado']['historiaClinica_id'] = $hc->id;
        Resultado::create($json['resultado']);

        return array("mensaje" => "Se ha registrado la historia clinica", "historia" => $hc->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
        $historia = HistoriaClinica::with([
            /* 'habitos',
             'antecedentes',
             'revisionporsistema',
             'manipulacionalimentos',
             'resultado',
             'examenfisico',
             'paraclinicos',
             'paraclinicosdatos',
             'impresiondiagnostica',
             'cargoanterior',
             'cargoactual',
             'empresa',*/
             'accidente',
             'enfermedad'
             ])->find($id);
             return $historia;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegisterHistoriaCRequest $request, $id)
    {

        //--------------------------historia clinica--------------------------
        $json = $request->all();
        $hc = HistoriaClinica::select('id')->where('id', decrypt($id))->first();
        $historiaClinica = HistoriaClinica::findOrFail($hc)[0];
        $empresa = Empresa::select('id')->where('nombre',$json['cargos']['empresa'])->get();
        $paciente = Patient::where('documento', $json['documento'])->get();
        $json['empresa_id'] = $empresa[0]->id;
        $paciente[0]->cargo = $json['cargoEvaluar'];
        $paciente[0]->save();
        $historiaClinica->update($json);

        //-----------------------------Ocupacionales--------------------------
        if ($json['cargos']['fecha'] == ""){
            $json['cargos']['fecha'] = "";
        }else{
            $json['cargos']['fecha'] = date('Y-m-d', strtotime($json['cargos']['fecha']));
        }
        if ($json['cargos']['fecha'] == "31-12-1969"){
            $json['cargos']['fecha'] = "";
        }
        $historiaClinica->cargoactual()->update($json['cargos']);

        if (isset($json['factores'])){
            $factoresArray = $json['factores'];
            $historiaClinica->cargoanterior()->delete();
            for ($i=0; $i < count($factoresArray); $i++) {
                $factoresArray[$i]['historiaClinica_id'] = $hc->id;
                Cargo::create($factoresArray[$i]);
            }
        }

        //-----------------------------Laboral--------------------------

        if (isset($json['accidentes'])){
            $accidentesArray = $json['accidentes'];
            $historiaClinica->accidentes()->delete();
            for ($i=0; $i < count($accidentesArray); $i++) {
                $accidentesArray[$i]['historiaclinica_id'] = $hc->id;
                $accidentesArray[$i]['paciente_id'] = $paciente[0]->id;
                AccidenteTrabajo::create($accidentesArray[$i]);
            }
        }else{
            $historiaClinica->accidentes()->delete();
        }

        if (isset($json['enfermedades'])){
            $enfermedadesArray = $json['enfermedades'];
            $historiaClinica->enfermedades()->delete();
            for ($i=0; $i < count($enfermedadesArray); $i++) {
                $enfermedadesArray[$i]['historiaclinica_id'] = $hc->id;
                $enfermedadesArray[$i]['paciente_id'] = $paciente[0]->id;
                EnfermedadProfesional::create($enfermedadesArray[$i]);
            }
        }else{
            $historiaClinica->enfermedades()->delete();
        }

        //-----------------------------Habitos--------------------------
        $historiaClinica->habitos()->update($json['habitos']);

        //-----------------------------Antecedentes--------------------------
        $historiaClinica->antecedentes()->update($json['antecedentes']);

        //-------------------------Revision por sistemas---------------------
        $historiaClinica->revisionporsistema()->update($json['revisionporsistemas']);

        //-----------------------------Examen fisico--------------------------
        $historiaClinica->examenfisico()->update($json['examenfisico']);

        //-----------------------------Manipulacion alimentos--------------------------
        $historiaClinica->manipulacionalimentos()->update($json['manipulacionalimentos']);

        //-----------------------------Paraclinicos--------------------------
        if (isset($json['paraclinicos'])){
            $paraclinicosArray = $json['paraclinicos'];
            $historiaClinica->paraclinicos()->delete();
            for ($i=0; $i < count($paraclinicosArray); $i++) {
                if (($paraclinicosArray[$i]['examenLaboratorio_id'] == -1)){
                    continue;
                }
                $paraclinicosArray[$i]['historiaClinica_id'] = $hc->id;
                Paraclinico::create($paraclinicosArray[$i]);
            }
        }else{
            $historiaClinica->paraclinicos()->delete();
        }

        $historiaClinica->paraclinicosdatos()->update($json['paraclinicosdatos']);

        //-----------------------------Impresion diag--------------------------
        if(isset($json['idiagnostica'])) {
            $impresionArray = $json['idiagnostica'];
            $historiaClinica->impresiondiagnostica()->delete();
            for ($i = 0; $i < count($impresionArray); $i++) {
                $impresionArray[$i]['historiaClinica_id'] = $hc->id;
                $cie10Id = CIE10::select('id')->where('descripcion', $impresionArray[$i]['diagnostico'])->first();
                if ($cie10Id != null) {
                    $impresionArray[$i]['cie10_id'] = $cie10Id['id'];
                }
                ImpresionDiagnostica::create($impresionArray[$i]);
            }
        } else{
            $historiaClinica->impresiondiagnostica()->delete();
        }
        //-----------------------------Resultado--------------------------
        $historiaClinica->resultado()->update($json['resultado']);

        return array("mensaje" => "Se ha actualizado la historia clinica correctamente");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateWithoutecrypt(RegisterHistoriaCRequest $request, $id)
    {

        //--------------------------historia clinica--------------------------
        $json = $request->all();
        $hc = HistoriaClinica::select('id')->where('id', ($id))->first();
        $historiaClinica = HistoriaClinica::findOrFail($hc)[0];
        $empresa = Empresa::select('id')->where('nombre', $json['cargos']['empresa'])->get();
        $paciente = Patient::where('documento', $json['documento'])->get();
        $json['empresa_id'] = $empresa[0]->id;
        $paciente[0]->cargo = $json['cargoEvaluar'];
        $paciente[0]->save();
        if ($request->has("medicoCierre_id")) {
            $firma = User::select('firma')->where('id', $request->input("medicoCierre_id"))->first();
            $json['firmaMedico'] = $firma['firma'];
        }
        $historiaClinica->update($json);

        //-----------------------------Ocupacionales--------------------------
        if ($json['cargos']['fecha'] == "") {
            $json['cargos']['fecha'] = "";
        } else {
            $json['cargos']['fecha'] = date('Y-m-d', strtotime($json['cargos']['fecha']));
        }
        if ($json['cargos']['fecha'] == "31-12-1969"){
            $json['cargos']['fecha'] = "";
        }
        $historiaClinica->cargoactual()->update($json['cargos']);

        if (isset($json['factores'])) {
            $factoresArray = $json['factores'];
            $historiaClinica->cargoanterior()->delete();
            for ($i = 0; $i < count($factoresArray); $i++) {
                $factoresArray[$i]['historiaClinica_id'] = $hc->id;
                Cargo::create($factoresArray[$i]);
            }
        }

        //-----------------------------Laboral--------------------------

        if (isset($json['accidentes'])) {
            $accidentesArray = $json['accidentes'];
            $historiaClinica->accidentes()->delete();
            for ($i = 0; $i < count($accidentesArray); $i++) {
                $accidentesArray[$i]['historiaclinica_id'] = $hc->id;
                $accidentesArray[$i]['paciente_id'] = $paciente[0]->id;
                AccidenteTrabajo::create($accidentesArray[$i]);
            }
        } else {
            $historiaClinica->accidentes()->delete();
        }

        if (isset($json['enfermedades'])) {
            $enfermedadesArray = $json['enfermedades'];
            $historiaClinica->enfermedades()->delete();
            for ($i = 0; $i < count($enfermedadesArray); $i++) {
                $enfermedadesArray[$i]['historiaclinica_id'] = $hc->id;
                $enfermedadesArray[$i]['paciente_id'] = $paciente[0]->id;
                EnfermedadProfesional::create($enfermedadesArray[$i]);
            }
        } else {
            $historiaClinica->enfermedades()->delete();
        }

        //-----------------------------Habitos--------------------------
        $historiaClinica->habitos()->update($json['habitos']);

        //-----------------------------Antecedentes--------------------------
        $historiaClinica->antecedentes()->update($json['antecedentes']);

        //-------------------------Revision por sistemas---------------------
        $historiaClinica->revisionporsistema()->update($json['revisionporsistemas']);

        //-----------------------------Examen fisico--------------------------
        $historiaClinica->examenfisico()->update($json['examenfisico']);

        //-----------------------------Manipulacion alimentos--------------------------
        $historiaClinica->manipulacionalimentos()->update($json['manipulacionalimentos']);

        //-----------------------------Paraclinicos--------------------------
        if (isset($json['paraclinicos'])) {
            $paraclinicosArray = $json['paraclinicos'];
            $historiaClinica->paraclinicos()->delete();
            for ($i = 0; $i < count($paraclinicosArray); $i++) {
                if (($paraclinicosArray[$i]['examenLaboratorio_id'] == -1)){
                    continue;
                }
                $paraclinicosArray[$i]['historiaClinica_id'] = $hc->id;
                Paraclinico::create($paraclinicosArray[$i]);
            }
        } else {
            $historiaClinica->paraclinicos()->delete();
        }

        $historiaClinica->paraclinicosdatos()->update($json['paraclinicosdatos']);

        //-----------------------------Impresion diag--------------------------
        if (isset($json['idiagnostica'])) {
            $impresionArray = $json['idiagnostica'];
            $historiaClinica->impresiondiagnostica()->delete();
            for ($i = 0; $i < count($impresionArray); $i++) {
                $impresionArray[$i]['historiaClinica_id'] = $hc->id;
                $cie10Id = CIE10::select('id')->where('descripcion', $impresionArray[$i]['diagnostico'])->first();
                if ($cie10Id != null) {
                    $impresionArray[$i]['cie10_id'] = $cie10Id['id'];
                }
                ImpresionDiagnostica::create($impresionArray[$i]);
            }
        } else {
            $historiaClinica->impresiondiagnostica()->delete();
        }
        //-----------------------------Resultado--------------------------
        $historiaClinica->resultado()->update($json['resultado']);

        return array("mensaje" => "Se ha actualizado la historia clinica correctamente");
    }

    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

    public function getHC(Request $request){
	    $tipo = $tipo = $request->has('tipo') ? 1 : 0;
        $id = $request->input('id');
        $historia = HistoriaClinica::with([
            'habitos',
            'antecedentes',
            'revisionporsistema',
            'manipulacionalimentos',
            'resultado',
            'examenfisico',
            'paraclinicos',
            'paraclinicosdatos',
            'impresiondiagnostica',
            'cargoanterior',
            'cargoactual',
            'empresa',
            'enfermedades',
            'accidentes',
            "medicoapertura"
            ])->find($id);

        if ($tipo = 1){
            if($historia != null){
                return ["mensaje" => "Historia encontrada", "historia" => $historia];
            }else{
                return $historia;
            }
        }else{
            return view('historiaClinica.historiaAbierta', compact('historia'));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHCWeb($id){

        $historia = HistoriaClinica::with([
            'medicoapertura',
            'medicocierre',
            'habitos',
            'antecedentes',
            'revisionporsistema',
            'manipulacionalimentos',
            'resultado',
            'examenfisico',
            'paraclinicos',
            'paraclinicosdatos',
            'impresiondiagnostica.cie10',
            'cargoanterior',
            'cargoactual',
            'empresa',
            'enfermedades',
            'accidentes',
            'notaEvolucion',
            'nuevavaloracion'
        ])->find(decrypt($id));
        $examenesLab = ExamenLaboratorio::all();
        if ($historia->estado == 'Abierta'){
            return view('historiaClinica.historiaAbierta', compact('historia','examenesLab'));
        }else{
            return view('historiaClinica.historiaCerrada', compact('historia','examenesLab'));
        }
//        return $historia;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
	public function buscarCIE10(Request $request){

//        $request->validate([
//            'buscarCIE10' => 'exists:hc_cie10,codigo'
//        ],[
//            'buscarCIE10.exists' => 'No existe una prueba asociada a este codigo'
//        ]);

	    $codigo = $request->input('buscarCIE10');

        $CIE10 = CIE10::select('id', 'descripcion')
            ->where('codigo', $codigo)
            ->first();
	    if ($CIE10 == null){
            return array('mensaje' => 'No se encontraron resultados');
        }
	    return $CIE10;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function autocompleteDiagnostico(Request $request){
        $diagnostico = $request->input('phrase');
        $cie10 = CIE10::select('descripcion', 'codigo')->where('descripcion', 'like', $diagnostico."%")
            ->get();
        return $cie10;
    }

    /**
     * @param $id
     */
    public function cerrarHistoriaClinica(Request $request, $id){
        $medico = $request->input('medicoCierre');
        $firma = User::select('firma')->where('id', $medico)->first();
        HistoriaClinica::where('id', decrypt($id))
            ->update([
                'estado' => 'Cerrada',
                'medicoCierre_id' => $medico,
                'firmaMedico' => $firma['firma']
            ]);
    }

    /**
     * @param $id
     */
    public function cerrarHistoriaClinicaApp(Request $request, $id)
    {
        $medico = $request->input('medicoCierre_id');
        $firma = User::select('firma')->where('id', $medico)->first();
        HistoriaClinica::where('id', ($id))
            ->update([
                'estado' => 'Cerrada',
                'medicoCierre_id' => $medico,
                'firmaMedico' => $firma['firma']
            ]);
        return ["mensaje" => "Se ha cerrado la historia clinica"];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function anularHistoriaClinica(Request $request){

	    $id = $request->input('llave');
        $medico = $request->input('medicoCierre');
        HistoriaClinica::where('id', decrypt($id))
            ->update([
                'estado' => 'Anulada',
                'medicoAnula_id' => $medico
                ]);
        return array("mensaje" => "Se ha anulado la historia clinica");
    }

    /**
     * @param Request $request
     * @return array
     */
    public function crearNotaEvolucion(Request $request){
        $json = $request->all();
        if (isset($json)){
            $hc_id = decrypt($json['historiaclinica_id']);
            $historia = HistoriaClinica::findOrFail($hc_id);

            //Esta decision registra las notas de evolucion, o las actualiza si ya hay notas registradas.
            //Tipo 0 = no hay notas registradas. Por lo que se realiza la acción de registro
            //Tipo = 1 la historia tiene notas. Por lo que se actualizan.
            if (isset($json['notas'])){
                $notas = $json['notas'];
                if ($historia->nota == 0){
                    for ($i = 0; $i < count($json['notas']); $i++){
                        $notas[$i]['historiaclinica_id'] = $hc_id;
                        NotaEvolucion::create($notas[$i]);
                        HistoriaClinica::where('id', $hc_id)->update(['nota' => 1]);
                    }
                }else{
                    for ($i = 0; $i < count($json['notas']); $i++){
                        $notas[$i]['historiaclinica_id'] = $hc_id;
                        NotaEvolucion::create($notas[$i]);
                    }
                }
            }
            if ($request->input('nuevaValoracionSN') == 'Si' && $historia->nuevaValoracion == 0){
                $json['historiaclinica_id'] = $hc_id;
                NuevaValoracion::create($json);
                HistoriaClinica::where('id', $hc_id)->update(['nuevaValoracion' => 1]);
                if (isset($json['notas'])){
                    return array('mensaje' => 'La nota de evolución y la nueva valoración han sido creadas.', 'tipo' => 0);
                }else{
                    return array('mensaje' => 'La nueva valoración ha sido creada.', 'tipo' => 1);
                }
            }
            if (isset($json['notas'])){
                return array('mensaje' => 'La nota de evolución ha sido creada.', 'tipo' => 2);
            }else{
                return array('mensaje' => 'No hay cambios por guardar.', 'tipo' => 3);
            }
        } else{
            return array('mensaje' => 'Ha ocurrido un problema. Intentalo nuevamente.', 'tipo' => 4);
        }
    }

    /**
     * @return mixed
     */
    public function export(Request $request)
    {
        $request->validate([
                        'fechaDesde' => 'required',
                        'fechaHasta' => 'required',
                        'tipo_historia' => 'required'
                    ],[
                        'fechaDesde.required' => 'El campo fecha desde es obligatorio.',
                        'fechaHasta.required' => 'El campo fecha hasta es obligatorio.',
                        'tipo_historia.required' => 'El campo tipo de historia es obligatorio.',
                    ]);
                $fecha_desde = date('Y-m-d', strtotime($request->input('fechaDesde')));
                $fecha_hasta = date('Y-m-d', strtotime($request->input('fechaHasta')));
                $tipo_historia = $request->input('tipo_historia');
        ini_set("memory_limit",'-1');
        ini_set("max_execution_time",'-1');
                if ($tipo_historia == 'Historia clínica'){
                    $historiaExport = new HistoriaClinicaExport($fecha_desde, $fecha_hasta);
                    return Excel::download($historiaExport, 'historiaclinica_'.$fecha_desde.'_'.$fecha_hasta.'.xlsx');

        }else if($tipo_historia == 'Historia optometría'){
                    $optometriaExport = new OptometriaExport($fecha_desde, $fecha_hasta);
                    return Excel::download($optometriaExport, 'optometriaBD_'.$fecha_desde.'_'.$fecha_hasta.'.xlsx');
        }else if ($tipo_historia == 'Historia audiometría'){
                    $audiometriaExport = new AudiometriaExport($fecha_desde, $fecha_hasta);
                    return Excel::download($audiometriaExport, 'audiometriaBD_'.$fecha_desde.'_'.$fecha_hasta.'.xlsx');
        }else if ($tipo_historia == 'Historia visiometría'){
                    $visiometriaExport = new VisiometriaExport($fecha_desde, $fecha_hasta);
                    return Excel::download($visiometriaExport, 'visiometriaBD_'.$fecha_desde.'_'.$fecha_hasta.'.xlsx');
        }
     }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewVistaAdministrativa(){
        $profesionales = User::select('nombre_completo')
            ->where('role_id',2)
            ->orWhere('role_id', 4)
            ->orWhere('role_id', 5)
            ->get();
        return view('historiaClinica.vistaAdministrativa', compact('profesionales'));
    }

    public function listarAtencionesCriterio($campoBD, $dato, $criterio){

        if ($criterio != "fecha"){
            $hclinica = DB::table('historiaclinica as hc')
                ->select(DB::raw('hc.id as id, hc.paciente_cedula as cedula, hc.paciente as nombre_paciente, hc.fecha as fecha, e.nombre as empresa, hc.estado as estado, "Historia Clínica" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoApertura_id')
                ->where($campoBD, $dato);

            $haudiometria = DB::table('historiaaudiometria as ha')
                ->select(DB::raw('ha.id as id, ha.paciente_cedula as cedula, ha.paciente as nombre_paciente, ha.fecha as fecha, e.nombre as empresa, ha.estado as estado, "Historia Audiometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoApertura_id')
                ->where($campoBD, $dato);

            $hoptometria = DB::table('historiaoptometria as ho')
                ->select(DB::raw('ho.id as id, ho.paciente_cedula as cedula, ho.paciente as nombre_paciente, ho.fecha as fecha, e.nombre as empresa, ho.estado as estado, "Historia Optometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoapertura_id')
                ->where($campoBD, $dato);

            $hvisiometria = DB::table('historiavisiometria as hv')
                ->select(DB::raw('hv.id as id, hv.paciente_cedula as cedula, hv.paciente as nombre_paciente, hv.fecha as fecha, e.nombre as empresa, hv.estado as estado, "Historia Visiometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoapertura_id')
                ->where($campoBD, $dato);

            $historias = $hclinica->union($haudiometria)->union($hoptometria)->union($hvisiometria);
            $result = DB::table(DB::raw("({$historias->toSql()}) as historias"))
                ->select(DB::raw('id, cedula, nombre_paciente, fecha, empresa, estado, tipo'))
                ->mergeBindings($historias)
                ->orderBy('fecha', 'DESC')
                ->paginate(20);
        }else{
            $hclinica = DB::table('historiaclinica as hc')
                ->select(DB::raw('hc.id as id, hc.paciente_cedula as cedula, hc.paciente as nombre_paciente, hc.created_at as fecha, e.nombre as empresa, hc.estado as estado, "Historia Clínica" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoApertura_id')
                ->whereDate($campoBD, $dato);

            $haudiometria = DB::table('historiaaudiometria as ha')
                ->select(DB::raw('ha.id as id, ha.paciente_cedula as cedula, ha.paciente as nombre_paciente, ha.created_at as fecha, e.nombre as empresa, ha.estado as estado, "Historia Audiometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoApertura_id')
                ->whereDate($campoBD, $dato);

            $hoptometria = DB::table('historiaoptometria as ho')
                ->select(DB::raw('ho.id as id, ho.paciente_cedula as cedula, ho.paciente as nombre_paciente, ho.created_at as fecha, e.nombre as empresa, ho.estado as estado, "Historia Optometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoapertura_id')
                ->whereDate($campoBD, $dato);

            $hvisiometria = DB::table('historiavisiometria as hv')
                ->select(DB::raw('hv.id as id, hv.paciente_cedula as cedula, hv.paciente as nombre_paciente, hv.created_at as fecha, e.nombre as empresa, hv.estado as estado, "Historia Visiometría" as tipo'))
                ->join('empresa as e', 'e.id', '=', 'empresa_id')
                ->join('users as u', 'u.id', '=', 'medicoapertura_id')
                ->whereDate($campoBD, $dato);

            $historias = $hclinica->union($haudiometria)->union($hoptometria)->union($hvisiometria);
            $result = DB::table(DB::raw("({$historias->toSql()}) as historias"))
                ->select(DB::raw('id, cedula, nombre_paciente, fecha, empresa, estado, tipo'))
                ->mergeBindings($historias)
                ->orderBy('fecha', 'DESC')
                ->paginate(20);
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function atencionesPorCriterio(Request $request){
        $criterio = $request->input('criterio');
        $result = "";
        $profesionales = User::select('nombre_completo')
            ->where('role_id',2)
            ->orWhere('role_id', 4)
            ->orWhere('role_id', 5)
            ->get();

        if ($criterio == 'profesional'){
            $medico = $request->input('profesional_select');
            $result = $this->listarAtencionesCriterio("u.nombre_completo", $medico, $criterio);
            if (isset($result)){
                if (count($result) > 0){
                    return view('historiaClinica.vistaAdministrativa', compact('result', 'profesionales', 'criterio', 'medico'));
                }else{
                    return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
                }
            } else{
                return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
            }
        }else if ($criterio == 'fecha'){
            $fecha = date('Y-m-d', strtotime($request->input('fecha_atencion')));
            $result = $this->listarAtencionesCriterio("fecha", $fecha, $criterio);
            if (isset($result)){
                if (count($result) > 0){
                    return view('historiaClinica.vistaAdministrativa', compact('result', 'profesionales', 'criterio', 'fecha'));
                }else{
                    return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
                }
            } else{
                return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
            }
        }else if ($criterio == 'estado'){
            $estado = $request->input('estado_select');
            $result = $this->listarAtencionesCriterio("estado", $estado, $criterio);
            if (isset($result)){
                if (count($result) > 0){
                    return view('historiaClinica.vistaAdministrativa', compact('result', 'profesionales', 'criterio', 'estado'));
                }else{
                    return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
                }
            } else{
                return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
            }

        } else if ($criterio == 'empresa'){
            $empresa = $request->input('empresa_select');
            $empresa_id = Empresa::select('id')->where("nombre", $empresa)->get();
            $result = $this->listarAtencionesCriterio("empresa_id", $empresa_id[0]->id, $criterio);
            if (isset($result)){
                if (count($result) > 0){
                    return view('historiaClinica.vistaAdministrativa', compact('result', 'profesionales', 'criterio', 'empresa'));
                }else{
                    return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
                }
            } else{
                return back()->with('nores', 'No se ha encontrado resultados para el criterio seleccionado.');
            }
        }else{
            return back()->with('nores', 'No se han encontrado resultados para el criterio seleccionado.');
        }
    }
    /**
    +     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    +     */
    public function viewExportDataBase(){
            return view('Exportacion.exportacionBD');
    }
}
