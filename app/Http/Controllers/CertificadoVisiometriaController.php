<?php

namespace App\Http\Controllers;

use App\HistoriaVisiometria;
use File;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CertificadoVisiometriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generateCertificate(Request $request, $id){
        $historia = HistoriaVisiometria::find($id);
        $template = new TemplateProcessor(public_path()."/historiaVisiometria.docx");

        $template->setValue('fecha', $historia->created_at);
        $template->setValue('motivoevaluacion', $historia->motivoevaluacion);

        $otrosME = $historia->motivoevaluacion_otro;
        if ($otrosME != "") {
            $otrosME = str_replace(",", ", ", substr($historia->motivoevaluacion_otro, 0, -1) . ".");
        }

        $template->setValue('motivoevaluacion_otros', $otrosME);
        $photoPatient = $historia->Paciente->foto;
        if (isset($photoPatient)) {
            $image = str_replace('data:image/png;base64,', '', $photoPatient);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            // Load the image
            $source = @imagecreatefromjpeg(public_path() . '/temp/img/' . $imagePatient);
            if (!$source){

            }else{
                $rotate = imagerotate($source, -90, 0);

                imagejpeg($rotate, public_path() . '/temp/img/' . $imagePatient);
                $template->setImageValue("foto",array("path" => public_path('/temp/img/' . $imagePatient),"width"=>240,"height"=>158));
            }

        }

        //------Identificacion trabajador
        $template->setValue('foto', $photoPatient);
        $template->setValue('nombre_completo', $historia->paciente);
        $template->setValue('genero', $historia->Paciente->genero);
        $template->setValue('documento', $historia->paciente_cedula);
        $template->setValue('direccion', $historia->Paciente->direccionDomicilio);
        $template->setValue('eps', $historia->Paciente->eps);
        $template->setValue('afp', $historia->Paciente->afp);
        $template->setValue('arl', $historia->Paciente->arl);
        $nombreContactoCasoEmergencia = $historia->Paciente->nombreAcompaniante . " " . $historia->Paciente->apellidoAcompaniante;
        $template->setValue('nombre_acompanante', $nombreContactoCasoEmergencia);
        $template->setValue('telefonoacompanante', $historia->Paciente->celularAcompaniante);

        //-----Identificacion empresa-----
        $template->setValue('nombre_empresa', $historia->empresa->nombre);
        $template->setValue('nit', $historia->empresa->nit);

        //-----Antecedentes personales-----
        $template->setValue('diabetes', $historia->antecedente->diabetes);
        $template->setValue('diabetes_obs', $historia->antecedente->diabetes_obs);
        $template->setValue('hipertension', $historia->antecedente->hipertension);
        $template->setValue('hipertension_obs', $historia->antecedente->hipertension_obs);
        $template->setValue('alergicos', $historia->antecedente->alergicos);
        $template->setValue('alergicos_obs', $historia->antecedente->alergicos_obs);
        $template->setValue('tiroides', $historia->antecedente->alteraciontiroides);
        $template->setValue('tiroides_obs', $historia->antecedente->alteraciontiroides_obs);
        $template->setValue('cardiacos', $historia->antecedente->problemascardiacos);
        $template->setValue('cardiacos_obs', $historia->antecedente->problemascardiacos_obs);
        $template->setValue('otros_personales', $historia->antecedente->personales_otros);
        $template->setValue('otros_personales_obs', $historia->antecedente->personales_otros_obs);

        //---Antecedentes oculares
        $template->setValue('usu_rx', $historia->antecedente->usuariosrx);
        $template->setValue('usu_rx_obs', $historia->antecedente->usuariosrx_obs);
        $template->setValue('quirur', $historia->antecedente->quirurgicos);
        $template->setValue('quirur_obs', $historia->antecedente->quirurgicos_obs);
        $template->setValue('trauma', $historia->antecedente->trauma);
        $template->setValue('trauma_obs', $historia->antecedente->trauma_obs);
        $template->setValue('esquir', $historia->antecedente->esquirlas);
        $template->setValue('esquir_obs', $historia->antecedente->esquirlas_obs);
        $template->setValue('quimic', $historia->antecedente->quimicos);
        $template->setValue('quimic_obs', $historia->antecedente->quimicos_obs);
        $template->setValue('glauco', $historia->antecedente->glaucoma);
        $template->setValue('glauco_obs', $historia->antecedente->glaucoma_obs);
        $template->setValue('rvisual', $historia->antecedente->rehabilitacionvisual);
        $template->setValue('rvisual_obs', $historia->antecedente->rehabilitacionvisual_obs);
        $template->setValue('catarata', $historia->antecedente->catarata);
        $template->setValue('catarata_obs', $historia->antecedente->catarata_obs);
        $template->setValue('ao_otros', $historia->antecedente->oculares_otros);
        $template->setValue('ao_otros_obs', $historia->antecedente->oculares_otros_obs);

        //------Sintomas----------
        $template->setValue('vblej', $historia->sintoma->visionborrosalejana);
        $template->setValue('vdoble', $historia->sintoma->visiondoble);
        $template->setValue('ardor', $historia->sintoma->ardor);
        $template->setValue('vbcer', $historia->sintoma->visionborrosacercana);
        $template->setValue('cefal', $historia->sintoma->cefalea);
        $template->setValue('nrefie', $historia->sintoma->norefiere);
        $template->setValue('fotof', $historia->sintoma->fotofobia);
        $template->setValue('enroj', $historia->sintoma->enrojecimiento);
        $template->setValue('docul', $historia->sintoma->dolorocular);
        $template->setValue('sueno', $historia->sintoma->suenoleer);

        //------Riesgo del cargo a evaluar------
        $template->setValue('trauma', $historia->riesgocargoevaluar->trauma);
        $template->setValue('expo_qs', $historia->riesgocargoevaluar->exposicionquimicossolventes);
        $template->setValue('radiacion', $historia->riesgocargoevaluar->radiacionesionizantes);
        $template->setValue('ex_gv', $historia->riesgocargoevaluar->exposiciongasesvapores);
        $template->setValue('exp_mat', $historia->riesgocargoevaluar->exposicionmaterialparticulado);
        $template->setValue('exp_vt', $historia->riesgocargoevaluar->exposicionvideoterminales);
        $template->setValue('exp_mp', $historia->riesgocargoevaluar->exposicionmaterialproyeccion);
        $template->setValue('rad_ni', $historia->riesgocargoevaluar->radiacionesnoionizantes);
        $template->setValue('ilumina', $historia->riesgocargoevaluar->iluminacion);
        $template->setValue('otros_rc', $historia->riesgocargoevaluar->otros);

        //------Agudeza visual---------
        $template->setValue('lsc_od', $historia->agudezavisual->lejana_sincorreccion_ojoderecho);
        $template->setValue('lsc_oi', $historia->agudezavisual->lejana_sincorreccion_ojoizquierdo);
        $template->setValue('lsc_binocular', $historia->agudezavisual->lejana_sincorreccion_binocular);
        $template->setValue('lcc_od', $historia->agudezavisual->lejana_concorreccion_ojoderecho);
        $template->setValue('lcc_oi', $historia->agudezavisual->lejana_concorreccion_ojoizquierdo);
        $template->setValue('lcc_binocular', $historia->agudezavisual->lejana_concorreccion_binocular);
        $template->setValue('csc_od', $historia->agudezavisual->cercana_sincorreccion_ojoderecho);
        $template->setValue('csc_oi', $historia->agudezavisual->cercana_sincorreccion_ojoizquierdo);
        $template->setValue('csc_binocular', $historia->agudezavisual->cercana_sincorreccion_binocular);
        $template->setValue('ccc_od', $historia->agudezavisual->cercana_concorreccion_ojoderecho);
        $template->setValue('ccc_oi', $historia->agudezavisual->cercana_concorreccion_ojoizquierdo);
        $template->setValue('ccc_binocular', $historia->agudezavisual->cercana_concorreccion_binocular);

        $template->setValue('lsc_od_obs', $historia->agudezavisual->lejana_sincorreccion_ojoderecho_obs);
        $template->setValue('lsc_oi_obs', $historia->agudezavisual->lejana_sincorreccion_ojoizquierdo_obs);
        $template->setValue('lsc_binocular_obs', $historia->agudezavisual->lejana_sincorreccion_binocular_obs);
        $template->setValue('lcc_od_obs', $historia->agudezavisual->lejana_concorreccion_ojoderecho_obs);
        $template->setValue('lcc_oi_obs', $historia->agudezavisual->lejana_concorreccion_ojoizquierdo_obs);
        $template->setValue('lcc_binocular_obs', $historia->agudezavisual->lejana_concorreccion_binocular_obs);
        $template->setValue('csc_od_obs', $historia->agudezavisual->cercana_sincorreccion_ojoderecho_obs);
        $template->setValue('csc_oi_obs', $historia->agudezavisual->cercana_sincorreccion_ojoizquierdo_obs);
        $template->setValue('csc_binocular_obs', $historia->agudezavisual->cercana_sincorreccion_binocular_obs);
        $template->setValue('ccc_od_obs', $historia->agudezavisual->cercana_concorreccion_ojoderecho_obs);
        $template->setValue('ccc_oi_obs', $historia->agudezavisual->cercana_concorreccion_ojoizquierdo_obs);
        $template->setValue('ccc_binocular_obs', $historia->agudezavisual->cercana_concorreccion_binocular_obs);

        $template->setValue('motilidad', $historia->agudezavisual->segmentoanterior_motilidadocular);
        $template->setValue('vision_color', $historia->agudezavisual->segmentoanterior_visioncolor);
        $template->setValue('vision_profundidad', $historia->agudezavisual->segmentoanterior_visionprofundidad);
        $template->setValue('fondo_od', $historia->agudezavisual->segmentoanterior_impresiondiagnostica_ojoderecho);
        $template->setValue('fondo_oi', $historia->agudezavisual->segmentoanterior_impresiondiagnostica_ojoizquierdo);
        $template->setValue('motilidad_obs', $historia->agudezavisual->segmentoanterior_motilidadocular_obs);
        $template->setValue('vision_color_obs', $historia->agudezavisual->segmentoanterior_visioncolor_obs);
        $template->setValue('vision_profundidad_obs', $historia->agudezavisual->segmentoanterior_visionprofundidad_obs);
        $template->setValue('fondo_od_obs', $historia->agudezavisual->segmentoanterior_impresiondiagnostica_ojoderecho_obs);
        $template->setValue('fondo_oi_obs', $historia->agudezavisual->segmentoanterior_impresiondiagnostica_ojoizquierdo_obs);
        //conducta y recomendaciones

        //------Conducta y recomendaciones------
        $template->setValue('ergonomiavisual', $historia->recomendacion->ergonomiavisual);
        $template->setValue('pautashigienevisual', $historia->recomendacion->pautashigienevisual);
        $template->setValue('higienevisual', $historia->recomendacion->higienevisual);
        $template->setValue('correccionopticapermanente', $historia->recomendacion->correccionopticapermanente);
        $template->setValue('correcionopticaprolongada', $historia->recomendacion->correccionopticavisualprologanda);
        $template->setValue('correccionopticaactual', $historia->recomendacion->correccionopticaactual);
        $template->setValue('correccionopticavisioncercana', $historia->recomendacion->correccionopticavisualcercana);
        $template->setValue('elementosproteccionvisual', $historia->recomendacion->usoelementosproteccionvisual);
        $template->setValue('proteccionvisualcorreccionoptica', $historia->recomendacion->proteccionvisualcorreccionoptica);
        $template->setValue('usogafas', $historia->recomendacion->usogafasfiltrouv);
        $template->setValue('controlanual', $historia->recomendacion->controlanual);
        $template->setValue('controloptometria', $historia->recomendacion->controloptometria);
        $template->setValue('controloftalmologia', $historia->recomendacion->controloftalmologia);
        $template->setValue('nousocorreccion', $historia->recomendacion->norequiereusocorreccionoptica);
        $template->setValue('otrasconductas', $historia->recomendacion->otrasconductas);


        //------Firmas-----------

        $template->setValue('nombremedico', $historia->medicocierre->nombre_completo);
        $template->setValue('cocumentomedico', $historia->medicocierre->document);
        $template->setValue('licencia', $historia->medicocierre->licencia);

        //-----------Exportar pdf-----------
        if (isset($historia->firmaMedico)){
            $image = str_replace('data:image/png;base64,', '', $historia->firmaMedico);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmamedico", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmamedico","");
        }

        if (isset($historia->firmaPaciente)){
            $image = str_replace('data:image/png;base64,', '', $historia->firmaPaciente);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmapaciente", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmapaciente","");
        }
        $template->saveAs(public_path() . '/generados_hc/' . $historia->Paciente->documento . '-visiometria.docx');
        $process = new Process("export HOME=/tmp && libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $historia->Paciente->documento . '-visiometria.docx');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        //exec("libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $paciente->documento . '.docx');

        return response()->download(public_path($historia->Paciente->documento.'-visiometria.pdf'));

    }

    public function textosEnter($texto1, $texto2)
    {

        $textFinal = $texto1 . '<w:br/>' . $texto2;

        return $textFinal;
    }

    public function view()
    {

        return view('visiometria.certificate');
    }
}
