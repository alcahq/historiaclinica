<?php

namespace App\Http\Controllers;

use App\HistoriaAudiometria;
use App\HistoriaClinica;
use App\HistoriaOptometria;
use App\HistoriaVisiometria;
use App\Http\Requests\RegisterPatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use App\Patient;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;

class PatientController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('paciente.register');
    }

    /**
     * Store a newly created resource in storage.
     * Se encarga de registrar el paciente
     * @param  RegisterPatientRequest  $request
     * @return Response
     */
    public function store(RegisterPatientRequest $request)
    {
        $requestData = $request->all();
        $requestData['fechaNacimiento'] = date('Y-m-d', strtotime($requestData['fechaNacimiento']));
        $requestData['edad'] = Carbon::parse($requestData['fechaNacimiento'])->age;
        $requestData['nombre_completo'] = $requestData['nombre'].' '.$requestData['apellido'];
        Patient::create($requestData);
        return back()->with('info','Paciente registrado exitosamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdatePatientRequest $request, $id)
    {
        $paciente = $request->all();
        $paciente['nombre_completo'] = $paciente['nombre']." ".$paciente['apellido'];
        $paciente['fechaNacimiento'] = date('Y-m-d', strtotime($paciente['fechaNacimiento']));
        $paciente['edad'] = Carbon::parse($paciente['fechaNacimiento'])->age;
        Patient::findOrFail(decrypt($id))->update($paciente);
        return back()->with('infoedit', 'Paciente actualizado exitosamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paciente = Patient::findOrFail(decrypt($id));
        $paciente['fechaNacimiento'] = date('d-m-Y', strtotime($paciente['fechaNacimiento']));
        return view('paciente.edit', compact('paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchById(Request $request)
    {
        $id = $request->input('id');
        $paciente = Patient::findOrFail($id);
        return $paciente;
    }

    /**
     * Metodo que busca un paciente por su numero de documento.
     * @param  \Illuminate\Http\Request $request
     * @return
     */
    public function findByDocumentOrName(Request $request){

        $paciente_buscado = $request->input('buscar_paciente');
        $tipo = $request->has('tipo') ? 1 : 0;
        if ($paciente_buscado == ""){
            if($tipo == 1){
                return Patient::simplePaginate(3);
            }else{
                return back();
            }
        }
        if(is_numeric($paciente_buscado)){
            if($tipo == 1){
                $pacientes = Patient::where('documento', $paciente_buscado)
                    ->simplePaginate(3);
                return $pacientes;
            }else{
                $pacientes = Patient::select('id','documento','nombre','apellido')->where('documento', $paciente_buscado)
                    ->paginate(10);
                return view('paciente.list', compact('pacientes'));
            }
        }else{
            if($tipo == 1){
                $pacientes = Patient::where('nombre_completo', 'like', '%'.$paciente_buscado.'%')
                    ->simplePaginate(3);
                return $pacientes;
            }else{
                $pacientes = Patient::select('id','documento','nombre','apellido')->where('nombre_completo', 'like', '%'.$paciente_buscado.'%')
                    ->paginate(10);
                return view('paciente.list', compact('pacientes'));
            }
        }


    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewList(){
        return view('paciente.list');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function findByDocument(Request $request){
        $request->validate([
            'documento' => 'exists:paciente,documento|regex:/^[0-9]{5,15}$/'
        ],[
            'documento.exists' => 'No existe un paciente asociado a este número de documento.',
            'documento.regex' => 'El campo número de documento es invalido.'
        ]);
        $documento = $request->input('documento');
        if ($documento == ""){
            return back();
        }
        $paciente = Patient::select('nombre','apellido','documento', 'genero','firma','id','edad','fechaNacimiento','cargo')
            ->where('documento', $documento)
            ->first();
        $paciente['edad'] = Carbon::parse($paciente['fechaNacimiento'])->age;
        return $paciente;
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllHistories(Request $request){
        $documento = $request->input('documento');
        $tipo = $request->has('tipo') ? 1 : 0;

        $historiaclinica = HistoriaClinica::selectRaw('id, fecha, paciente_cedula, paciente, "Historia Clinica" as tipo, estado')->where('paciente_cedula',$documento)->where('estado','<>','Anulada')->orderBy('fecha', 'desc')->get();
        $historiaaudiometria = HistoriaAudiometria::selectRaw('id, fecha, paciente_cedula, paciente, "Historia Audiometria" as tipo,estado')->where('paciente_cedula',$documento)->where('estado','<>','Anulada')->orderBy('fecha', 'desc')->get();
        $historiaoptometria = HistoriaOptometria::selectRaw('id, fecha, paciente_cedula, paciente, "Historia Optometria" as tipo,estado')->where('paciente_cedula',$documento)->where('estado','<>','Anulada')->orderBy('fecha', 'desc')->get();
        $historiavisiometria = HistoriaVisiometria::selectRaw('id, fecha, paciente_cedula, paciente, "Historia Visiometria" as tipo,estado')->where('paciente_cedula',$documento)->where('estado','<>','Anulada')->orderBy('fecha', 'desc')->get();

        if ($tipo == 1){
            $result = array_merge($historiaclinica->toArray(), $historiaaudiometria->toArray(), $historiaoptometria->toArray(),$historiavisiometria->toArray());
            return ["historiaclinica" => $historiaclinica, "historiaoptometria" => $historiaoptometria, "historiaaudiometria" => $historiaaudiometria, "historiavisiometria" => $historiavisiometria, ];
        }else{
            return view('paciente.consultarHistorias', compact('historiaclinica','historiaaudiometria','historiaoptometria','historiavisiometria'));
        }
    }

    public function viewHistorias(){
        return view('paciente.consultarHistorias');
    }


}
