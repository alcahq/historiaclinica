<?php

namespace App\Http\Controllers;

use App\Constantes_agudezavisual;
use App\Empresa;
use App\HO_AgudezaVisual;
use App\HO_Antecedentes;
use App\HO_Recomendaciones;
use App\Http\Requests\HistoriaOptometriaRequest;
use App\Patient;
use App\RiesgoCargoEvaluar;
use App\Sintomas;
use Illuminate\Http\Request;

class VisiometriaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $constantes_agudezavisual = Constantes_agudezavisual::all();
        $empresas = Empresa::all();
        return view('visiometria.register', compact('empresas', 'constantes_agudezavisual'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoriaOptometriaRequest $request) {
        $json = $request->all();
        $motivoevaluacion = isset($json["motivoEvaluacion"]) ? $json["motivoEvaluacion"] : "";
        $motivoevaluacion_otros = isset($json["motivoEvaluacion_otros"]) ? $json["motivoEvaluacion_otros"] : "";
        $medicoapertura_id = isset($json["medicoapertura_id"]) ? $json["medicoapertura_id"] : 1;
        $documento = $json["documento"];
        $paciente_id = Patient::select('id', 'firma')->where('documento',$documento)->first();

        $lensometria_add = $json['lensometria_add'];
        $lensometria_ojoderecho = $json['lensometria_ojoderecho'];
        $lensometria_ojoizquierdo = $json['lensometria_ojoizquierdo'];
        $lensometria_tipolente = $json['lensometria_tipolente'];

        $retinoscopia_add = $json['retinoscopia_add'];
        $retinoscopia_ojoderecho = $json['retinoscopia_ojoderecho'];
        $retinoscopia_ojoizquierdo = $json['retinoscopia_ojoizquierdo'];

        $segmentoanterior_camaraanterior = $json['segmentoanterior_camaraanterior'];
        $segmentoanterior_camaraanterior_obs = $json['segmentoanterior_camaraanterior_obs'];
        $segmentoanterior_cejas = $json['segmentoanterior_cejas'];
        $segmentoanterior_cejas_obs = $json['segmentoanterior_cejas_obs'];
        $segmentoanterior_conjuntiva = $json['segmentoanterior_conjuntiva'];
        $segmentoanterior_conjuntiva_obs = $json['segmentoanterior_conjuntiva_obs'];
        $segmentoanterior_cornea = $json['segmentoanterior_cornea'];
        $segmentoanterior_cornea_obs = $json['segmentoanterior_cornea_obs'];
        $segmentoanterior_esclerotica = $json['segmentoanterior_esclerotica'];
        $segmentoanterior_esclerotica_obs = $json['segmentoanterior_esclerotica_obs'];
        $segmentoanterior_fondoojoderecho = $json['segmentoanterior_fondoojoderecho'];
        $segmentoanterior_fondoojoderecho_obs = $json['segmentoanterior_fondoojoderecho_obs'];
        $segmentoanterior_fondoojoizquierdo = $json['segmentoanterior_fondoojoizquierdo'];
        $segmentoanterior_fondoojoizquierdo_obs = $json['segmentoanterior_fondoojoizquierdo_obs'];
        $segmentoanterior_iris = $json['segmentoanterior_iris'];
        $segmentoanterior_iris_obs = $json['segmentoanterior_iris_obs'];
        $segmentoanterior_motilidadocular = $json['segmentoanterior_motilidadocular'];
        $segmentoanterior_motilidadocular_obs = $json['segmentoanterior_motilidadocular_obs'];
        $segmentoanterior_parpados = $json['segmentoanterior_parpados'];
        $segmentoanterior_parpados_obs = $json['segmentoanterior_parpados_obs'];
        $segmentoanterior_pestanas = $json['segmentoanterior_pestanas'];
        $segmentoanterior_pestanas_obs = $json['segmentoanterior_pestanas_obs'];
        $segmentoanterior_pupilas = $json['segmentoanterior_pupilas'];
        $segmentoanterior_pupilas_obs = $json['segmentoanterior_pupilas_obs'];
        $segmentoanterior_viaslagrimales = $json['segmentoanterior_viaslagrimales'];
        $segmentoanterior_viaslagrimales_obs = $json['segmentoanterior_viaslagrimales_obs'];
        $segmentoanterior_visioncolor = $json['segmentoanterior_visioncolor'];
        $segmentoanterior_visioncolor_obs = $json['segmentoanterior_visioncolor_obs'];
        $segmentoanterior_visionprofundidad = $json['segmentoanterior_visionprofundidad'];
        $segmentoanterior_visionprofundidad_obs = $json['segmentoanterior_visionprofundidad_obs'];

        $agudezavisual_db = $this->ho_agudezavisual($json);
        $antecedentes = $this->ho_antecedentes($json);
        $recomendacion = $this->ho_recomendacion($json);
        $sintomas = $this->sintoma($json);
        $riesgocargoevaluar = $this->ho_riesgocargoevaluar($json);

        $historiaOptometria = new HistoriaOptometria();
        $historiaOptometria->fecha = $json['fecha'];
        $historiaOptometria->paciente_id = $paciente_id->id;
        $historiaOptometria->firmapaciente = $paciente_id->firma;
        $historiaOptometria->medicoapertura_id = $medicoapertura_id;
        $historiaOptometria->motivoevaluacion = $motivoevaluacion;
        $historiaOptometria->motivoevaluacion_otro = $motivoevaluacion_otros;

        $historiaOptometria->lensometria_tipolente = $lensometria_tipolente;
        $historiaOptometria->lensometria_ojoderecho = $lensometria_ojoderecho;
        $historiaOptometria->lensometria_ojoizquierdo = $lensometria_ojoizquierdo;
        $historiaOptometria->lensometria_add = $lensometria_add;

        $historiaOptometria->retinoscopia_ojoderecho = $retinoscopia_ojoderecho;
        $historiaOptometria->retinoscopia_ojoizquierdo = $retinoscopia_ojoizquierdo;
        $historiaOptometria->retinoscopia_add = $retinoscopia_add;

        $historiaOptometria->segmentoanterior_camaraanterior = $segmentoanterior_camaraanterior;
        $historiaOptometria->segmentoanterior_camaraanterior_obs = $segmentoanterior_camaraanterior_obs;
        $historiaOptometria->segmentoanterior_cejas = $segmentoanterior_cejas;
        $historiaOptometria->segmentoanterior_cejas_obs = $segmentoanterior_cejas_obs;
        $historiaOptometria->segmentoanterior_conjuntiva = $segmentoanterior_conjuntiva;
        $historiaOptometria->segmentoanterior_conjuntiva_obs = $segmentoanterior_conjuntiva_obs;
        $historiaOptometria->segmentoanterior_cornea = $segmentoanterior_cornea;
        $historiaOptometria->segmentoanterior_cornea_obs = $segmentoanterior_cornea_obs;
        $historiaOptometria->segmentoanterior_esclerotica = $segmentoanterior_esclerotica;
        $historiaOptometria->segmentoanterior_esclerotica_obs = $segmentoanterior_esclerotica_obs;
        $historiaOptometria->segmentoanterior_fondoojoderecho = $segmentoanterior_fondoojoderecho;
        $historiaOptometria->segmentoanterior_fondoojoderecho_obs = $segmentoanterior_fondoojoderecho_obs;
        $historiaOptometria->segmentoanterior_fondoojoizquierdo = $segmentoanterior_fondoojoizquierdo;
        $historiaOptometria->segmentoanterior_fondoojoizquierdo_obs = $segmentoanterior_fondoojoizquierdo_obs;
        $historiaOptometria->segmentoanterior_iris = $segmentoanterior_iris;
        $historiaOptometria->segmentoanterior_iris_obs = $segmentoanterior_iris_obs;
        $historiaOptometria->segmentoanterior_motilidadocular = $segmentoanterior_motilidadocular;
        $historiaOptometria->segmentoanterior_motilidadocular_obs = $segmentoanterior_motilidadocular_obs;
        $historiaOptometria->segmentoanterior_parpados = $segmentoanterior_parpados;
        $historiaOptometria->segmentoanterior_parpados_obs = $segmentoanterior_parpados_obs;
        $historiaOptometria->segmentoanterior_pestanas = $segmentoanterior_pestanas;
        $historiaOptometria->segmentoanterior_pestanas_obs = $segmentoanterior_pestanas_obs;
        $historiaOptometria->segmentoanterior_pupilas = $segmentoanterior_pupilas;
        $historiaOptometria->segmentoanterior_pupilas_obs = $segmentoanterior_pupilas_obs;
        $historiaOptometria->segmentoanterior_viaslagrimales = $segmentoanterior_viaslagrimales;
        $historiaOptometria->segmentoanterior_viaslagrimales_obs = $segmentoanterior_viaslagrimales_obs;
        $historiaOptometria->segmentoanterior_visioncolor = $segmentoanterior_visioncolor;
        $historiaOptometria->segmentoanterior_visioncolor_obs = $segmentoanterior_visioncolor_obs;
        $historiaOptometria->segmentoanterior_visionprofundidad = $segmentoanterior_visionprofundidad;
        $historiaOptometria->segmentoanterior_visionprofundidad_obs = $segmentoanterior_visionprofundidad_obs;
        $historiaOptometria->save();
        $historiaOptometria->agudezavisual()->save($agudezavisual_db);
        $historiaOptometria->antecedente()->save($antecedentes);
        $historiaOptometria->recomendacion()->save($recomendacion);
        $historiaOptometria->sintoma()->save($sintomas);
        $historiaOptometria->riesgocargoevaluar()->save($riesgocargoevaluar);

        return $historiaOptometria;

    }

    function ho_riesgocargoevaluar($json) {
        $riesgocargoevaluar_exposiciongasesvapores = $json["riesgocargoevaluar_exposiciongasesvapores"];
        $riesgocargoevaluar_exposicionmaterialparticulado = $json["riesgocargoevaluar_exposicionmaterialparticulado"];
        $riesgocargoevaluar_exposicionmaterialproyeccion = $json["riesgocargoevaluar_exposicionmaterialproyeccion"];
        $riesgocargoevaluar_exposicionquimicossolventes = $json["riesgocargoevaluar_exposicionquimicossolventes"];
        $riesgocargoevaluar_exposicionvideoterminales = $json["riesgocargoevaluar_exposicionvideoterminales"];
        $riesgocargoevaluar_iluminacion = $json["riesgocargoevaluar_iluminacion"];
        $riesgocargoevaluar_radiacionesionizantes = $json["riesgocargoevaluar_radiacionesionizantes"];
        $riesgocargoevaluar_radiacionesnoionizantes = $json["riesgocargoevaluar_radiacionesnoionizantes"];
        $riesgocargoevaluar_otros = $json["riesgocargoevaluar_otros"];
        $riesgocargoevaluar_trauma = $json["riesgocargoevaluar_trauma"];

        $riesgo = new RiesgoCargoEvaluar();
        $riesgo->trauma = $riesgocargoevaluar_trauma;
        $riesgo->exposicionmaterialparticulado = $riesgocargoevaluar_exposicionmaterialparticulado;
        $riesgo->iluminacion = $riesgocargoevaluar_iluminacion;
        $riesgo->exposicionvideoterminales = $riesgocargoevaluar_exposicionvideoterminales;
        $riesgo->exposicionquimicossolventes = $riesgocargoevaluar_exposicionquimicossolventes;
        $riesgo->exposiciongasesvapores = $riesgocargoevaluar_exposiciongasesvapores;
        $riesgo->radiacionesionizantes = $riesgocargoevaluar_radiacionesionizantes;
        $riesgo->radiacionesnoionizantes = $riesgocargoevaluar_radiacionesnoionizantes;
        $riesgo->otros = $riesgocargoevaluar_otros;
        return $riesgo;
    }

    function sintoma($json) {
        $sintomas_visionborrosalejana = $json["sintomas_visionborrosalejana"];
        $sintomas_visionborroscercana = $json["sintomas_visionborrosacercana"];
        $sintomas_fotofobia = $json["sintomas_fotofobia"];
        $sintomas_dolorocular = $json["sintomas_dolorocular"];
        $sintomas_visiondoble = $json["sintomas_visiondoble"];
        $sintomas_cefalea = $json["sintomas_cefalea"];
        $sintomas_enrojecimiento = $json["sintomas_enrojecimiento"];
        $sintomas_suenoleer = $json["sintomas_suenoleer"];
        $sintomas_ardor = $json["sintomas_ardor"];
        $sintomas_norefiere = $json["sintomas_norefiere"];

        $sintomas = new Sintomas();
        $sintomas->visionborrosalejana = $sintomas_visionborrosalejana;
        $sintomas->visionborrosacercana = $sintomas_visionborroscercana;
        $sintomas->fotofobia = $sintomas_fotofobia;
        $sintomas->dolorocular = $sintomas_dolorocular;
        $sintomas->visiondoble = $sintomas_visiondoble;
        $sintomas->cefalea = $sintomas_cefalea;
        $sintomas->enrojecimiento = $sintomas_enrojecimiento;
        $sintomas->suenoleer = $sintomas_suenoleer;
        $sintomas->ardor = $sintomas_ardor;
        $sintomas->norefiere = $sintomas_norefiere;
        return $sintomas;
    }

    function ho_antecedentes($json) {
        $antecedentes_diabetes = $json["antecedentes_diabetes"];
        $antecedentes_diabetes_obs = $json["antecedentes_diabetes_obs"];
        $antecedentes_problemascardiacos = $json["antecedentes_problemascardiacos"];
        $antecedentes_problemascardiacos_obs = $json["antecedentes_problemascardiacos_obs"];
        $antecedentes_alergicos = $json["antecedentes_alergicos"];
        $antecedentes_alergicos_obs = $json["antecedentes_alergicos_obs"];
        $antecedentes_hipertension = $json["antecedentes_hipertension"];
        $antecedentes_hipertension_obs = $json["antecedentes_hipertension_obs"];
        $antecedentes_alteraciontiroides = $json["antecedentes_alteraciontiroides"];
        $antecedentes_alteraciontiroides_obs = $json["antecedentes_alteraciontiroides_obs"];
        $antecedentes_personales_otros = $json["antecedentes_personales_otros"];
        $antecedentes_personales_otros_obs = $json["antecedentes_personales_otros_obs"];
        $antecedentes_usuariosRX = $json["antecedentes_usuariosrx"];
        $antecedentes_usuariosRX_obs = $json["antecedentes_usuariosrx_obs"];
        $antecedentes_glaucoma = $json["antecedentes_glaucoma"];
        $antecedentes_glaucoma_obs = $json["antecedentes_glaucoma_obs"];
        $antecedentes_quirurgicos = $json["antecedentes_quirurgicos"];
        $antecedentes_quirurgicos_obs = $json["antecedentes_quirurgicos_obs"];
        $antecedentes_rehabilitacionvisual = $json["antecedentes_rehabilitacionvisual"];
        $antecedentes_rehabilitacionvisual_obs = $json["antecedentes_rehabilitacionvisual_obs"];
        $antecedentes_trauma = $json["antecedentes_trauma"];
        $antecedentes_trauma_obs = $json["antecedentes_trauma_obs"];
        $antecedentes_catarata = $json["antecedentes_catarata"];
        $antecedentes_catarata_obs = $json["antecedentes_catarata_obs"];
        $antecedentes_esquirlas = $json["antecedentes_esquirlas"];
        $antecedentes_esquirlas_obs = $json["antecedentes_esquirlas_obs"];
        $antecedentes_oculares_otros = $json["antecedentes_oculares_otros"];
        $antecedentes_oculares_otros_obs = $json["antecedentes_oculares_otros_obs"];
        $antecedentes_quimicos = $json["antecedentes_quimicos"];
        $antecedentes_quimicos_obs = $json["antecedentes_quimicos_obs"];

        $antecedentes = new HO_Antecedentes();
        $antecedentes->diabetes = $antecedentes_diabetes;
        $antecedentes->diabetes_obs = $antecedentes_diabetes_obs;
        $antecedentes->problemascardiacos = $antecedentes_problemascardiacos;
        $antecedentes->problemascardiacos_obs = $antecedentes_problemascardiacos_obs;
        $antecedentes->alergicos = $antecedentes_alergicos;
        $antecedentes->alergicos_obs = $antecedentes_alergicos_obs;
        $antecedentes->hipertension = $antecedentes_hipertension;
        $antecedentes->hipertension_obs = $antecedentes_hipertension_obs;
        $antecedentes->alteraciontiroides = $antecedentes_alteraciontiroides;
        $antecedentes->alteraciontiroides_obs = $antecedentes_alteraciontiroides_obs;
        $antecedentes->personales_otros = $antecedentes_personales_otros;
        $antecedentes->personales_otros_obs = $antecedentes_personales_otros_obs;
        $antecedentes->usuariosrx = $antecedentes_usuariosRX;
        $antecedentes->usuariosrx_obs = $antecedentes_usuariosRX_obs;
        $antecedentes->glaucoma = $antecedentes_glaucoma;
        $antecedentes->glaucoma_obs = $antecedentes_glaucoma_obs;
        $antecedentes->quirurgicos = $antecedentes_quirurgicos;
        $antecedentes->quirurgicos_obs = $antecedentes_quirurgicos_obs;
        $antecedentes->rehabilitacionvisual = $antecedentes_rehabilitacionvisual;
        $antecedentes->rehabilitacionvisual_obs = $antecedentes_rehabilitacionvisual_obs;
        $antecedentes->trauma = $antecedentes_trauma;
        $antecedentes->trauma_obs = $antecedentes_trauma_obs;
        $antecedentes->catarata = $antecedentes_catarata;
        $antecedentes->catarata_obs = $antecedentes_catarata_obs;
        $antecedentes->esquirlas = $antecedentes_esquirlas;
        $antecedentes->esquirlas_obs = $antecedentes_esquirlas_obs;
        $antecedentes->oculares_otros = $antecedentes_oculares_otros;
        $antecedentes->oculares_otros_obs = $antecedentes_oculares_otros_obs;
        $antecedentes->quimicos = $antecedentes_quimicos;
        $antecedentes->quimicos_obs = $antecedentes_quimicos_obs;

        return $antecedentes;
    }

    function ho_recomendacion($json) {
        $recomendaciones_ergonomiavisual_obs = $json["recomendaciones_ergonomiavisual_obs"];
        $recomendaciones_pautashigienevisual_obs = $json["recomendaciones_pautashigienevisual_obs"];
        $recomendaciones_higienevisual_obs = $json["recomendaciones_higienevisual_obs"];
        $recomendaciones_correccionopticapermanente_obs = $json["recomendaciones_correccionopticapermanente_obs"];
        $recomendaciones_correccionopticavisionprolongada_obs = $json["recomendaciones_correccionopticavisionprolongada_obs"];
        $recomendaciones_correccionopticaactual_obs = $json["recomendaciones_correccionopticaactual_obs"];
        $recomendaciones_correccionopticavisioncercana_obs = $json["recomendaciones_correccionopticavisioncercana_obs"];
        $recomendaciones_usoelementosproteccionvisual_obs = $json["recomendaciones_usoelementosproteccionvisual_obs"];
        $recomendaciones_proteccionvisualcorreccionoptica_obs = $json["recomendaciones_proteccionvisualcorreccionoptica_obs"];
        $recomendaciones_usogafasfiltrouv_obs = $json["recomendaciones_usogafasfiltrouv_obs"];
        $recomendaciones_controlanual_obs = $json["recomendaciones_controlanual_obs"];
        $recomendaciones_controloptometria_obs = $json["recomendaciones_controloptometria_obs"];
        $recomendaciones_controloftalmologia_obs = $json["recomendaciones_controloftalmologia_obs"];
        $recomendaciones_norequiereusocorreccionoptica_obs = $json["recomendaciones_norequiereusocorreccionoptica_obs"];
        $recomendaciones_otrasconductas_obs = $json["recomendaciones_otrasconductas_obs"];
        $recomendaciones_ojoderecho = isset($json["recomendaciones_ojoderecho"]) ? $json["recomendaciones_ojoderecho"] : "";
        $recomendaciones_ojoizquierdo = isset($json["recomendaciones_ojoizquierdo"]) ? $json["recomendaciones_ojoizquierdo"] : "";
        $recomendaciones_pacientecompatible_obs = $json["recomendaciones_pacientecompatible_obs"];
        $recomendaciones_requierevaloracion_obs = $json["recomendaciones_requierevaloracion_obs"];
        $recomendaciones_requiereremision_obs = $json["recomendaciones_requiereremision_obs"];

        $recomendaciones_ergonomiavisual = isset($json["recomendaciones_ergonomiavisual"]) ? $json["recomendaciones_ergonomiavisual"] : "";
        $recomendaciones_pautashigienevisual = isset($json["recomendaciones_pautashigienevisual"]) ? $json["recomendaciones_pautashigienevisual"] : "";
        $recomendaciones_higienevisual = isset($json["recomendaciones_higienevisual"]) ? $json["recomendaciones_higienevisual"] : "";
        $recomendaciones_correccionopticapermanente = isset($json["recomendaciones_correccionopticapermanente"]) ? $json["recomendaciones_correccionopticapermanente"] : "";
        $recomendaciones_correccionopticavisionprolongada = isset($json["recomendaciones_correccionopticavisionprolongada"]) ? $json["recomendaciones_correccionopticavisionprolongada"] : "";
        $recomendaciones_correccionopticaactual = isset($json["recomendaciones_correccionopticaactual"]) ? $json["recomendaciones_correccionopticaactual"] : "";
        $recomendaciones_correccionopticavisioncercana = isset($json["recomendaciones_correccionopticavisioncercana"]) ? $json["recomendaciones_correccionopticavisioncercana"] : "";
        $recomendaciones_usoelementosproteccionvisual = isset($json["recomendaciones_usoelementosproteccionvisual"]) ? $json["recomendaciones_usoelementosproteccionvisual"] : "";
        $recomendaciones_proteccionvisualcorreccionoptica = isset($json["recomendaciones_proteccionvisualcorreccionoptica"]) ? $json["recomendaciones_proteccionvisualcorreccionoptica"] : "";
        $recomendaciones_usogafasfiltrouv = isset($json["recomendaciones_usogafasfiltrouv"]) ? $json["recomendaciones_usogafasfiltrouv"] : "";
        $recomendaciones_controlanual = isset($json["recomendaciones_controlanual"]) ? $json["recomendaciones_controlanual"] : "";
        $recomendaciones_controloptometria = isset($json["recomendaciones_controloptometria"]) ? $json["recomendaciones_controloptometria"] : "";
        $recomendaciones_controloftalmologia = isset($json["recomendaciones_controloftalmologia"]) ? $json["recomendaciones_controloftalmologia"] : "";
        $recomendaciones_norequiereusocorreccionoptica = isset($json["recomendaciones_norequiereusocorreccionoptica"]) ? $json["recomendaciones_norequiereusocorreccionoptica"] : "";
        $recomendaciones_otrasconductas = isset($json["recomendaciones_otrasconductas"]) ? $json["recomendaciones_otrasconductas"] : "";
        $recomendaciones_pacientecompatible = isset($json["recomendaciones_pacientecompatible"]) ? $json["recomendaciones_pacientecompatible"] : "";
        $recomendaciones_requierevaloracion = isset($json["recomendaciones_requierevaloracion"]) ? $json["recomendaciones_requierevaloracion"] : "";
        $recomendaciones_requiereremision = isset($json["recomendaciones_requiereremision"]) ? $json["recomendaciones_requiereremision"] : "";
        $recomendaciones_recomendacion = isset($json["recomendaciones_recomendacion"]) ? $json["recomendaciones_recomendacion"] : "";

        $recomendacion = new HO_Recomendaciones();
        $recomendacion->ergonomiavisual = $recomendaciones_ergonomiavisual;
        $recomendacion->ergonomiavisual_obs = $recomendaciones_ergonomiavisual_obs;
        $recomendacion->pautashigienevisual = $recomendaciones_pautashigienevisual;
        $recomendacion->pautashigienevisual_obs = $recomendaciones_pautashigienevisual_obs;
        $recomendacion->higienevisual = $recomendaciones_higienevisual;
        $recomendacion->higienevisual_obs = $recomendaciones_higienevisual_obs;
        $recomendacion->correccionopticapermanente = $recomendaciones_correccionopticapermanente;
        $recomendacion->correccionopticapermanente_obs = $recomendaciones_correccionopticapermanente_obs;
        $recomendacion->correccionopticavisionprolongada = $recomendaciones_correccionopticavisionprolongada;
        $recomendacion->correccionopticavisionprolongada_obs = $recomendaciones_correccionopticavisionprolongada_obs;
        $recomendacion->correccionopticaactual = $recomendaciones_correccionopticaactual;
        $recomendacion->correccionopticaactual_obs = $recomendaciones_correccionopticaactual_obs;
        $recomendacion->correccionopticavisioncercana = $recomendaciones_correccionopticavisioncercana;
        $recomendacion->correccionopticavisioncercana_obs = $recomendaciones_correccionopticavisioncercana_obs;
        $recomendacion->usoelementosproteccionvisual = $recomendaciones_usoelementosproteccionvisual;
        $recomendacion->usoelementosproteccionvisual_obs = $recomendaciones_usoelementosproteccionvisual_obs;
        $recomendacion->proteccionvisualcorreccionoptica = $recomendaciones_proteccionvisualcorreccionoptica;
        $recomendacion->proteccionvisualcorreccionoptica_obs = $recomendaciones_proteccionvisualcorreccionoptica_obs;
        $recomendacion->usogafasfiltrouv = $recomendaciones_usogafasfiltrouv;
        $recomendacion->usogafasfiltrouv_obs = $recomendaciones_usogafasfiltrouv_obs;
        $recomendacion->controlanual = $recomendaciones_controlanual;
        $recomendacion->controlanual_obs = $recomendaciones_controlanual_obs;
        $recomendacion->controloptometria = $recomendaciones_controloptometria;
        $recomendacion->controloptometria_obs = $recomendaciones_controloptometria_obs;
        $recomendacion->controloftalmologia = $recomendaciones_controloftalmologia;
        $recomendacion->controloftalmologia_obs = $recomendaciones_controloftalmologia_obs;
        $recomendacion->norequiereusocorreccionoptica = $recomendaciones_norequiereusocorreccionoptica;
        $recomendacion->norequiereusocorreccionoptica_obs = $recomendaciones_norequiereusocorreccionoptica_obs;
        $recomendacion->otrasconductas = $recomendaciones_otrasconductas;
        $recomendacion->otrasconductas_obs = $recomendaciones_otrasconductas_obs;
        $recomendacion->ojoderecho = $recomendaciones_ojoderecho;
        $recomendacion->ojoizquierdo = $recomendaciones_ojoizquierdo;
        $recomendacion->recomendacion = $recomendaciones_recomendacion;
        $recomendacion->pacientecompatible = $recomendaciones_pacientecompatible;
        $recomendacion->pacientecompatible_obs = $recomendaciones_pacientecompatible_obs;
        $recomendacion->requierevaloracion = $recomendaciones_requierevaloracion;
        $recomendacion->requierevaloracion_obs = $recomendaciones_requierevaloracion_obs;
        $recomendacion->requiereremision = $recomendaciones_requiereremision;
        $recomendacion->requiereremision_obs = $recomendaciones_requiereremision_obs;

        return $recomendacion;
    }

    function ho_agudezavisual($agudezavisual) {
        $lejana_concorreccion_ojobinocular = $agudezavisual['lejana_concorreccion_ojobinocular'];
        $lejana_concorreccion_ojobinocular_obs = $agudezavisual['lejana_concorreccion_ojobinocular_obs'];
        $lejana_concorreccion_ojoderecho = $agudezavisual['lejana_concorreccion_ojoderecho'];
        $lejana_concorreccion_ojoderecho_obs = $agudezavisual['lejana_concorreccion_ojoderecho_obs'];
        $lejana_concorreccion_ojoizquierdo = $agudezavisual['lejana_concorreccion_ojoizquierdo'];
        $lejana_concorreccion_ojoizquierdo_obs = $agudezavisual['lejana_concorreccion_ojoizquierdo_obs'];
        $lejana_sincorreccion_ojobinocular = $agudezavisual['lejana_sincorreccion_ojobinocular'];
        $lejana_sincorreccion_ojobinocular_obs = $agudezavisual['lejana_sincorreccion_ojobinocular_obs'];
        $lejana_sincorreccion_ojoderecho = $agudezavisual['lejana_sincorreccion_ojoderecho'];
        $lejana_sincorreccion_ojoderecho_obs = $agudezavisual['lejana_sincorreccion_ojoderecho_obs'];
        $lejana_sincorreccion_ojoizquierdo = $agudezavisual['lejana_sincorreccion_ojoizquierdo'];
        $lejana_sincorreccion_ojoizquierdo_obs = $agudezavisual['lejana_sincorreccion_ojoizquierdo_obs'];
        $cercana_concorreccion_ojobinocular = $agudezavisual['cercana_concorreccion_ojobinocular'];
        $cercana_concorreccion_ojobinocular_obs = $agudezavisual['cercana_concorreccion_ojobinocular_obs'];
        $cercana_concorreccion_ojoderecho = $agudezavisual['cercana_concorreccion_ojoderecho'];
        $cercana_concorreccion_ojoderecho_obs = $agudezavisual['cercana_concorreccion_ojoderecho_obs'];
        $cercana_concorreccion_ojoizquierdo = $agudezavisual['cercana_concorreccion_ojoizquierdo'];
        $cercana_concorreccion_ojoizquierdo_obs = $agudezavisual['cercana_concorreccion_ojoizquierdo_obs'];
        $cercana_sincorreccion_ojobinocular = $agudezavisual['cercana_sincorreccion_ojobinocular'];
        $cercana_sincorreccion_ojobinocular_obs = $agudezavisual['cercana_sincorreccion_ojobinocular_obs'];
        $cercana_sincorreccion_ojoderecho = $agudezavisual['cercana_sincorreccion_ojoderecho'];
        $cercana_sincorreccion_ojoderecho_obs = $agudezavisual['cercana_sincorreccion_ojoderecho_obs'];
        $cercana_sincorreccion_ojoizquierdo = $agudezavisual['cercana_sincorreccion_ojoizquierdo'];
        $cercana_sincorreccion_ojoizquierdo_obs = $agudezavisual['cercana_sincorreccion_ojoizquierdo_obs'];

        $agudezavisual_db = new HO_AgudezaVisual();
        $agudezavisual_db->ls_od = $lejana_sincorreccion_ojoderecho;
        $agudezavisual_db->ls_od_obs = $lejana_sincorreccion_ojoderecho_obs;
        $agudezavisual_db->ls_oi = $lejana_sincorreccion_ojoizquierdo;
        $agudezavisual_db->ls_oi_obs = $lejana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual_db->ls_ojobinocular = $lejana_sincorreccion_ojobinocular;
        $agudezavisual_db->ls_ojobinocular_obs = $lejana_sincorreccion_ojobinocular_obs;
        $agudezavisual_db->lc_od = $lejana_concorreccion_ojoderecho;
        $agudezavisual_db->lc_od_obs = $lejana_concorreccion_ojoderecho_obs;
        $agudezavisual_db->lc_oi = $lejana_concorreccion_ojoizquierdo;
        $agudezavisual_db->lc_oi_obs = $lejana_concorreccion_ojoizquierdo_obs;
        $agudezavisual_db->lc_ojobinocular = $lejana_concorreccion_ojobinocular;
        $agudezavisual_db->lc_ojobinocular_obs = $lejana_concorreccion_ojobinocular_obs;
        $agudezavisual_db->cs_od = $cercana_sincorreccion_ojoderecho;
        $agudezavisual_db->cs_od_obs = $cercana_sincorreccion_ojoderecho_obs;
        $agudezavisual_db->cs_oi = $cercana_sincorreccion_ojoizquierdo;
        $agudezavisual_db->cs_oi_obs = $cercana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual_db->cs_ojobinocular = $cercana_sincorreccion_ojobinocular;
        $agudezavisual_db->cs_ojobinocular_obs = $cercana_sincorreccion_ojobinocular_obs;
        $agudezavisual_db->cc_od = $cercana_concorreccion_ojoderecho;
        $agudezavisual_db->cc_od_obs = $cercana_concorreccion_ojoderecho_obs;
        $agudezavisual_db->cc_oi = $cercana_concorreccion_ojoizquierdo;
        $agudezavisual_db->cc_oi_obs = $cercana_concorreccion_ojoizquierdo_obs;
        $agudezavisual_db->cc_ojobinocular = $cercana_concorreccion_ojobinocular;
        $agudezavisual_db->cc_ojobinocular_obs = $cercana_concorreccion_ojobinocular_obs;
        return $agudezavisual_db;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $ho = HistoriaOptometria::with(['agudezavisual', 'antecedente', 'recomendacion', 'sintoma', 'riesgocargoevaluar', 'paciente'])->findOrFail($id);
        $constantes_agudezavisual = Constantes_agudezavisual::all();
        $empresas = Empresa::all();
        return view('optometria.view', compact('ho', 'empresas', 'constantes_agudezavisual'));
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
