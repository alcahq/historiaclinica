<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Exports\OptometriaExport;
use App\HistoriaOptometria;
use App\HO_AgudezaVisual;
use App\HO_Antecedentes;
use App\HO_Recomendaciones;
use App\Http\Requests\HistoriaOptometriaRequest;
use App\Patient;
use App\RiesgoCargoEvaluar;
use App\Sintomas;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use PDF;

class HistoriaOptometriaController extends Controller {

    public function __construct() {
		$this->middleware('auth');
    }

    public function home(){
        return view('home');
    }

    public function registerPatient(){
        return 'Registrando';
    }

    public function registerPatientView(){
        return view('paciente.register');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $empresas = Empresa::all();
        return view('optometria.register', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoriaOptometriaRequest $request) {
        $opto = $request->all();

        //Datos generales del paciente y empresa

        $documento = $opto["documento"];

        $empresa = Empresa::select('id')->where('nombre',$opto['empresa'])->get();
        $motivoevaluacion = isset($opto["motivoevaluacion"]) ? $opto["motivoevaluacion"] : "";
        $motivoevaluacion_otros = isset($opto["motivoevaluacion_otros"]) ? $opto["motivoevaluacion_otros"] : "";
        $medicoapertura_id = isset($opto["medicoApertura_id"]) ? $opto["medicoApertura_id"] :  $opto["medicoapertura_id"];
        $medicocierre_id = isset($opto["medicoApertura_id"]) ? $opto["medicoApertura_id"] : $opto["medicoapertura_id"];
        $paciente_id = Patient::select('id', 'firma', 'nombre_completo', 'edad')->where('documento',$documento)->first();
        $medico = User::select('firma')->find($medicocierre_id);

        $lensometria_add = $opto['lensometria_add'];
        $lensometria_ojoderecho = $opto['lensometria_ojoderecho'];
        $lensometria_ojoizquierdo = $opto['lensometria_ojoizquierdo'];
        $lensometria_tipolente = $opto['lensometria_tipolente'];

        $retinoscopia_add = $opto['retinoscopia_add'];
        $retinoscopia_ojoderecho = $opto['retinoscopia_ojoderecho'];
        $retinoscopia_ojoizquierdo = $opto['retinoscopia_ojoizquierdo'];

        $segmentoanterior_camaraanterior = $opto['segmentoanterior_camaraanterior'];
        $segmentoanterior_camaraanterior_obs = $opto['segmentoanterior_camaraanterior_obs'];
        $segmentoanterior_cejas = $opto['segmentoanterior_cejas'];
        $segmentoanterior_cejas_obs = $opto['segmentoanterior_cejas_obs'];
        $segmentoanterior_conjuntiva = $opto['segmentoanterior_conjuntiva'];
        $segmentoanterior_conjuntiva_obs = $opto['segmentoanterior_conjuntiva_obs'];
        $segmentoanterior_cornea = $opto['segmentoanterior_cornea'];
        $segmentoanterior_cornea_obs = $opto['segmentoanterior_cornea_obs'];
        $segmentoanterior_esclerotica = $opto['segmentoanterior_esclerotica'];
        $segmentoanterior_esclerotica_obs = $opto['segmentoanterior_esclerotica_obs'];
        $segmentoanterior_fondoojoderecho = $opto['segmentoanterior_fondoojoderecho'];
        $segmentoanterior_fondoojoderecho_obs = $opto['segmentoanterior_fondoojoderecho_obs'];
        $segmentoanterior_fondoojoizquierdo = $opto['segmentoanterior_fondoojoizquierdo'];
        $segmentoanterior_fondoojoizquierdo_obs = $opto['segmentoanterior_fondoojoizquierdo_obs'];
        $segmentoanterior_iris = $opto['segmentoanterior_iris'];
        $segmentoanterior_iris_obs = $opto['segmentoanterior_iris_obs'];
        $segmentoanterior_motilidadocular = $opto['segmentoanterior_motilidadocular'];
        $segmentoanterior_motilidadocular_obs = $opto['segmentoanterior_motilidadocular_obs'];
        $segmentoanterior_parpados = $opto['segmentoanterior_parpados'];
        $segmentoanterior_parpados_obs = $opto['segmentoanterior_parpados_obs'];
        $segmentoanterior_pestanas = $opto['segmentoanterior_pestanas'];
        $segmentoanterior_pestanas_obs = $opto['segmentoanterior_pestanas_obs'];
        $segmentoanterior_pupilas = $opto['segmentoanterior_pupilas'];
        $segmentoanterior_pupilas_obs = $opto['segmentoanterior_pupilas_obs'];
        $segmentoanterior_viaslagrimales = $opto['segmentoanterior_viaslagrimales'];
        $segmentoanterior_viaslagrimales_obs = $opto['segmentoanterior_viaslagrimales_obs'];
        $segmentoanterior_visioncolor = $opto['segmentoanterior_visioncolor'];
        $segmentoanterior_visioncolor_obs = $opto['segmentoanterior_visioncolor_obs'];
        $segmentoanterior_visionprofundidad = $opto['segmentoanterior_visionprofundidad'];
        $segmentoanterior_visionprofundidad_obs = $opto['segmentoanterior_visionprofundidad_obs'];
//
        $agudezavisual_db = $this->ho_agudezavisual($opto);
        $antecedentes = $this->ho_antecedentes($opto);
        $recomendacion = $this->ho_recomendacion($opto);
        $sintomas = $this->sintoma($opto);
        $riesgocargoevaluar = $this->ho_riesgocargoevaluar($opto);

        $historiaOptometria = new HistoriaOptometria();
        if (!isset($opto['fecha'])){
            $opto['fecha'] = Carbon::now('America/Bogota');
        }
        $historiaOptometria->fecha = $opto['fecha'];
        $historiaOptometria->estado = "Cerrada";
        $historiaOptometria->paciente_id = $paciente_id->id;
        $historiaOptometria->paciente_cedula = $documento;
        $historiaOptometria->paciente = $paciente_id->nombre_completo;
        $historiaOptometria->firmapaciente = $paciente_id->firma;
        $historiaOptometria->paciente_edad = $paciente_id->edad;
        $historiaOptometria->firmamedico = $medico->firma;
        $historiaOptometria->medicoapertura_id = $medicoapertura_id;
        $historiaOptometria->medicocierre_id = $medicoapertura_id;
        $historiaOptometria->motivoevaluacion = $motivoevaluacion;
        $historiaOptometria->motivoevaluacion_otro = $motivoevaluacion_otros;
        $historiaOptometria->cargo = $opto['cargo'];
        $historiaOptometria->empresa_id = $empresa[0]->id;

        $historiaOptometria->lensometria_tipolente = $lensometria_tipolente;
        $historiaOptometria->lensometria_ojoderecho = $lensometria_ojoderecho;
        $historiaOptometria->lensometria_ojoizquierdo = $lensometria_ojoizquierdo;
        $historiaOptometria->lensometria_add = $lensometria_add;

        $historiaOptometria->retinoscopia_ojoderecho = $retinoscopia_ojoderecho;
        $historiaOptometria->retinoscopia_ojoizquierdo = $retinoscopia_ojoizquierdo;
        $historiaOptometria->retinoscopia_add = $retinoscopia_add;

        $historiaOptometria->segmentoanterior_camaraanterior = $segmentoanterior_camaraanterior;
        $historiaOptometria->segmentoanterior_camaraanterior_obs = $segmentoanterior_camaraanterior_obs;
        $historiaOptometria->segmentoanterior_cejas = $segmentoanterior_cejas;
        $historiaOptometria->segmentoanterior_cejas_obs = $segmentoanterior_cejas_obs;
        $historiaOptometria->segmentoanterior_conjuntiva = $segmentoanterior_conjuntiva;
        $historiaOptometria->segmentoanterior_conjuntiva_obs = $segmentoanterior_conjuntiva_obs;
        $historiaOptometria->segmentoanterior_cornea = $segmentoanterior_cornea;
        $historiaOptometria->segmentoanterior_cornea_obs = $segmentoanterior_cornea_obs;
        $historiaOptometria->segmentoanterior_esclerotica = $segmentoanterior_esclerotica;
        $historiaOptometria->segmentoanterior_esclerotica_obs = $segmentoanterior_esclerotica_obs;
        $historiaOptometria->segmentoanterior_fondoojoderecho = $segmentoanterior_fondoojoderecho;
        $historiaOptometria->segmentoanterior_fondoojoderecho_obs = $segmentoanterior_fondoojoderecho_obs;
        $historiaOptometria->segmentoanterior_fondoojoizquierdo = $segmentoanterior_fondoojoizquierdo;
        $historiaOptometria->segmentoanterior_fondoojoizquierdo_obs = $segmentoanterior_fondoojoizquierdo_obs;
        $historiaOptometria->segmentoanterior_iris = $segmentoanterior_iris;
        $historiaOptometria->segmentoanterior_iris_obs = $segmentoanterior_iris_obs;
        $historiaOptometria->segmentoanterior_motilidadocular = $segmentoanterior_motilidadocular;
        $historiaOptometria->segmentoanterior_motilidadocular_obs = $segmentoanterior_motilidadocular_obs;
        $historiaOptometria->segmentoanterior_parpados = $segmentoanterior_parpados;
        $historiaOptometria->segmentoanterior_parpados_obs = $segmentoanterior_parpados_obs;
        $historiaOptometria->segmentoanterior_pestanas = $segmentoanterior_pestanas;
        $historiaOptometria->segmentoanterior_pestanas_obs = $segmentoanterior_pestanas_obs;
        $historiaOptometria->segmentoanterior_pupilas = $segmentoanterior_pupilas;
        $historiaOptometria->segmentoanterior_pupilas_obs = $segmentoanterior_pupilas_obs;
        $historiaOptometria->segmentoanterior_viaslagrimales = $segmentoanterior_viaslagrimales;
        $historiaOptometria->segmentoanterior_viaslagrimales_obs = $segmentoanterior_viaslagrimales_obs;
        $historiaOptometria->segmentoanterior_visioncolor = $segmentoanterior_visioncolor;
        $historiaOptometria->segmentoanterior_visioncolor_obs = $segmentoanterior_visioncolor_obs;
        $historiaOptometria->segmentoanterior_visionprofundidad = $segmentoanterior_visionprofundidad;
        $historiaOptometria->segmentoanterior_visionprofundidad_obs = $segmentoanterior_visionprofundidad_obs;
        $historiaOptometria->save();
        $historiaOptometria->agudezavisual()->save($agudezavisual_db);
        $historiaOptometria->antecedente()->save($antecedentes);
        $historiaOptometria->recomendacion()->save($recomendacion);
        $historiaOptometria->sintoma()->save($sintomas);
        $historiaOptometria->riesgocargoevaluar()->save($riesgocargoevaluar);
        if($historiaOptometria->id != null){
            return ["mensaje" => "Se ha registrado correctamente."];
        } else{
            return ["mensaje" => "Se ha encontrado un problema."];
        }
    }
    /**
     *
     */
    function ho_riesgocargoevaluar($opto) {
        $riesgocargoevaluar_exposiciongasesvapores = $opto["riesgocargoevaluar_exposiciongasesvapores"];
        $riesgocargoevaluar_exposicionmaterialparticulado = $opto["riesgocargoevaluar_exposicionmaterialparticulado"];
        $riesgocargoevaluar_exposicionmaterialproyeccion = $opto["riesgocargoevaluar_exposicionmaterialproyeccion"];
        $riesgocargoevaluar_exposicionquimicossolventes = $opto["riesgocargoevaluar_exposicionquimicossolventes"];
        $riesgocargoevaluar_exposicionvideoterminales = $opto["riesgocargoevaluar_exposicionvideoterminales"];
        $riesgocargoevaluar_iluminacion = $opto["riesgocargoevaluar_iluminacion"];
        $riesgocargoevaluar_radiacionesionizantes = $opto["riesgocargoevaluar_radiacionesionizantes"];
        $riesgocargoevaluar_radiacionesnoionizantes = $opto["riesgocargoevaluar_radiacionesnoionizantes"];
        $riesgocargoevaluar_otros = $opto["riesgocargoevaluar_otros"];
        $riesgocargoevaluar_trauma = $opto["riesgocargoevaluar_trauma"];

        $riesgo = new RiesgoCargoEvaluar();
        $riesgo->trauma = $riesgocargoevaluar_trauma;
        $riesgo->exposicionmaterialparticulado = $riesgocargoevaluar_exposicionmaterialparticulado;
        $riesgo->iluminacion = $riesgocargoevaluar_iluminacion;
        $riesgo->exposicionvideoterminales = $riesgocargoevaluar_exposicionvideoterminales;
        $riesgo->exposicionquimicossolventes = $riesgocargoevaluar_exposicionquimicossolventes;
        $riesgo->exposiciongasesvapores = $riesgocargoevaluar_exposiciongasesvapores;
        $riesgo->radiacionesionizantes = $riesgocargoevaluar_radiacionesionizantes;
        $riesgo->radiacionesnoionizantes = $riesgocargoevaluar_radiacionesnoionizantes;
        $riesgo->otros = $riesgocargoevaluar_otros;
        $riesgo->exposicionmaterialproyeccion = $riesgocargoevaluar_exposicionmaterialproyeccion;
        return $riesgo;
    }
    /**
     *
     */
    function sintoma($opto) {
        $sintomas_visionborrosalejana = $opto["sintomas_visionborrosalejana"];
        $sintomas_visionborroscercana = $opto["sintomas_visionborrosacercana"];
        $sintomas_fotofobia = $opto["sintomas_fotofobia"];
        $sintomas_dolorocular = $opto["sintomas_dolorocular"];
        $sintomas_visiondoble = $opto["sintomas_visiondoble"];
        $sintomas_cefalea = $opto["sintomas_cefalea"];
        $sintomas_enrojecimiento = $opto["sintomas_enrojecimiento"];
        $sintomas_suenoleer = $opto["sintomas_suenoleer"];
        $sintomas_ardor = $opto["sintomas_ardor"];
        $sintomas_norefiere = $opto["sintomas_norefiere"];
        $sintomas_otros = $opto['sintomas_otros'];

        $sintomas = new Sintomas();
        $sintomas->visionborrosalejana = $sintomas_visionborrosalejana;
        $sintomas->visionborrosacercana = $sintomas_visionborroscercana;
        $sintomas->fotofobia = $sintomas_fotofobia;
        $sintomas->dolorocular = $sintomas_dolorocular;
        $sintomas->visiondoble = $sintomas_visiondoble;
        $sintomas->cefalea = $sintomas_cefalea;
        $sintomas->enrojecimiento = $sintomas_enrojecimiento;
        $sintomas->suenoleer = $sintomas_suenoleer;
        $sintomas->ardor = $sintomas_ardor;
        $sintomas->norefiere = $sintomas_norefiere;
        $sintomas->otros = $sintomas_otros;
        return $sintomas;
    }
    /**
     *
     */
    function ho_antecedentes($opto) {
        $antecedentes_diabetes = $opto["antecedentes_diabetes"];
        $antecedentes_diabetes_obs = $opto["antecedentes_diabetes_obs"];
        $antecedentes_problemascardiacos = $opto["antecedentes_problemascardiacos"];
        $antecedentes_problemascardiacos_obs = $opto["antecedentes_problemascardiacos_obs"];
        $antecedentes_alergicos = $opto["antecedentes_alergicos"];
        $antecedentes_alergicos_obs = $opto["antecedentes_alergicos_obs"];
        $antecedentes_hipertension = $opto["antecedentes_hipertension"];
        $antecedentes_hipertension_obs = $opto["antecedentes_hipertension_obs"];
        $antecedentes_alteraciontiroides = $opto["antecedentes_alteraciontiroides"];
        $antecedentes_alteraciontiroides_obs = $opto["antecedentes_alteraciontiroides_obs"];
        $antecedentes_personales_otros = $opto["antecedentes_personales_otros"];
        $antecedentes_personales_otros_obs = $opto["antecedentes_personales_otros_obs"];
        $antecedentes_usuariosRX = $opto["antecedentes_usuariosrx"];
        $antecedentes_usuariosRX_obs = $opto["antecedentes_usuariosrx_obs"];
        $antecedentes_glaucoma = $opto["antecedentes_glaucoma"];
        $antecedentes_glaucoma_obs = $opto["antecedentes_glaucoma_obs"];
        $antecedentes_quirurgicos = $opto["antecedentes_quirurgicos"];
        $antecedentes_quirurgicos_obs = $opto["antecedentes_quirurgicos_obs"];
        $antecedentes_rehabilitacionvisual = $opto["antecedentes_rehabilitacionvisual"];
        $antecedentes_rehabilitacionvisual_obs = $opto["antecedentes_rehabilitacionvisual_obs"];
        $antecedentes_trauma = $opto["antecedentes_trauma"];
        $antecedentes_trauma_obs = $opto["antecedentes_trauma_obs"];
        $antecedentes_catarata = $opto["antecedentes_catarata"];
        $antecedentes_catarata_obs = $opto["antecedentes_catarata_obs"];
        $antecedentes_esquirlas = $opto["antecedentes_esquirlas"];
        $antecedentes_esquirlas_obs = $opto["antecedentes_esquirlas_obs"];
        $antecedentes_oculares_otros = $opto["antecedentes_oculares_otros"];
        $antecedentes_oculares_otros_obs = $opto["antecedentes_oculares_otros_obs"];
        $antecedentes_quimicos = $opto["antecedentes_quimicos"];
        $antecedentes_quimicos_obs = $opto["antecedentes_quimicos_obs"];

        $antecedentes = new HO_Antecedentes();
        $antecedentes->diabetes = $antecedentes_diabetes;
        $antecedentes->diabetes_obs = $antecedentes_diabetes_obs;
        $antecedentes->problemascardiacos = $antecedentes_problemascardiacos;
        $antecedentes->problemascardiacos_obs = $antecedentes_problemascardiacos_obs;
        $antecedentes->alergicos = $antecedentes_alergicos;
        $antecedentes->alergicos_obs = $antecedentes_alergicos_obs;
        $antecedentes->hipertension = $antecedentes_hipertension;
        $antecedentes->hipertension_obs = $antecedentes_hipertension_obs;
        $antecedentes->alteraciontiroides = $antecedentes_alteraciontiroides;
        $antecedentes->alteraciontiroides_obs = $antecedentes_alteraciontiroides_obs;
        $antecedentes->personales_otros = $antecedentes_personales_otros;
        $antecedentes->personales_otros_obs = $antecedentes_personales_otros_obs;
        $antecedentes->usuariosrx = $antecedentes_usuariosRX;
        $antecedentes->usuariosrx_obs = $antecedentes_usuariosRX_obs;
        $antecedentes->glaucoma = $antecedentes_glaucoma;
        $antecedentes->glaucoma_obs = $antecedentes_glaucoma_obs;
        $antecedentes->quirurgicos = $antecedentes_quirurgicos;
        $antecedentes->quirurgicos_obs = $antecedentes_quirurgicos_obs;
        $antecedentes->rehabilitacionvisual = $antecedentes_rehabilitacionvisual;
        $antecedentes->rehabilitacionvisual_obs = $antecedentes_rehabilitacionvisual_obs;
        $antecedentes->trauma = $antecedentes_trauma;
        $antecedentes->trauma_obs = $antecedentes_trauma_obs;
        $antecedentes->catarata = $antecedentes_catarata;
        $antecedentes->catarata_obs = $antecedentes_catarata_obs;
        $antecedentes->esquirlas = $antecedentes_esquirlas;
        $antecedentes->esquirlas_obs = $antecedentes_esquirlas_obs;
        $antecedentes->oculares_otros = $antecedentes_oculares_otros;
        $antecedentes->oculares_otros_obs = $antecedentes_oculares_otros_obs;
        $antecedentes->quimicos = $antecedentes_quimicos;
        $antecedentes->quimicos_obs = $antecedentes_quimicos_obs;

        return $antecedentes;
    }
    /**
     *
    */
    function ho_recomendacion($opto) {
        $recomendaciones_ergonomiavisual_obs = $opto["recomendaciones_ergonomiavisual_obs"];
        $recomendaciones_pautashigienevisual_obs = $opto["recomendaciones_pautashigienevisual_obs"];
        $recomendaciones_higienevisual_obs = $opto["recomendaciones_higienevisual_obs"];
        $recomendaciones_correccionopticapermanente_obs = $opto["recomendaciones_correccionopticapermanente_obs"];
        $recomendaciones_correccionopticavisionprolongada_obs = $opto["recomendaciones_correccionopticavisionprolongada_obs"];
        $recomendaciones_correccionopticaactual_obs = $opto["recomendaciones_correccionopticaactual_obs"];
        $recomendaciones_correccionopticavisioncercana_obs = $opto["recomendaciones_correccionopticavisioncercana_obs"];
        $recomendaciones_usoelementosproteccionvisual_obs = $opto["recomendaciones_usoelementosproteccionvisual_obs"];
        $recomendaciones_proteccionvisualcorreccionoptica_obs = $opto["recomendaciones_proteccionvisualcorreccionoptica_obs"];
        $recomendaciones_usogafasfiltrouv_obs = $opto["recomendaciones_usogafasfiltrouv_obs"];
        $recomendaciones_controlanual_obs = $opto["recomendaciones_controlanual_obs"];
        $recomendaciones_controloptometria_obs = $opto["recomendaciones_controloptometria_obs"];
        $recomendaciones_controloftalmologia_obs = $opto["recomendaciones_controloftalmologia_obs"];
        $recomendaciones_norequiereusocorreccionoptica_obs = $opto["recomendaciones_norequiereusocorreccionoptica_obs"];
        $recomendaciones_otrasconductas_obs = $opto["recomendaciones_otrasconductas_obs"];
        $recomendaciones_ojoderecho = isset($opto["recomendaciones_ojoderecho"]) ? $opto["recomendaciones_ojoderecho"] : "";
        $recomendaciones_ojoizquierdo = isset($opto["recomendaciones_ojoizquierdo"]) ? $opto["recomendaciones_ojoizquierdo"] : "";
        $recomendaciones_diagnostico = isset($opto["recomendaciones_diagnostico"]) ? $opto["recomendaciones_diagnostico"] : "";
        $recomendaciones_pacientecompatible = isset($opto["recomendaciones_pacientecompatible"]) ? $opto["recomendaciones_pacientecompatible"] : "";
        $recomendaciones_requierevaloracion = isset($opto["recomendaciones_requierevaloracion"]) ? $opto["recomendaciones_requierevaloracion"] : "";
        $recomendaciones_requiereremision = isset($opto["recomendaciones_requiereremision"]) ? $opto["recomendaciones_requiereremision"] : "";
        $recomendaciones_pacientecompatible_obs = $opto["recomendaciones_pacientecompatible_obs"];
        $recomendaciones_requierevaloracion_obs = $opto["recomendaciones_requierevaloracion_obs"];
        $recomendaciones_requiereremision_obs = $opto["recomendaciones_requiereremision_obs"];

        $recomendaciones_ergonomiavisual = isset($opto["recomendaciones_ergonomiavisual"]) ? $opto["recomendaciones_ergonomiavisual"] : "";
        $recomendaciones_pautashigienevisual = isset($opto["recomendaciones_pautashigienevisual"]) ? $opto["recomendaciones_pautashigienevisual"] : "";
        $recomendaciones_higienevisual = isset($opto["recomendaciones_higienevisual"]) ? $opto["recomendaciones_higienevisual"] : "";
        $recomendaciones_correccionopticapermanente = isset($opto["recomendaciones_correccionopticapermanente"]) ? $opto["recomendaciones_correccionopticapermanente"] : "";
        $recomendaciones_correccionopticavisionprolongada = isset($opto["recomendaciones_correccionopticavisionprolongada"]) ? $opto["recomendaciones_correccionopticavisionprolongada"] : "";
        $recomendaciones_correccionopticaactual = isset($opto["recomendaciones_correccionopticaactual"]) ? $opto["recomendaciones_correccionopticaactual"] : "";
        $recomendaciones_correccionopticavisioncercana = isset($opto["recomendaciones_correccionopticavisioncercana"]) ? $opto["recomendaciones_correccionopticavisioncercana"] : "";
        $recomendaciones_usoelementosproteccionvisual = isset($opto["recomendaciones_usoelementosproteccionvisual"]) ? $opto["recomendaciones_usoelementosproteccionvisual"] : "";
        $recomendaciones_proteccionvisualcorreccionoptica = isset($opto["recomendaciones_proteccionvisualcorreccionoptica"]) ? $opto["recomendaciones_proteccionvisualcorreccionoptica"] : "";
        $recomendaciones_usogafasfiltrouv = isset($opto["recomendaciones_usogafasfiltrouv"]) ? $opto["recomendaciones_usogafasfiltrouv"] : "";
        $recomendaciones_controlanual = isset($opto["recomendaciones_controlanual"]) ? $opto["recomendaciones_controlanual"] : "";
        $recomendaciones_controloptometria = isset($opto["recomendaciones_controloptometria"]) ? $opto["recomendaciones_controloptometria"] : "";
        $recomendaciones_controloftalmologia = isset($opto["recomendaciones_controloftalmologia"]) ? $opto["recomendaciones_controloftalmologia"] : "";
        $recomendaciones_norequiereusocorreccionoptica = isset($opto["recomendaciones_norequiereusocorreccionoptica"]) ? $opto["recomendaciones_norequiereusocorreccionoptica"] : "";
        $recomendaciones_otrasconductas = isset($opto["recomendaciones_otrasconductas"]) ? $opto["recomendaciones_otrasconductas"] : "";

        $recomendaciones_recomendacion = isset($opto["recomendaciones_recomendacion"]) ? $opto["recomendaciones_recomendacion"] : "";

        $recomendacion = new HO_Recomendaciones();
        $recomendacion->ergonomiavisual = $recomendaciones_ergonomiavisual;
        $recomendacion->ergonomiavisual_obs = $recomendaciones_ergonomiavisual_obs;
        $recomendacion->pautashigienevisual = $recomendaciones_pautashigienevisual;
        $recomendacion->pautashigienevisual_obs = $recomendaciones_pautashigienevisual_obs;
        $recomendacion->higienevisual = $recomendaciones_higienevisual;
        $recomendacion->higienevisual_obs = $recomendaciones_higienevisual_obs;
        $recomendacion->correccionopticapermanente = $recomendaciones_correccionopticapermanente;
        $recomendacion->correccionopticapermanente_obs = $recomendaciones_correccionopticapermanente_obs;
        $recomendacion->correccionopticavisionprolongada = $recomendaciones_correccionopticavisionprolongada;
        $recomendacion->correccionopticavisionprolongada_obs = $recomendaciones_correccionopticavisionprolongada_obs;
        $recomendacion->correccionopticaactual = $recomendaciones_correccionopticaactual;
        $recomendacion->correccionopticaactual_obs = $recomendaciones_correccionopticaactual_obs;
        $recomendacion->correccionopticavisioncercana = $recomendaciones_correccionopticavisioncercana;
        $recomendacion->correccionopticavisioncercana_obs = $recomendaciones_correccionopticavisioncercana_obs;
        $recomendacion->usoelementosproteccionvisual = $recomendaciones_usoelementosproteccionvisual;
        $recomendacion->usoelementosproteccionvisual_obs = $recomendaciones_usoelementosproteccionvisual_obs;
        $recomendacion->proteccionvisualcorreccionoptica = $recomendaciones_proteccionvisualcorreccionoptica;
        $recomendacion->proteccionvisualcorreccionoptica_obs = $recomendaciones_proteccionvisualcorreccionoptica_obs;
        $recomendacion->usogafasfiltrouv = $recomendaciones_usogafasfiltrouv;
        $recomendacion->usogafasfiltrouv_obs = $recomendaciones_usogafasfiltrouv_obs;
        $recomendacion->controlanual = $recomendaciones_controlanual;
        $recomendacion->controlanual_obs = $recomendaciones_controlanual_obs;
        $recomendacion->controloptometria = $recomendaciones_controloptometria;
        $recomendacion->controloptometria_obs = $recomendaciones_controloptometria_obs;
        $recomendacion->controloftalmologia = $recomendaciones_controloftalmologia;
        $recomendacion->controloftalmologia_obs = $recomendaciones_controloftalmologia_obs;
        $recomendacion->norequiereusocorreccionoptica = $recomendaciones_norequiereusocorreccionoptica;
        $recomendacion->norequiereusocorreccionoptica_obs = $recomendaciones_norequiereusocorreccionoptica_obs;
        $recomendacion->otrasconductas = $recomendaciones_otrasconductas;
        $recomendacion->otrasconductas_obs = $recomendaciones_otrasconductas_obs;
        $recomendacion->ojoderecho = $recomendaciones_ojoderecho;
        $recomendacion->ojoizquierdo = $recomendaciones_ojoizquierdo;
        $recomendacion->diagnostico = $recomendaciones_diagnostico;
        $recomendacion->recomendacion = $recomendaciones_recomendacion;
        $recomendacion->pacientecompatible = $recomendaciones_pacientecompatible;
        $recomendacion->pacientecompatible_obs = $recomendaciones_pacientecompatible_obs;
        $recomendacion->requierevaloracion = $recomendaciones_requierevaloracion;
        $recomendacion->requierevaloracion_obs = $recomendaciones_requierevaloracion_obs;
        $recomendacion->requiereremision = $recomendaciones_requiereremision;
        $recomendacion->requiereremision_obs = $recomendaciones_requiereremision_obs;

        return $recomendacion;
    }

    /**
     *
     */
    function ho_agudezavisual($agudezavisual) {
        $lejana_concorreccion_ojobinocular = $agudezavisual['lejana_concorreccion_ojobinocular'];
        $lejana_concorreccion_ojobinocular_obs = $agudezavisual['lejana_concorreccion_ojobinocular_obs'];
        $lejana_concorreccion_ojoderecho = $agudezavisual['lejana_concorreccion_ojoderecho'];
        $lejana_concorreccion_ojoderecho_obs = $agudezavisual['lejana_concorreccion_ojoderecho_obs'];
        $lejana_concorreccion_ojoizquierdo = $agudezavisual['lejana_concorreccion_ojoizquierdo'];
        $lejana_concorreccion_ojoizquierdo_obs = $agudezavisual['lejana_concorreccion_ojoizquierdo_obs'];
        $lejana_sincorreccion_ojobinocular = $agudezavisual['lejana_sincorreccion_ojobinocular'];
        $lejana_sincorreccion_ojobinocular_obs = $agudezavisual['lejana_sincorreccion_ojobinocular_obs'];
        $lejana_sincorreccion_ojoderecho = $agudezavisual['lejana_sincorreccion_ojoderecho'];
        $lejana_sincorreccion_ojoderecho_obs = $agudezavisual['lejana_sincorreccion_ojoderecho_obs'];
        $lejana_sincorreccion_ojoizquierdo = $agudezavisual['lejana_sincorreccion_ojoizquierdo'];
        $lejana_sincorreccion_ojoizquierdo_obs = $agudezavisual['lejana_sincorreccion_ojoizquierdo_obs'];
        $cercana_concorreccion_ojobinocular = $agudezavisual['cercana_concorreccion_ojobinocular'];
        $cercana_concorreccion_ojobinocular_obs = $agudezavisual['cercana_concorreccion_ojobinocular_obs'];
        $cercana_concorreccion_ojoderecho = $agudezavisual['cercana_concorreccion_ojoderecho'];
        $cercana_concorreccion_ojoderecho_obs = $agudezavisual['cercana_concorreccion_ojoderecho_obs'];
        $cercana_concorreccion_ojoizquierdo = $agudezavisual['cercana_concorreccion_ojoizquierdo'];
        $cercana_concorreccion_ojoizquierdo_obs = $agudezavisual['cercana_concorreccion_ojoizquierdo_obs'];
        $cercana_sincorreccion_ojobinocular = $agudezavisual['cercana_sincorreccion_ojobinocular'];
        $cercana_sincorreccion_ojobinocular_obs = $agudezavisual['cercana_sincorreccion_ojobinocular_obs'];
        $cercana_sincorreccion_ojoderecho = $agudezavisual['cercana_sincorreccion_ojoderecho'];
        $cercana_sincorreccion_ojoderecho_obs = $agudezavisual['cercana_sincorreccion_ojoderecho_obs'];
        $cercana_sincorreccion_ojoizquierdo = $agudezavisual['cercana_sincorreccion_ojoizquierdo'];
        $cercana_sincorreccion_ojoizquierdo_obs = $agudezavisual['cercana_sincorreccion_ojoizquierdo_obs'];

        $agudezavisual_db = new HO_AgudezaVisual();
        $agudezavisual_db->ls_od = $lejana_sincorreccion_ojoderecho;
        $agudezavisual_db->ls_od_obs = $lejana_sincorreccion_ojoderecho_obs;
        $agudezavisual_db->ls_oi = $lejana_sincorreccion_ojoizquierdo;
        $agudezavisual_db->ls_oi_obs = $lejana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual_db->ls_ojobinocular = $lejana_sincorreccion_ojobinocular;
        $agudezavisual_db->ls_ojobinocular_obs = $lejana_sincorreccion_ojobinocular_obs;
        $agudezavisual_db->lc_od = $lejana_concorreccion_ojoderecho;
        $agudezavisual_db->lc_od_obs = $lejana_concorreccion_ojoderecho_obs;
        $agudezavisual_db->lc_oi = $lejana_concorreccion_ojoizquierdo;
        $agudezavisual_db->lc_oi_obs = $lejana_concorreccion_ojoizquierdo_obs;
        $agudezavisual_db->lc_ojobinocular = $lejana_concorreccion_ojobinocular;
        $agudezavisual_db->lc_ojobinocular_obs = $lejana_concorreccion_ojobinocular_obs;
        $agudezavisual_db->cs_od = $cercana_sincorreccion_ojoderecho;
        $agudezavisual_db->cs_od_obs = $cercana_sincorreccion_ojoderecho_obs;
        $agudezavisual_db->cs_oi = $cercana_sincorreccion_ojoizquierdo;
        $agudezavisual_db->cs_oi_obs = $cercana_sincorreccion_ojoizquierdo_obs;
        $agudezavisual_db->cs_ojobinocular = $cercana_sincorreccion_ojobinocular;
        $agudezavisual_db->cs_ojobinocular_obs = $cercana_sincorreccion_ojobinocular_obs;
        $agudezavisual_db->cc_od = $cercana_concorreccion_ojoderecho;
        $agudezavisual_db->cc_od_obs = $cercana_concorreccion_ojoderecho_obs;
        $agudezavisual_db->cc_oi = $cercana_concorreccion_ojoizquierdo;
        $agudezavisual_db->cc_oi_obs = $cercana_concorreccion_ojoizquierdo_obs;
        $agudezavisual_db->cc_ojobinocular = $cercana_concorreccion_ojobinocular;
        $agudezavisual_db->cc_ojobinocular_obs = $cercana_concorreccion_ojobinocular_obs;
        return $agudezavisual_db;
    }

    public function print($id){

        $ho = HistoriaOptometria::find($id);
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Montserrat']);
        $pdf = PDF::loadView('optometria.print', compact('ho'));
        return $pdf->stream();
    }

    public function print2($id){

        $ho = HistoriaOptometria::find($id);
        return view('optometria.print', compact('ho'));
        //return view('optometria.print', compact('ho'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        // $ho = HistoriaOptometria::with(['agudezavisual', 'antecedente', 'recomendacion', 'sintoma', 'riesgocargoevaluar', 'paciente'])->findOrFail($id);
        // $constantes_agudezavisual = Constantes_agudezavisual::all();
        // $empresas = Empresa::all();
        // return view('optometria.view', compact('ho', 'empresas', 'constantes_agudezavisual'));
        $historia = HistoriaOptometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            'paciente',
            ])->find($id);
        if($historia != null){
            return ["mensaje" => "Historia encontrada", "historia" => $historia];
        }else{
            return ["mensaje" => "Historia no encontrada"];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function getHO(Request $request){
        $id = $request->input('id');

        $historia = HistoriaOptometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            ])->find($id);
        if($historia != null){
            return ["mensaje" => "Historia encontrada", "historia" => $historia];
        }else{
            return ["mensaje" => "Historia no encontrada"];
        }
    }

    public function getHOWeb($id){

        $historia = HistoriaOptometria::with([
            'agudezavisual',
            'antecedente',
            'recomendacion',
            'sintoma',
            'riesgocargoevaluar',
            'empresa'
        ])->find(decrypt($id));
        if($historia != null){
            return view('optometria.consultar', compact('historia'));
        }
    }

    public function anular(Request $request){
        $id = $request->input('id');
        $user_id = $request->input('user_id');
        $historia = HistoriaOptometria::find($id);
        $historia->estado = "Anulada";
        $historia->medicoAnula_id = $user_id;
        $historia->save();
        return ["mensaje" => "Se ha anulado correctamente"];

    }
}
