<?php

namespace App\Http\Controllers;

use App\HistoriaAudiometria;
use File;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CertificadoAudiometriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generateCertificate(Request $request, $id)
    {
        $historiaAudio = HistoriaAudiometria::find($id);
        $template = new TemplateProcessor(public_path() . "/historiaAudiometria.docx");
        $template->setValue('fecha', $historiaAudio->created_at);
        $template->setValue('motivoevaluacion', $historiaAudio->motivoevaluacion);

        $otrosME = $historiaAudio->motivoevaluacion_otros;
        if ($otrosME != "") {
            $otrosME = str_replace(",", ", ", substr($historiaAudio->motivoevaluacion_otros, 0, -1) . ".");
        }

        $template->setValue('motivoevaluacion_otros', $otrosME);

        // Generacion de la foto
        $photoPatient = $historiaAudio->Paciente->foto;
        if (isset($photoPatient)) {
            $image = str_replace('data:image/png;base64,', '', $photoPatient);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            // Load the image
            $source = @imagecreatefromjpeg(public_path() . '/temp/img/' . $imagePatient);
            if (!$source) {

            } else {
                $rotate = imagerotate($source, -90, 0);

                imagejpeg($rotate, public_path() . '/temp/img/' . $imagePatient);
                $template->setImageValue("foto", array("path" => public_path('/temp/img/' . $imagePatient), "width" => 240, "height" => 158));
            }

        }

        //------Identificacion trabajador
        $template->setValue('foto', $photoPatient);
        $template->setValue('nombre_completo', $historiaAudio->paciente);
        $template->setValue('genero', $historiaAudio->Paciente->genero);
        $template->setValue('documento', $historiaAudio->paciente_cedula);
        $template->setValue('direccion', $historiaAudio->Paciente->direccionDomicilio);
        $template->setValue('eps', $historiaAudio->Paciente->eps);
        $template->setValue('afp', $historiaAudio->Paciente->afp);
        $template->setValue('arl', $historiaAudio->Paciente->arl);
        $nombreContactoCasoEmergencia = $historiaAudio->Paciente->nombreAcompaniante . " " . $historiaAudio->Paciente->apellidoAcompaniante;
        $template->setValue('nombre_acompanante', $nombreContactoCasoEmergencia);
        $template->setValue('telefonoacompanante', $historiaAudio->Paciente->celularAcompaniante);

        //------Antecedentes ocupacionales
        $template->setValue('ocupacion_anterior', $historiaAudio->antecedentes->ocupacionales_ocupacionanterior);
        $template->setValue('tiempo_anterior', $historiaAudio->antecedentes->ocupacionales_ocupacionanterior_tiempo);
        $template->setValue('ocupacion_actual', $historiaAudio->antecedentes->ocupacionales_ocupacionactual);
        $template->setValue('tiempo_actual', $historiaAudio->antecedentes->ocupacionales_ocupacionactual_tiempo);
        $template->setValue('labores', $historiaAudio->antecedentes->ocupacionales_laboresdesempena);
        $template->setValue('jornada', $historiaAudio->antecedentes->ocupacionales_jornadalaboral);
        $template->setValue('tiempo_ruido', $historiaAudio->antecedentes->ocupacionales_tiempoexposicion);
        $template->setValue('proteccion_auditiva', $historiaAudio->antecedentes->ocupacionales_utilizaproteccion);
        $template->setValue('que_tipo', $historiaAudio->antecedentes->ocupacionales_utilizaproteccion_tipo);
        $template->setValue('hace_cuanto', $historiaAudio->antecedentes->ocupacionales_utilizaproteccion_tiempo);

        //------Antecedentes personales
        $template->setValue('familiares_otologicos', $historiaAudio->antecedentes->familiares_otologicos);
        $template->setValue('otalgia', $historiaAudio->antecedentes->otologicos_otalgia);
        $template->setValue('otitis', $historiaAudio->antecedentes->otologicos_otitis);
        $template->setValue('otorr', $historiaAudio->antecedentes->otologicos_otorrea);
        $template->setValue('pruri', $historiaAudio->antecedentes->otologicos_prurito);
        $template->setValue('oido_t', $historiaAudio->antecedentes->otologicos_sensacionoido);
        $template->setValue('vertig', $historiaAudio->antecedentes->otologicos_vertigo);
        $template->setValue('tinitus', $historiaAudio->antecedentes->otologicos_tinitus);
        $template->setValue('otro_o', $historiaAudio->antecedentes->otologicos_otros);
        $template->setValue('cual_o', $historiaAudio->antecedentes->otologicos_cuales);
        $template->setValue('hiper', $historiaAudio->antecedentes->patologico_hipertension);
        $template->setValue('diabe', $historiaAudio->antecedentes->patologico_diabetes);
        $template->setValue('paroti', $historiaAudio->antecedentes->patologico_parotiditis);
        $template->setValue('saram', $historiaAudio->antecedentes->patologico_sarampion);
        $template->setValue('rubeo', $historiaAudio->antecedentes->patologico_rubeola);
        $template->setValue('rin_sin', $historiaAudio->antecedentes->patologico_rinitis);
        $template->setValue('otro_p', $historiaAudio->antecedentes->patologico_otros);
        $template->setValue('cual_p', $historiaAudio->antecedentes->patologico_cuales);
        $template->setValue('tejo', $historiaAudio->antecedentes->extralaboral_tejo);
        $template->setValue('moto', $historiaAudio->antecedentes->extralaboral_moto);
        $template->setValue('musica', $historiaAudio->antecedentes->extralaboral_musica);
        $template->setValue('serv_m', $historiaAudio->antecedentes->extralaboral_serviciomilitar);
        $template->setValue('audifo', $historiaAudio->antecedentes->extralaboral_audifonos);
        $template->setValue('otro_e', $historiaAudio->antecedentes->extralaboral_otros);
        $template->setValue('cual_e', $historiaAudio->antecedentes->extralaboral_cuales);
        $template->setValue('indus', $historiaAudio->antecedentes->toxicosnervio_industriales);
        $template->setValue('farma', $historiaAudio->antecedentes->toxicosnervio_farmacos);
        $template->setValue('cirugia', $historiaAudio->antecedentes->quirurgicos_cirugiaoido);
        $template->setValue('timpa', $historiaAudio->antecedentes->quirurgicos_timpanoplastia);
        $template->setValue('otr_q', $historiaAudio->antecedentes->quirurgicos_otros);
        $template->setValue('cual_q', $historiaAudio->antecedentes->quirurgicos_cuales);
        $template->setValue('crane', $historiaAudio->antecedentes->traumaticos_craneo);
        $template->setValue('acusti', $historiaAudio->antecedentes->traumaticos_acustico);
        $template->setValue('otro_t', $historiaAudio->antecedentes->traumaticos_otros);
        $template->setValue('cual_t', $historiaAudio->antecedentes->traumaticos_cuales);

        //------Audiograma---------------
        $audiogramaod = $historiaAudio->audiogramaOD;
        if (isset($audiogramaod)) {
            $image = str_replace('data:image/png;base64,', '', $audiogramaod);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue('audio_derecho', array("path" => public_path('temp/img/' . $imagePatient), "width" => 280, "height" => 140));
        }

        $audiogramaoi = $historiaAudio->audiogramaOI;
        if (isset($audiogramaoi)) {
            $image = str_replace('data:image/png;base64,', '', $audiogramaoi);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue('audio_izquierdo', array("path" => public_path('temp/img/' . $imagePatient), "width" => 280, "height" => 140));
        }


        $template->setValue('od25', isset($historiaAudio->audiograma[0]->oidoderecho) ? $historiaAudio->audiograma[0]->oidoderecho : "");
        $template->setValue('oi25', $historiaAudio->audiograma[0]->oidoizquierdo);
        $template->setValue('od500', isset($historiaAudio->audiograma[1]->oidoderecho) ? $historiaAudio->audiograma[1]->oidoderecho : "");
        $template->setValue('oi500', $historiaAudio->audiograma[1]->oidoizquierdo);
        $template->setValue('od1000', isset($historiaAudio->audiograma[2]->oidoderecho) ? $historiaAudio->audiograma[2]->oidoderecho : "");
        $template->setValue('oi1000', $historiaAudio->audiograma[2]->oidoizquierdo);
        $template->setValue('od2000', isset($historiaAudio->audiograma[3]->oidoderecho) ? $historiaAudio->audiograma[3]->oidoderecho : "");
        $template->setValue('oi2000', $historiaAudio->audiograma[3]->oidoizquierdo);
        $template->setValue('od3000', isset($historiaAudio->audiograma[4]->oidoderecho) ? $historiaAudio->audiograma[4]->oidoderecho : "");
        $template->setValue('oi3000', $historiaAudio->audiograma[4]->oidoizquierdo);
        $template->setValue('od4000', isset($historiaAudio->audiograma[5]->oidoderecho) ? $historiaAudio->audiograma[5]->oidoderecho : "");
        $template->setValue('oi4000', $historiaAudio->audiograma[5]->oidoizquierdo);
        $template->setValue('od6000', isset($historiaAudio->audiograma[6]->oidoderecho) ? $historiaAudio->audiograma[6]->oidoderecho : "");
        $template->setValue('oi6000', $historiaAudio->audiograma[6]->oidoizquierdo);
        $template->setValue('od8000', isset($historiaAudio->audiograma[7]->oidoderecho) ? $historiaAudio->audiograma[7]->oidoderecho : "");
        $template->setValue('oi8000', $historiaAudio->audiograma[7]->oidoizquierdo);

        //---------Otoscopia-------
        $template->setValue('npa', $historiaAudio->otoscopia->pabellonauricular_normal);
        $template->setValue('ncae', $historiaAudio->otoscopia->cae_normal);
        $template->setValue('n_m', $historiaAudio->otoscopia->membranatimpanica_normal);
        $template->setValue('opac', $historiaAudio->otoscopia->membranatimpanica_opaca);
        $template->setValue('art', $historiaAudio->otoscopia->pabellonauricular_atresia);
        $template->setValue('tp', $historiaAudio->otoscopia->cae_taponparcial);
        $template->setValue('perf', $historiaAudio->otoscopia->membranatimpanica_perforada);
        $template->setValue('abul', $historiaAudio->otoscopia->membranatimpanica_abultada);
        $template->setValue('agn', $historiaAudio->otoscopia->pabellonauricular_agenesia);
        $template->setValue('tt', $historiaAudio->otoscopia->cae_tapontotal);
        $template->setValue('hipe', $historiaAudio->otoscopia->membranatimpanica_hiperemica);
        $template->setValue('nvis', $historiaAudio->otoscopia->membranatimpanica_nosevisualiza);
        $template->setValue('cica', $historiaAudio->otoscopia->pabellonauricular_cicatriz);
        $template->setValue('o_ca', $historiaAudio->otoscopia->cae_otros);
        $template->setValue('plac', $historiaAudio->otoscopia->membranatimpanica_placacalcarea);
        $template->setValue('o_m', $historiaAudio->otoscopia->membranatimpanica_otros);
        $template->setValue('o_p', $historiaAudio->otoscopia->pabellonauricular_otros);

        $template->setValue('impresión_diagnostica', $historiaAudio->recomendacion->impresiondiagnostica);
        $template->setValue('recomendaciones', $historiaAudio->recomendacion->recomendaciones);

        //--------Firmas------
        $template->setValue('nombre_completo', $historiaAudio->paciente);
        $template->setValue('documento', $historiaAudio->paciente_cedula);
        $template->setValue('tipo_doc', $historiaAudio->Paciente->tipodocumento);

        $template->setValue('nombremedico', $historiaAudio->medicocierre->nombre_completo);
        $template->setValue('cocumentomedico', $historiaAudio->medicocierre->document);
        $template->setValue('licencia', $historiaAudio->medicocierre->licencia);

        //-----------Exportar pdf-----------
        if (isset($historiaAudio->firma_medico)){
            $image = str_replace('data:image/png;base64,', '', $historiaAudio->firma_medico);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmamedico", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmamedico","");
        }

        if (isset($historiaAudio->firma_paciente)){
            $image = str_replace('data:image/png;base64,', '', $historiaAudio->firma_paciente);
            $image = str_replace(' ', '+', $image);
            $imagePatient = str_random(10) . '.' . 'png';
            File::put(public_path() . '/temp/img/' . $imagePatient, base64_decode($image));
            $template->setImageValue("firmapaciente", public_path('/temp/img/' . $imagePatient));
        }else{
            $template->setValue("firmapaciente","");
        }
        $template->saveAs(public_path() . '/generados_hc/' . $historiaAudio->Paciente->documento . '-audiometria.docx');
        $process = new Process("export HOME=/tmp && libreoffice --headless --convert-to pdf " . public_path() . '/generados_hc/' . $historiaAudio->Paciente->documento . '-audiometria.docx');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return response()->download(public_path($historiaAudio->Paciente->documento.'-audiometria.pdf'));

    }

    function printSeparator($section)
    {
        $section->addTextBreak();
        $lineStyle = array('weight' => 0.2, 'width' => 150, 'height' => 0, 'align' => 'center');
        $section->addLine($lineStyle);
        $section->addTextBreak(2);
    }
}
