<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use App\ExamenLaboratorio;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Route;
use Validator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 422);
        }

        $input = $request->all();
        $input['password'] = bcrypt($request->get('password'));
        $user = User::create($input);
        $token =  $user->createToken('MyApp')->accessToken;
        return response()->json([
            'token' => $token,
            'user' => $user
        ], 200);
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return [
                'message' => 'Unauthorized'
            ];
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now('America/Bogota')->addWeeks(1);
        $token->save();
        $role = Role::find($user->role_id);
        $empresas = Empresa::select("id", "nombre")->get();
        ini_set("memory_limit",'-1');
        ini_set("max_execution_time",'-1');
        $pacientes = DB::select(DB::raw('select p.documento, p.tipodocumento, p.nombre, p.apellido, p.genero, p.fechaNacimiento, 
p.lugarNacimiento, p.direccionDomicilio, p.ciudadDomicilio, p.telefono, p.celular, p.correoElectronico, p.escolaridad, 
p.estadoCivil, p.eps, p.arl, p.afp, p.nombreAcompaniante, p.apellidoAcompaniante, p.parentescoAcompaniante, p.celularAcompaniante, 
p.grupoSanguineo, p.rh, p.antiguedad, p.cargo, p.nombre_completo, p.edad 
from (select h.paciente_id as id from historiaclinica h where  h.empresa_id = 503 group by h.paciente_id) as t 
join paciente p on t.id = p.id'));
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => $user,
            'role' => $role,
            'empresas' => $empresas,
            'examenes' => ExamenLaboratorio::all(),
            'pacientes' => $pacientes
        ]);
    }

    public function profile()
    {
        $user = Auth::user();
        return response()->json(compact('user'), 200);
    }
     /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
