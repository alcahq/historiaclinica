<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Http\Requests\RegisterCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function __construct(){
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterCompanyRequest $request)
    {
        $requestData = $request->all();
        Empresa::create($requestData);
        return back()->with('createCompany','Empresa registrada exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $empresa = Empresa::findOrFail(decrypt($id));
        return view('empresa.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        Empresa::findOrFail(decrypt($id))
            ->update($request->all());
        return back()->with('UpdateCompany', 'Empresa actualizada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewList(){
        return view('empresa.list');
    }

    public function findByNitOrName(Request $request){
        $empresa_buscada = $request->input('buscar_empresa');
        if ($empresa_buscada == ""){
            return back;
        }
        $empresas = Empresa::select('id','nit','nombre','direccion','telefono')
            ->where('nombre', 'like', '%'.$empresa_buscada.'%')
            ->orWhere('nit', $empresa_buscada)->get();
        return view('empresa.list', compact('empresas'));
    }

    public function autocompleteCompany(Request $request){
        $nombre = $request->input('phrase');
        $empresa = Empresa::select('nombre')->where('nombre', 'like', "%".$nombre."%")
            ->get();
        return $empresa;
    }
}
