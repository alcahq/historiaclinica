<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriaAudiometria extends Model {
	protected $table = 'historiaaudiometria';

	protected $fillable = [
		"empresa_id",
		"medicoApertura_id",
		"medicoCierre_id",
		"motivoevaluacion",
		"cargoEvaluar",
		"fecha",
		"paciente_id",
		"motivoevaluacion_otros",
		'paciente_edad',
		'firma_medico',
		'firma_paciente',
		'paciente',
		'paciente_cedula',
		'estado',
		'audiogramaOD',
		'audiogramaOI',
	];

	public function antecedentes() {
		return $this->hasOne('App\ha_antecedente', 'historiaaudiometria_id');
	}
	public function otoscopia() {
		return $this->hasOne('App\ha_otoscopia', 'historiaaudiometria_id');
	}
	public function recomendacion() {
		return $this->hasOne('App\ha_recomendaciones', 'historiaaudiometria_id');
	}
	public function audiograma() {
		return $this->hasMany('App\ha_audiograma', 'historiaaudiometria_id')->orderBy("nivel_id");
	}
	public function medicoapertura() {
		return $this->belongsTo('App\User', 'medicoApertura_id');
	}

	public function medicocierre() {
		return $this->belongsTo('App\User', 'medicoCierre_id');
	}

	public function medicoanula() {
		return $this->belongsTo('App\User', 'medicoAnula_id');
	}

	public function empresa() {
		return $this->belongsTo('App\Empresa', 'empresa_id');
	}

	public function paciente() {
		return $this->belongsTo('App\Patient');
	}

}
