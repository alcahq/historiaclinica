<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ha_antecedente extends Model
{
    protected $table = 'ha_antecedentes';
    protected $fillable = [
        "extralaboral_musica",
        "otologicos_otorrea",
        "toxicosnervio_industriales",
        "toxicosnervio_farmacos",
        "patologico_cuales",
        "patologico_hipertension",
        "quirurgicos_cirugiaoido",
        "otologicos_cuales",
        "traumaticos_cuales",
        "extralaboral_serviciomilitar",
        "extralaboral_otros",
        "traumaticos_craneo",
        "extralaboral_audifonos",
        "otologicos_prurito",
        "otologicos_otitis",
        "otologicos_otros",
        "patologico_rinitis",
        "quirurgicos_timpanoplastia",
        "traumaticos_acustico",
        "extralaboral_tejo",
        "extralaboral_moto",
        "extralaboral_cuales",
        "patologico_sarampion",
        "quirurgicos_cuales",
        "otologicos_vertigo",
        "otologicos_sensacionoido",
        "traumaticos_otros",
        "patologico_parotiditis",
        "otologicos_otalgia",
        "patologico_diabetes",
        "quirurgicos_otros",
        "otologicos_tinitus",
        "patologico_otros",
        "patologico_rubeola",
        "ocupacionales_ocupacionactual_tiempo",
        "familiares_otologicos",
        "ocupacionales_horario",
        "ocupacionales_ocupacionanterior",
        "ocupacionales_ocupacionactual",
        "ocupacionales_ocupacionanterior_tiempo",
        "ocupacionales_utilizaproteccion_tipo",
        "ocupacionales_jornadalaboral",
        "ocupacionales_utilizaproteccion_tiempo",
        "ocupacionales_laboresdesempena",
        "ocupacionales_tiempoexposicion",
        "ocupacionales_utilizaproteccion",
        "historiaAudiometria_id"
        ];
    public function historiaAudiometria() {
		return $this->belongsTo('App\HistoriaAudiometria');
	}
}
