<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriaOptometria extends Model {
	protected $table = "historiaoptometria";

	public function agudezavisual()
    {
        return $this->hasOne('App\HO_AgudezaVisual', 'historiaoptometria_id');
    }
    public function antecedente()
    {
        return $this->hasOne('App\HO_Antecedentes', 'historiaoptometria_id');
    }
    public function recomendacion()
    {
        return $this->hasOne('App\HO_Recomendaciones', 'historiaoptometria_id');
    }
    public function sintoma()
    {
        return $this->hasOne('App\Sintomas', 'historiaoptometria_id');
    }
    public function riesgocargoevaluar()
    {
        return $this->hasOne('App\RiesgoCargoEvaluar', 'historiaoptometria_id');
    }

    public function paciente(){
        return $this->belongsTo('App\Patient');
    }

    public function medicoapertura() {
        return $this->belongsTo('App\User','medicoapertura_id');
    }

    public function medicocierre() {
        return $this->belongsTo('App\User','medicocierre_id');
    }

    public function medicoanula() {
        return $this->belongsTo('App\User','medicoAnula_id');
    }

    public function empresa(){
	    return $this->belongsTo('App\Empresa', 'empresa_id');
    }
}
