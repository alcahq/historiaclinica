<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = "hc_cargo";
    protected $fillable = [
        "empresa",
        "psicosociales",
        "seguridad",
        "quimicos",
        "biologicos",
        "ergonomicos",
        "otros",
        "cargo",
        "fisicos",
        "tiempoexposicion",
        "antiguedad",
        "historiaClinica_id"
    ];
    public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
}
