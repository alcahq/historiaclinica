<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParaclinicoDatos extends Model
{
    protected $table = "hc_datosParaclinico";

	protected $fillable = [
		"optometria",
        "optometria_alteracioncorregida",
        "optometria_alteracioncorregida_obs",
        "visiometria",
        "visiometria_alteracioncorregida",
        "visiometria_alteracioncorregida_obs",
        "espirometria",
        "espirometria_resultado",
        "espirometria_obs",
        "audiometria",
        "audiometria_resultado",
        "audiometria_obs",
        "psicometrico",
        "psicometrico_obs",
        "psicologico",
        "psicologico_obs",
        "pruebavestibular",
        "pruebavestibular_obs",
        "historiaClinica_id"
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}//
}
