<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriaClinica extends Model {

	protected $table = "historiaclinica";

	protected $fillable = [
        'motivoevaluacion',
        'motivoevaluacion_otros',
        'estado',
        'impreso',
        'cantImpresiones',
        'anular',
        'firmaPaciente',
        'firmaMedico',
        'medicoApertura_id',
        'medicoCierre_id',
        'paciente_id',
        'fecha',
        'cargoEvaluar',
        'empresa_id',
        'accidente',
        'enfermedad',
        'paciente_cedula',
        'paciente',
        'genero',
        'paciente_antiguedad',
        'paciente_estadoCivil',
        'paciente_escolaridad',
        'paciente_edad',
        'fecha_nota',
        'observacion_nota'
    ];

	public function habitos() {
		return $this->hasOne('App\Habitos', 'historiaclinica_id');
	}
	public function antecedentes() {
		return $this->hasOne('App\AntecedentesPersonales', 'historiaclinica_id');
	}
	public function revisionporsistema() {
		return $this->hasOne('App\RevisionPorSistema', 'historiaclinica_id');
	}
	public function manipulacionalimentos() {
		return $this->hasOne('App\ManipulacionAlimentos', 'historiaclinica_id');
	}
	public function resultado() {
		return $this->hasOne('App\Resultado', 'historiaclinica_id');
	}
	public function examenfisico() {
		return $this->hasOne('App\ExamenFisico', 'historiaclinica_id');
	}
	public function paraclinicos() {
		return $this->hasMany('App\Paraclinico', 'historiaclinica_id');
    }
    public function paraclinicosdatos() {
		return $this->hasOne('App\ParaclinicoDatos', 'historiaclinica_id');
    }
    public function impresiondiagnostica() {
		return $this->hasMany('App\ImpresionDiagnostica', 'historiaclinica_id');
    }
    public function cargoanterior() {
		return $this->hasMany('App\Cargo', 'historiaclinica_id');
    }
    public function cargoactual() {
		return $this->hasOne('App\CargoActual', 'historiaclinica_id');
    }
    public function empresa() {
	    return $this->belongsTo('App\Empresa', 'empresa_id');
    }
    public function accidentes() {
        return $this->hasMany('App\AccidenteTrabajo', 'historiaclinica_id');
    }
    public function enfermedades() {
        return $this->hasMany('App\EnfermedadProfesional', 'historiaclinica_id');
    }
    public function medicoapertura() {
        return $this->belongsTo('App\User','medicoApertura_id');
    }
    public function medicocierre() {
        return $this->belongsTo('App\User','medicoCierre_id');
    }
    public function medicoanula() {
        return $this->belongsTo('App\User','medicoAnula_id');
    }
    public function pacienteHC() {
        return $this->belongsTo('App\Patient', 'paciente_id');
    }
    public function notaEvolucion(){
	    return $this->hasMany('App\NotaEvolucion', 'historiaclinica_id');
    }
    public function nuevavaloracion(){
	    return $this->hasOne('App\NuevaValoracion', 'historiaclinica_id');
    }
}
