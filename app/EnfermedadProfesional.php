<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnfermedadProfesional extends Model
{
    protected $table = 'hc_enfermedadProfesional';

    protected $fillable = [
        'fecha',
        'empresa',
        'diagnostico',
        'arl',
        'reubicacion',
        'paciente_id',
        'historiaclinica_id',
    ];

    public function historiaClinica() {
        return $this->belongsTo('App\HistoriaClinica');
    }

    public function paciente() {
        return $this->belongsTo('App\Patient');
    }
}
