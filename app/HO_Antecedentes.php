<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HO_Antecedentes extends Model
{
	protected $table = 'ho_antecedente';

    public function historiaoptometria()
    {
        return $this->belongsTo('App\HistoriaOptometria');
    }
}
