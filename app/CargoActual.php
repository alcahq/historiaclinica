<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoActual extends Model
{
    protected $table = "hc_cargoactual";
    protected $fillable = [
        "antiguedad",
        "cargo",
        "elementos",
        "antiguedadMedida",
        "horas",
        "empresa",
        "fecha",
        "area",
        "jornada",
        'otros_elementos',
        "historiaClinica_id"
    ];
    public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
}
