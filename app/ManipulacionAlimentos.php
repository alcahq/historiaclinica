<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManipulacionAlimentos extends Model {
	protected $table = "hc_manipulacionAlimento";

	protected $fillable = [
		"solicitado",
		"respiratorio_cumple",
		"respiratorio_inspeccion",
		"respiratorio_ausculacion",
		"dermatologico_prurito",
		"dermatologico_medicamento",
		"dermatologico_cual",
		"dermatologico_lesiones",
		"observacion",
        "historiaClinica_id"
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
}
