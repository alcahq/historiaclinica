<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
    protected $table = "hc_resultado";

	protected $fillable = [
		"patologia",
		"puedeContinuarConLabor",
		"resultadoValoracionMedica",
		"reubicacionLaboral",
		"motivo",
		"recomendacion_medica",
		"recomendacion_ingresosve",
		"recomendacion_ocupacional",
		"recomendacion_habitos",
		"recomendacion_pruebacomplementaria",
		"recomendacion_pruebacomplementaria_otros",
		"restricciones",
		"recomendacionesAdicionales",
		"historiaClinica_id",
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
}
