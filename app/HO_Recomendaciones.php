<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HO_Recomendaciones extends Model
{
	protected $table = 'ho_recomendacion';

    public function historiaoptometria()
    {
        return $this->belongsTo('App\HistoriaOptometria');
    }
}
