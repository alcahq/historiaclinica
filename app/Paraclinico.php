<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paraclinico extends Model
{
    protected $table = "hc_paraclinico";

	protected $fillable = [
		"diagnostico",
		"observacion",
        "historiaClinica_id",
        "examenLaboratorio_id"
	];

	public function historiaClinica() {
		return $this->belongsTo('App\HistoriaClinica');
	}
	public function examenlab(){
        return $this->belongsTo('App\ExamenLaboratorio', "examenLaboratorio_id");
    }
}
