<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function () {

    //--------------------------------Paciente------------------------------------

    Route::post('/paciente/register', 'WSController@pacienteRegistro');
    Route::post('/paciente/search', 'PatientController@findByDocumentOrName');
    Route::post('/paciente/searchByDocument', 'PatientController@findByDocument');
    Route::post('/paciente/searchById', 'PatientController@searchById');
    Route::post('/paciente/historias', 'PatientController@getAllHistories');
    Route::post('/paciente/update', 'WSController@pacienteUpdate');
    Route::post('/paciente/updateFotoFirma', 'WSController@pacienteUpdateFotoFirma');
    Route::post('/paciente/sync', 'WSController@pacienteSync');

    //--------------------------------Historia Optometria------------------------------------

    Route::post('/optometria/register', 'HistoriaOptometriaController@store');
    Route::post('/optometria/anular', 'HistoriaOptometriaController@anular');
    Route::post('/historia/ho', 'HistoriaOptometriaController@getHO');

    //--------------------------------Historia Visiometria------------------------------------

    Route::post('/visiometria/register', 'HistoriaVisiometriaController@store');
    Route::post('/visiometria/anular', 'HistoriaVisiometriaController@anular');
    Route::post('/historia/hv', 'HistoriaVisiometriaController@getHV');

    //--------------------------------Historia Audiometria-----------------------------------

    Route::post('/audiometria/registrar', 'HistoriaAudiometriaController@store');
    Route::post('/audiometria/anular', 'HistoriaAudiometriaController@anular');
    Route::get('/audiometria/{id}', 'HistoriaAudiometriaController@show');
    Route::post('/historia/ha', 'HistoriaAudiometriaController@getHA');

    //--------------------------------Historia Clinica---------------------------------------

    Route::post('/historiaClinica/registrar','HistoriaClinicaController@store');
    Route::post('/historia/hc', 'HistoriaClinicaController@getHC');
    Route::post('/hc/update/{id}', 'HistoriaClinicaController@updateWithoutecrypt');
    Route::post('/hc/cerrar/{id}', 'HistoriaClinicaController@cerrarHistoriaClinicaApp');

    //--------------------------------Login--------------------------------------------------

    Route::post('signup', 'Api\AuthController@signup');

    Route::post('/usuario/search', 'UserController@findByDocumentOrName');
    Route::post('/usuario/update', 'WSController@update');

});



