<?php

use App\HistoriaAudiometria;
use App\Paciente;

// DB::listen(function($query){
// 	echo "<pre>{$query->time}</pre>";
// });
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//-----------------------------------------Patient-----------------------------------------------------
Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);

//Route::get('login',['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);

Route::post('login', 'Auth\LoginController@login');

Route::get('logout', 'Auth\LoginController@logout')->name('login');

Route::get('paciente/registrar', 'PatientController@create')->name('paciente.create');

Route::post('paciente', 'PatientController@store')->name('paciente.store');

Route::get('paciente/buscar', 'PatientController@viewList')->name('paciente.viewList');

Route::post('paciente/buscar', 'PatientController@findByDocumentOrName');

Route::get('paciente/resultados', 'PatientController@findByDocumentOrName')->name('paciente.findByDocumentOrName');

Route::get('paciente/editar/{id}', 'PatientController@edit')->name('paciente.edit');

Route::post('paciente/update/{id}', 'PatientController@update')->name('paciente.update');

Route::post('paciente/findByDocument', 'PatientController@findByDocument')->name('paciente.findByDocument');

Route::get('usuario/buscarHistorias', 'PatientController@viewHistorias')->name('paciente.viewHistorias');

Route::post('paciente/historias', 'PatientController@getAllHistories')->name('paciente.historias');

//-----------------------------------------Usuario-----------------------------------------------------

Route::get('usuario/registrar', 'UserController@create')->name('usuario.create');

Route::post('usuario', 'UserController@store')->name('usuario.store');

Route::get('usuario/buscar', 'UserController@viewList')->name('usuario.viewList');

Route::post('usuario/buscar', 'UserController@findByDocumentOrName')->name('usuario.findByDocumentOrName');

Route::get('usuario/editar/{id}', 'UserController@edit')->name('usuario.edit');

Route::post('usuario/update/{id}', 'UserController@update')->name('usuario.update');

Route::get('usuario/restablecerContrasenia', 'UserController@viewRecoverPassword')->name('usuario.viewRecoverPass');

Route::post('usuario/restablecerContrasenia', 'UserController@recoverPassword')->name('usuario.recoverPassword');

//-----------------------------------------Empresa-----------------------------------------------------

Route::get('empresa/registrar', 'CompanyController@create')->name('empresa.create');

Route::post('empresa','CompanyController@store')->name('empresa.store');

Route::get('empresa/buscar', 'CompanyController@viewList')->name('empresa.viewList');

Route::post('empresa/buscar', 'CompanyController@findByNitOrName')->name('empresa.findByNitOrName');

Route::get('empresa/editar/{id}', 'CompanyController@edit')->name('empresa.edit');

Route::post('empresa/update/{id}', 'CompanyController@update')->name('empresa.update');

Route::post('empresa/autocomplete', 'CompanyController@autocompleteCompany')->name('empresa.autocomplete');

//-----------------------------------------Historia Clinica-----------------------------------------------------

Route::get('historiaClinica/registrar', 'HistoriaClinicaController@create')->name('historiaClinica.create');

Route::post('historiaClinica/registrar','HistoriaClinicaController@store')->name('historiaClinica.store');

Route::post('historiaClinica/buscarCIE10','HistoriaClinicaController@buscarCIE10')->name('historiaClinica.buscarCIE10');

Route::get('historia/hc/{id}', 'HistoriaClinicaController@getHCWeb')->name('historiaClinica.getHC');

Route::post('hc/autocomplete', 'HistoriaClinicaController@autocompleteDiagnostico')->name('cie10.autocomplete');

Route::post('hc/update/{id}', 'HistoriaClinicaController@update')->name('hc.update');

Route::post('hc/cerrar/{id}', 'HistoriaClinicaController@cerrarHistoriaClinica')->name('hc.cerrar');

Route::post('hc/anular', 'HistoriaClinicaController@anularHistoriaClinica')->name('hc.anular');

Route::post('hc/crearNota', 'HistoriaClinicaController@crearNotaEvolucion')->name('hc.crearnota');

Route::get('hc/atenciones', 'HistoriaClinicaController@viewVistaAdministrativa')->name('historiaClinica.viewAtenncionesGenerales');

Route::get('hc/atencionesPage', 'HistoriaClinicaController@atencionesPorCriterio')->name('historiaclinica.atencionesPage');

//-----------------------------------------reportes-----------------------------------------------------

Route::get('historiaClinica/reporteDiagnostico', 'ReportController@view')->name('historia.reporteDiag');

Route::get('historiaClinica/{id}', 'HistoriaClinicaController@show');

Route::get('historiaOptometria/{id}', 'HistoriaOptometriaController@show');

Route::post('historiaClinica/reporteDiagnostico','ReportController@generateReport')->name('historia.generarDiag');

Route::get('reportes/reporteOptometria', 'CertificadoOptometriaController@view')->name('optometria.reporte');

Route::get('reportes/reporteOptometria/{id}','CertificadoOptometriaController@generateCertificate')->name('optometria.generarCertificado');

Route::get('reportes/reporteClinico/{id}','CertificadoHistoriaClinicaController@generateCertificate')->name('historiaclinica.generarCertificado');

Route::get('reportes/reporteClinicounificado/{id}','CertificadoHistoriaClinicaController@generateCertificateUnificado')->name('historiaclinica.generarInforme');

Route::get('reportes/reporteVisiometria', 'CertificadoVisiometriaController@view')->name('visiometria.reporte');

Route::get('reportes/reporteVisiometria/{id}','CertificadoVisiometriaController@generateCertificate')->name('visiometria.generarCertificado');

Route::get('reportes/reporteAudiometria/{id}','CertificadoAudiometriaController@generateCertificate')->name('audiometria.generarCertificado');

//----------------------------------------------------------------------------------------------

Route::post('registro', 'pagesController@registerPatient');

//-----------------------------------------Historia Optometria------------------------------------------

Route::resource('/optometria', 'HistoriaOptometriaController');

Route::get('historiaOptometria/{id}', 'HistoriaOptometriaController@getHOWeb')->name('historiaOptometria.getHO');

Route::post('ho/anular', 'HistoriaOptometriaController@anular')->name('ho.anular');

Route::get('/print/opto/{id}', 'HistoriaOptometriaController@print');

Route::get('/print/opto2/{id}', 'HistoriaOptometriaController@print2');

//-----------------------------------------Historia Visiometria------------------------------------------

Route::get('visiometria/register', 'HistoriaVisiometriaController@create')->name('historiaVisiometria.create');

Route::post('visiometria/register', 'HistoriaVisiometriaController@store')->name('historiaVisiometria.store');

Route::get('historiaVisiometria/{id}', 'HistoriaVisiometriaController@getHVWeb')->name('historiaVisiometria.getHV');

Route::post('hv/anular', 'HistoriaVisiometriaController@anular')->name('hv.anular');

//-----------------------------------------Historia Audiometria------------------------------------------

Route::get('audiometria/register', 'HistoriaAudiometriaController@create')->name('historiaAudiometria.register');

Route::post('audiometria/register', 'HistoriaAudiometriaController@store')->name('historiaAudiometria.store');

Route::get('audiometria/{id}', 'HistoriaAudiometriaController@getHAWeb')->name('historiaAudiometria.getHA');
/*Route::get('audiometria/{id}', function ($id){
    $historia = HistoriaAudiometria::with([
        'antecedentes',
        'otoscopia',
        'recomendacion',
        'audiograma', 'empresa'
    ])->find(decrypt($id));
    return $historia;
});*/

Route::post('ha/anular', 'HistoriaAudiometriaController@anular')->name('ha.anular');

//-----------------------------------------EXPORT--------------------------------------------------------

Route::get('export/bd', 'HistoriaClinicaController@viewExportDataBase')->name('historiaClinica.viewExport');

Route::get('export/historiaclinica', 'HistoriaClinicaController@export')->name('historiaClinica.export');

