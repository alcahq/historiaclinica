@extends('layouts')

@section('contenido')

    <div class="container-fluid vertical-center">
        <form method="POST" action="/login">
            @csrf
            <div class="row padding-top-log">
                <div class="offset-md-4 col-md-4 container-img">
                    <img class="img-login" src="/img/Logo_AnaliSO.jpg" alt="">
                </div>
            </div>

            <div class="row margin-login">
                <div class="offset-md-4 col-md-4">
                    <h3 class="title-sections-log">INICIAR SESIÓN</h3>
                    <input class="fild-login form-control" type="text" name="email" placeholder="Usuario" value="{{ old('email') }}">
                    {{ $errors->first('email') }}
                </div>
            </div>

            <div class="row margin-login">
                <div class="offset-md-4 col-md-4">
                    <input type="password" class="fild-login form-control" name="password" placeholder="Contraseña" value="{{ old('password') }}">
                    {{ $errors->first('password') }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input class="button-login btn btn-primary" type="submit" value="Ingresar">
                </div>
            </div>
        </form>
    </div>
    <br>
@stop
