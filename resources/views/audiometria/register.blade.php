@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
    <script src="{{asset('/js/historiaAudiometria.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
@stop
@section('title')
    <title>Historia Audiometria - Registrar</title>
@stop
@section('contenido')
    <div class="container-local">
        <h4 class="title-principal">REGISTRAR HISTORIA AUDIOMETRIA</h4>
    </div>
    <div class="container-local border-container">
        <div class="toast toast-local" data-autohide="false" id="toastHA">
            <div class="toast-header">
                <strong class="mr-auto">Alerta</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 padd-label" style="padding-right: 0">
                <label for="fecha" class="font-bold">Fecha:</label>
                <input value="{{ now('GMT-5') }}" id="fechaRegistro" name="fecha" class="form-control-form border-white" >
                <label for="fecha" class="font-bold" style="padding-right: 8px">Paciente:</label>
                <input class="form-control-form" placeholder="Buscar Paciente" name="documento" id="documentoBuscado">
            </div>
            <div class="col-md-6 padd-row-ppl padd-label">
                <label for="fecha" class="font-bold padd-label width-lbl">Médico:</label>
                <input value="{{ auth()->user()->name }} {{ auth()->user()->last_name }}" id="medicoNombre" class="form-control-form border-white size-input-header" style="color: #003594;" disabled>
                <input value="{{"CC: ".auth()->user()->document }}" id="medicoCedula" class="form-control-form border-white size-input-header" disabled style="color: #003594;">
                <input value="{{auth()->user()->id }}" id="id_medicoA" type="hidden">
                <label for="fecha" class="font-bold padd-label width-lbl">Nombre:</label>
                <input class="form-control-form border-white size-input-header" id="nombrePaciente" style="color: #003594;" disabled>
                <input class="form-control-form border-white size-input-header" id="documentoPaciente" style="color: #003594; width: 20%" disabled>
                <input type="text" class="form-control-form border-white" id="edadPaciente" style="color: #003594; width: 20%" disabled>
                <input type="hidden" id="estadoHA" value="Abierta">
            </div>
        </div>
        <div class="row" style="padding-top: 1%">
            <div class="col-md-2" style="padding-right: 0; padding-top: 6px">
                <label for="motivo-evaluacion" class="font-bold" style="padding-right: 8px">Motivo de la evaluación:</label>
            </div>
            <div class="col-md-3" style="padding-right: 0; padding-bottom: 1%; padding-left: 0">
                <div class="form-group size-motivoE div-select-form">
                    <select class="select-form form-control" id="motivoE" name="motivoEvaluacion" style="font-size: 0.8rem">
                        <option></option>
                        <option value="INGRESO">INGRESO</option>
                        <option value="PERIÓDICO">PERIÓDICO</option>
                        <option value="RETIRO">RETIRO</option>
                        <option value="POSTINCAPACIDAD">POSTINCAPACIDAD</option>
                    </select>
                </div>
            </div>
            <div class="col-md-7">
                <label for="cargo evaluar" class="font-bold" style="padding-left: 12px; padding-right: 5%">Cargo a evaluar:</label>
                <input type="text" name="cargoEvaluar" id="cargoE" class="size-input-cargo form-control-form" placeholder="Cargo a evaluar">
            </div>
        </div>
        <div class="row">
            <div class="col-md-1" style="padding-right: 0">
                <label for="rh" class="font-bold">Otros:</label>
            </div>
            <div class="col-md-11" style="padding-right: 0; padding-left: 0">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Trabajo en alturas">
                    <label class="form-check-label" for="Trabajo en alturas">Trabajo en alturas</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Reubicación laboral">
                    <label class="form-check-label" for="Reubicación laboral">Reubicación laboral</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label for="empresaActual" class="font-bold" style="margin-top: 4px">Empresa:</label>
            </div>
            <div class="col-md-9" style="padding-left: 0">
                <input type="text" name="empresa" id="empresaActual" class="size-input-emp form-control-form" autocomplete="off" style="padding: 3px 10px">
            </div>
        </div>
    </div>
    <div class="container-local">
        <ul class="nav nav-tabs" id="tabHC" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="antecedentes-tab" data-toggle="tab" href="#antecedentes" role="tab" aria-controls="antecedentes" aria-selected="true">Antecedentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="otoscopia-tab" data-toggle="tab" href="#otoscopia" role="tab" aria-controls="otoscopia" aria-selected="false">Otoscopia</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="audiograma-tab" data-toggle="tab" href="#audiograma" role="tab" aria-controls="audiograma" aria-selected="false">Audiograma</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="recomendaciones-tab" data-toggle="tab" href="#recomendaciones" role="tab" aria-controls="recomendaciones" aria-selected="false">Conducta y recomendaciones</a>
            </li>
        </ul>
        <div class="tab-content border-tabs" id="myTabContent">
            <div class="tab-pane fade show active" id="antecedentes" role="tabpanel" aria-labelledby="antecedentes-tab">
                <h5 class="title-sections" id="sub-title">ANTECEDENTES OCUPACIONALES</h5>
                <div class="border-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Ocupación anterior:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_ocupacionanterior" id="ocupacionales_ocupacionanterior">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Ocupación actual:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_ocupacionactual" id="ocupacionales_ocupacionactual">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Labores que desempeña:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_laboresdesempena" id="ocupacionales_laboresdesempena">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Jornada laboral:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_jornadalaboral" id="ocupacionales_jornadalaboral">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Utiliza protección auditiva:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="utilizaproteccionSi" name="ocupacionales_utilizaproteccion" value="Si">
                                        <label class="custom-control-label" for="utilizaproteccionSi">Si</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="utilizaproteccionNo" name="ocupacionales_utilizaproteccion" value="No" checked>
                                        <label class="custom-control-label" for="utilizaproteccionNo">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">¿De que tipo?:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_utilizaproteccion_tipo" id="ocupacionales_utilizaproteccion_tipo">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Tiempo ocupacion anterior:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_ocupacionanterior_tiempo" id="ocupacionales_ocupacionanterior_tiempo">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Tiempo en ocupación actual:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_ocupacionactual_tiempo" id="ocupacionales_ocupacionactual_tiempo">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Horario:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_horario" id="ocupacionales_horario">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Tiempo de exposición al ruido laboral:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="tiempoExp[]" value="Diurno">
                                        <label class="form-check-label" for="">Diurno</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="tiempoExp[]" value="Nocturno">
                                        <label class="form-check-label" for="">Nocturno</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="tiempoExp[]" value="Rotativo">
                                        <label class="form-check-label" for="">Rotativo</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="tiempoExp[]" value="Horas extras">
                                        <label class="form-check-label" for="">Horas extras</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 padding-top-lbl">
                                    <label for="" class="">Hace cuanto:</label>
                                </div>
                                <div class="col-md-8 nopadd-col">
                                    <input type="text" class="form-control-audio" name="ocupacionales_utilizaproteccion_tiempo" id="ocupacionales_utilizaproteccion_tiempo">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <h5 class="title-sections" id="sub-title">ANTECEDENTES FAMILIARES OTOLÓGICOS</h5>
                <div class="border-container">
                    <textarea name="familiares_otologicos" id="familiares_otologicos" class="form-control-form" placeholder="Antecedentes otologicos" rows="4" style="width: 100%"></textarea>
                </div>
                <h5 class="title-sections" id="sub-title">ANTECEDENTES PERSONALES</h5>
                <div class="border-container">
                    <div class="row">
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">OTOLÓGICOS</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12">
                                    <label for="">Otalgia:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOto" name="otologicos_otalgia" value="Si">
                                        <label class="custom-control-label" for="siOto">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOto" name="otologicos_otalgia" value="No" checked>
                                        <label class="custom-control-label" for="noOto">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otitis:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtitis" name="otologicos_otitis" value="Si">
                                        <label class="custom-control-label" for="siOtitis">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtitis" name="otologicos_otitis" value="No" checked>
                                        <label class="custom-control-label" for="noOtitis">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otorrea:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtorrea" name="otologicos_otorrea" value="Si">
                                        <label class="custom-control-label" for="siOtorrea">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtorrea" name="otologicos_otorrea" value="No" checked>
                                        <label class="custom-control-label" for="noOtorrea">No</label>
                                    </div>
                                    <br>
                                    <label for="">Prurito:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siPrurito" name="otologicos_prurito" value="Si">
                                        <label class="custom-control-label" for="siPrurito">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noPrurito" name="otologicos_prurito" value="No" checked>
                                        <label class="custom-control-label" for="noPrurito">No</label>
                                    </div>
                                    <br>
                                    <label for="">Sensación oido tapado:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOTapado" name="otologicos_sensacionoido" value="Si">
                                        <label class="custom-control-label" for="siOTapado">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOTapado" name="otologicos_sensacionoido" value="No" checked>
                                        <label class="custom-control-label" for="noOTapado">No</label>
                                    </div>
                                    <br>
                                    <label for="">Vértigo:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siVertigo" name="otologicos_vertigo" value="Si">
                                        <label class="custom-control-label" for="siVertigo">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noVertigo" name="otologicos_vertigo" value="No" checked>
                                        <label class="custom-control-label" for="noVertigo">No</label>
                                    </div>
                                    <br>
                                    <label for="">Tinitus:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siTinitus" name="otologicos_tinitus" value="Si">
                                        <label class="custom-control-label" for="siTinitus">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noTinitus" name="otologicos_tinitus" value="No" checked>
                                        <label class="custom-control-label" for="noTinitus">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otros:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtrosOto" name="otologicos_otros" value="Si">
                                        <label class="custom-control-label" for="siOtrosOto">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtrosOto" name="otologicos_otros" value="No" checked>
                                        <label class="custom-control-label" for="noOtrosOto">No</label>
                                    </div>
                                    <br>
                                    <label for="">¿Cuales?:</label>
                                    <input type="text" name="" id="otologicos_cuales" class="form-control-form" style="width: 100%">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">PATOLÓGICO</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12">
                                    <label for="">Hipertensión:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siHiper" name="patologico_hipertension" value="Si">
                                        <label class="custom-control-label" for="siHiper">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noHiper" name="patologico_hipertension" value="No" checked>
                                        <label class="custom-control-label" for="noHiper">No</label>
                                    </div>
                                    <br>
                                    <label for="">Diabetes:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siDiabetes" name="patologico_diabetes" value="Si">
                                        <label class="custom-control-label" for="siDiabetes">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noDiabetes" name="patologico_diabetes" value="No" checked>
                                        <label class="custom-control-label" for="noDiabetes">No</label>
                                    </div>
                                    <br>
                                    <label for="">Parotiditis:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siParotidi" name="patologico_parotiditis" value="Si">
                                        <label class="custom-control-label" for="siParotidi">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noParotidi" name="patologico_parotiditis" value="No" checked>
                                        <label class="custom-control-label" for="noParotidi">No</label>
                                    </div>
                                    <br>
                                    <label for="">Sarampión:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siSarampion" name="patologico_sarampion" value="Si">
                                        <label class="custom-control-label" for="siSarampion">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noSarampion" name="patologico_sarampion" value="No" checked>
                                        <label class="custom-control-label" for="noSarampion">No</label>
                                    </div>
                                    <br>
                                    <label for="">Rubéola:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siRubeola" name="patologico_rubeola" value="Si">
                                        <label class="custom-control-label" for="siRubeola">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noRubeola" name="patologico_rubeola" value="No" checked>
                                        <label class="custom-control-label" for="noRubeola">No</label>
                                    </div>
                                    <br>
                                    <label for="">Rinitis/Sinusitis:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siRinitis" name="patologico_rinitis" value="Si">
                                        <label class="custom-control-label" for="siRinitis">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noRinitis" name="patologico_rinitis" value="No" checked>
                                        <label class="custom-control-label" for="noRinitis">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otros:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtrosPatologia" name="patologico_otros" value="Si">
                                        <label class="custom-control-label" for="siOtrosPatologia">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtrosPatologia" name="patologico_otros" value="No" checked>
                                        <label class="custom-control-label" for="noOtrosPatologia">No</label>
                                    </div>
                                    <br>
                                    <label for="">¿Cuales?:</label>
                                    <input type="text" name="patologico_cuales" id="patologico_cuales" class="form-control-form" style="width: 100%;margin-bottom: 8%">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">EXTRA LABORAL</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12">
                                    <label for="">Tejo:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siTejo" name="extralaboral_tejo" value="Si">
                                        <label class="custom-control-label" for="siTejo">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noTejo" name="extralaboral_tejo" value="No" checked>
                                        <label class="custom-control-label" for="noTejo">No</label>
                                    </div>
                                    <label for="">Moto:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siMoto" name="extralaboral_moto" value="Si">
                                        <label class="custom-control-label" for="siMoto">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noMoto" name="extralaboral_moto" value="No" checked>
                                        <label class="custom-control-label" for="noMoto">No</label>
                                    </div>
                                    <br>
                                    <label for="">Musica alta:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siMusica" name="extralaboral_musica" value="Si">
                                        <label class="custom-control-label" for="siMusica">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noMusica" name="extralaboral_musica" value="No" checked>
                                        <label class="custom-control-label" for="noMusica">No</label>
                                    </div>
                                    <br>
                                    <label for="">Servicio militar:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siMilitar" name="extralaboral_serviciomilitar" value="Si">
                                        <label class="custom-control-label" for="siMilitar">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noMilitar" name="extralaboral_serviciomilitar" value="No" checked>
                                        <label class="custom-control-label" for="noMilitar">No</label>
                                    </div>
                                    <br>
                                    <label for="">Audifonos:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siAudifonos" name="extralaboral_audifonos" value="Si">
                                        <label class="custom-control-label" for="siAudifonos">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noAudifonos" name="extralaboral_audifonos" value="No" checked>
                                        <label class="custom-control-label" for="noAudifonos">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otros:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtrosExtra" name="extralaboral_otros" value="Si">
                                        <label class="custom-control-label" for="siOtrosExtra">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtrosExtra" name="extralaboral_otros" value="No" checked>
                                        <label class="custom-control-label" for="noOtrosExtra">No</label>
                                    </div>
                                    <br>
                                    <label for="">¿Cuales?:</label>
                                    <input type="text" name="extralaboral_cuales" id="extralaboral_cuales" class="form-control-form" style="width: 100%;margin-bottom: 15.7%">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">TÓXICOS DEL NERVIO ACÚSTICO</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12" style="margin-bottom: 21.8%">
                                    <label for="">Industriales:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siIndust" name="toxicosnervio_industriales" value="Si">
                                        <label class="custom-control-label" for="siIndust">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noIndust" name="toxicosnervio_industriales" value="No" checked>
                                        <label class="custom-control-label" for="noIndust">No</label>
                                    </div>
                                    <br>
                                    <label for="">Farmacos:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siFarm" name="toxicosnervio_farmacos" value="Si">
                                        <label class="custom-control-label" for="siFarm">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noFarm" name="toxicosnervio_farmacos" value="No" checked>
                                        <label class="custom-control-label" for="noFarm">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">QUIRÚRGICOS</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12">
                                    <label for="">Cirugia de oido:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siCirugia" name="quirurgicos_cirugiaoido" value="Si">
                                        <label class="custom-control-label" for="siCirugia">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noCirugia" name="quirurgicos_cirugiaoido" value="No" checked>
                                        <label class="custom-control-label" for="noCirugia">No</label>
                                    </div>
                                    <br>
                                    <label for="">Timpanoplastia:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siTimpano" name="quirurgicos_timpanoplastia" value="Si">
                                        <label class="custom-control-label" for="siTimpano">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noTimpano" name="quirurgicos_timpanoplastia" value="No" checked>
                                        <label class="custom-control-label" for="noTimpano">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otros:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtrosQuir" name="quirurgicos_otros" value="Si">
                                        <label class="custom-control-label" for="siOtrosQuir">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtrosQuir" name="quirurgicos_otros" value="No" checked>
                                        <label class="custom-control-label" for="noOtrosQuir">No</label>
                                    </div>
                                    <br>
                                    <label for="">¿Cuales?:</label>
                                    <input type="text" name="quirurgicos_cuales" id="quirurgicos_cuales" class="form-control-form" style="width: 100%">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">TRAUMÁTICOS</h6>
                            <div class="border-container">
                                <div id="divAP" class="col-md-12">
                                    <label for="">Cráneo:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siTrauma" name="traumaticos_craneo" value="Si">
                                        <label class="custom-control-label" for="siTrauma">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noTrauma" name="traumaticos_craneo" value="No" checked>
                                        <label class="custom-control-label" for="noTrauma">No</label>
                                    </div>
                                    <br>
                                    <label for="">Acústico:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siAcustico" name="traumaticos_acustico" value="Si">
                                        <label class="custom-control-label" for="siAcustico">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noAcustico" name="traumaticos_acustico" value="No" checked>
                                        <label class="custom-control-label" for="noAcustico">No</label>
                                    </div>
                                    <br>
                                    <label for="">Otros:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siOtrosTrauma" name="traumaticos_otros" value="Si">
                                        <label class="custom-control-label" for="siOtrosTrauma">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noOtrosTrauma" name="traumaticos_otros" value="No" checked>
                                        <label class="custom-control-label" for="noOtrosTrauma">No</label>
                                    </div>
                                    <br>
                                    <label for="">¿Cuales?:</label>
                                    <input type="text" name="traumaticos_cuales" id="traumaticos_cuales" class="form-control-form" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="otoscopia" class="tab-pane fade">
                <div class="row" style="margin-top: 1%;">
                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">PABELLÓN AURICULAR</h6>
                        <div class="border-container">
                            <div id="divAP" class="col-md-12" style="margin-bottom: 22%">
                                <label for="">Normal:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PANormal[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PANormal[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Atresia:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PAArtesia[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PAArtesia[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Agenesia:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PAAgenesia[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PAAgenesia[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Cicatriz:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PACicatriz[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="PACicatriz[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Otros:</label>
                                <input type="text" name="pabellonauricular_otros" id="pabellonauricular_otros" class="form-control-form" style="width: 100%">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">CAE</h6>
                        <div class="border-container">
                            <div id="divAP" class="col-md-12" style="margin-bottom: 29.7%">
                                <label for="">Normal:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAENormal[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAENormal[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Tapon parcial:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAETapon[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAETapon[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Tapon total:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAETaponT[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="CAETaponT[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Otros:</label>
                                <input type="text" name="cae_otros" id="cae_otros" class="form-control-form" style="width: 100%">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">MEMBRANA TIMPÁNICA</h6>
                        <div class="border-container">
                            <div id="divAP" class="col-md-12">
                                <label for="">Normal:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTNormal[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTNormal[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Perforada:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTPerforada[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTPerforada[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Hiperemica:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MThiperemica[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MThiperemica[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Placa calcarea:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTPlaca[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTPlaca[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Opaca:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTOpaca[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTOpaca[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Abultada:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTAbultada[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTAbultada[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">No se visualiza:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTNoVisu[]" value="OD">
                                    <label class="form-check-label" for="">OD</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="MTNoVisu[]" value="OI">
                                    <label class="form-check-label" for="">OI</label>
                                </div>
                                <br>
                                <label for="">Otros:</label>
                                <input type="text" name="membranatimpanica_otros" id="membranatimpanica_otros" class="form-control-form" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="audiograma" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-6">
                        <canvas id="myChart" width="400" height="200"></canvas>
                        <label for="">PTA oido derecho:</label>
                        <input type="text" id="ptaOD" value="0" class="font-bold" style="border: none" disabled>
                    </div>
                    <div class="col-md-6">
                        <canvas id="myChartOI" width="400" height="200"></canvas>
                        <label for="">PTA oido izquierdo:</label>
                        <input type="text" id="ptaOI" value="0" class="font-bold" style="border: none" disabled>
                    </div>
                </div>
                <div class="row" style="padding-top: 2%">
                    <div class="col-md-4 container-small">
                        <div class="border-container">
                            <div class="row">
                                <div class="col-md-4" id="colAudio">
                                    <label for="" class="font-bold" style="margin-top: 7px">Frecuencia</label><br>
                                    <label for="">250 Hz</label><br>
                                    <label for="">500 Hz</label><br>
                                    <label for="">1000 Hz</label><br>
                                    <label for="">2000 Hz</label><br>
                                    <label for="">3000 Hz</label><br>
                                    <label for="">4000 Hz</label><br>
                                    <label for="">6000 Hz</label><br>
                                    <label for="">8000 Hz</label>
                                </div>
                                <div class="col-md-4" id="colAudio" style="text-align: center">
                                    <label for="" class="font-bold">Oido derecho</label>
                                    <input type="text" id="OD250" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD500" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD1000" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD2000" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD3000" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD4000" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD6000" class="OD form-control-form" style="width: 100%">
                                    <input type="text" id="OD8000" class="OD form-control-form" style="width: 100%">
                                </div>
                                <div class="col-md-4" id="colAudio" style="text-align: center">
                                    <label for="" class="font-bold" style="width: 100%">Oido izquierdo</label>
                                    <input type="text" id="OI250" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI500" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI1000" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI2000" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI3000" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI4000" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI6000" class="OI form-control-form" style="width: 100%">
                                    <input type="text" id="OI8000" class="OI form-control-form" style="width: 100%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h6 class="tab-subtitle">IMPRESIÓN DIAGNÓSTICA</h6>
                        <div class="border-container">
                            <textarea class="form-control-form" name="impresiondiagnostica" id="impresiondiagnostica" rows="6" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div id="recomendaciones" class="tab-pane fade">
                <div class="row" style="margin-top: 2%">
                    <div class="col-md-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Control audiológico semestral">
                            <label class="form-check-label" for="">Control audiológico semestral</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="EPP Auditivo">
                            <label class="form-check-label" for="">EPP Auditivo</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Reposo auditivo extra laboral">
                            <label class="form-check-label" for="">Reposo auditivo extra laboral</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Conducir con la ventana cerrada">
                            <label class="form-check-label" for="">Conducir con la ventana cerrada</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Ingreso PVE">
                            <label class="form-check-label" for="">Ingreso PVE</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Rotar la diadema telefónica">
                            <label class="form-check-label" for="">Rotar la diadema telefónica</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Control audiológico anual">
                            <label class="form-check-label" for="">Control audiologico anual</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Examenes complementarios">
                            <label class="form-check-label" for="">Exámenes complementarios</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Audiometria confirmatoria">
                            <label class="form-check-label" for="">Audiometria confirmatoria</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Valoración/Control por ORL">
                            <label class="form-check-label" for="">Valoración/Control por ORL</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Remisión a EPS">
                            <label class="form-check-label" for="">Remision a EPS</label>
                        </div>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="recomendaciones[]" value="Otros">
                            <label class="form-check-label" for="">Otros</label>
                        </div>
                    </div>
                </div>
                <div class="border-container" style="margin-top: 1%">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Paciente es compatible para realizar la labor:</label>
                                </div>
                                <div class="col-md-6" style="padding-top: 1.2%;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="pacientecompatible" id="radioDiabetes1" value="Si">
                                        <label class="form-check-label" for="radioDiabetes1">Si</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="pacientecompatible" id="radioDiabetes2" value="No" checked>
                                        <label class="form-check-label" for="radioDiabetes2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Observaciones:</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control-form" style="width: 100%" id="pacientecompatible_obs" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Requiere nueva valoración:</label>
                                </div>
                                <div class="col-md-6" style="padding-top: 1.2%;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="requierevaloracion" id="radioDiabetes1" value="Si">
                                        <label class="form-check-label" for="radioDiabetes1">Si</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="requierevaloracion" id="radioDiabetes2" value="No" checked>
                                        <label class="form-check-label" for="radioDiabetes2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="recomendaciones_recomendaciones_requierevaloracion_obs">Observaciones:</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control-form" style="width: 100%" id="requierevaloracion_obs" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Requiere remision a especialista:</label>
                                </div>
                                <div class="col-md-6" style="padding-top: 1.2%;">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="requiereremision" id="radioDiabetes1" value="Si">
                                        <label class="form-check-label" for="radioDiabetes1">Si</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="requiereremision" id="radioDiabetes2" value="No" checked>
                                        <label class="form-check-label" for="radioDiabetes2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="Requiere valoracion obs">Observaciones:</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control-form" style="width: 100%" id="requiereremision_obs" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-5">
                <input type="submit" value="Guardar" class="button-form btn btn-primary" id="registrarHA">
            </div>
        </div>
    </div>
@stop
