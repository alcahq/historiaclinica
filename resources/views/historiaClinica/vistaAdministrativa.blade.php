@extends('layouts')
@section('js')
    <script src="{{asset('/js/atenciones.js')}}"></script>
@stop
@section('title')
    <title>Historia Clínica - Visión general</title>
@stop
@section('contenido')

<form action="{{ route('historiaclinica.atencionesPage') }}" method="GET">

    <div class="container-local">
        <h4 class="title-principal">VISIÓN GENERAL DE ATENCIONES</h4>
    </div>

    <div class="container-local border-container">
        {!! csrf_field() !!}
        <div class="row" style="padding-bottom: 1%">
            <div class="col-md-12" id="criterio_select">
                <label for="criterio seleccion" class="font-bold" style="padding-right: 1%">Seleccione el criterio de busqueda:</label>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="profesional" name="criterio" value="profesional" {{ isset($criterio) == 'true' && $criterio == 'profesional' ? "checked":"" }}>
                    <label class="custom-control-label" for="profesional">Profesional de la salud</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="fecha" name="criterio" value="fecha" {{ isset($criterio) == 'true' && $criterio == 'fecha' ? "checked":"" }}>
                    <label class="custom-control-label" for="fecha">Fecha de atención</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="estado" name="criterio" value="estado" {{ isset($criterio) == 'true' && $criterio == 'estado' ? "checked":"" }}>
                    <label class="custom-control-label" for="estado">Estado de atención</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="empresa" name="criterio" value="empresa" {{ isset($criterio) == 'true' && $criterio == 'empresa' ? "checked":"" }}>
                    <label class="custom-control-label" for="empresa">Empresa</label>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 2%">
            <div class="col-md-1">
                <label for="profesional" class="font-bold profesional" hidden="true">Profesional:</label>
                <label for="fecha" class="font-bold fecha" hidden="true">Fecha:</label>
                <label for="estado" class="font-bold estado" hidden="true">Estado:</label>
                <label for="espresa" class="font-bold empresa" hidden="true">Empresa:</label>
            </div>
            <div class="col-md-4">
                <div class="form-group div-select-form profesional" style="width: 100%" hidden="true">
                    <select class="select-form form-control" name="profesional_select" id="profesional_select">
                        <option value=""></option>
                        @if(isset($profesionales))
                            @foreach($profesionales as $profesional)
                                <option value="{{ $profesional->nombre_completo }}" {{ isset($medico) == 'true' && $medico == $profesional->nombre_completo ? "selected":"" }}>{{ $profesional->nombre_completo }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="fecha" hidden="true">
                    <input type="text" class="form-control-form datepicker" name="fecha_atencion" id="fecha_atencion" autocomplete="off" style="width: 100%;" value="{{ isset($fecha) == 'true' ? date('d-m-Y', strtotime($fecha)):"" }}">
                </div>
                <div class="form-group div-select-form estado" style="width: 100%" hidden="true">
                    <select class="select-form form-control" name="estado_select" id="estado_select">
                        <option value=""></option>
                        <option value="abierta" {{ isset($estado) == "true" && $estado == 'abierta' ? "selected":"" }}>Abierta</option>
                        <option value="cerrada" {{ isset($estado) == "true" && $estado == 'cerrada' ? "selected":"" }}>Cerrada</option>
                        <option value="anulada" {{ isset($estado) == "true" && $estado == 'anulada' ? "selected":"" }}>Anulada</option>
                    </select>
                </div>
                <div class="empresa" hidden="true">
                    <input type="text" name="empresa_select" id="empresa_select" class="size-input-emp form-control-form" autocomplete="off" style="padding: 3px 10px;" value="{{ isset($empresa) == 'true' ? $empresa:"" }}">
                </div>
            </div>
            <div class="col-md-1 buscar" hidden="true">
                <input type="submit" value="Buscar" class="button-form btn btn-primary" style="margin: 0; padding: 0.2rem 0.75rem;">
            </div>
        </div>
        @if(session()->has('nores'))
            <h2>{{ session()->get('nores') }}</h2>
        @endif
        <table class="table table-hover table-bordered">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Documento</th>
                    <th scope="col" style="width: 24%">Paciente</th>
                    <th scope="col" style="width: 14%">Fecha</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Estado</th>
                    <th scope="col" style="width: 13%">Tipo historia</th>
                    <th scope="col">Consultar</th>
                    <th scope="col">Certificado</th>
                </tr>
            </thead>
            <tbody>
            @if(@isset($result))
                @if($result->count())
                    @if(auth()->user()->hasRoles(['admin','medico']))
                        @foreach($result as $hc)
                            <tr>
                                <th scope="row" class="count"></th>
                                <td>{{ $hc->cedula}}</td>
                                <td>{{ $hc->nombre_paciente }}</td>
                                <td>{{ $hc->fecha }}</td>
                                <td>{{ $hc->empresa }}</td>
                                <td>{{ $hc->estado }}</td>
                                <td>{{ $hc->tipo }}</td>
                                @if($hc->tipo == "Historia Clínica")
                                    <td><a href="{{ route('historiaClinica.getHC', encrypt($hc->id)) }}">Consultar</a></td>
                                    <td>
                                        <a href="{{ route('historiaclinica.generarCertificado', $hc->id) }}">Certificado</a><br>
                                        <a href="{{ route('historiaclinica.generarInforme', $hc->id) }}">Informe</a>
                                    </td>
                                @endif
                                @if($hc->tipo == "Historia Optometría")
                                    <td><a href="{{ route('historiaOptometria.getHO', encrypt($hc->id)) }}">Consultar</a></td>
                                    <td><a href="{{ route('optometria.generarCertificado', $hc->id) }}">Certificado</a></td>
                                @endif
                                @if($hc->tipo == "Historia Audiometría")
                                    <td><a href="{{ route('historiaAudiometria.getHA', encrypt($hc->id)) }}">Consultar</a></td>
                                    <td><a href="{{ route('audiometria.generarCertificado', $hc->id) }}">Certificado</a></td>
                                @endif
                                @if($hc->tipo == "Historia Visiometría")
                                    <td><a href="{{ route('historiaVisiometria.getHV', encrypt($hc->id)) }}">Consultar</a></td>
                                    <td><a href="{{ route('visiometria.generarCertificado', $hc->id) }}">Certificado</a></td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                @endif
            @endif
            </tbody>
        </table>
        @isset($result)
            <div class="row">
                <div class="col-md-4 offset-4">
                    {{$result->appends(request()->input())->links()}}
                </div>
            </div>
        @endisset
    </div>
</form>
@stop
