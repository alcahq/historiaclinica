@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
    <script src="{{asset('/js/historiaClinica.js')}}"></script>
@stop
@section('title')
    <title>Historia Clínica - Registrar</title>
@stop
@section('contenido')
    <div class="container-local">
        <h4 class="title-principal">REGISTRAR HISTORIA CLÍNICA</h4>
    </div>
    <div class="container-local border-container">
        <div class="toast toast-local" data-autohide="false" id="toastHC">
            <div class="toast-header">
                <strong class="mr-auto">Alerta</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 padd-label" style="padding-right: 0">
                <label for="fecha" class="font-bold">Fecha:</label>
                <input value="{{ now('GMT-5') }}" id="fechaRegistro" class="form-control-form border-white" disabled>
                <label for="fecha" class="font-bold" style="padding-right: 8px">Paciente:</label>
                <input class="form-control-form" placeholder="Buscar Paciente" name="documento" id="documentoBuscado">
            </div>

            <div class="col-md-6 padd-row-ppl padd-label">
                <label for="fecha" class="font-bold padd-label width-lbl">Médico:</label>
                <input value="{{ auth()->user()->name }} {{ auth()->user()->last_name }}" id="medicoNombre" class="form-control-form border-white size-input-header" style="color: #003594;" disabled>
                <input value="{{"CC: ".auth()->user()->document }}" id="medicoCedula" class="form-control-form border-white size-input-header" disabled style="color: #003594;">
                <input value="{{auth()->user()->id }}" id="id_medicoA" type="hidden">
                <input value="{{auth()->user()->id }}" name="medicoCierre" id="id_medicoC" type="hidden">
                <label for="fecha" class="font-bold padd-label width-lbl">Nombre:</label>
                <input class="form-control-form border-white size-input-header" id="nombrePaciente" style="color: #003594;" disabled>
                <input class="form-control-form border-white" id="documentoPaciente" style="color: #003594; width: 20%" disabled>
                <input type="text" class="form-control-form border-white" id="edadPaciente" style="color: #003594; width: 20%" disabled>
                <input type="hidden" id="estadoHC" value="Abierta">
            </div>

            <div class="col-md-2 offset-md-1">
                <div class="container-estado">
                    <div class="row">
                        <div class="col-md-12 col-title-estado">
                            <span class="">Estado: Abierta</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 1%">
            <div class="col-md-2" style="padding-right: 0; padding-top: 6px">
                <label for="motivo-evaluacion" class="font-bold" style="padding-right: 8px">Motivo de la evaluación:</label>
            </div>
            <div class="col-md-3" style="padding-right: 0; padding-bottom: 1%; padding-left: 0">
                <div class="form-group size-motivoE div-select-form">
                    <select class="select-form form-control" id="motivoE" name="motivoEvaluacion" style="font-size: 0.8rem">
                        <option value=""></option>
                        <option value="INGRESO">INGRESO</option>
                        <option value="PERIÓDICO">PERIÓDICO</option>
                        <option value="RETIRO">RETIRO</option>
                        <option value="POSTINCAPACIDAD">POSTINCAPACIDAD</option>
                    </select>
                </div>
            </div>
            <div class="col-md-7">
                <label for="cargo evaluar" class="font-bold" style="padding-left: 12px; padding-right: 5%">Cargo a evaluar:</label>
                <input type="text" name="cargoEvaluar" id="cargoE" class="size-input-cargo form-control-form" placeholder="Cargo a evaluar">
            </div>
        </div>
        <div class="row">
            <div class="col-md-1" style="padding-right: 0">
                <label for="rh" class="font-bold">Otros:</label>
            </div>
            <div class="col-md-11" style="padding-right: 0; padding-left: 0">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Trabajo en alturas">
                    <label class="form-check-label" for="Trabajo en alturas">Trabajo en alturas</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Manipulación de alimentos">
                    <label class="form-check-label" for="Manipulación de alimentos">Manipulación de alimentos</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Enfasis osteomuscular">
                    <label class="form-check-label" for="Enfasis osteomuscular">Enfasis osteomuscular</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="otros[]" value="Reubicación laboral">
                    <label class="form-check-label" for="Reubicación laboral">Reubicación laboral</label>
                </div>
            </div>
        </div>
    </div>
    <div class="container-local">
        <ul class="nav nav-tabs" id="tabHC" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="ocupacionales-tab" data-toggle="tab" href="#ocupacionales" role="tab" aria-controls="ocupacionales" aria-selected="true">Ocupacionales</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="laboral-tab" data-toggle="tab" href="#laboral" role="tab" aria-controls="laboral" aria-selected="false">Laboral</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="habitos-tab" data-toggle="tab" href="#habitos" role="tab" aria-controls="habitos" aria-selected="false">Hábitos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="antecedentes-tab" data-toggle="tab" href="#antecedentes" role="tab" aria-controls="antecedentes" aria-selected="true">Antecedentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="revisionPorSistemas-tab" data-toggle="tab" href="#revisionPorSistemas" role="tab" aria-controls="revisionPorSistemas" aria-selected="false">Revisión por Sistemas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="examenFisico-tab" data-toggle="tab" href="#examenFisico" role="tab" aria-controls="examenFisico" aria-selected="false">Examen físico</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="manipulacionAlimentos-tab" data-toggle="tab" href="#manipulacionAlimentos" role="tab" aria-controls="manipulacionAlimentos" aria-selected="true">Manipulación alimentos / Dermatológico</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="paraclinicos-tab" data-toggle="tab" href="#paraclinicos" role="tab" aria-controls="paraclinicos" aria-selected="false">Paraclínicos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="impresionDiagnostica-tab" data-toggle="tab" href="#impresionDiagnostica" role="tab" aria-controls="impresionDiagnostica" aria-selected="false">Impresion Diagnóstica</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="result-tab" data-toggle="tab" href="#result" role="tab" aria-controls="resultado" aria-selected="false">Resultado</a>
            </li>
        </ul>
        <div class="tab-content border-tabs" id="myTabContent">
            <div class="tab-pane fade show active" id="ocupacionales" role="tabpanel" aria-labelledby="ocupacionales-tab">
                <h5 class="title-sections" id="sub-title">CARGO A EVALUAR</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="fechaNacimiento" class="size-label-small">Fecha de Ingreso:</label>
                            <input type="text" class="size-input-small form-control-form datepicker" name="fechaIngreso" id="fechaIngreso" autocomplete="off" style="padding: 3px">
                        </div>
                        <div class="col-md-4" style="padding-left: 0; padding-right: 0">
                            <label for="cargo" class="size-label-small-c">Cargo:</label>
                            <input type="text" name="cargoActual" id="cargoActual" class="size-input-small-c form-control-form" placeholder="Cargo actual" disabled>
                        </div>
                        <div class="col-md-5 padding-lbl-0" id="jornada-tab">
                            <label for="jornada" class="size-label-small-j">Jornada de trabajo:</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="Diurno" name="jornadaTrabajo" value="Diurno">
                                <label class="custom-control-label" for="Diurno">Diurno</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="Nocturno" name="jornadaTrabajo" value="Nocturno">
                                <label class="custom-control-label" for="Nocturno">Nocturno</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="Rotativo" name="jornadaTrabajo" value="Rotativo">
                                <label class="custom-control-label" for="Rotativo">Rotativo</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="Horasextras" name="jornadaTrabajo" value="Horasextras">
                                <label class="custom-control-label" for="Horasextras">Horas extras</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cargo" class="size-label-antig">Antiguedad:</label>
                            <input type="text" name="antiguedadO" id="antiguedadO" class="form-control-form antiguedadO-size">
                            <div class="form-group div-select-form size-row-antig">
                                <select class="select-form form-control" name="antiguedadM" id="antiguedadM" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Días">Días</option>
                                    <option value="Meses">Meses</option>
                                    <option value="Años">Años</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding-left: 0; padding-right: 0">
                            <label for="cargo" class="size-label-small-c">Area:</label>
                            <input type="text" name="area" id="area" class="size-input-small-c form-control-form" placeholder="Area" value="{{ old('area') }}">
                        </div>
                        <div class="col-md-5 padding-lbl-0">
                            <label for="cargo" style="width: 26%" class="padding-lbl-0">Horas trabajadas:</label>
                            <input type="text" name="horasT" id="horasT" class="form-control-form" style="width: 8%">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            <label for="empresaActual" style="margin-top: 4px">Empresa:</label>
                        </div>
                        <div class="col-md-9" style="padding-left: 0">
                            <input type="text" name="empresa" id="empresaActual" class="size-input-emp form-control-form" autocomplete="off" style="padding: 3px 10px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 size-col-ep padding-lbl-0">
                            <label for="jornada" class="">Elementos de protección:</label>
                        </div>
                        <div class="col-md-10 observacionOtro" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Gafas">
                                <label class="form-check-label" for="Gafas">Gafas</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Botas">
                                <label class="form-check-label" for="Botas">Botas</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Guantes">
                                <label class="form-check-label" for="Guantes">Guantes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Tapabocas">
                                <label class="form-check-label" for="Tapabocas">Tapabocas</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Casco">
                                <label class="form-check-label" for="Casco">Casco</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Protector respiratorio">
                                <label class="form-check-label" for="Protector respiratorio">Protector respiratorio</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Overol">
                                <label class="form-check-label" for="Overol">Overol</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Protector auditivo">
                                <label class="form-check-label" for="Protector auditivo">Protector auditivo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Cofia">
                                <label class="form-check-label" for="Cofia">Cofia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Equipo trabajo en alturas/confinados">
                                <label class="form-check-label" for="Equipo trabajo en alturas/confinados">Equipo trabajo en alturas/confinados</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Careta">
                                <label class="form-check-label" for="Careta">Careta</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Calzado antideslizante">
                                <label class="form-check-label" for="Calzado antideslizante">Calzado antideslizante</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Ropa de trabajo">
                                <label class="form-check-label" for="Ropa de trabajo">Ropa de trabajo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Ninguno">
                                <label class="form-check-label" for="Ninguno">Ninguno</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="elementosP[]" value="Otros" id="otrosEP">
                                <label class="form-check-label" for="Otros">Otros</label>
                            </div>
                        </div>
                        <div class="col-md-2 size-col-ep">
                            <label for="Otros elementos" id="OtrosLbl" hidden="true">Otros elementos:</label>
                        </div>
                        <div class="col-md-9" style="padding-left: 0">
                            <input type="text" name="otrosObs" id="otrosObs" class="size-input-small-c form-control-form" value="{{ old('otrosObs') }}" hidden="true">
                        </div>
                    </div>
                </div>
                <h5 class="title-sections" id="sub-title">FACTORES DE RIESGO DEL CARGO A EVALUAR Y ANTERIORES</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;" data-spy="scroll" data-offset="0">
                    <div class="generalcontainer" id="generalcontainer0">
                        <div class="row">
                            <div class="col-md-5">
                                <label for="empresa" class="size-lbl-efr font-bold">Empresa:</label>
                                <input type="text" name="empresaFR" id="empresaFR0" class="form-control-form size-input-efr" disabled>
                            </div>
                            <div class="col-md-4" style="padding-left: 0">
                                <label for="cargo" class="size-lbl-cfr font-bold">Cargo:</label>
                                <input type="text" name="cargoFR" id="cargoFR0" class="form-control-form size-input-cfr" disabled>
                            </div>
                            <div class="col-md-3" style="padding-left: 0; padding-right: 0">
                                <label for="cargo" class="size-lbl-texp font-bold">Tiempo de exposición:</label>
                                <input type="text" name="tiempoExp" id="tiempoExp0" class="form-control-form size-input-texp">
                                <div class="form-group div-select-form size-row-antigTE">
                                    <select class="select-form form-control" name="antiguedadTE" id="antiguedadTE0" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Días">Días</option>
                                        <option value="Meses">Meses</option>
                                        <option value="Años">Años</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 3px">
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-2" style="max-width: 15.8%">
                                        <label for="fisicos" class="font-bold">Físicos:</label>
                                    </div>
                                    <div class="col-md-5" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Iluminación">
                                            <label class="form-check-label" for="iluminacion">Iluminación</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Temperatura alta">
                                            <label class="form-check-label" for="temperatura alta">Temperatura alta</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Radiaciones ionizantes">
                                            <label class="form-check-label" for="radiaciones ionizantes">Radiaciones ionizantes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Vibraciones">
                                            <label class="form-check-label" for="inlineCheckbox1">Vibraciones</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Temperatura baja">
                                            <label class="form-check-label" for="inlineCheckbox1">Temperatura baja</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Presión barométrica">
                                            <label class="form-check-label" for="inlineCheckbox2">Presión barométrica</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Radiaciones no ionizantes">
                                            <label class="form-check-label" for="inlineCheckbox2">Radiaciones no ionizantes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="fisicos0[]" value="Ruido">
                                            <label class="form-check-label" for="inlineCheckbox2">Ruido</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" style="padding-left: 0">
                                <div class="row">
                                    <div class="col-md-4" style="max-width: 30%">
                                        <label for="quimicos" class="font-bold">Químicos:</label>
                                    </div>
                                    <div class="col-md-3" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Gases">
                                            <label class="form-check-label" for="inlineCheckbox1">Gases</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Vapores">
                                            <label class="form-check-label" for="inlineCheckbox2">Vapores</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Humos">
                                            <label class="form-check-label" for="inlineCheckbox1">Humos</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3" id="jornada-tab">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Fibras">
                                            <label class="form-check-label" for="inlineCheckbox1">Fibras</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Polvos">
                                            <label class="form-check-label" for="inlineCheckbox1">Polvos</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="quimicos0[]" value="Líquidos">
                                            <label class="form-check-label" for="inlineCheckbox2">Líquidos</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" style="padding-left: 0">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="fisicos" class="font-bold">Ergononómicos:</label>
                                    </div>
                                    <div class="col-md-7" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="ergonomicos0[]"  value="Posturas prolongadas">
                                            <label class="form-check-label" for="inlineCheckbox1">Posturas prolongadas</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="ergonomicos0[]" value="Manejo de cargas">
                                            <label class="form-check-label" for="inlineCheckbox2">Manejo de cargas</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="ergonomicos0[]" value="Movimientos repetitivos">
                                            <label class="form-check-label" for="inlineCheckbox1">Movimientos repetitivos</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="ergonomicos0[]" value="Video terminales">
                                            <label class="form-check-label" for="inlineCheckbox1">Video terminales</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 2px">
                            <div class="col-md-3" style="max-width: 22.8%;">
                                <div class="row">
                                    <div class="col-md-5" style="max-width: 37%;">
                                        <label for="fisicos" class="font-bold">Biológicos:</label>
                                    </div>
                                    <div class="col-md-6" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="biologicos0[]" value="Microorganismos">
                                            <label class="form-check-label" for="inlineCheckbox1">Microorganismos</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="biologicos0[]" value="Animales">
                                            <label class="form-check-label" for="inlineCheckbox2">Animales</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="biologicos0[]" value="Vegetales">
                                            <label class="form-check-label" for="inlineCheckbox1">Vegetales</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-5" style="max-width: 39%">
                                        <label for="fisicos" class="font-bold">Psicosociales:</label>
                                    </div>
                                    <div class="col-md-7" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="psicosociales0[]" value="Relaciones humanas">
                                            <label class="form-check-label" for="inlineCheckbox1">Relaciones humanas</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="psicosociales0[]" value="Gestión">
                                            <label class="form-check-label" for="inlineCheckbox2">Gestión</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="psicosociales0[]" value="Ambiente de trabajo">
                                            <label class="form-check-label" for="inlineCheckbox1">Ambiente de trabajo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" style="max-width: 22.8%;">
                                <div class="row">
                                    <div class="col-md-5" style="max-width: 37%;">
                                        <label for="fisicos" class="font-bold">Seguridad:</label>
                                    </div>
                                    <div class="col-md-6" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="seguridad0[]" value="Eléctricos">
                                            <label class="form-check-label" for="inlineCheckbox1">Eléctricos</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="seguridad0[]" value="Incendio">
                                            <label class="form-check-label" for="inlineCheckbox2">Incendio</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="seguridad0[]" value="Mecánicos">
                                            <label class="form-check-label" for="inlineCheckbox1">Mecánicos</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="seguridad0[]" value="Locativos">
                                            <label class="form-check-label" for="inlineCheckbox1">Locativos</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="jornada" class="font-bold">Otros:</label>
                                <textarea class="form-control" id="otrosFactores0" rows="3"></textarea>
                            </div>
                        </div>
{{--                        <div class="row" id="" style="padding-top: 1%;">--}}
{{--                            <div class="col-md-6 offset-6" style="text-align: right">--}}
{{--                                <a href="" id="eliminarFactorRiesgo" style=" color: red">Eliminar factores de riesgo del cargo actual</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="prueba">
                        <div class="col-md-6">
                            <a href="" id="factorRiesgo">Añadir factores de riesgo del cargo actual</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="laboral" role="tabpanel" aria-labelledby="laboral-tab">
                <h5 class="title-sections" id="sub-title">ACCIDENTES DE TRABAJO</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;">
                    <div class="row" style="padding-bottom: 1%">
                        <div id="divCheckAT" class="col-md-12">
                            <label for="accidentesSiNo" style="width: 16%">¿Tiene accidentes de trabajo?:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="Si" name="accidentesSN" value="Si">
                                <label class="custom-control-label" for="Si">Si</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="no" name="accidentesSN" value="No" checked>
                                <label class="custom-control-label" for="no">No</label>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-responsive-md" id="tableAT" hidden="true">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 10%">Fecha</th>
                            <th scope="col" style="width: 22%">Empresa</th>
                            <th scope="col" style="width: 28%">Lesión</th>
                            <th scope="col" style="width: 11%">Días incapacidad</th>
                            <th scope="col" style="width: 20%">Secuelas</th>
                            <th scope="col" style="width: 6%">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyAt">
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 offset-md-6" style="text-align: right">
                            <a href="" id="anadirAT" hidden="true">Añadir accidente de trabajo</a>
                        </div>
                    </div>
                </div>

                <h5 class="title-sections" id="sub-title">ENFERMEDAD LABORAL</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;">
                    <div class="row" style="padding-bottom: 1%">
                        <div id="divCheckEP" class="col-md-12">
                            <label for="enfermedadSiNo" class="width-lbl-ep">¿Tiene enfermedades profesionales?:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siEP" name="enfermedadSN" value="Si" >
                                <label class="custom-control-label" for="siEP">Si</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noEP" name="enfermedadSN" value="No" checked>
                                <label class="custom-control-label" for="noEP">No</label>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-responsive-md" id="tableEP" hidden="true">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 10%">Fecha</th>
                            <th scope="col" style="width: 22%">Empresa</th>
                            <th scope="col" style="width: 28%">Diagnóstico</th>
                            <th scope="col" style="width: 11%">ARL</th>
                            <th scope="col" style="width: 20%">Reubicación</th>
                            <th scope="col" style="width: 6%">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyEP">
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 offset-md-6" style="text-align: right">
                            <a href="" id="anadirEP" hidden="true">Añadir enfermedad profesional</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade tab-container" id="habitos" role="tabpanel" aria-labelledby="habitos-tab">
                <div class="row">
                    {{--Alcohol--}}
                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">ALCOHOL</h6>
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="alcoholSiNo">Alcohol:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siA" name="alcoholSN" value="Si">
                                    <label class="custom-control-label" for="siA">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noA" name="alcoholSN" value="No" checked>
                                    <label class="custom-control-label" for="noA">No</label>
                                </div>
                                <br>
                                <label for="frecuencia">Frecuencia:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="frecuenciaA" name="frecuenciaA" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincenal">Quincenal</option>
                                        <option value="Mensual">Mensual</option>
                                        <option value="Otra">Otra</option>
                                    </select>
                                </div>
                                <br>
                                <label for="cantidad">Cantidad:</label>
                                <input type="text" name="cantidadA" id="cantidadA" class="form-control-form input-container-small">
                            </div>
                        </div>
                    </div>
                    {{--Sedentarismo--}}
                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">SEDENTARISMO</h6>
                        <div class="border-container">
                            <div class="col-md-12" id="divAlcohol">
                                <label for="alcoholSiNo">Sedentarismo:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siS" name="sedentarismoSN" value="Si">
                                    <label class="custom-control-label" for="siS">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noS" name="sedentarismoSN" value="No" checked>
                                    <label class="custom-control-label" for="noS">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--Sustacias psicoactivas--}}
                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">SUSTANCIAS PSICOACTIVAS</h6>
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="alcoholSiNo" class="width-lbl-spsc">Sustancias psicoactivas:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siSPC" name="psicoactivasSN" value="Si">
                                    <label class="custom-control-label" for="siSPC">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noSPC" name="psicoactivasSN" value="No" checked>
                                    <label class="custom-control-label" for="noSPC">No</label>
                                </div>
                                <br>
                                <label for="frecuenciaPSC">Frecuencia:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="frecuenciaPSC" name="frecuenciaPSC" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincenal">Quincenal</option>
                                        <option value="Mensual">Mensual</option>
                                        <option value="Otra">Otra</option>
                                    </select>
                                </div>
                                <br>
                                <label for="cual">Cual:</label>
                                <input type="text" name="cualPSC" id="cualPSC" class="form-control-form input-container-small">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    {{--tabaquismo--}}
                    <div class="col-md-4 offset-md-2 container-small">
                        <h6 class="tab-subtitle" id="sub-title">TABAQUISMO</h6>
                        <div class="border-container" style="padding-bottom: 15%">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="tabaquismo" class="width-lbl-spsc">Fumador:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siT" name="tabaquismoSN" value="Si">
                                    <label class="custom-control-label" for="siT">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noT" name="tabaquismoSN" value="No">
                                    <label class="custom-control-label" for="noT">No</label>
                                </div>
                                <br>

                                <label for="cantidad cigariilos" class="width-lbl-spsc">Cantidad cigarrillos día:</label>
                                <input type="text" name="cantidadCD" id="cantidadCD" class="form-control-form width-lblgeneral">
                                <br>

                                <label for="ex fumador" class="width-lbl-spsc">Ex fumador:</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="exfumador" value="Si">
                                    <label class="form-check-label" for="Si">Si</label>
                                </div>
                                <br>

                                <label for="tiempo fumador" class="width-lbl-spsc">Cuanto tiempo fumó:</label>
                                <input type="text" name="tiempoFumador" id="tiempoFumador" class="form-control-form" style="max-width: 12%">
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="tiempoFM" id="tiempoFM" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Días">Días</option>
                                        <option value="Meses">Meses</option>
                                        <option value="Años">Años</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--Deportes--}}
                    <div class="col-md-4 container-small">
                        <h6 class="tab-subtitle" id="sub-title">DEPORTES</h6>
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="deporte" class="width-lbl-spsc">Deporte:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siD" name="deporteSN" value="Si">
                                    <label class="custom-control-label" for="siD">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noD" name="deporteSN" value="No" checked>
                                    <label class="custom-control-label" for="noD">No</label>
                                </div>
                                <br>

                                <label for="cual deporte" class="width-lbl-spsc">Cual:</label>
                                <input type="text" name="cualD" id="cualD" class="form-control-form width-lblgeneral">
                                <br>

                                <label for="frecuencia" class="width-lbl-spsc">Frecuencia:</label>
                                <div class="form-group div-select-form width-lblgeneral">
                                    <select class="select-form form-control" id="frecuenciaD" name="frecuenciaD" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Diario">Diario</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincenal">Quincenal</option>
                                        <option value="Mensual">Mensual</option>
                                        <option value="Otra">Otra</option>
                                    </select>
                                </div>
                                <br>

                                <label for="ex fumador" class="width-lbl-spsc">Lesiones deportivas:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siLD" name="lesionesD" value="Si">
                                    <label class="custom-control-label" for="siLD">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noLD" name="lesionesD" value="No" checked>
                                    <label class="custom-control-label" for="noLD">No</label>
                                </div>
                                <br>

                                <label for="tiempo fumador" class="width-lbl-spsc">Cual:</label>
                                <input type="text" name="cualLD" id="cualLD" class="form-control-form width-lblgeneral">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="antecedentes" role="tabpanel" aria-labelledby="antecedentes-tab">
                <h5 class="title-sections" id="sub-title">ANTECEDENTES FAMILIARES</h5>
                <div class="border-container">
                    <div class="row">
                        <div class="col-md-5 offset-1">
                            <div class="row">
                                <div class="col-md-3 offset-md-2">
                                    <h5 for="patologia" style="font-family: montserrat-bold">Patología</h5>
                                </div>
                                <div class="col-md-3 offset-2">
                                    <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 10%">Parentesco</h5>
                                </div>
                            </div>
                            <label for="hta" class="width-lbl-antecedentesF">H.T.A:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siHTA" name="hta" value="Si">
                                <label class="custom-control-label" for="siHTA">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noHTA" name="hta" value="No" checked>
                                <label class="custom-control-label" for="noHTA">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoHTA" id="parentescoHTA" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="eap" class="width-lbl-antecedentesF">E.A.P:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siEAP" name="eap" value="Si">
                                <label class="custom-control-label" for="siEAP">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noEAP" name="eap" value="No" checked>
                                <label class="custom-control-label" for="noEAP">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoEAP" id="parentescoEAP" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="ecv" class="width-lbl-antecedentesF">E.C.V:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siECV" name="ecv" value="Si">
                                <label class="custom-control-label" for="siECV">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noECV" name="ecv" value="No" checked>
                                <label class="custom-control-label" for="noECV">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoECV" id="parentescoECV" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="tbc" class="width-lbl-antecedentesF">T.B.C.:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siTBC" name="tbc" value="Si">
                                <label class="custom-control-label" for="siTBC">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noTBC" name="tbc" value="No" checked>
                                <label class="custom-control-label" for="noTBC">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoTBC" id="parentescoTBC" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="asma" class="width-lbl-antecedentesF">Asma:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siAsma" name="asma" value="Si">
                                <label class="custom-control-label" for="siAsma">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noAsma" name="asma" value="No" checked>
                                <label class="custom-control-label" for="noAsma">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoAsma" id="parentescoAsma" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="alergias" class="width-lbl-antecedentesF">Alergias:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siAlergias" name="alergias" value="Si">
                                <label class="custom-control-label" for="siAlergias">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noAlergias" name="alergias" value="No" checked>
                                <label class="custom-control-label" for="noAlergias">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoAlergias" id="parentescoAlergias" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="Artritis" class="width-lbl-antecedentesF">Artritis:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siArtritis" name="artritis" value="Si">
                                <label class="custom-control-label" for="siArtritis">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noArtritis" name="artritis" value="No" checked>
                                <label class="custom-control-label" for="noArtritis">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoArtritis" id="parentescoArtritis" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="Varices" class="width-lbl-antecedentesF">Várices:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siVarices" name="varices" value="Si">
                                <label class="custom-control-label" for="siVarices">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noVarices" name="varices" value="No" checked>
                                <label class="custom-control-label" for="noVarices">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoVarices" id="parentescoVarices" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-3 offset-md-2">
                                    <h5 for="patologia" style="font-family: montserrat-bold">Patología</h5>
                                </div>
                                <div class="col-md-3 offset-2">
                                    <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 10%">Parentesco</h5>
                                </div>
                            </div>
                            <label for="cancer" class="width-lbl-antecedentesF">Cancer:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="cancer" id="siCancer" value="Si">
                                <label class="custom-control-label" for="siCancer">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="cancer" id="noCancer" value="No" checked>
                                <label class="custom-control-label" for="noCancer">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoCancer" id="parentescoCancer" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="diabetes" class="width-lbl-antecedentesF">Diabetes:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="diabetes" id="siDiabetes" value="Si">
                                <label class="custom-control-label" for="siDiabetes">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="diabetes" id="noDiabetes" value="No" checked>
                                <label class="custom-control-label" for="noDiabetes">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoDiabetes" id="parentescoDiabetes" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="dislipidemia" class="width-lbl-antecedentesF">Enfermedad Renal:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="enfermedadRenal" id="siEnfermedadRenal" value="Si">
                                <label class="custom-control-label" for="siEnfermedadRenal">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="enfermedadRenal" id="noEnfermedadRenal" value="No" checked>
                                <label class="custom-control-label" for="noEnfermedadRenal">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoEnfermedadRenal" id="parentescoEnfermedadRenal" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="dislipidemia" class="width-lbl-antecedentesF">Dislipidemia:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="dislipidemia" id="siDislipidemia" value="Si">
                                <label class="custom-control-label" for="siDislipidemia">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="dislipidemia" id="noDislipidemia" value="No" checked>
                                <label class="custom-control-label" for="noDislipidemia">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoDislipidemia" id="parentescoDislipidemia" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="enfermedadCoronaria" class="width-lbl-antecedentesF">Enfermedad Coronaria:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="enfermedadCoronaria" id="siEnfermedadCoronaria" value="Si">
                                <label class="custom-control-label" for="siEnfermedadCoronaria">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="enfermedadCoronaria" id="noEnfermedadCoronaria" value="No" checked>
                                <label class="custom-control-label" for="noEnfermedadCoronaria">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoEnfermedadCoronaria" id="parentescoEnfermedadCoronaria" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="enfermedadColagenosis" class="width-lbl-antecedentesF">Enfermedad Colagenosis:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="enfermedadColagenosis" id="siEnfermedadColagenosis" value="Si">
                                <label class="custom-control-label" for="siEnfermedadColagenosis">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="enfermedadColagenosis" id="noEnfermedadColagenosis" value="No" checked>
                                <label class="custom-control-label" for="noEnfermedadColagenosis">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoEnfermedadColagenosis" id="parentescoEnfermedadColagenosis" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="enfermedadTiroidea" class="width-lbl-antecedentesF">Enfermedad Tiroidea:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="enfermedadTiroidea" id="siEnfermedadTiroidea" value="Si">
                                <label class="custom-control-label" for="siEnfermedadTiroidea">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="enfermedadTiroidea" id="noEnfermedadTiroidea" value="No" checked>
                                <label class="custom-control-label" for="noEnfermedadTiroidea">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoEnfermedadTiroidea" id="parentescoEnfermedadTiroidea" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="otra" class="width-lbl-antecedentesF">Otra:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="otra" id="siOtra" value="Si">
                                <label class="custom-control-label" for="siOtra">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="otra" id="noOtra" value="No" checked>
                                <label class="custom-control-label" for="noOtra">No</label>
                            </div>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="parentescoOtra" id="parentescoOtra" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Padre">Padre</option>
                                    <option value="Madre">Madre</option>
                                    <option value="Abuelo">Abuelo</option>
                                    <option value="Abuela">Abuela</option>
                                    <option value="Hermana">Hermana</option>
                                    <option value="Hermano">Hermano</option>
                                    <option value="Padre/Madre">Padre/Madre</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>
                            <label for="otra" class="width-lbl-antecedentesF">Observaciones:</label>
                            <input type="text" class="form-control-form" id="familiaresOtraObs" name="familiaresOtraObs" style="width: 56.5%" placeholder="Observaciones">
                        </div>
                    </div>
                </div>
                <h5 class="title-sections" id="sub-title">ANTECEDENTES PERSONALES</h5>
                <div class="border-container">
                    <div class="row">
                        <div class="col-md-5 offset-1">
                            <div class="row">
                                <div class="col-md-4 offset-6">
                                    <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 15%">Observaciones</h5>
                                </div>
                            </div>

                            <label for="patologicos" class="width-lbl-antecedentesP">Patológicos:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siPatologicos" name="patologicos" value="Si">
                                <label class="custom-control-label" for="siPatologicos">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noPatologicos" name="patologicos" value="No" checked>
                                <label class="custom-control-label" for="noPatologicos">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesPatologico" id="observacionesPatologico">

                            <label for="quirurgico" class="width-lbl-antecedentesP">Quirúrgicos:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siquirurgico" name="quirurgico" value="Si">
                                <label class="custom-control-label" for="siquirurgico">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noquirurgico" name="quirurgico" value="No" checked>
                                <label class="custom-control-label" for="noquirurgico">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesQuirurgico" id="observacionesQuirurgico">

                            <label for="Traumaticos" class="width-lbl-antecedentesP">Traumáticos:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siTraumaticos" name="traumaticos" value="Si">
                                <label class="custom-control-label" for="siTraumaticos">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noTraumaticos" name="traumaticos" value="No" checked>
                                <label class="custom-control-label" for="noTraumaticos">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesTraumaticos" id="observacionesTraumaticos">
                        </div>

                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-4 offset-6">
                                    <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 15%">Observaciones</h5>
                                </div>
                            </div>

                            <label for="farmacologicos" class="width-lbl-antecedentesP">Farmacológicos:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siFarmacologicos" name="farmacologicos" value="Si">
                                <label class="custom-control-label" for="siFarmacologicos">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noFarmacologicos" name="farmacologicos" value="No" checked>
                                <label class="custom-control-label" for="noFarmacologicos">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesFarmacologico" id="observacionesFarmacologico">

                            <label for="Transfusionales" class="width-lbl-antecedentesP">Transfusionales:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siTransfusionales" name="Transfusionales" value="Si">
                                <label class="custom-control-label" for="siTransfusionales">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noTransfusionales" name="Transfusionales" value="No" checked>
                                <label class="custom-control-label" for="noTransfusionales">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesTransfusionales" id="observacionesTransfusionales">

                            <label for="Otros" class="width-lbl-antecedentesP">Otros:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siOtros" name="otros" value="Si">
                                <label class="custom-control-label" for="siOtros">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noOtros" name="otros" value="No" checked>
                                <label class="custom-control-label" for="noOtros">No</label>
                            </div>
                            <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesOtros" id="observacionesOtros">
                        </div>
                    </div>
                </div>
                <h5 class="title-sections" id="sub-title">ANTECEDENTES INMUNOLÓGICOS</h5>
                <div class="border-container">
                    <div class="row">
                        <div class="col-md-5 offset-1">
                            <label for="cancer" class="width-lbl-antecedentesF">Carnet de vacunación:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" name="carnetV" id="siCarnetV" value="Si">
                                <label class="custom-control-label" for="siCarnetV">Si</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" name="carnetV" id="noCarnetV" value="No" checked>
                                <label class="custom-control-label" for="noCarnetV">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 offset-1">

                            <label for="diabetes" class="width-lbl-antecedentesF">Hepatitis A:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="hepatitisA" id="hepatitisA" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="diabetes" class="width-lbl-antecedentesF">Hepatitis B:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="hepatitisB" id="hepatitisB" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="dislipidemia" class="width-lbl-antecedentesF">Triple viral:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="tripleV" id="tripleV" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="dislipidemia" class="width-lbl-antecedentesF">Tétano:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="tetano" id="tetano" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-5 ">

                            <label for="diabetes" class="width-lbl-antecedentesF">Varicela:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="varicelaAI" id="varicelaAI" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="Influenza" class="width-lbl-antecedentesF">Influenza:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="influenza" id="influenza" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="fiebre amarilla" class="width-lbl-antecedentesF">Fiebre amarilla:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="fiebreA" id="fiebreA" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>

                            <label for="otros AI" class="width-lbl-antecedentesF">Otros:</label>
                            <div class="form-group div-select-form" id="select-medida">
                                <select class="select-form form-control" name="otrosAI" id="otrosAI" style="font-size: 0.8rem">
                                    <option value=""></option>
                                    <option value="Completa">Completa</option>
                                    <option value="Incompleta">Incompleta</option>
                                    <option value="N/A" selected>N/A</option>
                                </select>
                            </div>
                            <label for="otra" class="width-lbl-antecedentesF">Observaciones:</label>
                            <input type="text" class="form-control-form" id="inmunologicosOtrosObs" name="inmunologicosOtrosObs" style="width: 56.5%" placeholder="Observaciones">
                        </div>
                    </div>
                </div>
                <h5 class="title-sections ocultarAG" id="sub-title">ANTECEDENTES GINECOOBSTETRICOS</h5>
                <div class="border-container ocultarAG">
                    <div class="row">
                        <div class="col-md-5 offset-1">
                            <label for="menarquia" class="width-lbl-antecedentesF">Menarquia:</label>
                            <input type="text" class="form-control-form width-lbl-antecedentesF" name="menarquia" id="menarquia">&nbsp&nbsp&nbsp<label for="">Años</label>
                            <label for="ciclos" class="width-lbl-antecedentesF">Ciclos:</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="ciclos" id="ciclos">
                            <label for="ciclos">x</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="ciclos2" id="ciclos2"><br>
                            <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultimo periodo:</label>
                            <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUPeriodo" id="fechaUPeriodo" autocomplete="off" style="padding: 3px">
                            <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultimo parto:</label>
                            <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUParto" id="fechaUParto" autocomplete="off" style="padding: 3px">
                        </div>
                        <div class="col-md-5">
                            <label for="ciclos" class="width-lbl-antecedentesF">F. obstétrica:</label>
                            <label for="ciclos">G</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="obstetricaG" id="obstetricaG">
                            <label for="ciclos">P</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="obstetricaP" id="obstetricaP">
                            <label for="ciclos">A</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="obstetricaA" id="obstetricaA">
                            <label for="ciclos">C</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="obstetricaC" id="obstetricaC">
                            <label for="ciclos">E</label>
                            <input type="text" class="form-control-form input-antecedente-small" name="obstetricaE" id="obstetricaE">
                            <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultima citología:</label>
                            <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUC" id="fechaUC" autocomplete="off" style="padding: 3px">
                            <label for="menarquia" class="width-lbl-antecedentesF">Resultado:</label>
                            <input type="text" class="form-control-form width-lbl-antecedentesF" name="resultado" id="resultado">
                            <label for="menarquia" class="width-lbl-antecedentesF">Método de planificación:</label>
                            <input type="text" class="form-control-form width-lbl-antecedentesF" name="metodoP" id="metodoP">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="revisionPorSistemas" role="tabpanel" aria-labelledby="revisionPorSistemas-tab">
                <h5 class="title-sections" id="sub-title">REVISIÓN POR SISTEMAS</h5>
                <div class="border-container">
                    <div class="row" style="padding-bottom: 1%">
                        <div class="col-md-1">
                            <label for="neurologico" class="font-bold">Neurológico:</label>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="neuro[]" value="Cefalea">
                                <label class="form-check-label" for="inlineCheckbox1">Cefalea</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. memoria">
                                <label class="form-check-label" for="inlineCheckbox1">Alt. memoria</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. Sensibilidad">
                                <label class="form-check-label" for="inlineCheckbox2">Alt. Sensibilidad</label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 1px; padding-left: 0; padding-right: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. motora">
                                <label class="form-check-label" for="inlineCheckbox1">Alt. motora</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. sueño">
                                <label class="form-check-label" for="inlineCheckbox2">Alt. sueño</label>
                            </div>
                        </div>
                        <span style="padding-right: 2%"></span>
                        <div class="col-md-1" style="max-width: 9%">
                            <label for="fisicos" class="font-bold">Cardiovascular:</label>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cardio[]" value="Dolor precordial">
                                <label class="form-check-label" for="inlineCheckbox1">Dolor precordial</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cardio[]" value="Palpitaciones">
                                <label class="form-check-label" for="inlineCheckbox1">Palpitaciones</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cardio[]" value="Lipotomia">
                                <label class="form-check-label" for="inlineCheckbox2">Lipotomia</label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cardio[]" value="Sincope">
                                <label class="form-check-label" for="inlineCheckbox1">Sincope</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="cardio[]" value="Disnea">
                                <label class="form-check-label" for="inlineCheckbox2">Disnea</label>
                            </div>
                        </div>

                        <div class="col-md-1" style="max-width: 9%">
                            <label for="fisicos" class="font-bold">Genitourinario:</label>
                        </div>
                        <div class="col-md-2 width-col-geni">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="geni[]" value="Hematuria">
                                <label class="form-check-label" for="inlineCheckbox1">Hematuria</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="geni[]" value="Poliaquiuria">
                                <label class="form-check-label" for="inlineCheckbox1">Poliaquiuria</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="geni[]" value="Alt. ciclo menstrual">
                                <label class="form-check-label" for="inlineCheckbox2">Alt. ciclo menstrual</label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="geni[]" value="Nicturia">
                                <label class="form-check-label" for="inlineCheckbox1">Nicturia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="geni[]" value="Disuria">
                                <label class="form-check-label" for="inlineCheckbox2">Disuria</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            <label for="neurologico" class="font-bold">Digestivo:</label>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="digestivo[]" value="Dolor abdomen">
                                <label class="form-check-label" for="inlineCheckbox1">Dolor abdomen</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="digestivo[]" value="Epigastralgia">
                                <label class="form-check-label" for="inlineCheckbox1">Epigastralgia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="digestivo[]" value="Estreñimiento">
                                <label class="form-check-label" for="inlineCheckbox2">Estreñimiento</label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 1px; padding-left: 0; padding-right: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="digestivo[]" value="Dispepsia">
                                <label class="form-check-label" for="inlineCheckbox1">Dispepsia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="digestivo[]" value="Diarrea">
                                <label class="form-check-label" for="inlineCheckbox2">Diarrea</label>
                            </div>
                        </div>
                        <span style="padding-right: 2%"></span>
                        <div class="col-md-1" style="max-width: 9%">
                            <label for="fisicos" class="font-bold">Dermatológico:</label>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="derma[]" value="Descamación">
                                <label class="form-check-label" for="inlineCheckbox1">Descamación</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="derma[]" value="Hiperhidrosis">
                                <label class="form-check-label" for="inlineCheckbox1">Hiperhidrosis</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="derma[]" value="Sequedad">
                                <label class="form-check-label" for="inlineCheckbox2">Sequedad</label>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="derma[]" value="Eritema">
                                <label class="form-check-label" for="inlineCheckbox1">Eritema</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="derma[]" value="Prurito">
                                <label class="form-check-label" for="inlineCheckbox2">Prurito</label>
                            </div>
                        </div>

                        <div class="col-md-1" style="max-width: 9%">
                            <label for="fisicos" class="font-bold">Hermatológico:</label>
                            <label for="fisicos" class="font-bold">Respiratorio:</label>
                            <label for="fisicos" class="font-bold">Psiquiátrico:</label>
                        </div>
                        <div class="col-md-2 width-col-geni">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="hermatologico" value="Sangrado">
                                <label class="form-check-label" for="inlineCheckbox1">Sangrado</label>
                            </div>
                            <div class="form-check form-check-inline" style="padding-top: 4%">
                                <input class="form-check-input" type="checkbox" name="respiratorio" value="Tos frecuente">
                                <label class="form-check-label" for="inlineCheckbox1">Tos frecuente</label>
                            </div>
                            <div class="form-check form-check-inline" style="padding-top: 4%">
                                <input class="form-check-input" type="checkbox" name="psiquiatrico" value="Depresión">
                                <label class="form-check-label" for="inlineCheckbox2">Depresión</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="border-container" style="margin-top: 2%">
                    <div class="row">
                        <div class="col-md-1" style="max-width: 9%">
                            <label for="neurologico" class="font-bold">Osteomuscular:</label>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Cervicalgia">
                                <label class="form-check-label" for="inlineCheckbox1">Cervicalgia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dorsalgia">
                                <label class="form-check-label" for="inlineCheckbox1">Dorsalgia</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Lumbagia">
                                <label class="form-check-label" for="inlineCheckbox2">Lumbagia</label>
                            </div>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dolor matutino">
                                <label class="form-check-label" for="inlineCheckbox1">Dolor matutino</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dolor articular">
                                <label class="form-check-label" for="inlineCheckbox1">Dolor articular</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Alt. marcha">
                                <label class="form-check-label" for="inlineCheckbox2">Alt. marcha</label>
                            </div>
                        </div>
                        <div class="col-md-2 width-col-rs" style="max-width: 14%">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Parestesias">
                                <label class="form-check-label" for="inlineCheckbox1">Parestesias</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Inflama. articular">
                                <label class="form-check-label" for="inlineCheckbox1">Inflama. articular</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Tendinitis">
                                <label class="form-check-label" for="inlineCheckbox2">Tendinitis</label>
                            </div>
                        </div>
                        <div class="col-md-2 width-col-rs" style="max-width: 14%">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Sd túnel del carpo">
                                <label class="form-check-label" for="inlineCheckbox1">Sd túnel del carpo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Esguinces">
                                <label class="form-check-label" for="inlineCheckbox2">Esguinces</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Luxación">
                                <label class="form-check-label" for="inlineCheckbox1">Luxación</label>
                            </div>
                        </div>
                        <div class="col-md-2 width-col-rs">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Neuropatía">
                                <label class="form-check-label" for="inlineCheckbox1">Neuropatía</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Ciática">
                                <label class="form-check-label" for="inlineCheckbox2">Ciática</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Otro">
                                <label class="form-check-label" for="inlineCheckbox1">Otro</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="border-container" style="margin-top: 2%">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="jornada" class="font-bold">Observaciones:</label>
                            <textarea class="form-control" id="observacionesRS" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="examenFisico" role="tabpanel" aria-labelledby="examenFisico-tab">
                <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide carousel-ef" data-ride="carousel" data-interval="false">
                        <ol class="carousel-indicators" style="bottom: -42px">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row" style="padding-top: 2%">
                                    <div class="col-md-12">
                                        <div class="border-container">
                                            <div class="row" id="lbl-examenF1">
                                                <div class="col-md-4">
                                                    <label for="">Estado general:</label>
                                                    <div class="form-group div-select-form">
                                                        <select class="select-form form-control" name="estadoG" id="estadoG" style="font-size: 0.8rem">
                                                            <option value=""></option>
                                                            <option value="Bueno">Bueno</option>
                                                            <option value="Regular">Regular</option>
                                                            <option value="Malo">Malo</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Dominancia:</label>
                                                    <div class="form-group div-select-form">
                                                        <select class="select-form form-control" name="dominancia" id="dominancia" style="font-size: 0.8rem">
                                                            <option value=""></option>
                                                            <option value="Zurdo">Zurdo</option>
                                                            <option value="Diestro">Diestro</option>
                                                            <option value="Ambidiestro">Ambidiestro</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Tensión arterial:</label>
                                                    <input type="text" class="form-control-form" name="tenArt1" id="tenArt1" style="width: 12%">
                                                    <sapan>/</sapan>
                                                    <input type="text" class="form-control-form" name="tenArt2" id="tenArt2" style="width: 12%">
                                                    <label style="width: 5%">(mm/hg)</label>
                                                </div>
                                            </div>

                                            <div class="row" id="lbl-examenF1">
                                                <div class="col-md-4">
                                                    <label for="">Frecuencia cardiaca:</label>
                                                    <input type="text" class="form-control-form" name="fc" id="fc">
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Frecuencia R.:</label>
                                                    <input type="text" class="form-control-form" name="fr" id="fr">
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Temperatura:</label>
                                                    <input type="text" class="form-control-form" name="t" id="t">
                                                </div>
                                            </div>

                                            <div class="row" id="lbl-examenF1">
                                                <div class="col-md-4">
                                                    <label for="">Perímetro abdominal:</label>
                                                    <input type="text" class="form-control-form" name="pa" id="pa">
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Talla (cm):</label>
                                                    <input type="text" class="form-control-form" name="talla" id="talla">
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="">Peso (Kg):</label>
                                                    <input type="text" class="form-control-form" name="peso" id="peso">
                                                </div>
                                            </div>

                                            <div class="row" id="lbl-examenF1">
                                                <div class="col-md-4">
                                                    <label for="">IMC:</label>
                                                    <input type="text" class="form-control-form" name="imc" id="imc" disabled>
                                                </div>

                                                <div class="col-md-8">
                                                    <label for="lbl-info" class="lbl-info" style="width: 100%; padding-top: 1%"><b>Interpretación: Bajo peso: </b>< 18,5 - <b>Normal: </b>18,5 a 24,9 - <b>Sobrepeso: </b>25 a 29,9 - <b>Obesidad: </b>> 30 </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-top: 1%">
                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Cabeza / Cuello:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="cabezaCuello" id="cabezaCuello" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="cabezaCDescripcion" id="cabezaCDescripcion" placeholder="Descripción" style="width: 97%">
                                        </div>
                                    </div>

                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Oido:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="oidoEF" id="oidoEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="oidoDescriocion" id="oidoDescriocion" placeholder="Descripción" style="width: 97%">
                                        </div>
                                    </div>

                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Piel:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="pielEF" id="pielEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="pielDescripcion" id="pielDescripcion" placeholder="Descripción" style="width: 97%">
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-top: 1%">
                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Ojos:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="ojosEF" id="ojosEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="ojosDescripcion" id="ojosDescripcion" placeholder="Descripción" style="width: 97%;margin-bottom: 2%">
                                            <label for="" style="font-family: montserrat-bold">Fondo de ojos</label><br>
                                            <label for="">Ojo derecho:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="ojoDerecho" id="ojoDerecho" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <label for="">Ojo izquierdo:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="ojoIzquierdo" id="ojoIzquierdo" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Abdomen:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="abdomenEF" id="abdomenEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="abdomenDescriocion" id="abdomenDescriocion" placeholder="Descripción" style="width: 97%; margin-bottom: 1.3%">
                                            <label for="">Hernias:</label>
                                            <input type="text" class="form-control-form" name="herniaDescripcion" id="herniaDescripcion">
                                        </div>
                                    </div>

                                    <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                        <div class="border-container">
                                            <label for="">Torax:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="toraxEF" id="toraxEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <label for="">RsCs:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="RsCs" id="RsCs" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Rítmicos" selected>Rítmicos</option>
                                                    <option value="Arrítmicos">Arrítmicos</option>
                                                </select>
                                            </div>
                                            <br>
                                            <input type="text" class="form-control-form" name="rscslDescripcion" id="rscslDescripcion" placeholder="Descripción" style="width: 97%; margin-bottom: 1%">
                                            <label for="">RsRs:</label>
                                            <input type="text" class="form-control-form" name="rsrs" id="rsrs">
                                            <label for="">Senos:</label>
                                            <input type="text" class="form-control-form" name="senosEF" id="senosEF">
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-top: 1%">
                                    <div class="col-md-6 container-small-ef" id="lbl-examenF3">
                                        <div class="border-container">
                                            <label for="">Extremidad superior:</label>
                                            <div class="form-group div-select-form" style="width: 30%">
                                                <select class="select-form form-control" name="extSup" id="extSup" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input type="text" class="form-control-form" name="extSupDescripcion" id="extSupDescripcion" placeholder="Descripción" style="width: 42%">
                                            <label for="">Derecho tinel:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="derechoTin" id="derechoTin" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Izquierdo tinel:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="izquierdoTin" id="izquierdoTin" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                            <label for="">Derecho phanel:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="derechoPhan" id="derechoPhan" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Izquierdo phanel:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="izquierdoPhan" id="izquierdoPhan" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                            <label for="">Derecho finkelstein:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="derechoFink" id="derechoFink" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Izquierdo finkelstein:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="izquierdoFink" id="izquierdoFink" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo" selected>Negativo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 container-small-ef" id="lbl-examenF3">
                                        <div class="border-container">
                                            <label for="">Neurológico:</label>
                                            <div class="form-group div-select-form" style="width: 30%">
                                                <select class="select-form form-control" name="neuroEF" id="neuroEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input type="text" class="form-control-form" name="neuroDescripcion" id="neuroDescripcion" placeholder="Descripción" style="width: 42%">
                                            <label for="">Pares craneanos:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="paresCraneo" id="paresCraneo" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Reflejos:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="reflejosEF" id="reflejosEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                </select>
                                            </div>
                                            <label for="">Sensibilidad:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="sensibilidadEF" id="sensibilidadEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Coord. romber:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="coorRomber" id="coorRomber" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo">Negativo</option>
                                                </select>
                                            </div>
                                            <label for="">Marcha en linea:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="marchaLinea" id="marchaLinea" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Positivo">Positivo</option>
                                                    <option value="Negativo">Negativo</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="">Esfera mental:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="esferaMental" id="esferaMental" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-top: 1%">

                                    <div class="col-md-6 offset-3 container-small-ef" id="lbl-examenF3">
                                        <div class="border-container">
                                            <label for="">Extremidad inferior:</label>
                                            <div class="form-group div-select-form" style="width: 30%">
                                                <select class="select-form form-control" name="extInferior" id="extInferior" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input type="text" class="form-control-form" name="extInferiorDescripcion" id="extInferiorDescripcion" placeholder="Descripción" style="width: 42%">
                                            <label for="">Varices:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="varicesEF" id="varicesEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                            <input type="text" class="form-control-form" name="varicesDescripcion" id="varicesDescripcion" placeholder="Descripción" style="width: 51%">

                                            <label for="">Juanetes:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="juanetesEF" id="juanetesEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                            <input type="text" class="form-control-form" name="juanetesDescripcion" id="juanetesDescripcion" placeholder="Descripción" style="width: 51%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row" style="padding-top: 2%">
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">INSPECCIÓN</h5>
                                        <div class="border-container">
                                            <label for="simetria" style="width: 15%">Simetría:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="simetriaI" id="simetriaI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionSimetria" id="descripcionSimetria" class="form-control-form width-input-antecedentesP" placeholder="Descripción"><br>
                                            <label for="curcatura" style="width: 15%">Curvatura:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="curvaturaP" id="curvaturaP" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionCurvatura" id="descripcionCurvatura" class="form-control-form width-input-antecedentesP" placeholder="Descripción">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">PALPACIÓN</h5>
                                        <div class="border-container">
                                            <label for="dolor" style="width: 15%">Dolor:</label>
                                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                                <input type="radio" class="custom-control-input" id="siDolor" name="dolorEF" value="Si">
                                                <label class="custom-control-label" for="siDolor">Si</label>
                                            </div>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="noDolor" name="dolorEF" value="No" checked>
                                                <label class="custom-control-label" for="noDolor">No</label>
                                            </div>
                                            <input name="descripcionDolor" id="descripcionDolor" class="form-control-form" placeholder="Descripción" style="width: 66%">

                                            <label for="simetria" style="width: 15%">Espasmo:</label>
                                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                                <input type="radio" class="custom-control-input" id="siEspasmo" name="espasmoEF" value="Si">
                                                <label class="custom-control-label" for="siEspasmo">Si</label>
                                            </div>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" class="custom-control-input" id="noEspasmo" name="espasmoEF" value="No" checked>
                                                <label class="custom-control-label" for="noEspasmo">No</label>
                                            </div>
                                            <input name="descripcionEspasmo" id="descripcionEspasmo" class="form-control-form" placeholder="Descripción" style="width: 66%">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="title-sections" id="sub-title">MOVILIDAD</h5>
                                        <div class="border-container">
                                            <label for="simetria" style="width: 7%">Flexión:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="flexionM" id="flexionM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionFlexion" id="descripcionFlexion" class="form-control-form" placeholder="Descripción" style="width: 27%">
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="curcatura" style="width: 7%">Extensión:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="extensionM" id="extensionM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionExtension" id="descripcionExtension" class="form-control-form" placeholder="Descripción" style="width: 27%"><br>

                                            <label for="simetria" style="width: 7%">Flexión lat.:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="flexionLM" id="flexionLM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionFlexionL" id="descripcionFlexionL" class="form-control-form" placeholder="Descripción" style="width: 27%">
                                            &nbsp&nbsp&nbsp&nbsp
                                            <label for="curcatura" style="width: 7%">Rotación:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="rotacionM" id="rotacionM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionRotacion" id="descripcionRotacion" class="form-control-form" placeholder="Descripción" style="width: 27%">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">MARCHA</h5>
                                        <div class="border-container">
                                            <label for="puntas" style="width: 15%">Puntas:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="puntasM" id="puntasM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionPuntas" id="descripcionPuntas" class="form-control-form width-input-antecedentesP"><br>

                                            <label for="talones" style="width: 15%">Talones:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="talonesM" id="talonesM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionTalones" id="descripcionTalones" class="form-control-form width-input-antecedentesP" placeholder="Descripción"><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">TEST</h5>
                                        <div class="border-container">
                                            <label for="Wells" style="width: 15%">Wells:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="WellsM" id="WellsM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionWells" id="descripcionWells" class="form-control-form width-input-antecedentesP" placeholder="Descripción"><br>

                                            <label for="shober" style="width: 15%">Shober:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="ShoberM" id="ShoberM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="descripcionShober" id="descripcionShober" class="form-control-form width-input-antecedentesP" placeholder="Descripción">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">TROFISMO MUSCULAR</h5>
                                        <div class="border-container">
                                            <label for="trofismo" style="width: 15%">Trofismo:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="trofismoM" id="trofismoM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <label for="trofismo" style="width: 15%">Descripción:</label>
                                            <input name="descripcionTrofismo" id="descripcionTrofismo" class="form-control-form" placeholder="Descripción" style="width: 84%"><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">DIAGNOSTICO COLUMNA</h5>
                                        <div class="border-container">
                                            <label for="columna" style="width: 15%">Columna:</label>
                                            <div class="form-group div-select-form" style="width: 28%">
                                                <select class="select-form form-control" name="columnaEF" id="columnaEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <br>
                                            <label for="trofismo" style="width: 15%">Descripción:</label>
                                            <input name="descripcionColumna" id="descripcionColumna" class="form-control-form" placeholder="Descripción" style="width: 84%"><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="title-sections" id="sub-title">BALANCE MUSCULAR</h5>
                                        <div class="border-container" id="lbl-balanceM">
                                            <label for="cintura escapular">Cintura escapular:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="cinturaEscapular" id="cinturaEscapular" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesCinEs" id="observacionesCinEs" class="form-control-form" placeholder="Observaciones">
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                            <label for="pectoral">Pectoral:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="pectoralEF" id="pectoralEF" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesPect" id="observacionesPect" class="form-control-form" placeholder="Observaciones"><br>

                                            <label for="simetria">Brazo:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="brazoBM" id="brazoBM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesBrazo" id="observacionesBrazo" class="form-control-form" placeholder="Observaciones">
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                            <label for="antebrazo">Antebrazo:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="antebrazoBM" id="antebrazoBM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesAnteb" id="observacionesAnteb" class="form-control-form" placeholder="Observaciones" >
                                            <label for="pectoral">Cadera-Gluteo:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="caderaGluteo" id="caderaGluteo" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesCadGlu" id="observacionesCadGlu" class="form-control-form" placeholder="Observaciones">
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                            <label for="muslos">Muslos:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="muslosBM" id="muslosBM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesMuslos" id="observacionesMuslos" class="form-control-form" placeholder="Observaciones">
                                            <label for="piernas">Piernas:</label>
                                            <div class="form-group div-select-form" style="width: 14%">
                                                <select class="select-form form-control" name="piernasBM" id="piernasBM" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="Normal" selected>Normal</option>
                                                    <option value="Anormal">Anormal</option>
                                                    <option value="No examinado">No examinado</option>
                                                    <option value="No solicitado">No solicitado</option>
                                                    <option value="Pendiente resultado">Pendiente resultado</option>
                                                </select>
                                            </div>
                                            <input name="observacionesPiernas" id="observacionesPiernas" class="form-control-form" placeholder="Observaciones"><br>
                                            <label for="explicacion">Explicación:</label>
                                            <textarea name="explicacion" id="explicacionBM" rows="4" style="width: 100%"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">REFLEJOS OSTEOTENDINOSOS</h5>
                                        <div class="border-container" id="reflejos-osteo">
                                            <label for=""></label>
                                            <label for="" style="font-family: montserrat-bold; width: 25%">Derecho</label>
                                            <label for="" style="font-family: montserrat-bold">Izquierdo</label><br>
                                            <label for="bicipital">Bicipital:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="bicipitalD" id="bicipitalD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            &nbsp&nbsp
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="bicipitalI" id="bicipitalI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            <br>
                                            <label for="">Radial:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="radialD" id="radialD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            &nbsp&nbsp
                                            <div class="form-group div-select-form" >
                                                <select class="select-form form-control" name="radialI" id="radialI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            <br>
                                            <label for="">Rotuliano:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="rotulianoD" id="rotulianoD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            &nbsp&nbsp
                                            <div class="form-group div-select-form" >
                                                <select class="select-form form-control" name="rotulianoI" id="rotulianoI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            <br>
                                            <label for="">Aquiliano:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="aquilianoD" id="aquilianoD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            &nbsp&nbsp
                                            <div class="form-group div-select-form" >
                                                <select class="select-form form-control" name="aquilianoI" id="aquilianoI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="I">I</option>
                                                    <option value="II" selected>II</option>
                                                    <option value="III">III</option>
                                                    <option value="IIII">IIII</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn>/++++</sapn>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="title-sections" id="sub-title">FUERZA</h5>
                                        <div class="border-container" id="reflejos-osteo">
                                            <label for="" style="font-family: montserrat-bold; width: 40%">Miembros superioriores</label>
                                            <label for="" style="font-family: montserrat-bold;width: 25%">Miembros inferiores</label><br>
                                            <label for="derecha">Derecha:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="miembroD" id="miembroD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5" selected>5</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <span style="font-family: montserrat-bold">/5</span>
                                            <label for="derecha">Derecha:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="miembroInfD" id="miembroInfD" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5" selected>5</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <sapn style="font-family: montserrat-bold">/5</sapn>
                                            <br>
                                            <label for="izquierda">Izquierda:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="miembroI" id="miembroI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5" selected>5</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <span style="font-family: montserrat-bold">/5</span>
                                            <label for="izquierda">Izquierda:</label>
                                            <div class="form-group div-select-form">
                                                <select class="select-form form-control" name="miembroInfI" id="miembroInfI" style="font-size: 0.8rem">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5" selected>5</option>
                                                </select>
                                            </div>
                                            &nbsp&nbsp
                                            <spann style="font-family: montserrat-bold">/5</spann>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="manipulacionAlimentos" role="tabpanel" aria-labelledby="manipulacionAlimentos-tab">
                <h5 class="title-sections" id="sub-title">MANIPULACIÓN DE ALIMENTOS / DERMATOLÓGICO</h5>
                <div class="border-container">
                    <div class="row" style="padding-bottom: 1%">
                        <div id="divCheckMA" class="col-md-12">
                            <label for="solicitadoSiNo" style="width: 9%">¿Fue solicitado?:</label>
                            <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                <input type="radio" class="custom-control-input" id="siSolicitado" name="solicitadoSN" value="Si">
                                <label class="custom-control-label" for="siSolicitado">Si</label>
                            </div>

                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="noSolicitado" name="solicitadoSN" value="No">
                                <label class="custom-control-label" for="noSolicitado">No</label>
                            </div>
                        </div>
                    </div>

                    <div id="contenido-ma" hidden="true">
                        <h5 class="title-sections">RESPIRATORIO</h5>
                        <div class="border-container" style="padding: 2% 1% 1% 1%;">
                            <div class="row" style="padding-bottom: 1%">
                                <div class="col-md-12">
                                    <label for="pregunta" style="padding-right: 1%">¿El paciente cumple con la definición de sintomático respiratorio? (Ha tenido tos y expectoración por mas de 15 días)</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siPregunta" name="pregunta" value="Si">
                                        <label class="custom-control-label" for="siPregunta">Si</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noPregunta" name="pregunta" value="No">
                                        <label class="custom-control-label" for="noPregunta">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label for="inspeccion" class="font-bold">Inspección:</label>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Normal">
                                        <label class="form-check-label" for="inlineCheckbox1">Normal</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Rinorrea">
                                        <label class="form-check-label" for="inlineCheckbox1">Rinorrea</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Escurrimiento posterior">
                                        <label class="form-check-label" for="inlineCheckbox2">Escurrimiento posterior</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Mucosas congestivas">
                                        <label class="form-check-label" for="inlineCheckbox2">Mucosas congestivas</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="inspeccion" class="font-bold">Auscultación:</label>
                                </div>
                                <div class="col-md-12">
                                    <textarea name="" id="auscultacion" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                        <h5 class="title-sections" ID="sub-title">DERMATOLÓGICO</h5>
                        <div class="border-container" style="padding: 2% 1% 1% 1%;">
                            <div class="row" style="padding-bottom: 1%">
                                <div class="col-md-12">
                                    <label for="pregunta2" style="padding-right: 1%">¿El paciente ha presentado prurito, cambios en la piel o
                                        enrojecimiento en alguna zona del cuerpo? (principalmente en las manos) en los ultimos 6 meses?</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siPregunta2" name="pregunta2" value="Si">
                                        <label class="custom-control-label" for="siPregunta2">Si</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noPregunta2" name="pregunta2" value="No">
                                        <label class="custom-control-label" for="noPregunta2">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 1%">
                                <div class="col-md-12">
                                    <label for="pregunta3" style="padding-right: 1%">¿El paciente usa actualmente algún medicamento que deba
                                        aplicar sobre la piel?</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siPregunta3" name="pregunta3" value="Si">
                                        <label class="custom-control-label" for="siPregunta3">Si</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noPregunta3" name="pregunta3" value="No">
                                        <label class="custom-control-label" for="noPregunta3">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label for="lesiones" class="font-bold">Lesiones:</label>
                                </div>
                                <div class="col-md-11">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Eritema">
                                        <label class="form-check-label" for="inlineCheckbox1">Eritema</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Excoriación">
                                        <label class="form-check-label" for="inlineCheckbox1">Excoriación</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Fisura">
                                        <label class="form-check-label" for="inlineCheckbox2">Fisura</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Ampolla/Vesícula">
                                        <label class="form-check-label" for="inlineCheckbox2">Ampolla/Vesícula</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Forúnculo">
                                        <label class="form-check-label" for="inlineCheckbox1">Forúnculo</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Costra">
                                        <label class="form-check-label" for="inlineCheckbox1">Costra</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Pústula">
                                        <label class="form-check-label" for="inlineCheckbox2">Pústula</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Ulcera">
                                        <label class="form-check-label" for="inlineCheckbox2">Ulcera</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Descamación">
                                        <label class="form-check-label" for="inlineCheckbox2">Descamación</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="cual" class="font-bold">¿Cual?:</label>
                                    <textarea class="form-control" id="cualMA" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="border-container" style="margin-top: 2%">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="observaciones" class="font-bold">Observaciones:</label>
                                    <textarea class="form-control" id="observacionesMA" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="paraclinicos" role="tabpanel" aria-labelledby="paraclinicos-tab">
                <h5 class="title-sections" id="sub-title">PARACLÍNICOS Y PRUEBAS COMPLEMENTARIAS</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;">
                    <table class="table table-bordered table-responsive-md" id="tablePARA" >
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 24%">Exámenes de laboratorio</th>
                            <th scope="col" style="width: 10%">Resultado</th>
                            <th scope="col" style="width: 28%">Observaciones</th>
                            <th scope="col" style="width: 4%">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyPARA">

                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 offset-md-6" style="text-align: right">
                            <a href="" id="anadirPara">Añadir paraclínico</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="optometria" class="font-bold">Optometria:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="optometriaPara" name="optometriaPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Alterada">Alterada</option>
                                    </select>
                                </div>
                                <br>
                                <label for="alteracion corregida" class="font-bold">Alteración corregida:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siACo" name="alteracionSN" value="Si">
                                    <label class="custom-control-label" for="siACo">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noACo" name="alteracionSN" value="No">
                                    <label class="custom-control-label" for="noACo">No</label>
                                </div>
                                <br>
                                <label for="diagnostico" class="font-bold">Diagnostico:</label>
                                <textarea name="diagnosticoOpto" id="diagnosticoOpto" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="visiometria" class="font-bold">Visiometria:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="visiometriaPara" name="visiometriaPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Alterada">Alterada</option>
                                    </select>
                                </div>
                                <br>
                                <label for="alteracion corregida" class="font-bold">Alteración corregida:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siACoV" name="alteracionVSN" value="Si">
                                    <label class="custom-control-label" for="siACoV">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noACoV" name="alteracionVSN" value="No">
                                    <label class="custom-control-label" for="noACoV">No</label>
                                </div>
                                <br>
                                <label for="diagnostico" class="font-bold">Diagnostico:</label>
                                <textarea name="diagnosticoVisio" id="diagnosticoVisio" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="audiometria" class="font-bold">Audiometria:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siAudio" name="audioSN" value="Si">
                                    <label class="custom-control-label" for="siAudio">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noAudio" name="audioSN" value="No" checked>
                                    <label class="custom-control-label" for="noAudio">No</label>
                                </div>
                                <br>
                                <label for="resultado" class="font-bold">Resultado:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siResAud" name="resultadoAudioSN" value="Normal">
                                    <label class="custom-control-label" for="siResAud">Normal</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noResAud" name="resultadoAudioSN" value="Alterada">
                                    <label class="custom-control-label" for="noResAud">Alterada</label>
                                </div>
                                <br>
                                <label for="diagnostico" class="font-bold">Observaciones:</label>
                                <textarea name="observacionesAudio" id="observacionesAudio" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="espirometria" class="font-bold">Espirometria:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siEspiro" name="espiroSN" value="Si">
                                    <label class="custom-control-label" for="siEspiro">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noEspiro" name="espiroSN" value="No" checked>
                                    <label class="custom-control-label" for="noEspiro">No</label>
                                </div>
                                <br>
                                <label for="resultado" class="font-bold">Resultado:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="espirometriaPara" name="espirometriaPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Patrón restrictivo">Patrón restrictivo</option>
                                        <option value="Patrón obstructivo">Patrón obstructivo</option>
                                        <option value="Patrón mixto">Patrón mixto</option>
                                    </select>
                                </div>
                                <br>
                                <label for="diagnostico" class="font-bold">Observaciones:</label>
                                <textarea name="observacionesEspiro" id="observacionesEspiro" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="psicologico" class="font-bold">Psicológico:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="psicologicoPara" name="psicologicoPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Anormal">Anormal</option>
                                        <option value="No examinado">No examinado</option>
                                        <option value="No solicitado">No solicitado</option>
                                        <option value="Pendiente resultado">Pendiente resultado</option>
                                    </select>
                                </div>
                                <br>
                                <label for="diagnostico" class="font-bold">Observaciones:</label>
                                <textarea name="observacionesPsi" id="observacionesPsi" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="psicometricos" class="font-bold">Psicométricos:</label>
                                <div class="form-group div-select-form input-container-small">
                                    <select class="select-form form-control" id="psicometricosPara" name="psicometricosPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Anormal">Anormal</option>
                                        <option value="No examinado">No examinado</option>
                                        <option value="No solicitado">No solicitado</option>
                                        <option value="Pendiente resultado">Pendiente resultado</option>
                                    </select>
                                </div>
                                <br>
                                <label for="observaciones" class="font-bold">Observaciones:</label>
                                <textarea name="observacionesPsiMe" id="observacionesPsiMe" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 container-medium">
                        <div class="border-container">
                            <div id="divAlcohol" class="col-md-12">
                                <label for="prueba vestibular" class="font-bold" style="width: 34%">Prueba vestibular:</label>
                                <div class="form-group div-select-form" style="width: 64%">
                                    <select class="select-form form-control" id="vestibularPara" name="vestibularPara" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Normal">Normal</option>
                                        <option value="Anormal">Anormal</option>
                                        <option value="No examinado">No examinado</option>
                                        <option value="No solicitado">No solicitado</option>
                                        <option value="Pendiente resultado">Pendiente resultado</option>
                                    </select>
                                </div>
                                <br>
                                <label for="observaciones" class="font-bold">Observaciones:</label>
                                <textarea name="observacionesVesti" id="observacionesVesti" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="impresionDiagnostica" role="tabpanel" aria-labelledby="impresionDiagnostica-tab">
                <h5 class="title-sections" id="sub-title">IMPRESIÓN DIAGNOSTICA</h5>
                <div class="border-container" style="padding: 2% 1% 1% 1%;">

                    <table class="table table-bordered table-responsive-md" id="tableImpresionD" >
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 10%">Buscar (codigo)</th>
                            <th scope="col" style="width: 80%">Diagnóstico</th>
                            <th scope="col" style="width: 4%">Registrar</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyImpresionD2">
                        <tr id="tr" class="impresionDTR0">
                            <td>
                                <input type="text" class="input-table impresiondiagnostica" name="buscarCIE10" id="codigoCIE10">
                            </td>
                            <td>
                                <input type="text" class="input-table diagnosticoid autocomplete-paracl" name="diagnosticoID" id="diagnosticoCIE10" style="font-size: 0.8rem; height: 44px;border-color: #0000;
                                    border-radius: 0;border-style: none; border-width: 0; box-shadow: none" >
                            </td>
                            <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="anadirID">Añadir</a></td>
                        </tr>
                        </tbody>
                    </table>


                    <table class="table table-bordered table-responsive-md" id="tableImpresionD" >
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" style="width: 10%">Código</th>
                            <th scope="col" style="width: 24%">Diagnóstico</th>
                            <th scope="col" style="width: 10%">Sospecha de origen</th>
                            <th scope="col" style="width: 12%">Tipo de diagnostico</th>
                            <th scope="col" style="width: 4%">Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyImpresionD">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="result" role="tabpanel" aria-labelledby="result-tab">
                <h5 class="title-sections" id="sub-title">RESULTADO</h5>
                <div class="border-container">
                    <h5 class="title-sections">RESULTADO VALORACIÓN MÉDICA</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO">
                                <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO CON RECOMENDACIONES">
                                <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - APTO CON RECOMENDACIONES</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO CON RESTRICCIONES">
                                <label class="form-check-label" for="inlineCheckbox2">PRE-EMPLEO - APTO CON RESTRICCIONES</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">PRE-EMPLEO - APLAZADO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - NO APTO">
                                <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - NO APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIODICO - SATISFACTORIO">
                                <label class="form-check-label" for="inlineCheckbox1">PERIODICO - SATISFACTORIO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIÓDICO - SATISFACTORIO CON RECOMENDACIONES QUE REQUIEREN VALORACIÓN">
                                <label class="form-check-label" for="inlineCheckbox2">PERIÓDICO - SATISFACTORIO CON RECOMENDACIONES QUE REQUIEREN VALORACIÓN</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIÓDICO - NO SATISFACTORIO">
                                <label class="form-check-label" for="inlineCheckbox2">PERIÓDICO - NO SATISFACTORIO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="RETIRO - SIN HALLAZGOS CLÍNICOS">
                                <label class="form-check-label" for="inlineCheckbox2">RETIRO - SIN HALLAZGOS CLÍNICOS</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="RETIRO - EXAMEN CON HALLAZGOS CLÍNICOS QUE REQUIEREN CONTROL">
                                <label class="form-check-label" for="inlineCheckbox2">RETIRO - EXAMEN CON HALLAZGOS CLÍNICOS QUE REQUIEREN CONTROL</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - APTO">
                                <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - NO APTO">
                                <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - NO APTO</label>
                            </div>
                            <br>

                        </div>

                        <div class="col-md-6">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - APLAZADO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - APTO">
                                <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - NO APTO">
                                <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - NO APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - APLAZADO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO">
                                <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO CON RECOMENDACIONES">
                                <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO CON RECOMENDACIONES</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO CON RESTRICCIONES">
                                <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO CON RESTRICCIONES</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APLAZADO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="ESPACIOS CONFINADOS - APTO">
                                <label class="form-check-label" for="inlineCheckbox2">ESPACIOS CONFINADOS - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="ESPACIOS CONFINADOS - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">ESPACIOS CONFINADOS - APLAZADO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="CONDUCTOR - APTO">
                                <label class="form-check-label" for="inlineCheckbox2">CONDUCTOR - APTO</label>
                            </div>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="CONDUCTOR - APLAZADO">
                                <label class="form-check-label" for="inlineCheckbox2">CONDUCTOR - APLAZADO</label>
                            </div>
                        </div>
                    </div>

                    <h5 class="title-sections" id="sub-title">RECOMENDACIONES DE SALUD OCUPACIONAL</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-3">
                                <h6 class="font-bold">Médicas</h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Remisión a EPS">
                                    <label class="form-check-label" for="inlineCheckbox2">Remisión a EPS</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Continuar manejo Médico">
                                    <label class="form-check-label" for="inlineCheckbox2">Continuar manejo Médico</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Remision a ARL">
                                    <label class="form-check-label" for="inlineCheckbox2">Remision a ARL</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Seguimiento caso por ARL">
                                    <label class="form-check-label" for="inlineCheckbox2">Seguimiento caso por ARL</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Citología Cervico-Vaginal">
                                    <label class="form-check-label" for="inlineCheckbox2">Citología Cervico-Vaginal</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Tamizaje Prostatico">
                                    <label class="form-check-label" for="inlineCheckbox2">Tamizaje Prostatico</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Esquema de Vacunación (Adulto)">
                                    <label class="form-check-label" for="inlineCheckbox2">Esquema de Vacunación (Adulto)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Audiologico Periodico">
                                    <label class="form-check-label" for="inlineCheckbox2">Control Audiologico Periodico</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Anual Optometrico">
                                    <label class="form-check-label" for="inlineCheckbox2">Control Anual Optometrico</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Odontologico Periodico">
                                    <label class="form-check-label" for="inlineCheckbox2">Control Odontologico Periodico</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6 class="font-bold">Ingreso a SVE</h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Ergonomico)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Ergonomico)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Ruido)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Ruido)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Biológico)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Biológico)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Visual)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Visual)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Psicolaboral)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Psicolaboral)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Cardiovascular)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Cardiovascular)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Metabólico)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Metabólico)</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Químico)">
                                    <label class="form-check-label" for="inlineCheckbox2">SVE (Químico)</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6 class="font-bold">Ocupacionales</h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Uso de EPP">
                                    <label class="form-check-label" for="">Uso de EPP</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Higiene Postural">
                                    <label class="form-check-label" for="inlineCheckbox2">Higiene Postural</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas Activas">
                                    <label class="form-check-label" for="inlineCheckbox2">Pausas Activas</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Distribucion de fuerzas y cargas">
                                    <label class="form-check-label" for="inlineCheckbox2">Distribucion de fuerzas y cargas</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas activas musculo esqueléticas">
                                    <label class="form-check-label" for="inlineCheckbox2">Pausas activas musculo esqueléticas</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas activas visuales">
                                    <label class="form-check-label" for="inlineCheckbox2">Pausas activas visuales</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h6 class="font-bold">Hábitos y estilos de vida saludable</h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Inicio de actividad Física">
                                    <label class="form-check-label" for="">Inicio de actividad Física</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Dejar de Fumar">
                                    <label class="form-check-label" for="">Dejar de Fumar</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Reducir Consumo de Alcohol">
                                    <label class="form-check-label" for="">Reducir Consumo de Alcohol</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Control Nutricional y de Peso">
                                    <label class="form-check-label" for="">Control Nutricional y de Peso</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Continuar con actividad física">
                                    <label class="form-check-label" for="">Continuar con actividad física</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">PRUEBAS COMPLEMENTARIAS</h5>
                    <div class="border-container" style="padding-top: 2%;margin-top: 1%">
                        <div class="row">
                            <div class="col-md-2" id="divResultados">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Ninguna">
                                    <label class="form-check-label" for="inlineCheckbox2">Ninguna</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Audiometría">
                                    <label class="form-check-label" for="inlineCheckbox2">Audiometría</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Visiometría">
                                    <label class="form-check-label" for="inlineCheckbox2">Visiometría</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Optometría">
                                    <label class="form-check-label" for="inlineCheckbox2">Optometría</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Espirometría">
                                    <label class="form-check-label" for="inlineCheckbox2">Espirometría</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Electrocardiograma">
                                    <label class="form-check-label" for="inlineCheckbox2">Electrocardiograma</label>
                                </div>
                            </div>
                            <div class="col-md-2" id="divResultados">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Evaluación Psicológica">
                                    <label class="form-check-label" for="inlineCheckbox2">Evaluación Psicológica</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Evaluación psicosensométrica">
                                    <label class="form-check-label" for="inlineCheckbox2">Evaluación psicosensométrica</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Pruebas vestibulares">
                                    <label class="form-check-label" for="inlineCheckbox2">Pruebas vestibulares</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Coprológico/Cultivo">
                                    <label class="form-check-label" for="inlineCheckbox2">Coprológico/Cultivo</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Directo Hongos">
                                    <label class="form-check-label" for="inlineCheckbox2">Directo Hongos</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Frotis Faringeo">
                                    <label class="form-check-label" for="inlineCheckbox2">Frotis Faringeo</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Glicemia">
                                    <label class="form-check-label" for="inlineCheckbox2">Glicemia</label>
                                </div>
                            </div>
                            <div class="col-md-2" id="divResultados">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Perfil Lipídico">
                                    <label class="form-check-label" for="inlineCheckbox2">Perfil Lipídico</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Cuadro hemático">
                                    <label class="form-check-label" for="inlineCheckbox2">Cuadro hemático</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Parcial de orina">
                                    <label class="form-check-label" for="inlineCheckbox2">Parcial de orina</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Serología para Sífilis">
                                    <label class="form-check-label" for="inlineCheckbox2">Serología para Sífilis</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Otra">
                                    <label class="form-check-label" for="inlineCheckbox2">Otra</label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <label for="motivo" class="font-bold">Otro:</label>
                            </div>
                            <div class="col-md-5">
                                <textarea name="otroPC" id="otroPC" rows="6" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="title-sections" id="sub-title">RESTRICCIONES</h5>
                            <div class="border-container">
                                <textarea name="restricciones" id="restriccionesID" class="form-control" rows="3" placeholder="Restricciones" ></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5 class="title-sections" id="sub-title">RECOMENDACIONES ADICIONALES</h5>
                            <div class="border-container">
                                <textarea name="recomendacionesA" id="recomendacionesID" class="form-control"  rows="3" placeholder="Recomendaciones adicionales" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-5">
                <input type="submit" value="Guardar" class="button-form btn btn-primary" id="registrarHC">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var examen = {!! json_encode($examenesLab) !!};
        {{--var cie10 = {!! json_encode($cie10) !!};--}}

    </script>
@stop

