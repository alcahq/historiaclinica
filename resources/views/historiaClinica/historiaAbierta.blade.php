@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
    <script src="{{asset('/js/historiaClinica.js')}}"></script>
@stop
@section('title')
    <title>Historia Clínica - Actualizar</title>
@stop
@section('contenido')
    @if(session()->has('actualizarHCAbierta'))
        <div class="container-local">
            <h1>{{ session()->get('registerHC') }}</h1>
        </div>
    @else
        <div class="container-local border-container">
            <div class="toast toast-local" data-autohide="false" id="toastHC">
                <div class="toast-header">
                    <strong class="mr-auto">Alerta</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 padd-label" style="padding-right: 0">
                    <label for="fecha" class="font-bold">Fecha:</label>
                    <input value="{{ $historia->fecha }}" id="fechaRegistro" class="form-control-form border-white" disabled>
                    <label for="fecha" hidden="true" style="padding-right: 8px">Paciente:</label>
                    <input class="form-control-form" type="hidden" name="documento" id="documentoBuscado" value="{{ $historia->paciente_cedula }}">
                </div>
                <div class="col-md-6 padd-row-ppl padd-label">
                    <label for="fecha" class="font-bold padd-label width-lbl">Médico:</label>
                    <input value="{{ $historia['medicoapertura']->nombre_completo }}" id="medicoNombre" class="form-control-form border-white size-input-header" style="color: #003594;" disabled>
                    <input value="{{"CC: ".$historia['medicoapertura']->document }}" id="medicoCedula" class="form-control-form border-white size-input-header" disabled style="color: #003594;">
                    <input value="{{ $historia->medicoApertura_id }}" id="id_medicoA" type="hidden">
                    <input value="{{auth()->user()->id }}" id="id_medicoC" type="hidden">
                    <label for="fecha" class="font-bold padd-label width-lbl">Nombre:</label>
                    <input class="form-control-form border-white size-input-header" id="nombrePaciente" value="{{ $historia->paciente }}" style="color: #003594;" disabled>
                    <input class="form-control-form border-white size-input-header" id="documentoPaciente" value="{{ "CC: ".$historia->paciente_cedula }}" style="color: #003594; width: 20%" disabled>
                    <input type="text" class="form-control-form border-white" id="edadPaciente" value="{{ "Edad: " . $historia->paciente_edad . " años" }}" style="color: #003594; width: 20%" disabled>
                    <input type="hidden" id="estadoHC" value="{{ $historia->estado }}">
                </div>
                <div class="col-md-3">
                    <div class="container-estado">
                        <div class="row">
                            <div class="col-md-10 offset-1 col-title-estado">
                                <span class="">Estado: {{ $historia->estado }}</span>
                            </div>
                            @if($historia->estado != "Anulada" and auth()->user()->hasRoles(['admin', 'medico']))
                                <div class="col-md-10 offset-1" id="estado-btn">
                                    <input type="submit" value="Anular" id="anularHC" class="button-form btn btn-primary">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 1%">
                <div class="col-md-2" style="padding-right: 0; padding-top: 6px">
                    <label for="motivo-evaluacion" class="font-bold" style="padding-right: 8px">Motivo de la evaluación:</label>
                </div>
                <div class="col-md-3" style="padding-right: 0; padding-bottom: 1%; padding-left: 0">
                    <div class="form-group size-motivoE div-select-form">
                        <select class="select-form form-control" id="motivoE" name="motivoEvaluacion" style="font-size: 0.8rem">
                            <option value=""></option>
                            <option value="INGRESO" {{ old('motivoEvaluacion') == 'INGRESO' || $historia->motivoevaluacion == 'INGRESO' ? "selected":""}}>INGRESO</option>
                            <option value="PERIÓDICO" {{ old('motivoEvaluacion') == 'PERIÓDICO' || $historia->motivoevaluacion == 'PERIÓDICO' ? "selected":""}}>PERIÓDICO</option>
                            <option value="RETIRO" {{ old('motivoEvaluacion') == 'RETIRO' || $historia->motivoevaluacion == 'RETIRO' ? "selected":""}}>RETIRO</option>
                            <option value="POSTINCAPACIDAD" {{ old('motivoEvaluacion') == 'POSTINCAPACIDAD' || $historia->motivoevaluacion == 'POSTINCAPACIDAD' ? "selected":""}}>POSTINCAPACIDAD</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-7">
                    <label for="cargo evaluar" class="font-bold" style="padding-left: 12px; padding-right: 5%">Cargo a evaluar:</label>
                    <input type="text" name="cargoEvaluar" id="cargoE" class="size-input-cargo form-control-form" value="{{ $historia->cargoEvaluar }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-1" style="padding-right: 0">
                    <label for="rh" class="font-bold">Otros:</label>
                </div>
                <div class="col-md-11" style="padding-right: 0; padding-left: 0">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Trabajo en alturas" {{ strpos($historia->motivoevaluacion_otros, 'Trabajo en alturas') !== false || old('otros[]') == 'Trabajo en alturas'  ? "checked":"" }}>
                        <label class="form-check-label" for="Trabajo en alturas">Trabajo en alturas</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Manipulación de alimentos" {{ strpos($historia->motivoevaluacion_otros, 'Manipulación de alimentos')!== false || old('otros[]') == 'Manipulación de alimentos'  ? "checked":"" }}>
                        <label class="form-check-label" for="Manipulación de alimentos">Manipulación de alimentos</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Enfasis osteomuscular" {{ strpos($historia->motivoevaluacion_otros, 'Enfasis osteomuscular')!== false || old('otros[]') == 'Enfasis osteomuscular'  ? "checked":"" }}>
                        <label class="form-check-label" for="Enfasis osteomuscular">Enfasis osteomuscular</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Reubicación laboral" {{ strpos($historia->motivoevaluacion_otros, 'Reubicación laboral')!== false || old('otros[]') == 'Reubicación laboral'  ? "checked":"" }}>
                        <label class="form-check-label" for="Reubicación laboral">Reubicación laboral</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-local">
            <ul class="nav nav-tabs" id="tabHC" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="ocupacionales-tab" data-toggle="tab" href="#ocupacionales" role="tab" aria-controls="ocupacionales" aria-selected="true">Ocupacionales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="laboral-tab" data-toggle="tab" href="#laboral" role="tab" aria-controls="laboral" aria-selected="false">Laboral</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="habitos-tab" data-toggle="tab" href="#habitos" role="tab" aria-controls="habitos" aria-selected="false">Hábitos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="antecedentes-tab" data-toggle="tab" href="#antecedentes" role="tab" aria-controls="antecedentes" aria-selected="true">Antecedentes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="revisionPorSistemas-tab" data-toggle="tab" href="#revisionPorSistemas" role="tab" aria-controls="revisionPorSistemas" aria-selected="false">Revisión por Sistemas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="examenFisico-tab" data-toggle="tab" href="#examenFisico" role="tab" aria-controls="examenFisico" aria-selected="false">Examen físico</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="manipulacionAlimentos-tab" data-toggle="tab" href="#manipulacionAlimentos" role="tab" aria-controls="manipulacionAlimentos" aria-selected="true">Manipulación alimentos / Dermatológico</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="paraclinicos-tab" data-toggle="tab" href="#paraclinicos" role="tab" aria-controls="paraclinicos" aria-selected="false">Paraclínicos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="impresionDiagnostica-tab" data-toggle="tab" href="#impresionDiagnostica" role="tab" aria-controls="impresionDiagnostica" aria-selected="false">Impresion Diagnóstica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="result-tab" data-toggle="tab" href="#result" role="tab" aria-controls="resultado" aria-selected="false">Resultado</a>
                </li>
            </ul>
            <div class="tab-content border-tabs" id="myTabContent">
                <div class="tab-pane fade show active" id="ocupacionales" role="tabpanel" aria-labelledby="ocupacionales-tab">
                    <h5 class="title-sections" id="sub-title">CARGO A EVALUAR</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="fechaNacimiento" class="size-label-small">Fecha de Ingreso:</label>
                                <input type="text" class="size-input-small form-control-form datepicker" value="{{$historia['cargoactual']->fecha}}" name="fechaIngreso" id="fechaIngreso" autocomplete="off" style="padding: 3px">
                            </div>
                            <div class="col-md-4" style="padding-left: 0; padding-right: 0">
                                <label for="cargo" class="size-label-small-c">Cargo:</label>
                                <input type="text" name="cargoActual" id="cargoActual" class="size-input-small-c form-control-form" value="{{ $historia['cargoactual']->cargo }}" disabled>
                            </div>
                            <div class="col-md-5 padding-lbl-0" id="jornada-tab">
                                <label for="jornada" class="size-label-small-j">Jornada de trabajo:</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="Diurno" name="jornadaTrabajo" value="Diurno" {{ old('jornadaTrabajo') == 'Diurno' || $historia['cargoactual']->jornada == 'Diurno' ? "checked":""}}>
                                    <label class="custom-control-label" for="Diurno">Diurno</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="Nocturno" name="jornadaTrabajo" value="Nocturno" {{ old('jornadaTrabajo') == 'Nocturno' || $historia['cargoactual']->jornada == 'Nocturno' ? "checked":""}}>
                                    <label class="custom-control-label" for="Nocturno">Nocturno</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="Rotativo" name="jornadaTrabajo" value="Rotativo" {{ old('jornadaTrabajo') == 'Rotativo' || $historia['cargoactual']->jornada == 'Rotativo' ? "checked":""}}>
                                    <label class="custom-control-label" for="Rotativo">Rotativo</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="Horasextras" name="jornadaTrabajo" value="Horasextras" {{ old('jornadaTrabajo') == 'Horasextras' || $historia['cargoactual']->jornada == 'Horasextras' ? "checked":""}}>
                                    <label class="custom-control-label" for="Horasextras">Horas extras</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="cargo" class="size-label-antig">Antiguedad:</label>
                                <input type="text" name="antiguedadO" id="antiguedadO" class="form-control-form antiguedadO-size" value="{{ $historia['cargoactual']->antiguedad }}">
                                <div class="form-group div-select-form size-row-antig">
                                    <select class="select-form form-control" name="antiguedadM" id="antiguedadM" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Días" {{ old('antiguedadM') == 'Días' || $historia['cargoactual']->antiguedadMedida == 'Días' ? "selected":""}}>Días</option>
                                        <option value="Meses" {{ old('antiguedadM') == 'Meses' || $historia['cargoactual']->antiguedadMedida == 'Meses' ? "selected":""}}>Meses</option>
                                        <option value="Años" {{ old('antiguedadM') == 'Años' || $historia['cargoactual']->antiguedadMedida == 'Años' ? "selected":""}}>Años</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0; padding-right: 0">
                                <label for="cargo" class="size-label-small-c">Area:</label>
                                <input type="text" name="area" id="area" class="size-input-small-c form-control-form" placeholder="Area" value="{{ $historia['cargoactual']->area }}">
                            </div>
                            <div class="col-md-5 padding-lbl-0">
                                <label for="cargo" style="width: 26%" class="padding-lbl-0">Horas trabajadas:</label>
                                <input type="text" name="horasT" id="horasT" class="form-control-form" style="width: 8%" value="{{ $historia['cargoactual']->horas }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <label for="empresaActual" style="margin-top: 4px">Empresa:</label>
                            </div>
                            <div class="col-md-9" style="padding-left: 0">
                                <input type="text" name="empresa" id="empresaActual" class="size-input-emp form-control-form" autocomplete="off" style="padding: 3px 10px" value="{{ $historia['cargoactual']->empresa }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 size-col-ep padding-lbl-0">
                                <label for="jornada" class="">Elementos de protección:</label>
                            </div>
                            <div class="col-md-10 observacionOtro" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Gafas" {{ strpos($historia['cargoactual']->elementos, 'Gafas') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="Gafas">Gafas</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Botas" {{ strpos($historia['cargoactual']->elementos, 'Botas')!== false || old('elementosP[]') == 'Botas' ? "checked":""}}>
                                    <label class="form-check-label" for="Botas">Botas</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Guantes" {{ strpos($historia['cargoactual']->elementos, 'Guantes')!== false || old('elementosP[]') == 'Guantes' ? "checked":""}}>
                                    <label class="form-check-label" for="Guantes">Guantes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Tapabocas" {{ strpos($historia['cargoactual']->elementos, 'Tapabocas')!== false || old('elementosP[]') == 'Tapabocas' ? "checked":""}}>
                                    <label class="form-check-label" for="Tapabocas">Tapabocas</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Casco" {{ strpos($historia['cargoactual']->elementos, 'Casco')!== false || old('elementosP[]') == 'Casco' ? "checked":""}}>
                                    <label class="form-check-label" for="Casco">Casco</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Protector respiratorio" {{ strpos($historia['cargoactual']->elementos, 'Protector respiratorio')!== false || old('elementosP[]') == 'Protector respiratorio' ? "checked":""}}>
                                    <label class="form-check-label" for="Protector respiratorio">Protector respiratorio</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Overol" {{ strpos($historia['cargoactual']->elementos, 'Overol')!== false || old('elementosP[]') == 'Overol' ? "checked":""}}>
                                    <label class="form-check-label" for="Overol">Overol</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Protector auditivo" {{ strpos($historia['cargoactual']->elementos, 'Protector auditivo')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Protector auditivo">Protector auditivo</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Cofia" {{ strpos($historia['cargoactual']->elementos, 'Cofia')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Cofia">Cofia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Equipo trabajo en alturas/confinados"{{ strpos($historia['cargoactual']->elementos, 'Equipo trabajo en alturas/confinados')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Equipo trabajo en alturas/confinados">Equipo trabajo en alturas/confinados</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Careta"{{ strpos($historia['cargoactual']->elementos, 'Careta')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Careta">Careta</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Calzado antideslizante"{{ strpos($historia['cargoactual']->elementos, 'Calzado antideslizante')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Calzado antideslizante">Calzado antideslizante</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Ropa de trabajo"{{ strpos($historia['cargoactual']->elementos, 'Ropa de trabajo')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Ropa de trabajo">Ropa de trabajo</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Ninguno"{{ strpos($historia['cargoactual']->elementos, 'Ninguno')!== false ? "checked":""}}>
                                    <label class="form-check-label" for="Ninguno">Ninguno</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="elementosP[]" value="Otros" id="otrosEP" {{ strpos($historia['cargoactual']->elementos, 'Otros') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="Otros">Otros</label>
                                </div>
                            </div>
                            <div class="col-md-2 size-col-ep">
                                <label for="Otros elementos" id="OtrosLbl" {{ strpos($historia['cargoactual']->elementos, 'Otros') !== false ? "":"hidden=\"true\""}}>Otros elementos:</label>
                            </div>
                            <div class="col-md-9" style="padding-left: 0">
                                <input type="text" name="otrosObs" id="otrosObs" class="size-input-small-c form-control-form" value="{{ $historia['cargoactual']->otros_elementos }}" {{ strpos($historia['cargoactual']->elementos, 'Otros') !== false ? "":"hidden=\"true\""}}>
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">FACTORES DE RIESGOS EVALUAR Y ANTERIORES</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;" data-spy="scroll" data-offset="0">
                        @for($i = 0; $i < count($historia['cargoanterior']); $i++)
                            <div class="generalcontainer" id="generalContainer{{$i}}">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="empresa" class="size-lbl-efr font-bold">Empresa:</label>
                                        <input type="text" name="empresaFR" id="{{ "empresaFR".$i }}" class="form-control-form size-input-efr" value="{{ $historia['cargoanterior'][$i]['empresa'] }}" {{$i == 0 ? "disabled" : ""}}>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0">
                                        <label for="cargo" class="size-lbl-cfr font-bold">Cargo:</label>
                                        <input type="text" name="cargoFR" id="{{ "cargoFR".$i }}" class="form-control-form size-input-cfr" value="{{ $historia['cargoanterior'][$i]['cargo'] }}" {{$i == 0 ? "disabled" : ""}}>
                                    </div>
                                    <div class="col-md-3" style="padding-left: 0; padding-right: 0">
                                        <label for="cargo" class="size-lbl-texp font-bold">Tiempo de exposición:</label>
                                        <input type="text" name="tiempoExp" id="{{ "tiempoExp".$i }}" value="{{ $historia['cargoanterior'][$i]['tiempoexposicion'] }}" class="form-control-form size-input-texp">
                                        <div class="form-group div-select-form size-row-antigTE">
                                            <select class="select-form form-control" name="antiguedadTE" id="{{ "antiguedadTE".$i }}" style="font-size: 0.8rem">
                                                <option value=""></option>
                                                <option value="Días" {{old('antiguedadM') == 'Días' || $historia['cargoanterior'][$i]['antiguedad'] == 'Días' ? "selected":""}}>Días</option>
                                                <option value="Meses" {{old('antiguedadM') == 'Meses' || $historia['cargoanterior'][$i]['antiguedad'] == 'Meses' ? "selected":""}}>Meses</option>
                                                <option value="Años" {{old('antiguedadM') == 'Años' || $historia['cargoanterior'][$i]['antiguedad'] == 'Años' ? "selected":""}}>Años</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 3px">
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-2" style="max-width: 15.8%">
                                                <label for="fisicos" class="font-bold">Físicos:</label>
                                            </div>
                                            <div class="col-md-5" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]" }}" value="Iluminación" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Iluminación')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="iluminacion">Iluminación</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Temperatura alta" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Temperatura alta')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="temperatura alta">Temperatura alta</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Radiaciones ionizantes" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Radiaciones ionizantes')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="radiaciones ionizantes">Radiaciones ionizantes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Vibraciones" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Vibraciones')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Vibraciones</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5" id="jornada-tab" style="padding-left: 0; padding-right: 0">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Temperatura baja" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Temperatura baja')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Temperatura baja</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Presión barométrica" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Presión barométrica')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Presión barométrica</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Radiaciones no ionizantes" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Radiaciones no ionizantes')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Radiaciones no ionizantes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "fisicos".$i."[]"}}" value="Ruido" {{ strpos($historia['cargoanterior'][$i]['fisicos'], 'Ruido')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Ruido</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3" style="padding-left: 0">
                                        <div class="row">
                                            <div class="col-md-4" style="max-width: 30%">
                                                <label for="quimicos" class="font-bold">Químicos:</label>
                                            </div>
                                            <div class="col-md-3" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Gases" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Gases')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Gases</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Vapores" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Vapores')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Vapores</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Humos" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Humos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Humos</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3" id="jornada-tab">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Fibras" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Fibras')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Fibras</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Polvos" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Polvos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Polvos</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "quimicos".$i."[]"}}" value="Líquidos" {{ strpos($historia['cargoanterior'][$i]['quimicos'], 'Líquidos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Líquidos</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3" style="padding-left: 0">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label for="fisicos" class="font-bold">Ergononómicos:</label>
                                            </div>
                                            <div class="col-md-7" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "ergonomicos".$i."[]"}}"  value="Posturas prolongadas" {{ strpos($historia['cargoanterior'][$i]['ergonomicos'], 'Posturas prolongadas')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Posturas prolongadas</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "ergonomicos".$i."[]"}}" value="Manejo de cargas" {{ strpos($historia['cargoanterior'][$i]['ergonomicos'], 'Manejo de cargas')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Manejo de cargas</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "ergonomicos".$i."[]"}}" value="Movimientos repetitivos" {{ strpos($historia['cargoanterior'][$i]['ergonomicos'], 'Movimientos repetitivos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Movimientos repetitivos</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "ergonomicos".$i."[]"}}" value="Video terminales" {{ strpos($historia['cargoanterior'][$i]['ergonomicos'], 'Video terminales')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Video terminales</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 2px">
                                    <div class="col-md-3" style="max-width: 22.8%;">
                                        <div class="row">
                                            <div class="col-md-5" style="max-width: 37%;">
                                                <label for="fisicos" class="font-bold">Biológicos:</label>
                                            </div>
                                            <div class="col-md-6" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "biologicos".$i."[]"}}" value="Microorganismos" {{ strpos($historia['cargoanterior'][$i]['biologicos'], 'Microorganismos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Microorganismos</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "biologicos".$i."[]"}}" value="Animales" {{ strpos($historia['cargoanterior'][$i]['biologicos'], 'Animales')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Animales</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "biologicos".$i."[]"}}" value="Vegetales" {{ strpos($historia['cargoanterior'][$i]['biologicos'], 'Vegetales')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Vegetales</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-5" style="max-width: 39%">
                                                <label for="fisicos" class="font-bold">Psicosociales:</label>
                                            </div>
                                            <div class="col-md-7" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "psicosociales".$i."[]"}}" value="Relaciones humanas" {{ strpos($historia['cargoanterior'][$i]['psicosociales'], 'Relaciones humanas')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Relaciones humanas</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "psicosociales".$i."[]"}}" value="Gestión" {{ strpos($historia['cargoanterior'][$i]['psicosociales'], 'Gestión')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Gestión</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "psicosociales".$i."[]"}}" value="Ambiente de trabajo" {{ strpos($historia['cargoanterior'][$i]['psicosociales'], 'Ambiente de trabajo')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Ambiente de trabajo</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3" style="max-width: 22.8%;">
                                        <div class="row">
                                            <div class="col-md-5" style="max-width: 37%;">
                                                <label for="fisicos" class="font-bold">Seguridad:</label>
                                            </div>
                                            <div class="col-md-6" id="jornada-tab" style="padding-left: 0; padding-right: 0;">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "seguridad".$i."[]"}}" value="Eléctricos" {{ strpos($historia['cargoanterior'][$i]['seguridad'], 'Eléctricos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Eléctricos</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "seguridad".$i."[]"}}" value="Incendio" {{ strpos($historia['cargoanterior'][$i]['seguridad'], 'Incendio')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox2">Incendio</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "seguridad".$i."[]"}}" value="Mecánicos" {{ strpos($historia['cargoanterior'][$i]['seguridad'], 'Mecánicos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Mecánicos</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="{{ "seguridad".$i."[]"}}" value="Locativos" {{ strpos($historia['cargoanterior'][$i]['seguridad'], 'Locativos')!== false  ? "checked":""}}>
                                                    <label class="form-check-label" for="inlineCheckbox1">Locativos</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="jornada" class="font-bold">Otros:</label>
                                        <textarea class="form-control" id="{{ "otrosFactores".$i }}" rows="3">{{ $historia['cargoanterior'][$i]['otros'] }}</textarea>
                                    </div>
                                </div>
                                @if($i != 0)
                                <div class="row" id="" style="padding-top: 1%;">
                                    <div class="col-md-6 offset-6" style="text-align: right">
                                        <a href="" id="eliminarFactorRiesgo" style=" color: red">Eliminar factores de riesgo del cargo actual</a>
                                    </div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        @endfor
                            <div class="row" id="prueba">
                                <div class="col-md-6">
                                    <a href="" id="factorRiesgo">Añadir factores de riesgo del cargo actual</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="tab-pane fade" id="laboral" role="tabpanel" aria-labelledby="laboral-tab">
                    <h5 class="title-sections" id="sub-title">ACCIDENTES DE TRABAJO</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;">
                        <div class="row" style="padding-bottom: 1%">
                            <div id="divCheckAT" class="col-md-12">
                                <label for="accidentesSiNo" style="width: 16%">¿Tiene accidentes de trabajo?:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="Si" name="accidentesSN" value="Si" {{ $historia->accidente == 'Si' ? "checked":""}}>
                                    <label class="custom-control-label" for="Si">Si</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="no" name="accidentesSN" value="No" {{ $historia->accidente == 'No' ? "checked":""}}>
                                    <label class="custom-control-label" for="no">No</label>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered table-responsive-md" id="tableAT" {{ $historia->accidente == 'Si' ? "":"hidden=\"true\"" }}>
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 10%">Fecha</th>
                                <th scope="col" style="width: 22%">Empresa</th>
                                <th scope="col" style="width: 28%">Lesión</th>
                                <th scope="col" style="width: 11%">Días incapacidad</th>
                                <th scope="col" style="width: 20%">Secuelas</th>
                                <th scope="col" style="width: 6%">Eliminar</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyAt">
                            @foreach($historia['accidentes'] as $accidente)
                                <tr id="tr" class="accidenteTR">
                                    <td><input type="text" class="form-control-form input-table" name="fechaAT" id="fechaAT" value="{{ $accidente->fecha }}" autocomplete="off"></td>
                                    <td><input type="text" class="input-table" name="empresaAT" id="empresaAT" value="{{ $accidente->empresa }}"></td>
                                    <td><input type="text" class="input-table" name="lesionAT" id="lesionAT" value="{{ $accidente->lesion }}"></td>
                                    <td><input type="text" class="input-table" name="diasIncapacidadAT" id="diasIncapacidadAT" value="{{ $accidente->dias_incapacidad }}"></td>
                                    <td><input type="text" class="input-table" name="secuelasAT" id="secuelasAT" value="{{ $accidente->secuelas }}"></td>
                                    <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="eliminarAT"><img src="/img/delete.png" alt="delete" style="width: 30px"></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6 offset-md-6" style="text-align: right">
                                <a href="" id="anadirAT" {{ $historia->accidente == 'Si' ? "":"hidden=\"true\"" }}>Añadir accidente de trabajo</a>
                            </div>
                        </div>
                    </div>

                    <h5 class="title-sections" id="sub-title">ENFERMEDAD LABORAL</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;">
                        <div class="row" style="padding-bottom: 1%">
                            <div id="divCheckEP" class="col-md-12">
                                <label for="enfermedadSiNo" class="width-lbl-ep">¿Tiene enfermedades profesionales?:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siEP" name="enfermedadSN" value="Si" {{ $historia->enfermedad == 'Si' ? "checked":""}}>
                                    <label class="custom-control-label" for="siEP">Si</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noEP" name="enfermedadSN" value="No" {{ $historia->enfermedad == 'No' ? "checked":""}}>
                                    <label class="custom-control-label" for="noEP">No</label>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered table-responsive-md" id="tableEP" {{ $historia->enfermedad == 'Si' ? "":"hidden=\"true\"" }}>
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 10%">Fecha</th>
                                <th scope="col" style="width: 22%">Empresa</th>
                                <th scope="col" style="width: 28%">Diagnóstico</th>
                                <th scope="col" style="width: 11%">ARL</th>
                                <th scope="col" style="width: 20%">Reubicación</th>
                                <th scope="col" style="width: 6%">Eliminar</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyEP">
                            @foreach($historia['enfermedades'] as $enfermedad)
                                <tr id="tr" class="enfermedadTR">
                                    <td><input type="text" class="form-control-form input-table" name="fechaEP" id="fechaEP" value="{{ $enfermedad->fecha }}" autocomplete="off"></td>
                                    <td><input type="text" class="input-table" name="empresaEP" id="empresaEP" value="{{ $enfermedad->empresa }}"></td>
                                    <td><input type="text" class="input-table" name="diagnosticoEP" id="diagnosticoEP" value="{{ $enfermedad->diagnostico }}"></td>
                                    <td><input type="text" class="input-table" name="arlEP" id="arlEP" value="{{ $enfermedad->arl }}"></td>
                                    <td><input type="text" class="input-table" name="reubicacionEP" id="reubicacionEP" value="{{ $enfermedad->reubicacion }}"></td>
                                    <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="eliminarEP"><img src="/img/delete.png" alt="delete" style="width: 30px"></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6 offset-md-6" style="text-align: right">
                                <a href="" id="anadirEP" {{ $historia->enfermedad == 'Si' ? "":"hidden=\"true\"" }}>Añadir enfermedad profesional</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade tab-container" id="habitos" role="tabpanel" aria-labelledby="habitos-tab">
                    <div class="row">
                        {{--Alcohol--}}
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">ALCOHOL</h6>
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="alcoholSiNo">Alcohol:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siA" name="alcoholSN" value="Si" {{ $historia['habitos']->alcohol == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siA">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noA" name="alcoholSN" value="No" {{ $historia['habitos']->alcohol == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noA">No</label>
                                    </div>
                                    <br>
                                    <label for="frecuencia">Frecuencia:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="frecuenciaA" name="frecuenciaA" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Diario" {{ $historia['habitos']->alcohol_frecuencia == 'Diario' ? "selected":""}}>Diario</option>
                                            <option value="Semanal" {{ $historia['habitos']->alcohol_frecuencia == 'Semanal' ? "selected":""}}>Semanal</option>
                                            <option value="Quincenal" {{ $historia['habitos']->alcohol_frecuencia == 'Quincenal' ? "selected":""}}>Quincenal</option>
                                            <option value="Mensual" {{ $historia['habitos']->alcohol_frecuencia == 'Mensual' ? "selected":""}}>Mensual</option>
                                            <option value="Otra" {{ $historia['habitos']->alcohol_frecuencia == 'Otra' ? "selected":""}}>Otra</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="cantidad">Cantidad:</label>
                                    <input type="text" name="cantidadA" id="cantidadA" class="form-control-form input-container-small" value="{{ $historia['habitos']->alcohol_cantidad }}">
                                </div>
                            </div>
                        </div>
                        {{--Sedentarismo--}}
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">SEDENTARISMO</h6>
                            <div class="border-container">
                                <div class="col-md-12" id="divAlcohol">
                                    <label for="alcoholSiNo">Sedentarismo:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siS" name="sedentarismoSN" value="Si" {{ $historia['habitos']->sedentarismo == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siS">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noS" name="sedentarismoSN" value="No" {{ $historia['habitos']->sedentarismo == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noS">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--Sustacias psicoactivas--}}
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">SUSTANCIAS PSICOACTIVAS</h6>
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="alcoholSiNo" class="width-lbl-spsc">Sustancias psicoactivas:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siSPC" name="psicoactivasSN" value="Si" {{ $historia['habitos']->sustancias == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siSPC">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noSPC" name="psicoactivasSN" value="No" {{ $historia['habitos']->sustancias == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noSPC">No</label>
                                    </div>
                                    <br>
                                    <label for="frecuenciaPSC">Frecuencia:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="frecuenciaPSC" name="frecuenciaPSC" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Diario" {{ $historia['habitos']->sustancias_frecuencia == 'Diario' ? "selected":""}}>Diario</option>
                                            <option value="Semanal" {{ $historia['habitos']->sustancias_frecuencia == 'Semanal' ? "selected":""}}>Semanal</option>
                                            <option value="Quincenal" {{ $historia['habitos']->sustancias_frecuencia == 'Quincenal' ? "selected":""}}>Quincenal</option>
                                            <option value="Mensual" {{ $historia['habitos']->sustancias_frecuencia == 'Mensual' ? "selected":""}}>Mensual</option>
                                            <option value="Otra" {{ $historia['habitos']->sustancias_frecuencia == 'Otra' ? "selected":""}}>Otra</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="cual">Cual:</label>
                                    <input type="text" name="cualPSC" id="cualPSC" class="form-control-form input-container-small" value="{{ $historia['habitos']->sustancias_cual }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {{--tabaquismo--}}
                        <div class="col-md-4 offset-md-2 container-small">
                            <h6 class="tab-subtitle" id="sub-title">TABAQUISMO</h6>
                            <div class="border-container" style="padding-bottom: 15%">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="tabaquismo" class="width-lbl-spsc">Fumador:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siT" name="tabaquismoSN" value="Si" {{ $historia['habitos']->tabaquismo == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siT">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noT" name="tabaquismoSN" value="No" {{ $historia['habitos']->tabaquismo == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noT">No</label>
                                    </div>
                                    <br>

                                    <label for="cantidad cigariilos" class="width-lbl-spsc">Cantidad cigarrillos día:</label>
                                    <input type="text" name="cantidadCD" id="cantidadCD" class="form-control-form width-lblgeneral" value="{{ $historia['habitos']->tabaquismo_cantidad }}">
                                    <br>

                                    <label for="ex fumador" class="width-lbl-spsc">Ex fumador:</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="exfumador" value="Si" value="Si" {{ $historia['habitos']->tabaquismo_exfumador == 'Si' ? "checked":""}}>
                                        <label class="form-check-label" for="Si">Si</label>
                                    </div>
                                    <br>

                                    <label for="tiempo fumador" class="width-lbl-spsc">Cuanto tiempo fumó:</label>
                                    <input type="text" name="tiempoFumador" id="tiempoFumador" class="form-control-form" style="max-width: 12%" value="{{ $historia['habitos']->tabaquismo_tiempo }}">
                                    <div class="form-group div-select-form" id="select-medida">
                                        <select class="select-form form-control" name="tiempoFM" id="tiempoFM" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Días" {{ $historia['habitos']->tabaquismo_tiempoMedida == 'Días' ? "selected":""}}>Días</option>
                                            <option value="Meses" {{ $historia['habitos']->tabaquismo_tiempoMedida == 'Meses' ? "selected":""}}>Meses</option>
                                            <option value="Años" {{ $historia['habitos']->tabaquismo_tiempoMedida == 'Años' ? "selected":""}}>Años</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--Deportes--}}
                        <div class="col-md-4 container-small">
                            <h6 class="tab-subtitle" id="sub-title">DEPORTES</h6>
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="deporte" class="width-lbl-spsc">Deporte:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siD" name="deporteSN" value="Si" {{ $historia['habitos']->deporte == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siD">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noD" name="deporteSN" value="No" {{ $historia['habitos']->deporte == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noD">No</label>
                                    </div>
                                    <br>

                                    <label for="cual deporte" class="width-lbl-spsc">Cual:</label>
                                    <input type="text" name="cualD" id="cualD" class="form-control-form width-lblgeneral" value="{{ $historia['habitos']->deporte_cual }}">
                                    <br>

                                    <label for="frecuencia" class="width-lbl-spsc">Frecuencia:</label>
                                    <div class="form-group div-select-form width-lblgeneral">
                                        <select class="select-form form-control" id="frecuenciaD" name="frecuenciaD" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Diario" {{ $historia['habitos']->deporte_frecuencia == 'Diario' ? "selected":""}}>Diario</option>
                                            <option value="Semanal" {{ $historia['habitos']->deporte_frecuencia == 'Semanal' ? "selected":""}}>Semanal</option>
                                            <option value="Quincenal" {{ $historia['habitos']->deporte_frecuencia == 'Quincenal' ? "selected":""}}>Quincenal</option>
                                            <option value="Mensual" {{ $historia['habitos']->deporte_frecuencia == 'Mensual' ? "selected":""}}>Mensual</option>
                                            <option value="Otra" {{ $historia['habitos']->deporte_frecuencia == 'Otra' ? "selected":""}}>Otra</option>
                                        </select>
                                    </div>
                                    <br>

                                    <label for="ex fumador" class="width-lbl-spsc">Lesiones deportivas:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siLD" name="lesionesD" value="Si" {{ $historia['habitos']->deporte_lesion == 'Si' ? "checked":""}}>
                                        <label class="custom-control-label" for="siLD">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noLD" name="lesionesD" value="No" {{ $historia['habitos']->deporte_lesion == 'No' ? "checked":""}}>
                                        <label class="custom-control-label" for="noLD">No</label>
                                    </div>
                                    <br>

                                    <label for="tiempo fumador" class="width-lbl-spsc">Cual:</label>
                                    <input type="text" name="cualLD" id="cualLD" class="form-control-form width-lblgeneral" value="{{ $historia['habitos']->deporte_lesion_cual }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="antecedentes" role="tabpanel" aria-labelledby="antecedentes-tab">
                    <h5 class="title-sections" id="sub-title">ANTECEDENTES FAMILIARES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-5 offset-1">
                                <div class="row">
                                    <div class="col-md-3 offset-md-2">
                                        <h5 for="patologia" style="font-family: montserrat-bold">Patología</h5>
                                    </div>
                                    <div class="col-md-3 offset-2">
                                        <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 10%">Parentesco</h5>
                                    </div>
                                </div>
                                <label for="hta" class="width-lbl-antecedentesF">H.T.A:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siHTA" name="hta" value="Si" {{ $historia['antecedentes']->familiares_hta == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siHTA">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noHTA" name="hta" value="No" {{ $historia['antecedentes']->familiares_hta == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noHTA">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoHTA" id="parentescoHTA" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_hta_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_hta_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="eap" class="width-lbl-antecedentesF">E.A.P:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siEAP" name="eap" value="Si" {{ $historia['antecedentes']->familiares_eap == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siEAP">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noEAP" name="eap" value="No" {{ $historia['antecedentes']->familiares_eap == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noEAP">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoEAP" id="parentescoEAP" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_eap_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_eap_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="ecv" class="width-lbl-antecedentesF">E.C.V:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siECV" name="ecv" value="Si" {{ $historia['antecedentes']->familiares_ecv == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siECV">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noECV" name="ecv" value="No" {{ $historia['antecedentes']->familiares_ecv == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noECV">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoECV" id="parentescoECV" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_ecv_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="tbc" class="width-lbl-antecedentesF">T.B.C.:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siTBC" name="tbc" value="Si" {{ $historia['antecedentes']->familiares_tbc == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siTBC">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noTBC" name="tbc" value="No" {{ $historia['antecedentes']->familiares_tbc == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noTBC">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoTBC" id="parentescoTBC" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_tbc_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="asma" class="width-lbl-antecedentesF">Asma:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siAsma" name="asma" value="Si" {{ $historia['antecedentes']->familiares_asma == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siAsma">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noAsma" name="asma" value="No" {{ $historia['antecedentes']->familiares_asma == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noAsma">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoAsma" id="parentescoAsma" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_asma_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_asma_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="alergias" class="width-lbl-antecedentesF">Alergias:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siAlergias" name="alergias" value="Si" {{ $historia['antecedentes']->familiares_alergia == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siAlergias">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noAlergias" name="alergias" value="No" {{ $historia['antecedentes']->familiares_alergia == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noAlergias">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoAlergias" id="parentescoAlergias" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_alergia_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="Artritis" class="width-lbl-antecedentesF">Artritis:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siArtritis" name="artritis" value="Si" {{ $historia['antecedentes']->familiares_artritis == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siArtritis">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noArtritis" name="artritis" value="No" {{ $historia['antecedentes']->familiares_artritis == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noArtritis">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoArtritis" id="parentescoArtritis" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_artritis_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="Varices" class="width-lbl-antecedentesF">Várices:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siVarices" name="varices" value="Si" {{ $historia['antecedentes']->familiares_varices == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siVarices">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noVarices" name="varices" value="No" {{ $historia['antecedentes']->familiares_varices == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noVarices">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoVarices" id="parentescoVarices" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_varices_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_varices_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-3 offset-md-2">
                                        <h5 for="patologia" style="font-family: montserrat-bold">Patología</h5>
                                    </div>
                                    <div class="col-md-3 offset-2">
                                        <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 10%">Parentesco</h5>
                                    </div>
                                </div>
                                <label for="cancer" class="width-lbl-antecedentesF">Cancer:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="cancer" id="siCancer" value="Si" {{ $historia['antecedentes']->familiares_cancer == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siCancer">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="cancer" id="noCancer" value="No" {{ $historia['antecedentes']->familiares_cancer == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noCancer">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoCancer" id="parentescoCancer" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_cancer_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="diabetes" class="width-lbl-antecedentesF">Diabetes:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="diabetes" id="siDiabetes" value="Si" {{ $historia['antecedentes']->familiares_diabetes == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siDiabetes">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="diabetes" id="noDiabetes" value="No" {{ $historia['antecedentes']->familiares_diabetes == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noDiabetes">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoDiabetes" id="parentescoDiabetes" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_diabetes_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="dislipidemia" class="width-lbl-antecedentesF">Enfermedad Renal:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="enfermedadRenal" id="siEnfermedadRenal" value="Si" {{ $historia['antecedentes']->familiares_enfermedadRenal == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siEnfermedadRenal">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="enfermedadRenal" id="noEnfermedadRenal" value="No" {{ $historia['antecedentes']->familiares_enfermedadRenal == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noEnfermedadRenal">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoEnfermedadRenal" id="parentescoEnfermedadRenal" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_enfermedadRenal_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="dislipidemia" class="width-lbl-antecedentesF">Dislipidemia:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="dislipidemia" id="siDislipidemia" value="Si" {{ $historia['antecedentes']->familiares_displipidemia == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siDislipidemia">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="dislipidemia" id="noDislipidemia" value="No" {{ $historia['antecedentes']->familiares_displipidemia == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noDislipidemia">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoDislipidemia" id="parentescoDislipidemia" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_displipidemia_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="enfermedadCoronaria" class="width-lbl-antecedentesF">Enfermedad Coronaria:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="enfermedadCoronaria" id="siEnfermedadCoronaria" value="Si" {{ $historia['antecedentes']->familiares_enfermedadCoronaria == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siEnfermedadCoronaria">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="enfermedadCoronaria" id="noEnfermedadCoronaria" value="No" {{ $historia['antecedentes']->familiares_enfermedadCoronaria == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noEnfermedadCoronaria">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoEnfermedadCoronaria" id="parentescoEnfermedadCoronaria" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_enfermedadCoronaria_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="enfermedadColagenosis" class="width-lbl-antecedentesF">Enfermedad Colagenosis:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="enfermedadColagenosis" id="siEnfermedadColagenosis" value="Si" {{ $historia['antecedentes']->familiares_enfermedadColagenosis == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siEnfermedadColagenosis">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="enfermedadColagenosis" id="noEnfermedadColagenosis" value="No" {{ $historia['antecedentes']->familiares_enfermedadColagenosis == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noEnfermedadColagenosis">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoEnfermedadColagenosis" id="parentescoEnfermedadColagenosis" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre"  {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre"  {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->familiares_enfermedadColagenosis_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="enfermedadTiroidea" class="width-lbl-antecedentesF">Enfermedad Tiroidea:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="enfermedadTiroidea" id="siEnfermedadTiroidea" value="Si" {{ $historia['antecedentes']->familiares_enfermedadTiroidea == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siEnfermedadTiroidea">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="enfermedadTiroidea" id="noEnfermedadTiroidea" value="No" {{ $historia['antecedentes']->familiares_enfermedadTiroidea == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noEnfermedadTiroidea">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoEnfermedadTiroidea" id="parentescoEnfermedadTiroidea" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_enfermedadTiroidea_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="otra" class="width-lbl-antecedentesF">Otra:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="otra" id="siOtra" value="Si" {{ $historia['antecedentes']->familiares_otra == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siOtra">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="otra" id="noOtra" value="No" {{ $historia['antecedentes']->familiares_otra == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noOtra">No</label>
                                </div>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="parentescoOtra" id="parentescoOtra" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Padre" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Padre' ? "selected":"" }}>Padre</option>
                                        <option value="Madre" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Madre' ? "selected":"" }}>Madre</option>
                                        <option value="Abuelo" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Abuelo' ? "selected":"" }}>Abuelo</option>
                                        <option value="Abuela" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Abuela' ? "selected":"" }}>Abuela</option>
                                        <option value="Hermana" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Hermana' ? "selected":"" }}>Hermana</option>
                                        <option value="Hermano" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Hermano' ? "selected":"" }}>Hermano</option>
                                        <option value="Padre/Madre" {{ $historia['antecedentes']->familiares_otra_parentesco == 'Padre/Madre' ? "selected":"" }}>Padre/Madre</option>
                                        <option value="N/A" {{ $historia['antecedentes']->familiares_otra_parentesco == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>
                                <label for="otra" class="width-lbl-antecedentesF">Observaciones:</label>
                                <input type="text" class="form-control-form" id="familiaresOtraObs" name="familiaresOtraObs" value="{{ $historia['antecedentes']->familiares_otra_observacion }}" style="width: 56.5%" placeholder="Observaciones">
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">ANTECEDENTES PERSONALES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-5 offset-1">
                                <div class="row">
                                    <div class="col-md-4 offset-6">
                                        <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 15%">Observaciones</h5>
                                    </div>
                                </div>

                                <label for="patologicos" class="width-lbl-antecedentesP">Patológicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siPatologicos" name="patologicos" value="Si" {{ $historia['antecedentes']->personales_patologicos == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siPatologicos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noPatologicos" name="patologicos" value="No" {{ $historia['antecedentes']->personales_patologicos == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noPatologicos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesPatologico" id="observacionesPatologico" value="{{ $historia['antecedentes']->personales_patologicos_parentesco }}">

                                <label for="quirurgico" class="width-lbl-antecedentesP">Quirúrgicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siquirurgico" name="quirurgico" value="Si" {{ $historia['antecedentes']->personales_quirurgicos == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siquirurgico">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noquirurgico" name="quirurgico" value="No" {{ $historia['antecedentes']->personales_quirurgicos == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noquirurgico">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesQuirurgico" id="observacionesQuirurgico" value="{{ $historia['antecedentes']->personales_quirurgicos_parentesco }}">

                                <label for="Traumaticos" class="width-lbl-antecedentesP">Traumáticos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siTraumaticos" name="traumaticos" value="Si" {{ $historia['antecedentes']->personales_traumaticos == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siTraumaticos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noTraumaticos" name="traumaticos" value="No" {{ $historia['antecedentes']->personales_traumaticos == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noTraumaticos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesTraumaticos" id="observacionesTraumaticos" value="{{ $historia['antecedentes']->personales_traumaticos_parentesco }}">
                            </div>

                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-4 offset-6">
                                        <h5 for="patologia" style="font-family: montserrat-bold; padding-left: 15%">Observaciones</h5>
                                    </div>
                                </div>

                                <label for="farmacologicos" class="width-lbl-antecedentesP">Farmacológicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siFarmacologicos" name="farmacologicos" value="Si" {{ $historia['antecedentes']->personales_farmacologicos == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siFarmacologicos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noFarmacologicos" name="farmacologicos" value="No" {{ $historia['antecedentes']->personales_farmacologicos == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noFarmacologicos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesFarmacologico" id="observacionesFarmacologico"  value="{{ $historia['antecedentes']->personales_farmacologicos_parentesco}}">

                                <label for="Transfusionales" class="width-lbl-antecedentesP">Transfusionales:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siTransfusionales" name="Transfusionales" value="Si" {{ $historia['antecedentes']->personales_transfusionales == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siTransfusionales">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noTransfusionales" name="Transfusionales" value="No" {{ $historia['antecedentes']->personales_transfusionales == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noTransfusionales">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesTransfusionales" id="observacionesTransfusionales" value="{{ $historia['antecedentes']->personales_transfusionales_parentesco }}">

                                <label for="Otros" class="width-lbl-antecedentesP">Otros:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siOtros" name="otros" value="Si" {{ $historia['antecedentes']->personales_otros == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siOtros">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noOtros" name="otros" value="No" {{ $historia['antecedentes']->personales_otros == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noOtros">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="observacionesOtros" id="observacionesOtros" value="{{ $historia['antecedentes']->personales_otros_parentesco }}">
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">ANTECEDENTES INMUNOLÓGICOS</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-5 offset-1">
                                <label for="cancer" class="width-lbl-antecedentesF">Carnet de vacunación:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" name="carnetV" id="siCarnetV" value="Si" {{ $historia['antecedentes']->inmunologicos_carnetVacunacion == 'Si' ? "checked":"" }}>
                                    <label class="custom-control-label" for="siCarnetV">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="carnetV" id="noCarnetV" value="No" {{ $historia['antecedentes']->inmunologicos_carnetVacunacion == 'No' ? "checked":"" }}>
                                    <label class="custom-control-label" for="noCarnetV">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 offset-1">

                                <label for="diabetes" class="width-lbl-antecedentesF">Hepatitis A:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="hepatitisA" id="hepatitisA" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_hepatitisA == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_hepatitisA == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_hepatitisA == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="diabetes" class="width-lbl-antecedentesF">Hepatitis B:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="hepatitisB" id="hepatitisB" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_hepatitisB == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_hepatitisB == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_hepatitisB == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="dislipidemia" class="width-lbl-antecedentesF">Triple viral:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="tripleV" id="tripleV" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_tripleViral == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_tripleViral == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_tripleViral == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="dislipidemia" class="width-lbl-antecedentesF">Tétano:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="tetano" id="tetano" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_tetano == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_tetano == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_tetano == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-5 ">

                                <label for="diabetes" class="width-lbl-antecedentesF">Varicela:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="varicelaAI" id="varicelaAI" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_varicela == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_varicela == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_varicela == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="Influenza" class="width-lbl-antecedentesF">Influenza:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="influenza" id="influenza" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_influenza == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_influenza == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A" {{ $historia['antecedentes']->inmunologicos_influenza == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="fiebre amarilla" class="width-lbl-antecedentesF">Fiebre amarilla:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="fiebreA" id="fiebreA" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_fiebreAmarilla == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_fiebreAmarilla == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A" {{ $historia['antecedentes']->inmunologicos_fiebreAmarilla == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>

                                <label for="otros AI" class="width-lbl-antecedentesF">Otros:</label>
                                <div class="form-group div-select-form" id="select-medida">
                                    <select class="select-form form-control" name="otrosAI" id="otrosAI" style="font-size: 0.8rem">
                                        <option value=""></option>
                                        <option value="Completa" {{ $historia['antecedentes']->inmunologicos_otros == 'Completa' ? "selected":"" }}>Completa</option>
                                        <option value="Incompleta" {{ $historia['antecedentes']->inmunologicos_otros == 'Incompleta' ? "selected":"" }}>Incompleta</option>
                                        <option value="N/A"  {{ $historia['antecedentes']->inmunologicos_otros == 'N/A' ? "selected":"" }}>N/A</option>
                                    </select>
                                </div>
                                <label for="otra" class="width-lbl-antecedentesF">Observaciones:</label>
                                <input type="text" class="form-control-form" id="inmunologicosOtrosObs" name="inmunologicosOtrosObs" value="{{ $historia['antecedentes']->inmunologicos_otros_obs }}" style="width: 56.5%" placeholder="Observaciones">
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections ocultarAG" id="sub-title" {{ $historia->genero == 'MASCULINO' ? "hidden = \"true\"":"" }}>ANTECEDENTES GINECOOBSTETRICOS</h5>
                    <div class="border-container ocultarAG" {{ $historia->genero == 'MASCULINO' ? "hidden = \"true\"":"" }}>
                        <div class="row">
                            <div class="col-md-5 offset-1">
                                <label for="menarquia" class="width-lbl-antecedentesF">Menarquia:</label>
                                <input type="text" class="form-control-form width-lbl-antecedentesF" name="menarquia" id="menarquia" value="{{ $historia['antecedentes']->ginecoobstetricos_menarquia }}">&nbsp&nbsp&nbsp<label for="">Años</label>
                                <label for="ciclos" class="width-lbl-antecedentesF">Ciclos:</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="ciclos" id="ciclos" value="{{ $historia['antecedentes']->ginecoobstetricos_ciclos_1}}">
                                <label for="ciclos">x</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="ciclos2" id="ciclos2" value="{{ $historia['antecedentes']->ginecoobstetricos_ciclos_2}}"><br>
                                <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultimo periodo:</label>
                                <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUPeriodo" id="fechaUPeriodo" autocomplete="off" style="padding: 3px" value="{{ isset($historia['antecedentes']->ginecoobstetricos_fechaUltimoPeriodo) ? $historia['antecedentes']->ginecoobstetricos_fechaUltimoPeriodo:""}}">
                                <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultimo parto:</label>
                                <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUParto" id="fechaUParto" autocomplete="off" style="padding: 3px" value="{{ isset($historia['antecedentes']->ginecoobstetricos_fechaUltimoParto) ? $historia['antecedentes']->ginecoobstetricos_fechaUltimoParto:""}}">
                            </div>
                            <div class="col-md-5">
                                <label for="ciclos" class="width-lbl-antecedentesF">F. obstétrica:</label>
                                <label for="ciclos">G</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="obstetricaG" id="obstetricaG" value="{{ $historia['antecedentes']->ginecoobstetricos_G}}">
                                <label for="ciclos">P</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="obstetricaP" id="obstetricaP" value="{{ $historia['antecedentes']->ginecoobstetricos_P}}">
                                <label for="ciclos">A</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="obstetricaA" id="obstetricaA" value="{{ $historia['antecedentes']->ginecoobstetricos_A}}">
                                <label for="ciclos">C</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="obstetricaC" id="obstetricaC" value="{{ $historia['antecedentes']->ginecoobstetricos_C}}">
                                <label for="ciclos">E</label>
                                <input type="text" class="form-control-form input-antecedente-small" name="obstetricaE" id="obstetricaE" value="{{ $historia['antecedentes']->ginecoobstetricos_E}}">
                                <label for="ciclos" class="width-lbl-antecedentesF">Fecha ultima citología:</label>
                                <input type="text" class="form-control-form datepicker width-lbl-antecedentesF" name="fechaUC" id="fechaUC" autocomplete="off" style="padding: 3px" value="{{ isset($historia['antecedentes']->ginecoobstetricos_fechaUltimaCitologia) ? $historia['antecedentes']->ginecoobstetricos_fechaUltimaCitologia:""}}">
                                <label for="menarquia" class="width-lbl-antecedentesF">Resultado:</label>
                                <input type="text" class="form-control-form width-lbl-antecedentesF" name="resultado" id="resultado" value="{{ $historia['antecedentes']->ginecoobstetricos_resultado}}">
                                <label for="menarquia" class="width-lbl-antecedentesF">Método de planificación:</label>
                                <input type="text" class="form-control-form width-lbl-antecedentesF" name="metodoP" id="metodoP" value="{{ $historia['antecedentes']->ginecoobstetricos_metodoPlanificacion}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="revisionPorSistemas" role="tabpanel" aria-labelledby="revisionPorSistemas-tab">
                    <h5 class="title-sections" id="sub-title">REVISIÓN POR SISTEMAS</h5>
                    <div class="border-container">
                        <div class="row" style="padding-bottom: 1%">
                            <div class="col-md-1">
                                <label for="neurologico" class="font-bold">Neurológico:</label>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="neuro[]" value="Cefalea" {{ strpos($historia['revisionporsistema']->neurologico, 'Cefalea') !== false || old('neuro[]') == 'Cefalea'  ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Cefalea</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. memoria" {{ strpos($historia['revisionporsistema']->neurologico, 'Alt. memoria') !== false || old('neuro[]') == 'Alt. memoria'  ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Alt. memoria</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. Sensibilidad" {{ strpos($historia['revisionporsistema']->neurologico, 'Alt. Sensibilidad') !== false || old('neuro[]') == 'Alt. Sensibilidad'  ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Alt. Sensibilidad</label>
                                </div>
                            </div>
                            <div class="col-md-1" style="padding-top: 1px; padding-left: 0; padding-right: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. motora" {{ strpos($historia['revisionporsistema']->neurologico, 'Alt. motora') !== false || old('neuro[]') == 'Alt. motora'  ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Alt. motora</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="neuro[]" value="Alt. sueño" {{ strpos($historia['revisionporsistema']->neurologico, 'Alt. sueño') !== false || old('neuro[]') == 'Alt. sueño'  ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Alt. sueño</label>
                                </div>
                            </div>
                            <span style="padding-right: 2%"></span>
                            <div class="col-md-1" style="max-width: 9%">
                                <label for="fisicos" class="font-bold">Cardiovascular:</label>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="cardio[]" value="Dolor precordial" {{ strpos($historia['revisionporsistema']->cardiovascular, 'Dolor precordial') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dolor precordial</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="cardio[]" value="Palpitaciones" {{ strpos($historia['revisionporsistema']->cardiovascular, 'Palpitaciones') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Palpitaciones</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="cardio[]" value="Lipotomia" {{ strpos($historia['revisionporsistema']->cardiovascular, 'Lipotomia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Lipotomia</label>
                                </div>
                            </div>
                            <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="cardio[]" value="Sincope" {{ strpos($historia['revisionporsistema']->cardiovascular, 'Sincope') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Sincope</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="cardio[]" value="Disnea" {{ strpos($historia['revisionporsistema']->cardiovascular, 'Disnea') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Disnea</label>
                                </div>
                            </div>

                            <div class="col-md-1" style="max-width: 9%">
                                <label for="fisicos" class="font-bold">Genitourinario:</label>
                            </div>
                            <div class="col-md-2 width-col-geni">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="geni[]" value="Hematuria" {{ strpos($historia['revisionporsistema']->genitourinario, 'Hematuria') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Hematuria</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="geni[]" value="Poliaquiuria" {{ strpos($historia['revisionporsistema']->genitourinario, 'Poliaquiuria') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Poliaquiuria</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="geni[]" value="Alt. ciclo menstrual" {{ strpos($historia['revisionporsistema']->genitourinario, 'Alt. ciclo menstrual') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Alt. ciclo menstrual</label>
                                </div>
                            </div>
                            <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="geni[]" value="Nicturia" {{ strpos($historia['revisionporsistema']->genitourinario, 'Nicturia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Nicturia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="geni[]" value="Disuria" {{ strpos($historia['revisionporsistema']->genitourinario, 'Disuria') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Disuria</label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <label for="neurologico" class="font-bold">Digestivo:</label>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="digestivo[]" value="Dolor abdomen" {{ strpos($historia['revisionporsistema']->digestivo, 'Dolor abdomen') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dolor abdomen</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="digestivo[]" value="Epigastralgia" {{ strpos($historia['revisionporsistema']->digestivo, 'Epigastralgia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Epigastralgia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="digestivo[]" value="Estreñimiento" {{ strpos($historia['revisionporsistema']->digestivo, 'Estreñimiento') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Estreñimiento</label>
                                </div>
                            </div>
                            <div class="col-md-1" style="padding-top: 1px; padding-left: 0; padding-right: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="digestivo[]" value="Dispepsia" {{ strpos($historia['revisionporsistema']->digestivo, 'Dispepsia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dispepsia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="digestivo[]" value="Diarrea" {{ strpos($historia['revisionporsistema']->digestivo, 'Diarrea') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Diarrea</label>
                                </div>
                            </div>
                            <span style="padding-right: 2%"></span>
                            <div class="col-md-1" style="max-width: 9%">
                                <label for="fisicos" class="font-bold">Dermatológico:</label>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="derma[]" value="Descamación" {{ strpos($historia['revisionporsistema']->dermatologico, 'Descamación') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Descamación</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="derma[]" value="Hiperhidrosis" {{ strpos($historia['revisionporsistema']->dermatologico, 'Hiperhidrosis') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Hiperhidrosis</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="derma[]" value="Sequedad" {{ strpos($historia['revisionporsistema']->dermatologico, 'Sequedad') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Sequedad</label>
                                </div>
                            </div>
                            <div class="col-md-1" style="padding-top: 1px; padding-left: 0">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="derma[]" value="Eritema" {{ strpos($historia['revisionporsistema']->dermatologico, 'Eritema') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Eritema</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="derma[]" value="Prurito" {{ strpos($historia['revisionporsistema']->dermatologico, 'Prurito') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Prurito</label>
                                </div>
                            </div>

                            <div class="col-md-1" style="max-width: 9%">
                                <label for="fisicos" class="font-bold">Hermatológico:</label>
                                <label for="fisicos" class="font-bold">Respiratorio:</label>
                                <label for="fisicos" class="font-bold">Psiquiátrico:</label>
                            </div>
                            <div class="col-md-2 width-col-geni">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="hermatologico" value="Sangrado" {{ strpos($historia['revisionporsistema']->hematologico, 'Sangrado') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Sangrado</label>
                                </div>
                                <div class="form-check form-check-inline" style="padding-top: 4%">
                                    <input class="form-check-input" type="checkbox" name="respiratorio" value="Tos frecuente" {{ strpos($historia['revisionporsistema']->respiratorio, 'Tos frecuente') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Tos frecuente</label>
                                </div>
                                <div class="form-check form-check-inline" style="padding-top: 4%">
                                    <input class="form-check-input" type="checkbox" name="psiquiatrico" value="Depresión" {{ strpos($historia['revisionporsistema']->psiquiatrico, 'Depresión') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Depresión</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="border-container" style="margin-top: 2%">
                        <div class="row">
                            <div class="col-md-1" style="max-width: 9%">
                                <label for="neurologico" class="font-bold">Osteomuscular:</label>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Cervicalgia" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Cervicalgia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Cervicalgia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dorsalgia" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Dorsalgia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dorsalgia</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Lumbagia" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Lumbagia') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Lumbagia</label>
                                </div>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dolor matutino" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Dolor matutino') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dolor matutino</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Dolor articular" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Dolor articular') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Dolor articular</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Alt. marcha" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Alt. marcha') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Alt. marcha</label>
                                </div>
                            </div>
                            <div class="col-md-2 width-col-rs" style="max-width: 14%">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Parestesias" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Parestesias') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Parestesias</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Inflama. articular" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Inflama. articular') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Inflama. articular</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Tendinitis" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Tendinitis') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Tendinitis</label>
                                </div>
                            </div>
                            <div class="col-md-2 width-col-rs" style="max-width: 14%">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Sd túnel del carpo" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Sd túnel del carpo') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Sd túnel del carpo</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Esguinces" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Esguinces') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Esguinces</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Luxación" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Luxación') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Luxación</label>
                                </div>
                            </div>
                            <div class="col-md-2 width-col-rs">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Neuropatía" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Neuropatía') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Neuropatía</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Ciática" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Ciática') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox2">Ciática</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="osteomuscular[]" value="Otro" {{ strpos($historia['revisionporsistema']->osteomuscular, 'Otro') !== false ? "checked":"" }}>
                                    <label class="form-check-label" for="inlineCheckbox1">Otro</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="border-container" style="margin-top: 2%">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="jornada" class="font-bold">Observaciones:</label>
                                <textarea class="form-control" id="observacionesRS" rows="3">{{ $historia['revisionporsistema']->observaciones }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="examenFisico" role="tabpanel" aria-labelledby="examenFisico-tab">
                    <div class="bd-example">
                        <div id="carouselExampleCaptions" class="carousel slide carousel-ef" data-ride="carousel" data-interval="false">
                            <ol class="carousel-indicators" style="bottom: -42px">
                                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="row" style="padding-top: 2%">
                                        <div class="col-md-12">
                                            <div class="border-container">
                                                <div class="row" id="lbl-examenF1">
                                                    <div class="col-md-4">
                                                        <label for="">Estado general:</label>
                                                        <div class="form-group div-select-form">
                                                            <select class="select-form form-control" name="estadoG" id="estadoG" style="font-size: 0.8rem">
                                                                <option value=""></option>
                                                                <option value="Bueno" {{ $historia['examenfisico']->estadogeneral == 'Bueno' ? "selected":"" }}>Bueno</option>
                                                                <option value="Regular" {{ $historia['examenfisico']->estadogeneral == 'Regular' ? "selected":"" }}>Regular</option>
                                                                <option value="Malo" {{ $historia['examenfisico']->estadogeneral == 'Malo' ? "selected":"" }}>Malo</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Dominancia:</label>
                                                        <div class="form-group div-select-form">
                                                            <select class="select-form form-control" name="dominancia" id="dominancia" style="font-size: 0.8rem">
                                                                <option value=""></option>
                                                                <option value="Zurdo" {{ $historia['examenfisico']->dominancia == 'Zurdo' ? "selected":"" }}>Zurdo</option>
                                                                <option value="Diestro" {{ $historia['examenfisico']->dominancia == 'Diestro' ? "selected":"" }}>Diestro</option>
                                                                <option value="Ambidiestro" {{ $historia['examenfisico']->dominancia == 'Ambidiestro' ? "selected":"" }}>Ambidiestro</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Tensión arterial:</label>
                                                        <input type="text" class="form-control-form" name="tenArt1" id="tenArt1" style="width: 12%" value="{{ $historia['examenfisico']->tas }}">
                                                        <sapan>/</sapan>
                                                        <input type="text" class="form-control-form" name="tenArt2" id="tenArt2" style="width: 12%" value="{{ $historia['examenfisico']->tad }}">
                                                        <label style="width: 5%">(mm/hg)</label>
                                                    </div>
                                                </div>

                                                <div class="row" id="lbl-examenF1">
                                                    <div class="col-md-4">
                                                        <label for="">Frecuencia cardiaca:</label>
                                                        <input type="text" class="form-control-form" name="fc" id="fc" value="{{ $historia['examenfisico']->fc }}">
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Frecuencia R.:</label>
                                                        <input type="text" class="form-control-form" name="fr" id="fr" value="{{ $historia['examenfisico']->fr }}">
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Temperatura:</label>
                                                        <input type="text" class="form-control-form" name="t" id="t" value="{{ $historia['examenfisico']->t }}">
                                                    </div>
                                                </div>

                                                <div class="row" id="lbl-examenF1">
                                                    <div class="col-md-4">
                                                        <label for="">Perímetro abdominal:</label>
                                                        <input type="text" class="form-control-form" name="pa" id="pa" value="{{ $historia['examenfisico']->perimetroabdominal }}">
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Talla (cm):</label>
                                                        <input type="text" class="form-control-form" name="talla" id="talla" value="{{ $historia['examenfisico']->talla }}">
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label for="">Peso (Kg):</label>
                                                        <input type="text" class="form-control-form" name="peso" id="peso" value="{{ $historia['examenfisico']->peso }}">
                                                    </div>
                                                </div>

                                                <div class="row" id="lbl-examenF1">
                                                    <div class="col-md-4">
                                                        <label for="">IMC:</label>
                                                        <input type="text" class="form-control-form" name="imc" id="imc" value="{{ $historia['examenfisico']->imc }}" disabled>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <label for="lbl-info" class="lbl-info" style="width: 100%; padding-top: 1%"><b>Interpretación: Bajo peso: </b>< 18,5 - <b>Normal: </b>18,5 a 24,9 - <b>Sobrepeso: </b>25 a 29,9 - <b>Obesidad: </b>> 30 </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 1%">
                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Cabeza / Cuello:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="cabezaCuello" id="cabezaCuello" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->cabezacuello == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->cabezacuello == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->cabezacuello == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->cabezacuello == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->cabezacuello == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="cabezaCDescripcion" id="cabezaCDescripcion" placeholder="Descripción" style="width: 97%" value="{{ $historia['examenfisico']->cabezacuello_obs }}">
                                            </div>
                                        </div>

                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Oido:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="oidoEF" id="oidoEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->oido == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->oido == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->oido == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->oido == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->oido == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="oidoDescriocion" id="oidoDescriocion" placeholder="Descripción" style="width: 97%" value="{{ $historia['examenfisico']->oido_obs }}">
                                            </div>
                                        </div>

                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Piel:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="pielEF" id="pielEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->piel == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->piel == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->piel == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->piel == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->piel == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="pielDescripcion" id="pielDescripcion" placeholder="Descripción" style="width: 97%" value="{{ $historia['examenfisico']->piel_obs }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 1%">
                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Ojos:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="ojosEF" id="ojosEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->ojos == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->ojos == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->ojos == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->ojos == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->ojos == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="ojosDescripcion" id="ojosDescripcion" placeholder="Descripción" style="width: 97%;margin-bottom: 2%" value="{{ $historia['examenfisico']->ojos_obs }}">
                                                <label for="" style="font-family: montserrat-bold">Fondo de ojos</label><br>
                                                <label for="">Ojo derecho:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="ojoDerecho" id="ojoDerecho" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->ojos_ojoderecho == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->ojos_ojoderecho == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->ojos_ojoderecho == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->ojos_ojoderecho == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->ojos_ojoderecho == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <label for="">Ojo izquierdo:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="ojoIzquierdo" id="ojoIzquierdo" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->ojos_ojoizquierdo == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->ojos_ojoizquierdo == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->ojos_ojoizquierdo == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->ojos_ojoizquierdo == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->ojos_ojoizquierdo == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Abdomen:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="abdomenEF" id="abdomenEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->abdomen == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->abdomen == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->abdomen == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->abdomen == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->abdomen == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="abdomenDescriocion" id="abdomenDescriocion" placeholder="Descripción" style="width: 97%; margin-bottom: 1.3%" value="{{ $historia['examenfisico']->abdomen_obs }}">
                                                <label for="">Hernias:</label>
                                                <input type="text" class="form-control-form" name="herniaDescripcion" id="herniaDescripcion" value="{{ $historia['examenfisico']->abdomen_hernias }}">
                                            </div>
                                        </div>

                                        <div class="col-md-4 container-small-ef" id="lbl-examenF2">
                                            <div class="border-container">
                                                <label for="">Torax:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="toraxEF" id="toraxEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->torax == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->torax == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->torax == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->torax == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->torax == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <label for="">RsCs:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="RsCs" id="RsCs" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Rítmicos" {{ $historia['examenfisico']->torax_rscs == 'Rítmicos' ? "selected":"" }}>Rítmicos</option>
                                                        <option value="Arrítmicos" {{ $historia['examenfisico']->torax_rscs == 'Arrítmicos' ? "selected":"" }}>Arrítmicos</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <input type="text" class="form-control-form" name="rscslDescripcion" id="rscslDescripcion" placeholder="Descripción" style="width: 97%; margin-bottom: 1%" value="{{ $historia['examenfisico']->torax_rscs_obs }}">
                                                <label for="">RsRs:</label>
                                                <input type="text" class="form-control-form" name="rsrs" id="rsrs" value="{{ $historia['examenfisico']->torax_rsrs }}">
                                                <label for="">Senos:</label>
                                                <input type="text" class="form-control-form" name="senosEF" id="senosEF" value="{{ $historia['examenfisico']->torax_senos }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 1%">
                                        <div class="col-md-6 container-small-ef" id="lbl-examenF3">
                                            <div class="border-container">
                                                <label for="">Extremidad superior:</label>
                                                <div class="form-group div-select-form" style="width: 30%">
                                                    <select class="select-form form-control" name="extSup" id="extSup" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->extremidadsuperior == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->extremidadsuperior == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->extremidadsuperior == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->extremidadsuperior == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->extremidadsuperior == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input type="text" class="form-control-form" name="extSupDescripcion" id="extSupDescripcion" placeholder="Descripción" style="width: 42%" value="{{ $historia['examenfisico']->extremidadsuperior_descripcion }}">
                                                <label for="">Derecho tinel:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="derechoTin" id="derechoTin" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_derechotinel == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_derechotinel == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Izquierdo tinel:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="izquierdoTin" id="izquierdoTin" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_izquierdotinel == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_izquierdotinel == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                <label for="">Derecho phanel:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="derechoPhan" id="derechoPhan" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_derechophanel == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_derechophanel == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Izquierdo phanel:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="izquierdoPhan" id="izquierdoPhan" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_izquierdophanel == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_izquierdophanel == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                <label for="">Derecho finkelstein:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="derechoFink" id="derechoFink" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_derechofinkelstein == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_derechofinkelstein == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Izquierdo finkelstein:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="izquierdoFink" id="izquierdoFink" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->extremidadsuperior_izquierdofinkelstein == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->extremidadsuperior_izquierdofinkelstein == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 container-small-ef" id="lbl-examenF3">
                                            <div class="border-container">
                                                <label for="">Neurológico:</label>
                                                <div class="form-group div-select-form" style="width: 30%">
                                                    <select class="select-form form-control" name="neuroEF" id="neuroEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->neurologico == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->neurologico == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->neurologico == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->neurologico == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->neurologico == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input type="text" class="form-control-form" name="neuroDescripcion" id="neuroDescripcion" placeholder="Descripción" style="width: 42%" value="{{ $historia['examenfisico']->neurologico_obs }}">
                                                <label for="">Pares craneanos:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="paresCraneo" id="paresCraneo" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->neurologico_parescraneanos == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->neurologico_parescraneanos == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->neurologico_parescraneanos == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Reflejos:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="reflejosEF" id="reflejosEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->neurologico_reflejos == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->neurologico_reflejos == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->neurologico_reflejos == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                    </select>
                                                </div>
                                                <label for="">Sensibilidad:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="sensibilidadEF" id="sensibilidadEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->neurologico_sensibilidad == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->neurologico_sensibilidad == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->neurologico_sensibilidad == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Coord. romber:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="coorRomber" id="coorRomber" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->neurologico_coordinacionromber == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->neurologico_coordinacionromber == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                <label for="">Marcha en linea:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="marchaLinea" id="marchaLinea" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Positivo" {{ $historia['examenfisico']->neurologico_marchaenlinea == 'Positivo' ? "selected":"" }}>Positivo</option>
                                                        <option value="Negativo" {{ $historia['examenfisico']->neurologico_marchaenlinea == 'Negativo' ? "selected":"" }}>Negativo</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="">Esfera mental:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="esferaMental" id="esferaMental" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->neurologico_esferamental == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->neurologico_esferamental == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->neurologico_esferamental == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 1%">

                                        <div class="col-md-6 offset-3 container-small-ef" id="lbl-examenF3">
                                            <div class="border-container">
                                                <label for="">Extremidad inferior:</label>
                                                <div class="form-group div-select-form" style="width: 30%">
                                                    <select class="select-form form-control" name="extInferior" id="extInferior" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->extremidadinferior == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->extremidadinferior == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->extremidadinferior == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->extremidadinferior == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->extremidadinferior == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input type="text" class="form-control-form" name="extInferiorDescripcion" id="extInferiorDescripcion" placeholder="Descripción" style="width: 42%" value="{{ $historia['examenfisico']->extremidadinferior_obs }}">
                                                <label for="">Varices:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="varicesEF" id="varicesEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Si" {{ $historia['examenfisico']->extremidadinferior_varices == 'Si' ? "selected":"" }}>Si</option>
                                                        <option value="No" {{ $historia['examenfisico']->extremidadinferior_varices == 'No' ? "selected":"" }}>No</option>
                                                    </select>
                                                </div>
                                                <input type="text" class="form-control-form" name="varicesDescripcion" id="varicesDescripcion" placeholder="Descripción" style="width: 51%" value="{{ $historia['examenfisico']->extremidadinferior_varices_obs }}">

                                                <label for="">Juanetes:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="juanetesEF" id="juanetesEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Si" {{ $historia['examenfisico']->extremidadinferior_juanetes == 'Si' ? "selected":"" }}>Si</option>
                                                        <option value="No" {{ $historia['examenfisico']->extremidadinferior_juanetes == 'No' ? "selected":"" }}>No</option>
                                                    </select>
                                                </div>
                                                <input type="text" class="form-control-form" name="juanetesDescripcion" id="juanetesDescripcion" placeholder="Descripción" style="width: 51%" value="{{ $historia['examenfisico']->extremidadinferior_juanetes_obs }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="row" style="padding-top: 2%">
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">INSPECCIÓN</h5>
                                            <div class="border-container">
                                                <label for="simetria" style="width: 15%">Simetría:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="simetriaI" id="simetriaI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->columna_inspeccion_simetria == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_inspeccion_simetria == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_inspeccion_simetria == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_inspeccion_simetria == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_inspeccion_simetria == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionSimetria" id="descripcionSimetria" class="form-control-form width-input-antecedentesP" placeholder="Descripción" value="{{ $historia['examenfisico']->columna_inspeccion_simetria_obs }}"><br>
                                                <label for="curcatura" style="width: 15%">Curvatura:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="curvaturaP" id="curvaturaP" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_inspeccion_curvatura == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_inspeccion_curvatura == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_inspeccion_curvatura == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_inspeccion_curvatura == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_inspeccion_curvatura == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionCurvatura" id="descripcionCurvatura" class="form-control-form width-input-antecedentesP" placeholder="Descripción" value="{{ $historia['examenfisico']->columna_inspeccion_curvatura_obs }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">PALPACIÓN</h5>
                                            <div class="border-container">
                                                <label for="dolor" style="width: 15%">Dolor:</label>
                                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                                    <input type="radio" class="custom-control-input" id="siDolor" name="dolorEF" value="Si" {{ $historia['examenfisico']->columna_palpacion_dolor == 'Si' ? "checked":"" }}>
                                                    <label class="custom-control-label" for="siDolor">Si</label>
                                                </div>

                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="noDolor" name="dolorEF" value="No" {{ $historia['examenfisico']->columna_palpacion_dolor == 'No' ? "checked":"" }}>
                                                    <label class="custom-control-label" for="noDolor">No</label>
                                                </div>
                                                <input name="descripcionDolor" id="descripcionDolor" class="form-control-form" placeholder="Descripción" style="width: 66%" value="{{ $historia['examenfisico']->columna_palpacion_dolor_obs }}">

                                                <label for="simetria" style="width: 15%">Espasmo:</label>
                                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                                    <input type="radio" class="custom-control-input" id="siEspasmo" name="espasmoEF" value="Si" {{ $historia['examenfisico']->columna_palpacion_espasmo == 'Si' ? "checked":"" }}>
                                                    <label class="custom-control-label" for="siEspasmo">Si</label>
                                                </div>

                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="noEspasmo" name="espasmoEF" value="No" {{ $historia['examenfisico']->columna_palpacion_espasmo == 'No' ? "checked":"" }}>
                                                    <label class="custom-control-label" for="noEspasmo">No</label>
                                                </div>
                                                <input name="descripcionEspasmo" id="descripcionEspasmo" class="form-control-form" placeholder="Descripción" style="width: 66%" value="{{ $historia['examenfisico']->columna_palpacion_espasmo_obs }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="title-sections" id="sub-title">MOVILIDAD</h5>
                                            <div class="border-container">
                                                <label for="simetria" style="width: 7%">Flexión:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="flexionM" id="flexionM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_movilidad_flexion == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_movilidad_flexion == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_movilidad_flexion == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_movilidad_flexion == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_movilidad_flexion == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionFlexion" id="descripcionFlexion" class="form-control-form" placeholder="Descripción" style="width: 27%" value="{{ $historia['examenfisico']->columna_movilidad_flexion_obs }}">
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="curcatura" style="width: 7%">Extensión:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="extensionM" id="extensionM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_movilidad_extension == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_movilidad_extension == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_movilidad_extension == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_movilidad_extension == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_movilidad_extension == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionExtension" id="descripcionExtension" class="form-control-form" placeholder="Descripción" style="width: 27%" value="{{ $historia['examenfisico']->columna_movilidad_extension_obs }}"><br>

                                                <label for="simetria" style="width: 7%">Flexión lat.:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="flexionLM" id="flexionLM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal" {{ $historia['examenfisico']->columna_movilidad_flexionlateral == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_movilidad_flexionlateral == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_movilidad_flexionlateral == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_movilidad_flexionlateral == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_movilidad_flexionlateral == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionFlexionL" id="descripcionFlexionL" class="form-control-form" placeholder="Descripción" style="width: 27%" value="{{ $historia['examenfisico']->columna_movilidad_flexionlateral_obs }}">
                                                {{--Aqui voy--}}
                                                &nbsp&nbsp&nbsp&nbsp
                                                <label for="curcatura" style="width: 7%">Rotación:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="rotacionM" id="rotacionM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_movilidad_rotacion == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_movilidad_rotacion == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_movilidad_rotacion == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_movilidad_rotacion == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_movilidad_rotacion == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionRotacion" id="descripcionRotacion" class="form-control-form" placeholder="Descripción" style="width: 27%" value="{{ $historia['examenfisico']->columna_movilidad_rotacion_obs }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">MARCHA</h5>
                                            <div class="border-container">
                                                <label for="puntas" style="width: 15%">Puntas:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="puntasM" id="puntasM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_marcha_puntas == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_marcha_puntas == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_marcha_puntas == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_marcha_puntas == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_marcha_puntas == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionPuntas" id="descripcionPuntas" class="form-control-form width-input-antecedentesP" value="{{ $historia['examenfisico']->columna_marcha_puntas_obs }}"><br>

                                                <label for="talones" style="width: 15%">Talones:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="talonesM" id="talonesM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_marcha_talones == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_marcha_talones == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_marcha_talones == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_marcha_talones == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_marcha_talones == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionTalones" id="descripcionTalones" class="form-control-form width-input-antecedentesP" placeholder="Descripción" value="{{ $historia['examenfisico']->columna_marcha_talones_obs }}"><br>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">TEST</h5>
                                            <div class="border-container">
                                                <label for="Wells" style="width: 15%">Wells:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="WellsM" id="WellsM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_test_wells == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_test_wells == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_test_wells == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_test_wells == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_test_wells == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionWells" id="descripcionWells" class="form-control-form width-input-antecedentesP" placeholder="Descripción" value="{{ $historia['examenfisico']->columna_test_wells_obs }}"><br>

                                                <label for="shober" style="width: 15%">Shober:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="ShoberM" id="ShoberM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_test_shober == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna_test_shober == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_test_shober == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_test_shober == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_test_shober == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="descripcionShober" id="descripcionShober" class="form-control-form width-input-antecedentesP" placeholder="Descripción" value="{{ $historia['examenfisico']->columna_test_shober_obs }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">TROFISMO MUSCULAR</h5>
                                            <div class="border-container">
                                                <label for="trofismo" style="width: 15%">Trofismo:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="trofismoM" id="trofismoM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna_trofismomuscular_trofismo == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal"  {{ $historia['examenfisico']->columna_trofismomuscular_trofismo == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna_trofismomuscular_trofismo == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna_trofismomuscular_trofismo == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna_trofismomuscular_trofismo == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <label for="trofismo" style="width: 15%">Descripción:</label>
                                                <input name="descripcionTrofismo" id="descripcionTrofismo" class="form-control-form" placeholder="Descripción" style="width: 84%" value="{{ $historia['examenfisico']->columna_trofismomuscular_trofismo_obs }}"><br>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">DIAGNOSTICO COLUMNA</h5>
                                            <div class="border-container">
                                                <label for="columna" style="width: 15%">Columna:</label>
                                                <div class="form-group div-select-form" style="width: 28%">
                                                    <select class="select-form form-control" name="columnaEF" id="columnaEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->columna == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->columna == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->columna == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->columna == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->columna == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <label for="trofismo" style="width: 15%">Descripción:</label>
                                                <input name="descripcionColumna" id="descripcionColumna" class="form-control-form" placeholder="Descripción" style="width: 84%" value="{{ $historia['examenfisico']->columna_descripcion }}"><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="title-sections" id="sub-title">BALANCE MUSCULAR</h5>
                                            <div class="border-container" id="lbl-balanceM">
                                                <label for="cintura escapular">Cintura escapular:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="cinturaEscapular" id="cinturaEscapular" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_cinturaescapular == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_cinturaescapular == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_cinturaescapular == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_cinturaescapular == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_cinturaescapular == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesCinEs" id="observacionesCinEs" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_cinturaescapular_obs }}">
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <label for="pectoral">Pectoral:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="pectoralEF" id="pectoralEF" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_pectoral == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_pectoral == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_pectoral == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_pectoral == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_pectoral == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesPect" id="observacionesPect" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_pectoral_obs }}"><br>

                                                <label for="simetria">Brazo:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="brazoBM" id="brazoBM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_brazo == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_brazo == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_brazo == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_brazo == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_brazo == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesBrazo" id="observacionesBrazo" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_brazo_obs }}">
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <label for="antebrazo">Antebrazo:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="antebrazoBM" id="antebrazoBM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_antebrazo == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_antebrazo == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_antebrazo == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_antebrazo == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_antebrazo == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesAnteb" id="observacionesAnteb" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_antebrazo_obs }}">
                                                <label for="pectoral">Cadera-Gluteo:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="caderaGluteo" id="caderaGluteo" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_caderagluteo == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_caderagluteo == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_caderagluteo == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_caderagluteo == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_caderagluteo == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesCadGlu" id="observacionesCadGlu" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_caderagluteo_obs }}">
                                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                <label for="muslos">Muslos:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="muslosBM" id="muslosBM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_muslos == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_muslos == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_muslos == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_muslos == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_muslos == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesMuslos" id="observacionesMuslos" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_muslos_obs }}">
                                                <label for="piernas">Piernas:</label>
                                                <div class="form-group div-select-form" style="width: 14%">
                                                    <select class="select-form form-control" name="piernasBM" id="piernasBM" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="Normal"  {{ $historia['examenfisico']->balancemuscular_piernas == 'Normal' ? "selected":"" }}>Normal</option>
                                                        <option value="Anormal" {{ $historia['examenfisico']->balancemuscular_piernas == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                        <option value="No examinado" {{ $historia['examenfisico']->balancemuscular_piernas == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                        <option value="No solicitado" {{ $historia['examenfisico']->balancemuscular_piernas == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                        <option value="Pendiente resultado" {{ $historia['examenfisico']->balancemuscular_piernas == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                                    </select>
                                                </div>
                                                <input name="observacionesPiernas" id="observacionesPiernas" class="form-control-form" placeholder="Observaciones" value="{{ $historia['examenfisico']->balancemuscular_piernas_obs }}"><br>
                                                <label for="explicacion">Explicación:</label>
                                                <textarea name="explicacion" id="explicacionBM" rows="4" style="width: 100%">{{ $historia['examenfisico']->balancemuscular_explicacion }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">REFLEJOS OSTEOTENDINOSOS</h5>
                                            <div class="border-container" id="reflejos-osteo">
                                                <label for=""></label>
                                                <label for="" style="font-family: montserrat-bold; width: 25%">Derecho</label>
                                                <label for="" style="font-family: montserrat-bold">Izquierdo</label><br>
                                                <label for="bicipital">Bicipital:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="bicipitalD" id="bicipitalD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_derecho == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_derecho == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_derecho == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_derecho == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                &nbsp&nbsp
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="bicipitalI" id="bicipitalI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_izquierdo == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_izquierdo == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_izquierdo == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_bicipital_izquierdo == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                <br>
                                                <label for="">Radial:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="radialD" id="radialD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_radial_derecho == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_radial_derecho == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_radial_derecho == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_radial_derecho == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                &nbsp&nbsp
                                                <div class="form-group div-select-form" >
                                                    <select class="select-form form-control" name="radialI" id="radialI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_radial_izquierdo == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_radial_izquierdo == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_radial_izquierdo == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_radial_izquierdo == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                <br>
                                                <label for="">Rotuliano:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="rotulianoD" id="rotulianoD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_derecho == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_derecho == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_derecho == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_derecho == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                &nbsp&nbsp
                                                <div class="form-group div-select-form" >
                                                    <select class="select-form form-control" name="rotulianoI" id="rotulianoI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_izquierdo == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_izquierdo == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_izquierdo == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_rotuliano_izquierdo == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                <br>
                                                <label for="">Aquiliano:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="aquilianoD" id="aquilianoD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_derecho == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_derecho == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_derecho == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_derecho == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                &nbsp&nbsp
                                                <div class="form-group div-select-form" >
                                                    <select class="select-form form-control" name="aquilianoI" id="aquilianoI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="I" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_izquierdo == 'I' ? "selected":"" }}>I</option>
                                                        <option value="II"  {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_izquierdo == 'II' ? "selected":"" }}>II</option>
                                                        <option value="III" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_izquierdo == 'III' ? "selected":"" }}>III</option>
                                                        <option value="IIII" {{ $historia['examenfisico']->reflejososteotendinosos_alquiliano_izquierdo == 'IIII' ? "selected":"" }}>IIII</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn>/++++</sapn>
                                                <br>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="title-sections" id="sub-title">FUERZA</h5>
                                            <div class="border-container" id="reflejos-osteo">
                                                <label for="" style="font-family: montserrat-bold; width: 40%">Miembros superioriores</label>
                                                <label for="" style="font-family: montserrat-bold;width: 25%">Miembros inferiores</label><br>
                                                <label for="derecha">Derecha:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="miembroD" id="miembroD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="1" {{ $historia['examenfisico']->fuerza_miembrosuperior_derecho == '1' ? "selected":"" }}>1</option>
                                                        <option value="2" {{ $historia['examenfisico']->fuerza_miembrosuperior_derecho == '2' ? "selected":"" }}>2</option>
                                                        <option value="3" {{ $historia['examenfisico']->fuerza_miembrosuperior_derecho == '3' ? "selected":"" }}>3</option>
                                                        <option value="4" {{ $historia['examenfisico']->fuerza_miembrosuperior_derecho == '4' ? "selected":"" }}>4</option>
                                                        <option value="5" {{ $historia['examenfisico']->fuerza_miembrosuperior_derecho == '5' ? "selected":"" }}>5</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <span style="font-family: montserrat-bold">/5</span>
                                                <label for="derecha">Derecha:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="miembroInfD" id="miembroInfD" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="1" {{ $historia['examenfisico']->fuerza_miembroinferior_derecho == '1' ? "selected":"" }}>1</option>
                                                        <option value="2" {{ $historia['examenfisico']->fuerza_miembroinferior_derecho == '2' ? "selected":"" }}>2</option>
                                                        <option value="3" {{ $historia['examenfisico']->fuerza_miembroinferior_derecho == '3' ? "selected":"" }}>3</option>
                                                        <option value="4" {{ $historia['examenfisico']->fuerza_miembroinferior_derecho == '4' ? "selected":"" }}>4</option>
                                                        <option value="5" {{ $historia['examenfisico']->fuerza_miembroinferior_derecho == '5' ? "selected":"" }}>5</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <sapn style="font-family: montserrat-bold">/5</sapn>
                                                <br>
                                                <label for="izquierda">Izquierda:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="miembroI" id="miembroI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="1" {{ $historia['examenfisico']->fuerza_miembrosuperior_izquierda == '1' ? "selected":"" }}>1</option>
                                                        <option value="2" {{ $historia['examenfisico']->fuerza_miembrosuperior_izquierda == '2' ? "selected":"" }}>2</option>
                                                        <option value="3" {{ $historia['examenfisico']->fuerza_miembrosuperior_izquierda == '3' ? "selected":"" }}>3</option>
                                                        <option value="4" {{ $historia['examenfisico']->fuerza_miembrosuperior_izquierda == '4' ? "selected":"" }}>4</option>
                                                        <option value="5" {{ $historia['examenfisico']->fuerza_miembrosuperior_izquierda == '5' ? "selected":"" }}>5</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <span style="font-family: montserrat-bold">/5</span>
                                                <label for="izquierda">Izquierda:</label>
                                                <div class="form-group div-select-form">
                                                    <select class="select-form form-control" name="miembroInfI" id="miembroInfI" style="font-size: 0.8rem">
                                                        <option value=""></option>
                                                        <option value="1" {{ $historia['examenfisico']->fuerza_miembroinferior_izquierda == '1' ? "selected":"" }}>1</option>
                                                        <option value="2" {{ $historia['examenfisico']->fuerza_miembroinferior_izquierda == '2' ? "selected":"" }}>2</option>
                                                        <option value="3" {{ $historia['examenfisico']->fuerza_miembroinferior_izquierda == '3' ? "selected":"" }}>3</option>
                                                        <option value="4" {{ $historia['examenfisico']->fuerza_miembroinferior_izquierda == '4' ? "selected":"" }}>4</option>
                                                        <option value="5" {{ $historia['examenfisico']->fuerza_miembroinferior_izquierda == '5' ? "selected":"" }}>5</option>
                                                    </select>
                                                </div>
                                                &nbsp&nbsp
                                                <spann style="font-family: montserrat-bold">/5</spann>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="manipulacionAlimentos" role="tabpanel" aria-labelledby="manipulacionAlimentos-tab">
                    <h5 class="title-sections" id="sub-title">MANIPULACIÓN DE ALIMENTOS / DERMATOLÓGICO</h5>
                    <div class="border-container">
                        <div class="row" style="padding-bottom: 1%">
                            <div id="divCheckMA" class="col-md-12">
                                <label for="solicitadoSiNo" style="width: 9%">¿Fue solicitado?:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siSolicitado" name="solicitadoSN" value="Si" {{ $historia['manipulacionalimentos']->solicitado == 'Si' ? "checked":""}}>
                                    <label class="custom-control-label" for="siSolicitado">Si</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noSolicitado" name="solicitadoSN" value="No" {{ $historia['manipulacionalimentos']->solicitado == 'No' ? "checked":""}}>
                                    <label class="custom-control-label" for="noSolicitado">No</label>
                                </div>
                            </div>
                        </div>

                        <div id="contenido-ma" {{ $historia['manipulacionalimentos']->solicitado == 'Si' ? "":"hidden=\"true\"" }}>
                            <h5 class="title-sections">RESPIRATORIO</h5>
                            <div class="border-container" style="padding: 2% 1% 1% 1%;">
                                <div class="row" style="padding-bottom: 1%">
                                    <div class="col-md-12">
                                        <label for="pregunta" style="padding-right: 1%">¿El paciente cumple con la definición de sintomático respiratorio? (Ha tenido tos y expectoración por mas de 15 días)</label>
                                        <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                            <input type="radio" class="custom-control-input" id="siPregunta" name="pregunta" value="Si" {{ $historia['manipulacionalimentos']->respiratorio_cumple == 'Si' ? "checked":""}}>
                                            <label class="custom-control-label" for="siPregunta">Si</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="noPregunta" name="pregunta" value="No" {{ $historia['manipulacionalimentos']->respiratorio_cumple == 'No' ? "checked":""}}>
                                            <label class="custom-control-label" for="noPregunta">No</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <label for="inspeccion" class="font-bold">Inspección:</label>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Normal" {{ strpos($historia['manipulacionalimentos']->respiratorio_inspeccion, 'Normal') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Normal</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Rinorrea" {{ strpos($historia['manipulacionalimentos']->respiratorio_inspeccion, 'Rinorrea') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Rinorrea</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Escurrimiento posterior" {{ strpos($historia['manipulacionalimentos']->respiratorio_inspeccion, 'Escurrimiento posterior') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Escurrimiento posterior</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="inspeccion[]" value="Mucosas congestivas" {{ strpos($historia['manipulacionalimentos']->respiratorio_inspeccion, 'Mucosas congestivas') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Mucosas congestivas</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inspeccion" class="font-bold">Auscultación:</label>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea name="" id="auscultacion" rows="4" class="form-control">{{ $historia['manipulacionalimentos']->respiratorio_ausculacion }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <h5 class="title-sections" ID="sub-title">DERMATOLÓGICO</h5>
                            <div class="border-container" style="padding: 2% 1% 1% 1%;">
                                <div class="row" style="padding-bottom: 1%">
                                    <div class="col-md-12">
                                        <label for="pregunta2" style="padding-right: 1%">¿El paciente ha presentado prurito, cambios en la piel o
                                            enrojecimiento en alguna zona del cuerpo? (principalmente en las manos) en los ultimos 6 meses?</label>
                                        <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                            <input type="radio" class="custom-control-input" id="siPregunta2" name="pregunta2" value="Si" {{ $historia['manipulacionalimentos']->dermatologico_prurito == 'Si' ? "checked":"" }}>
                                            <label class="custom-control-label" for="siPregunta2">Si</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="noPregunta2" name="pregunta2" value="No" {{ $historia['manipulacionalimentos']->dermatologico_prurito == 'No' ? "checked":"" }}>
                                            <label class="custom-control-label" for="noPregunta2">No</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 1%">
                                    <div class="col-md-12">
                                        <label for="pregunta3" style="padding-right: 1%">¿El paciente usa actualmente algún medicamento que deba
                                            aplicar sobre la piel?</label>
                                        <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                            <input type="radio" class="custom-control-input" id="siPregunta3" name="pregunta3" value="Si" {{ $historia['manipulacionalimentos']->dermatologico_medicamento == 'Si' ? "checked":"" }}>
                                            <label class="custom-control-label" for="siPregunta3">Si</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="noPregunta3" name="pregunta3" value="No" {{ $historia['manipulacionalimentos']->dermatologico_medicamento == 'No' ? "checked":"" }}>
                                            <label class="custom-control-label" for="noPregunta3">No</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <label for="lesiones" class="font-bold">Lesiones:</label>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Eritema" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Eritema') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Eritema</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Excoriación" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Excoriación') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Excoriación</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Fisura" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Fisura') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Fisura</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Ampolla/Vesícula" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Ampolla/Vesícula') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Ampolla/Vesícula</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Forúnculo" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Forúnculo') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Forúnculo</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Costra" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Costra') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox1">Costra</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Pústula" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Pústula') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Pústula</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Ulcera" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Ulcera') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Ulcera</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="lesionesMA[]" value="Descamación" {{ strpos($historia['manipulacionalimentos']->dermatologico_lesiones, 'Descamación') !== false ? "checked":"" }}>
                                            <label class="form-check-label" for="inlineCheckbox2">Descamación</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="cual" class="font-bold">¿Cual?:</label>
                                        <textarea class="form-control" id="cualMA" rows="3">{{ $historia['manipulacionalimentos']->dermatologico_cual }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="border-container" style="margin-top: 2%">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="observaciones" class="font-bold">Observaciones:</label>
                                        <textarea class="form-control" id="observacionesMA" rows="3">{{ $historia['manipulacionalimentos']->observacion }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="paraclinicos" role="tabpanel" aria-labelledby="paraclinicos-tab">
                    <h5 class="title-sections" id="sub-title">PARACLÍNICOS Y PRUEBAS COMPLEMENTARIAS</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;">
                        <table class="table table-bordered table-responsive-md" id="tablePARA" >
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 24%">Exámenes de laboratorio</th>
                                <th scope="col" style="width: 10%">Resultado</th>
                                <th scope="col" style="width: 28%">Observaciones</th>
                                <th scope="col" style="width: 4%">Eliminar</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPARA">
                            @foreach($historia['paraclinicos'] as $paraclinico)
                                <tr id="tr" class="paraclinicosTR">
                                    <td>
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control input-table" id="examenesLab" name="examenesLab" style="font-size: 0.8rem; height: 44px">
                                                <option value=""></option>
                                                @foreach($examenesLab as $examen)
                                                    <option value="{{ $examen->id }}" {{ $paraclinico->examenLaboratorio_id == $examen->id ? "selected":"" }}>{{ $examen->descripcion }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control input-table" id="diagnosticoPara" name="diagnoticoPara" style="font-size: 0.8rem; height: 44px">
                                                <option value=""></option>
                                                <option value="Normal" {{ $paraclinico->diagnostico == 'Normal' ? "selected":"" }}>Normal</option>
                                                <option value="Anormal" {{ $paraclinico->diagnostico == 'Anormal' ? "selected":"" }}>Anormal</option>
                                                <option value="No examinado" {{ $paraclinico->diagnostico == 'No examinado' ? "selected":"" }}>No examinado</option>
                                                <option value="No solicitado" {{ $paraclinico->diagnostico == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                                <option value="Pendiente resultado" {{ $paraclinico->diagnostico == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td><input type="text" class="input-table" name="observacionesPara" id="observacionesPara" value="{{ $paraclinico->observacion }}"></td>
                                    <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="eliminarPara"><img src="/img/delete.png" alt="delete" style="width: 30px"></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-6 offset-md-6" style="text-align: right">
                                <a href="" id="anadirPara">Añadir paraclínico</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="optometria" class="font-bold">Optometria:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="optometriaPara" name="optometriaPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->optometria == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Alterada" {{ $historia['paraclinicosdatos']->optometria == 'Alterada' ? "selected":"" }}>Alterada</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="alteracion corregida" class="font-bold">Alteración corregida:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siACo" name="alteracionSN" value="Si" {{ $historia['paraclinicosdatos']->optometria_alteracioncorregida == 'Si' ? "checked":"" }}>
                                        <label class="custom-control-label" for="siACo">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noACo" name="alteracionSN" value="No" {{ $historia['paraclinicosdatos']->optometria_alteracioncorregida == 'No' ? "checked":"" }}>
                                        <label class="custom-control-label" for="noACo">No</label>
                                    </div>
                                    <br>
                                    <label for="diagnostico" class="font-bold">Diagnostico:</label>
                                    <textarea name="diagnosticoOpto" id="diagnosticoOpto" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->optometria_alteracioncorregida_obs }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="visiometria" class="font-bold">Visiometria:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="visiometriaPara" name="visiometriaPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->visiometria == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Alterada" {{ $historia['paraclinicosdatos']->visiometria == 'Alterada' ? "selected":"" }}>Alterada</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="alteracion corregida" class="font-bold">Alteración corregida:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siACoV" name="alteracionVSN" value="Si" {{ $historia['paraclinicosdatos']->visiometria_alteracioncorregida == 'Si' ? "checked":"" }}>
                                        <label class="custom-control-label" for="siACoV">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noACoV" name="alteracionVSN" value="No" {{ $historia['paraclinicosdatos']->visiometria_alteracioncorregida == 'No' ? "checked":"" }}>
                                        <label class="custom-control-label" for="noACoV">No</label>
                                    </div>
                                    <br>
                                    <label for="diagnostico" class="font-bold">Diagnostico:</label>
                                    <textarea name="diagnosticoVisio" id="diagnosticoVisio" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->visiometria_alteracioncorregida_obs }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="audiometria" class="font-bold">Audiometria:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siAudio" name="audioSN" value="Si" {{ $historia['paraclinicosdatos']->audiometria == 'Si' ? "checked":"" }}>
                                        <label class="custom-control-label" for="siAudio">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noAudio" name="audioSN" value="No" {{ $historia['paraclinicosdatos']->audiometria == 'No' ? "checked":"" }}>
                                        <label class="custom-control-label" for="noAudio">No</label>
                                    </div>
                                    <br>
                                    <label for="resultado" class="font-bold">Resultado:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siResAud" name="resultadoAudioSN" value="Normal" {{ $historia['paraclinicosdatos']->audiometria_resultado == 'Normal' ? "checked":"" }}>
                                        <label class="custom-control-label" for="siResAud">Normal</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noResAud" name="resultadoAudioSN" value="Alterada" {{ $historia['paraclinicosdatos']->audiometria_resultado == 'Alterada' ? "checked":"" }}>
                                        <label class="custom-control-label" for="noResAud">Alterada</label>
                                    </div>
                                    <br>
                                    <label for="diagnostico" class="font-bold">Observaciones:</label>
                                    <textarea name="observacionesAudio" id="observacionesAudio" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->audiometria_obs }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="espirometria" class="font-bold">Espirometria:</label>
                                    <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                        <input type="radio" class="custom-control-input" id="siEspiro" name="espiroSN" value="Si" {{ $historia['paraclinicosdatos']->espirometria == 'Si' ? "checked":"" }}>
                                        <label class="custom-control-label" for="siEspiro">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="noEspiro" name="espiroSN" value="No" {{ $historia['paraclinicosdatos']->espirometria == 'No' ? "checked":"" }}>
                                        <label class="custom-control-label" for="noEspiro">No</label>
                                    </div>
                                    <br>
                                    <label for="resultado" class="font-bold">Resultado:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="espirometriaPara" name="espirometriaPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->espirometria_resultado == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Patrón restrictivo" {{ $historia['paraclinicosdatos']->espirometria_resultado == 'Patrón restrictivo' ? "selected":""}}>Patrón restrictivo</option>
                                            <option value="Patrón obstructivo" {{ $historia['paraclinicosdatos']->espirometria_resultado == 'Patrón obstructivo' ? "selected":""}}>Patrón obstructivo</option>
                                            <option value="Patrón mixto" {{ $historia['paraclinicosdatos']->espirometria_resultado == 'Patrón mixto' ? "selected":""}}>Patrón mixto</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="diagnostico" class="font-bold">Observaciones:</label>
                                    <textarea name="observacionesEspiro" id="observacionesEspiro" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->espirometria_obs }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="psicologico" class="font-bold">Psicológico:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="psicologicoPara" name="psicologicoPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->psicologico == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Anormal" {{ $historia['paraclinicosdatos']->psicologico == 'Anormal' ? "selected":"" }}>Anormal</option>
                                            <option value="No examinado" {{ $historia['paraclinicosdatos']->psicologico == 'No examinado' ? "selected":"" }}>No examinado</option>
                                            <option value="No solicitado" {{ $historia['paraclinicosdatos']->psicologico == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                            <option value="Pendiente resultado" {{ $historia['paraclinicosdatos']->psicologico == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="diagnostico" class="font-bold">Observaciones:</label>
                                    <textarea name="observacionesPsi" id="observacionesPsi" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->psicologico_obs }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="psicometricos" class="font-bold">Psicométricos:</label>
                                    <div class="form-group div-select-form input-container-small">
                                        <select class="select-form form-control" id="psicometricosPara" name="psicometricosPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->psicometrico == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Anormal" {{ $historia['paraclinicosdatos']->psicometrico == 'Anormal' ? "selected":"" }}>Anormal</option>
                                            <option value="No examinado" {{ $historia['paraclinicosdatos']->psicometrico == 'No examinado' ? "selected":"" }}>No examinado</option>
                                            <option value="No solicitado" {{ $historia['paraclinicosdatos']->psicometrico == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                            <option value="Pendiente resultado" {{ $historia['paraclinicosdatos']->psicometrico == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="observaciones" class="font-bold">Observaciones:</label>
                                    <textarea name="observacionesPsiMe" id="observacionesPsiMe" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->psicometrico_obs }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 container-medium">
                            <div class="border-container">
                                <div id="divAlcohol" class="col-md-12">
                                    <label for="prueba vestibular" class="font-bold" style="width: 34%">Prueba vestibular:</label>
                                    <div class="form-group div-select-form" style="width: 64%">
                                        <select class="select-form form-control" id="vestibularPara" name="vestibularPara" style="font-size: 0.8rem">
                                            <option value=""></option>
                                            <option value="Normal" {{ $historia['paraclinicosdatos']->pruebavestibular == 'Normal' ? "selected":"" }}>Normal</option>
                                            <option value="Anormal" {{ $historia['paraclinicosdatos']->pruebavestibular == 'Anormal' ? "selected":"" }}>Anormal</option>
                                            <option value="No examinado" {{ $historia['paraclinicosdatos']->pruebavestibular == 'No examinado' ? "selected":"" }}>No examinado</option>
                                            <option value="No solicitado" {{ $historia['paraclinicosdatos']->pruebavestibular == 'No solicitado' ? "selected":"" }}>No solicitado</option>
                                            <option value="Pendiente resultado" {{ $historia['paraclinicosdatos']->pruebavestibular == 'Pendiente resultado' ? "selected":"" }}>Pendiente resultado</option>
                                        </select>
                                    </div>
                                    <br>
                                    <label for="observaciones" class="font-bold">Observaciones:</label>
                                    <textarea name="observacionesVesti" id="observacionesVesti" rows="3" class="form-control">{{ $historia['paraclinicosdatos']->pruebavestibular_obs }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="impresionDiagnostica" role="tabpanel" aria-labelledby="impresionDiagnostica-tab">
                    <h5 class="title-sections" id="sub-title">IMPRESIÓN DIAGNOSTICA</h5>
                    <div class="border-container" style="padding: 2% 1% 1% 1%;">

                        <table class="table table-bordered table-responsive-md" id="tableImpresionD" >
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 10%">Buscar (codigo)</th>
                                <th scope="col" style="width: 24%">Diagnóstico</th>
                                <th scope="col" style="width: 4%">Registrar</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyImpresionD2">
                            <tr id="tr" class="impresionDTR0">
                                <td>
                                    <input type="text" class="input-table impresiondiagnostica" name="buscarCIE10" id="codigoCIE10">
                                </td>
                                <td>
                                    <input type="text" class="input-table diagnosticoid autocomplete-paracl" name="diagnosticoID" id="diagnosticoCIE10" style="font-size: 0.8rem; height: 44px;border-color: #0000;
                                    border-radius: 0;border-style: none; border-width: 0; box-shadow: none" >
                                    <input type="hidden" class="input-table" name="id_diag" id="id_diag0" >
                                </td>
                                <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="anadirID">Añadir</a></td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-responsive-md" id="tableImpresionD" >
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 10%">Código</th>
                                <th scope="col" style="width: 24%">Diagnóstico</th>
                                <th scope="col" style="width: 10%">Sospecha de origen</th>
                                <th scope="col" style="width: 12%">Tipo de diagnostico</th>
                                <th scope="col" style="width: 4%">Eliminar</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyImpresionD">
                            @foreach($historia['impresiondiagnostica'] as $impresion)
                                <tr id="tr" class="impresionDTR">
                                    <td>
                                        <input type="text" class="input-table impresiondiagnostica" name="buscarCIE10" id="buscarCIE10" value="{{ $impresion['cie10']['codigo'] }}" disabled>
                                    </td>
                                    <td>
                                        <input type="text" class="input-table diagnosticoid" name="diagnosticoID" id="diagnosticoID" value="{{ $impresion->diagnostico }}" style="font-size: 0.8rem; height: 44px;border-color: #0000;
                                        border-radius: 0;border-style: none; border-width: 0; box-shadow: none" disabled>
                                        <input type="hidden" class="input-table" name="id_diag" id="id_diag0" >
                                    </td>
                                    <td>
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control input-table" id="sospechaOrigen" name="sospechaOrigen" style="font-size: 0.8rem; height: 44px">
                                                <option value=""></option>
                                                <option value="Común" {{ $impresion->sospechaOrigen == 'Común' ? "selected":"" }}>Común</option>
                                                <option value="Profesional" {{ $impresion->sospechaOrigen == 'Profesional' ? "selected":"" }}>Profesional</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control input-table " id="tipoDiagnostico" name="tipoDiagnostico" style="font-size: 0.8rem; height: 44px;">
                                                <option value=""></option>
                                                <option value="Impresión diagnóstica" {{ $impresion->tipodiagnostico == 'Impresión diagnóstica' ? "selected":"" }}>Impresión diagnóstica</option>
                                                <option value="Confirmado nuevo" {{ $impresion->tipodiagnostico == 'Confirmado nuevo' ? "selected":"" }}>Confirmado nuevo</option>
                                                <option value="Confirmado repetido" {{ $impresion->tipodiagnostico == 'Confirmado repetido' ? "selected":"" }}>Confirmado repetido</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="tdImg" style="padding: 7px; text-align: center"><a href="" id="eliminarID"><img src="/img/delete.png" alt="delete" style="width: 30px"></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="result" role="tabpanel" aria-labelledby="result-tab">
                    <h5 class="title-sections" id="sub-title">RESULTADO</h5>
                    <div class="border-container">
                        <h5 class="title-sections">RESULTADO VALORACIÓN MÉDICA</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PRE-EMPLEO - APTO,') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO CON RECOMENDACIONES" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PRE-EMPLEO - APTO CON RECOMENDACIONES') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - APTO CON RECOMENDACIONES</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APTO CON RESTRICCIONES" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PRE-EMPLEO - APTO CON RESTRICCIONES') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">PRE-EMPLEO - APTO CON RESTRICCIONES</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PRE-EMPLEO - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">PRE-EMPLEO - APLAZADO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PRE-EMPLEO - NO APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PRE-EMPLEO - NO APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox1">PRE-EMPLEO - NO APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIODICO - SATISFACTORIO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PERIODICO - SATISFACTORIO,') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox1">PERIODICO - SATISFACTORIO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIÓDICO - SATISFACTORIO CON RECOMENDACIONES QUE REQUIEREN VALORACIÓN" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PERIÓDICO - SATISFACTORIO CON RECOMENDACIONES QUE REQUIEREN VALORACIÓN') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">PERIÓDICO - SATISFACTORIO CON RECOMENDACIONES QUE REQUIEREN VALORACIÓN</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="PERIÓDICO - NO SATISFACTORIO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'PERIÓDICO - NO SATISFACTORIO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">PERIÓDICO - NO SATISFACTORIO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="RETIRO - SIN HALLAZGOS CLÍNICOS" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'RETIRO - SIN HALLAZGOS CLÍNICOS') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">RETIRO - SIN HALLAZGOS CLÍNICOS</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="RETIRO - EXAMEN CON HALLAZGOS CLÍNICOS QUE REQUIEREN CONTROL" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'RETIRO - EXAMEN CON HALLAZGOS CLÍNICOS QUE REQUIEREN CONTROL') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">RETIRO - EXAMEN CON HALLAZGOS CLÍNICOS QUE REQUIEREN CONTROL</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'TRABAJO EN ALTURAS - APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - NO APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'TRABAJO EN ALTURAS - NO APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - NO APTO</label>
                                </div>
                            </div>

                            <div class="col-md-5 offset-1">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="TRABAJO EN ALTURAS - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'TRABAJO EN ALTURAS - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">TRABAJO EN ALTURAS - APLAZADO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'MANIPULACIÓN DE ALIMENTOS - APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - APTO</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - NO APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'MANIPULACIÓN DE ALIMENTOS - NO APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - NO APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="MANIPULACIÓN DE ALIMENTOS - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'MANIPULACIÓN DE ALIMENTOS - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">MANIPULACIÓN DE ALIMENTOS - APLAZADO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'POSTINCAPACIDAD - APTO,') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO CON RECOMENDACIONES" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'POSTINCAPACIDAD - APTO CON RECOMENDACIONES') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO CON RECOMENDACIONES</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APTO CON RESTRICCIONES" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'POSTINCAPACIDAD - APTO CON RESTRICCIONES') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APTO CON RESTRICCIONES</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="POSTINCAPACIDAD - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'POSTINCAPACIDAD - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">POSTINCAPACIDAD - APLAZADO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="ESPACIOS CONFINADOS - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'ESPACIOS CONFINADOS - APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">ESPACIOS CONFINADOS - APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="ESPACIOS CONFINADOS - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'ESPACIOS CONFINADOS - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">ESPACIOS CONFINADOS - APLAZADO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="CONDUCTOR - APTO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'CONDUCTOR - APTO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">CONDUCTOR - APTO</label>
                                </div>
                                <br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="valoracionMedica[]" value="CONDUCTOR - APLAZADO" {{ strpos($historia['resultado']->resultadoValoracionMedica, 'CONDUCTOR - APLAZADO') !== false ? "checked":""}}>
                                    <label class="form-check-label" for="inlineCheckbox2">CONDUCTOR - APLAZADO</label>
                                </div>
                            </div>
                        </div>

                        <h5 class="title-sections" id="sub-title">RECOMENDACIONES DE SALUD OCUPACIONAL</h5>
                        <div class="border-container">
                            <div class="row">
                                <div class="col-md-3">
                                    <h6 class="font-bold">Médicas</h6>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Remisión a EPS" {{ strpos($historia['resultado']->recomendacion_medica, 'Remisión a EPS') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Remisión a EPS</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Continuar manejo Médico"  {{ strpos($historia['resultado']->recomendacion_medica, 'Continuar manejo Médico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Continuar manejo Médico</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Remision a ARL"  {{ strpos($historia['resultado']->recomendacion_medica, 'Remision a ARL') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Remision a ARL</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Seguimiento caso por ARL"  {{ strpos($historia['resultado']->recomendacion_medica, 'Seguimiento caso por ARL') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Seguimiento caso por ARL</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Citología Cervico-Vaginal"  {{ strpos($historia['resultado']->recomendacion_medica, 'Citología Cervico-Vaginal') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Citología Cervico-Vaginal</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Tamizaje Prostatico" {{ strpos($historia['resultado']->recomendacion_medica, 'Tamizaje Prostatico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Tamizaje Prostatico</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Esquema de Vacunación (Adulto)" {{ strpos($historia['resultado']->recomendacion_medica, 'Esquema de Vacunación (Adulto)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Esquema de Vacunación (Adulto)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Audiologico Periodico" {{ strpos($historia['resultado']->recomendacion_medica, 'Control Audiologico Periodico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Control Audiologico Periodico</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Anual Optometrico" {{ strpos($historia['resultado']->recomendacion_medica, 'Control Anual Optometrico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Control Anual Optometrico</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="medicasresult[]" value="Control Odontologico Periodico" {{ strpos($historia['resultado']->recomendacion_medica, 'Control Odontologico Periodico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Control Odontologico Periodico</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h6 class="font-bold">Ingreso a SVE</h6>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Ergonomico)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Ergonomico)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Ergonomico)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Ruido)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Ruido)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Ruido)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Biológico)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Biológico)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Biológico)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Visual)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Visual)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Visual)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Psicolaboral)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Psicolaboral)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Psicolaboral)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Cardiovascular)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Cardiovascular)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Cardiovascular)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Metabólico)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Metabólico)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Metabólico)</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ingresoSVE[]" value="SVE (Químico)" {{ strpos($historia['resultado']->recomendacion_ingresosve, 'SVE (Químico)') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">SVE (Químico)</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h6 class="font-bold">Ocupacionales</h6>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Uso de EPP" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Uso de EPP') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Uso de EPP</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Higiene Postural" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Higiene Postural') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Higiene Postural</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas Activas" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Pausas Activas') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Pausas Activas</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Distribucion de fuerzas y cargas" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Distribucion de fuerzas y cargas') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Distribucion de fuerzas y cargas</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas activas musculo esqueléticas" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Pausas activas musculo esqueléticas') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Pausas activas musculo esqueléticas</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="ocupacionales[]" value="Pausas activas visuales" {{ strpos($historia['resultado']->recomendacion_ocupacional, 'Pausas activas visuales') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Pausas activas visuales</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h6 class="font-bold">Hábitos y estilos de vida saludable</h6>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Inicio de actividad Física" {{ strpos($historia['resultado']->recomendacion_habitos, 'Inicio de actividad Física') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Inicio de actividad Física</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Dejar de Fumar" {{ strpos($historia['resultado']->recomendacion_habitos, 'Dejar de Fumar') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Dejar de Fumar</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Reducir Consumo de Alcohol" {{ strpos($historia['resultado']->recomendacion_habitos, 'Reducir Consumo de Alcohol') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Reducir Consumo de Alcohol</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Control Nutricional y de Peso" {{ strpos($historia['resultado']->recomendacion_habitos, 'Control Nutricional y de Peso') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Control Nutricional y de Peso</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="habitosSaludables[]" value="Continuar con actividad física" {{ strpos($historia['resultado']->recomendacion_habitos, 'Continuar con actividad física') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="">Continuar con actividad física</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5 class="title-sections" id="sub-title">PRUEBAS COMPLEMENTARIAS</h5>
                        <div class="border-container" style="padding-top: 2%;margin-top: 1%">
                            <div class="row">
                                <div class="col-md-2" id="divResultados">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Ninguna" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Ninguna') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Ninguna</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Audiometría" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Audiometría') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Audiometría</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Visiometría" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Visiometría') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Visiometría</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Optometría" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Optometría') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Optometría</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Espirometría" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Espirometría') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Espirometría</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Electrocardiograma" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Electrocardiograma') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Electrocardiograma</label>
                                    </div>
                                </div>
                                <div class="col-md-2" id="divResultados">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Evaluación Psicológica" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Evaluación Psicológica') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Evaluación Psicológica</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Evaluación psicosensométrica" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Evaluación psicosensométrica') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Evaluación psicosensométrica</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Pruebas vestibulares" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Pruebas vestibulares') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Pruebas vestibulares</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Coprológico/Cultivo" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Coprológico/Cultivo') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Coprológico/Cultivo</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Directo Hongos" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Directo Hongos') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Directo Hongos</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Frotis Faringeo" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Frotis Faringeo') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Frotis Faringeo</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Glicemia" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Glicemia') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Glicemia</label>
                                    </div>
                                </div>
                                <div class="col-md-2" id="divResultados">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Perfil Lipídico" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Perfil Lipídico') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Perfil Lipídico</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Cuadro hemático" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Cuadro hemático') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Cuadro hemático</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Parcial de orina" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Parcial de orina') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Parcial de orina</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Serología para Sífilis" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Serología para Sífilis') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Serología para Sífilis</label>
                                    </div>
                                    <br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="pruebasComplementarias[]" value="Otra" {{ strpos($historia['resultado']->recomendacion_pruebacomplementaria, 'Otra') !== false ? "checked":""}}>
                                        <label class="form-check-label" for="inlineCheckbox2">Otra</label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <label for="motivo" class="font-bold">Otro:</label>
                                </div>
                                <div class="col-md-5">
                                    <textarea name="otroPC" id="otroPC" rows="6" class="form-control">{{ $historia['resultado']->recomendacion_pruebacomplementaria_otros }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="title-sections" id="sub-title">RESTRICCIONES</h5>
                                <div class="border-container">
                                    <textarea name="restricciones" id="restriccionesID" class="form-control" rows="3" placeholder="Restricciones" >{{ $historia['resultado']->restricciones }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5 class="title-sections" id="sub-title">RECOMENDACIONES ADICIONALES</h5>
                                <div class="border-container">
                                    <textarea name="recomendacionesA" id="recomendacionesID" class="form-control"  rows="3" placeholder="Recomendaciones adicionales" >{{ $historia['resultado']->recomendacionesAdicionales }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="llave" name="llave" value="{{ encrypt($historia->id) }}">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary" id="actualizarHC">
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var examen = {!! json_encode($examenesLab) !!};
            {{--var cie10 = {!! json_encode($cie10) !!};--}}

        </script>
    @endif
@stop
