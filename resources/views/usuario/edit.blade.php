@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Usuario - Editar</title>
@stop
@section('contenido')
    @if(session()->has('infoUpdate'))
        <div class="container-local">
            <h1>{{ session()->get('infoUpdate') }}</h1>
        </div>
    @else
    <form method="POST" action="{{ route('usuario.update', encrypt($usuario->id)) }}">
        {!! csrf_field() !!}

        @if (count($errors) > 0)
            <div class="container-local">
                <div class="toast toast-local" data-autohide="false">
                    <div class="toast-header">
                        <strong class="mr-auto">Alerta</strong>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="container-local">
            <h4 class="title-principal">EDITAR DATOS DEL USUARIO</h4>
        </div>

        <div class="container-local border-container">
            <div class="row">
                <div class="col-md-6">
                    <label for="tipoDocumento" class="row-size-label">Tipo de documento:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="cc" name="type_document" value="CC" {{ $usuario->type_document == 'CC' || old('type_document') == 'CC'  ? "checked":"" }}>
                        <label class="custom-control-label" for="cc">CC</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ti" name="type_document" value="TI" {{ $usuario->type_document == 'TI' || old('type_document') == 'TI'  ? "checked":"" }}>
                        <label class="custom-control-label" for="ti">TI</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="pa" name="type_document" value="PA" {{ $usuario->type_document == 'PA' || old('type_document') == 'PA'  ? "checked":"" }}>
                        <label class="custom-control-label" for="pa">PA</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ceei" name="type_document" value="CE" {{ $usuario->type_document == 'CE' || old('type_document') == 'CE'  ? "checked":"" }}>
                        <label class="custom-control-label" for="ceei">CE</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="documento" class="row-size-label">
                        N° de documento:
                    </label>
                    <input type="text" name="document" placeholder="N° de documento" class="row-size-input form-control-form form-control-form" value="{{ old('document') == '' || $usuario->document == old('document') ? $usuario->document : old('document') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombre" class="row-size-label"> Nombres:</label>
                    <input type="text" name="name" placeholder="Nombres" class="row-size-input form-control-form form-control-form" value="{{ old('name') == '' || $usuario->name == old('name') ? $usuario->name : old('name') }}">
                </div>
                <div class="col-md-6">
                    <label for="apellido" class="row-size-label">Apellidos:</label>
                    <input type="text" name="last_name" placeholder="Nombres" class="row-size-input form-control-form form-control-form" value="{{ old('last_name') == '' || $usuario->last_name == old('last_name') ? $usuario->last_name : old('last_name') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="registro_medico" class="row-size-label">Registro médico:</label>
                    <input type="text" name="registro_medico" placeholder="Registro médico" class="row-size-input form-control-form" value="{{ old('registro_medico') == '' || $usuario->registro_medico == old('registro_medico') ? $usuario->registro_medico : old('registro_medico') }}">
                </div>
                <div class="col-md-6">
                    <label for="licencia" class="row-size-label">Licencia SO: </label>
                    <input type="text" name="licencia" placeholder="Licencia" class="row-size-input form-control-form" value="{{ old('licencia') == '' || $usuario->licencia == old('licencia') ? $usuario->licencia : old('licencia') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombreUsuario" class="row-size-label">
                        Nombre de usuario:
                    </label>
                    <input type="text" name="email" placeholder="Nombre de usuario" class="row-size-input form-control-form" value="{{ old('email') == '' || $usuario->email == old('email') ? $usuario->email : old('email') }}">
                </div>
                <div class="col-md-6">
                    <label for="role" class="row-size-label">Rol usuario:</label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" name="role_id" value="{{ old('role_id') }}" style="font-size: 0.8rem">
                            <option value=""></option>
                            @foreach($roles as $role)
                                @if(old('role_id') == $role->id || $usuario->role_id == $role->id)
                                    <option value="{{ $role->id }}" selected>{{ $role->nombre_descripcion }}</option>
                                @else
                                    <option value="{{ $role->id }}">{{ $role->nombre_descripcion }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            @if($usuario->firma != '')
                <div class="row" style="padding-top: 2%">
                    <div class="offset-md-6 col-md-6" style="width: 100%">
                        <img id="firma-img" src="data:image/png;base64,{{$usuario->firma}}" class="img-firma"/>
                        <input id="firma" type="hidden" name="firma" value="{{$usuario->firma}}">
                    </div>
                </div>

                <div class="row">
                    <div class="offset-md-6 col-md-2">
                        <input type="button" id="eliminar-firmaUsuario" class="btn button-form button-delete btn-primary" value="Eliminar firma">
                    </div>
                </div>
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
    @endif
@stop
