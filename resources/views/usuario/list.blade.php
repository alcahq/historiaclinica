@extends('layouts')
@section('title')
    <title>Usuario - Buscar</title>
@stop
@section('contenido')

    <div class="container-local">
        <h4 class="title-principal">BUSCAR USUARIO</h4>
    </div>

    <div class="container-local border-container">
        <form method="POST" action="{{ route('usuario.findByDocumentOrName') }}">
            {!! csrf_field() !!}
            <div class="row find-row">
                <div class="col-md-3 col-find-input">
                    <input type="text" name="buscar_usuario" class="form-control find-input" placeholder="No documento o nombre">
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Buscar" class="button-form btn btn-primary find-button">
                </div>
            </div>
        </form>
        
        <table class="table table-hover table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope = "col">#</th>
                <th scope = "col">N° documento</th>
                <th scope = "col">Nombres</th>
                <th scope = "col">Apellidos</th>
                <th scope = "col">Role</th>
                <th scope = "col">Editar</th>
            </tr>
            </thead>
            <tbody>
            @isset($usuarios)
                @forelse($usuarios as $usuario)
                    <tr>
                        <td scope="row" class="count"></td>
                        <td>{{ $usuario->document }}</td>
                        <td>{{ $usuario->name }}</td>
                        <td>{{ $usuario->last_name }}</td>
                        <td>{{ $usuario->role->nombre_descripcion }}</td>
                        <td><a href="{{ route('usuario.edit', encrypt($usuario->id)) }}">Editar</a></td>
                    </tr>
                @empty
                    <h1>No se encontraron resultados</h1>
                @endforelse
            @endisset
            </tbody>
        </table>
    </div>

@endsection
