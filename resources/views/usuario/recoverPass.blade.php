@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Usuario - Restablecer Contraseña</title>
@stop
@section('contenido')

    @if(count($errors) > 0)
        <div class="container-local">
            <div class="toast toast-local" data-autohide="false">
                <div class="toast-header">
                    <strong class="mr-auto">Alerta</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('SiUser'))
        <div class="container-local">
            <h1>{{ session()->get('SiUser') }}</h1>
        </div>
    @else

    <form method="POST" action="{{ route('usuario.recoverPassword') }}">
        {!! csrf_field() !!}
        <div class="container-local">
            <h4 class="title-pass">RESTABLECER CONTRASEÑA</h4>
        </div>

        <div class="container-pass border-container">
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="documento usuario" class="row-size-pass" placeholder="No de documento">No de documento</label>
                    <input type="text" class="form-control-form row-size-pass" name="document" value="{{ old('document') }}">
                </div>
            </div>
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="documento usuario" class="row-size-pass">Nueva contraseña</label>
                    <input type="password" class="form-control-form row-size-pass" name="password">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <label for="documento usuario" class="row-size-pass">Repetir contraseña</label>
                    <input type="password" class="form-control-form row-size-pass" name="password_confirmation">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
    @endif
@stop
