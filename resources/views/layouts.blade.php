<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="\css\app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="/js/app.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap-datepicker.standalone.css')}}">
    <script src="{{asset('/js/bootstrap-datepicker.js')}}"></script>
{{--    <meta http-equiv="Expires" content="0" />--}}
{{--    <meta http-equiv="Pragma" content="no-cache" />--}}

{{--    <script type="text/javascript">--}}
{{--        if(history.forward(1)){--}}
{{--            location.replace( history.forward(1) );--}}
{{--        }--}}
{{--    </script>--}}
    @yield('js')
    <!-- Languaje -->
    <script src="{{asset('/js/bootstrap-datepicker.es.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <link rel="icon" type="image/ico" href="/img/favicon-analizar[1].png" />
    @if(request()->is('/'))
        <title>Inicio de sesión</title>
    @endif
    @yield('title')
</head>
<body>
    <header>
        <div class="container-local">
            @if(auth()->check())
                <div class="row margin-header">
                    <div class="col-md-6">
                        <img src="/img/analizar-logo.png" class="size-img">
                    </div>
                    <div class="col-md-3 offset-md-3 logout">
                        <a href="/logout">Cerrar sesión de {{ auth()->user()->name }}</a>
                    </div>
                </div>
            @endif
            @if(auth()->check())
                <nav class="navbar navbar-expand-lg navbar-light menu justify-content-center">
                    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        
                        <div class="collapse navbar-collapse style-dropdown" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                @if(auth()->user()->hasRoles(['admin']))
                                    <li class="nav-item dropdown {{ setActive('usuario/registrar') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            USUARIO
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('usuario.create') }}">REGISTRAR</a>
                                            <a class="dropdown-item" href="{{ route('usuario.viewList') }}">BUSCAR</a>
                                            <a class="dropdown-item" href="{{ route('usuario.viewRecoverPass') }}">RESTABLECER CONTRASEÑA</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin','fonoaudiologa','optometra','recepcionista']))
                                    <li class="nav-item dropdown {{ setActive('paciente/registrar') }} {{ setActive('paciente/buscar') }} {{ setActive('paciente/editar/*') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            PACIENTE
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('paciente.create') }}">REGISTRAR</a>
                                            <a class="dropdown-item" href="{{ route('paciente.viewList') }}">BUSCAR</a>
                                            <a class="dropdown-item" href="{{ route('paciente.viewHistorias') }}">CONSULTAR HISTORIAS</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin']))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            HISTORIA CLíNICA
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('historiaClinica.create') }}">REGISTRAR</a>
                                            @if(auth()->user()->hasRoles(['admin']))
                                                <a class="dropdown-item" href="{{ route('historiaClinica.viewAtenncionesGenerales') }}">VISIÓN GENERAL ATENCIONES</a>
                                            @endif

                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin','optometra',]))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            HISTORIA OPTOMETRÍA
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('optometria.index') }}">REGISTRAR</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin','optometra',]))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            HISTORIA VISIOMETRÍA
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('historiaVisiometria.create') }}">REGISTRAR</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin','fonoaudiologa']))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            HISTORIA AUDIOMETRÍA
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('historiaAudiometria.register') }}">REGISTRAR</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['admin']))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            EMPRESA
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('empresa.create') }}">REGISTRAR</a>
                                            <a class="dropdown-item" href="{{ route('empresa.viewList') }}">BUSCAR</a>
                                        </div>
                                    </li>
                                @endif
                                @if(auth()->user()->hasRoles(['medico','admin']))
                                    <li class="nav-item dropdown {{ setActive('') }}">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            REPORTES
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('historia.reporteDiag') }}">REPORTE DIAGNOSTICO</a>
                                            <a class="dropdown-item" href="{{ route('historiaClinica.viewExport') }}">EXPORTACIÓN BASE DE DATOS</a>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>
            @endif
        </div>
    </header>
    @yield('contenido')
    @if(auth()->check())
        <br>

        <footer>
            <div class="container-footer">
                <div class="row">
                    <div class="col-md-12" style="padding-left: 0">
                        <h6 class="">Analizar laboratorio clínico automatizado © {{ date('Y') }}</h6>
                    </div>
                </div>
            </div>
        </footer>
    @endif
</body>
</html>
