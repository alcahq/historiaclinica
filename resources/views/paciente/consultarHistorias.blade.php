@extends('layouts')
@section('title')
    <title>Paciente - Historias</title>
@stop
@section('contenido')

    <div class="container-local">
        <h4 class="title-principal">BUSCAR HISTORIAS DEL PACIENTE</h4>
    </div>

    <div class="container-local border-container">
        <form method="POST" action="{{ route('paciente.historias') }}">
            {!! csrf_field() !!}
            <div class="row find-row">
                <div class="col-md-3 col-find-input">
                    <input type="text" name="documento" class="form-control find-input" placeholder="No documento">
                    <input type="hidden" name="role_id" value="{{ auth()->user()->role_id }}" disable>
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Buscar" class="button-form btn btn-primary find-button">
                </div>
            </div>
        </form>

        <table class="table table-hover table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Fecha</th>
                <th scope="col">No documento</th>
                <th scope="col">Nombre</th>
                <th scope="col">Tipo historia</th>
                <th scope="col">Estado</th>
                <th scope="col">Consultar</th>
                <th scope="col">Imprimir certificado</th>
            </tr>
            </thead>
            <tbody>
            @if(@isset($historiaclinica) and @isset($historiaaudiometria) and @isset($historiaoptometria) and @isset($historiavisiometria))
                @if($historiaclinica->count() > 0 or $historiaaudiometria->count() > 0  or $historiaoptometria->count() > 0 or $historiavisiometria->count() > 0 )
                    @if(auth()->user()->hasRoles(['admin','medico','recepcionista']))
                        @foreach($historiaclinica as $hc)
                            <tr>
                                <th scope="row" class="count"></th>
                                <td>{{ $hc->fecha}}</td>
                                <td>{{ $hc->paciente_cedula }}</td>
                                <td>{{ $hc->paciente }}</td>
                                <td>Historia clínica</td>
                                <td>{{ $hc->estado }}</td>
                                @if(auth()->user()->hasRoles(['admin','medico']))
                                    <td><a href="{{ route('historiaClinica.getHC', encrypt($hc->id)) }}">Consultar</a></td>
                                @endif
                                <td>
                                    <a href="{{ route('historiaclinica.generarCertificado', $hc->id) }}">Imprimir certificado</a>
                                    <br>
                                    <a href="{{ route('historiaclinica.generarInforme', $hc->id) }}">Imprimir informe</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    @if(auth()->user()->hasRoles(['admin','fonoaudiologa','medico','recepcionista']))
                        @foreach($historiaaudiometria as $ha)
                            <tr>
                                <th scope="row" class="count"></th>
                                <td>{{ $ha->fecha}}</td>
                                <td>{{ $ha->paciente_cedula }}</td>
                                <td>{{ $ha->paciente }}</td>
                                <td>Historia audiometria</td>
                                <td>{{ $ha->estado }}</td>
                                @if(auth()->user()->hasRoles(['admin','fonoaudiologa','medico']))
                                    <td><a href="{{ route('historiaAudiometria.getHA', encrypt($ha->id)) }}">Consultar</a></td>
                                @endif
                                <td><a href="{{ route('audiometria.generarCertificado', $ha->id) }}">Imprimir certificado</a></td>
                            </tr>
                        @endforeach
                    @endif


                    @if(auth()->user()->hasRoles(['admin','optometra','medico','recepcionista']))
                        @foreach($historiaoptometria as $ho)
                            <tr>
                                <th scope="row" class="count"></th>
                                <td>{{ $ho->fecha}}</td>
                                <td>{{ $ho->paciente_cedula }}</td>
                                <td>{{ $ho->paciente }}</td>
                                <td>Historia optometria</td>
                                <td>{{ $ho->estado }}</td>
                                @if(auth()->user()->hasRoles(['admin','optometra','medico']))
                                    <td><a href="{{ route('historiaOptometria.getHO', encrypt($ho->id)) }}">Consultar</a></td>
                                @endif
                                <td><a href="{{ route('optometria.generarCertificado', $ho->id) }}">Imprimir certificado</a></td>
                            </tr>
                        @endforeach
                    @endif

                    @if(auth()->user()->hasRoles(['admin','optometra','medico','recepcionista']))
                        @foreach($historiavisiometria as $hv)
                            <tr>
                                <th scope="row" class="count"></th>
                                <td>{{ $hv->fecha}}</td>
                                <td>{{ $hv->paciente_cedula }}</td>
                                <td>{{ $hv->paciente }}</td>
                                <td>Historia visiometria</td>
                                <td>{{ $hv->estado }}</td>
                                @if(auth()->user()->hasRoles(['admin','optometra','medico']))
                                    <td><a href="{{ route('historiaVisiometria.getHV', encrypt($hv->id)) }}">Consultar</a></td>
                                @endif
                                <td><a href="{{ route('visiometria.generarCertificado', ($hv->id)) }}">Imprimir certificado</a></td>
                            </tr>
                        @endforeach
                    @endif


                @else
                    <h1>No se encontraron resultados</h1>
                @endif
            @endif

            </tbody>
        </table>
    </div>
@endsection
