@extends('layouts')
@section('js')
    <script src="{{asset('/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Paciente - Registro</title>
@stop
@section('contenido')
    @if(session()->has('info'))
        <div class="container-local">
            <h1>{{ session()->get('info') }}</h1>
        </div>
    @else
    <form method="POST" action="{{ route('paciente.store') }}">
        @if (count($errors) > 0)
            <div class="container-local">
                <div class="toast toast-local" id="toastG" data-autohide="false">
                    <div class="toast-header">
                        <strong class="mr-auto">Alerta</strong>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="container-local">
            <h4 class="title-principal">REGISTRAR PACIENTE</h4>
        </div>

        <div class="container-local border-container">
            <h5 class="title-sections">DATOS PERSONALES</h5>
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-6">
                    <label for="tipodocumento" class="row-size-label">Tipo de documento:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="cc" name="tipodocumento" value="CC" {{ 'CC' == old('tipodocumento') ? "checked":"" }}>
                        <label class="custom-control-label" for="cc">CC</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ti" name="tipodocumento" value="TI" {{ 'TI' == old('tipodocumento') ? "checked":"" }}>
                        <label class="custom-control-label" for="ti">TI</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="pa" name="tipodocumento" value="PA" {{ 'PA' == old('tipodocumento') ? "checked":"" }}>
                        <label class="custom-control-label" for="pa">PA</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ceei" name="tipodocumento" value="CE" {{ 'CE' == old('tipodocumento') ? "checked":"" }}>
                        <label class="custom-control-label" for="ceei">CE</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="documento" class="row-size-label">N° de documento:</label>
                    <input type="text" name="documento" class="row-size-input form-control-form" value="{{ old('documento') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombre" class="row-size-label">Nombres:</label>
                    <input type="text" name="nombre" class="row-size-input form-control-form" value="{{ old('nombre') }}">
                </div>
                <div class="col-md-6">
                    <label for="apellido" class="row-size-label">Apellidos:</label>
                    <input type="text" name="apellido" class="row-size-input form-control-form" value="{{ old('apellido') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="genero" class="row-size-label">Sexo:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="m" name="genero" value="MASCULINO">
                        <label class="custom-control-label" for="m">MASCULINO</label>
                    </div>

                    <!-- Default inline 2-->
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="f" name="genero" value="FEMENINO">
                        <label class="custom-control-label" for="f">FEMENINO</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="fechaNacimiento" class="row-size-label">Fecha de nacimiento:</label>
                    <input type="text" class="row-size-input form-control-form datepicker" name="fechaNacimiento" id="fechaN" autocomplete="off" value="{{old('fechaNacimiento')}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="edad" class="row-size-label">Edad:</label>
                    <input type="text" name="edad2" id="edadCalculada" class="row-size-input form-control-form" value="{{ old('edad') }}" disabled>
                    <input type="hidden" id="edadCalculada2" name="edad">
                </div>
                <div class="col-md-6">
                    <label for="lugarNacimiento" class="row-size-label">Lugar de nacimiento:</label>
                    <input type="text" name="lugarNacimiento" class="row-size-input form-control-form" value="{{ old('lugarNacimiento') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="direccionDomicilio" class="row-size-label">
                        Dirección de domicilio:
                    </label>
                    <input type="text" name="direccionDomicilio" class="row-size-input form-control-form" value="{{ old('direccionDomicilio') }}">
                </div>
                <div class="col-md-6">
                    <label for="ciudad" class="row-size-label">
                        Ciudad:
                    </label>
                    <input type="text" name="ciudadDomicilio" class="row-size-input form-control-form" value="{{ old('ciudadDomicilio') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="" class="row-size-label">
                        Teléfono celular:
                    </label>
                    <input type="text" name="celular" class="row-size-input form-control-form" value="{{ old('celular') }}">
                </div>
                <div class="col-md-6">
                    <label for="ciudad" class="row-size-label">
                        Teléfono fijo:
                    </label>
                    <input type="text" name="telefono" class="row-size-input form-control-form" value="{{ old('telefono') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="cargo" class="row-size-label">
                        Cargo:
                    </label>
                    <input type="text" name="cargo" class="row-size-input form-control-form" value="{{ old('cargo') }}">
                </div>
                <div class="col-md-6">
                    <label for="antiguedad" class="row-size-label">
                        Antiguedad:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="antiguedad">
                            <option value=""></option>
                            <option value="< 1 año" {{ old('antiguedad') == '< 1 año' ? "selected":"" }}>< 1 año</option>
                            <option value="1 - 5 años" {{ old('antiguedad') == '1 - 5 años' ? "selected":"" }}>1 - 5 años</option>
                            <option value="5 - 10 años" {{ old('antiguedad') == '5 - 10 años' ? "selected":"" }}>5 - 10 años</option>
                            <option value="> 10 años" {{ old('antiguedad') == 'Mayor a 10 años' ? "selected":"" }}>> 10 años</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="email" class="row-size-label">
                        Correo electrónico:
                    </label>
                    <input type="text" name="correoElectronico" class="row-size-input form-control-form" value="{{ old('correoElectronico') }}">
                </div>
                <div class="col-md-6">
                    <label for="escolaridad" class="row-size-label">
                        Nivel de escolaridad:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="escolaridad">
                            <option value=""></option>
                            <option value="ANALFABETA" {{ old('escolaridad') == 'ANALFABETA' ? "selected":"" }}>ANALFABETA</option>
                            <option value="PRIMARIA" {{ old('escolaridad') == 'PRIMARIA' ? "selected":"" }}>PRIMARIA</option>
                            <option value="SECUNDARIA" {{ old('escolaridad') == 'SECUNDARIA' ? "selected":"" }}>SECUNDARIA</option>
                            <option value="TECNICO" {{ old('escolaridad') == 'TECNICO' ? "selected":"" }}>TÉCNICO</option>
                            <option value="TECNOLOGO" {{ old('escolaridad') == 'TECNOLOGO' ? "selected":"" }}>TECNÓLOGO</option>
                            <option value="UNIVERSITARIO" {{ old('escolaridad') == 'UNIVERSITARIO' ? "selected":"" }}>UNIVERSITARIO</option>
                            <option value="ESPECIALIZACION" {{ old('escolaridad') == 'ESPECIALIZACION' ? "selected":"" }}>ESPECIALIZACIÓN</option>
                            <option value="MAESTRIA" {{ old('escolaridad') == 'MAESTRIA' ? "selected":"" }}>MAESTRIA</option>
                            <option value="DOCTORADO" {{ old('escolaridad') == 'DOCTORADO' ? "selected":"" }}>DOCTORADO</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="estadoCivil" class="row-size-label">
                        Estado civil:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect2" name="estadoCivil">
                            <option value=""></option>
                            <option value="SOLTERO" {{ old('estadoCivil') == 'SOLTERO' ? "selected":"" }}>SOLTERO</option>
                            <option value="CASADO" {{ old('estadoCivil') == 'CASADO' ? "selected":"" }}>CASADO</option>
                            <option value="UNIONLIBRE" {{ old('estadoCivil') == 'UNIONLIBRE' ? "selected":"" }}>UNIÓN LIBRE</option>
                            <option value="SEPARADO" {{ old('estadoCivil') == 'SEPARADO' ? "selected":"" }}>SEPARADO</option>
                            <option value="VIUDO" {{ old('estadoCivil') == 'VIUDO' ? "selected":"" }}>VIUDO</option>
                        </select>
                    </div>

                </div>
                <div class="col-md-6">
                    <label for="eps" class="row-size-label">
                        EPS:
                    </label>
                    <input type="text" name="eps" class="row-size-input form-control-form" value="{{ old('eps') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="arl" class="row-size-label">
                        ARL:
                    </label>
                    <input type="text" name="arl" class="row-size-input form-control-form" value="{{ old('arl') }}">
                </div>
                <div class="col-md-6">
                    <label for="afp" class="row-size-label">
                        AFP:
                    </label>
                    <input type="text" name="afp" class="row-size-input form-control-form" value="{{ old('afp') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="grupoSanguineo" class="row-size-label">
                        Grupo sanguineo:
                    </label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="A" name="grupoSanguineo" value="A" {{ 'A' == old('grupoSanguineo') ? "checked":"" }}>
                        <label class="custom-control-label" for="A">A</label>
                    </div>

                    <!-- Default inline 2-->
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="B" name="grupoSanguineo" value="B" {{ 'B' == old('grupoSanguineo') ? "checked":"" }}>
                        <label class="custom-control-label" for="B">B</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="AB" name="grupoSanguineo" value="AB" {{ 'AB' == old('grupoSanguineo') ? "checked":"" }}>
        <label class="custom-control-label" for="AB">AB</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="O" name="grupoSanguineo" value="O" {{ 'O' == old('grupoSanguineo') ? "checked":"" }}>
                        <label class="custom-control-label" for="O">O</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="rh" class="row-size-label">
                        RH:
                    </label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="P" name="rh" value="POSITIVO" {{ 'POSITIVO' == old('rh') ? "checked":"" }}>
                        <label class="custom-control-label" for="P">POSITIVO</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="N" name="rh" value="NEGATIVO" {{ 'NEGATIVO' == old('rh') ? "checked":"" }}>
                        <label class="custom-control-label" for="N">NEGATIVO</label>
                    </div>
                </div>
            </div>

            <h5 class="title-sections" id="sub-title">INFORMACIÓN DEL ACOMPAÑANTE</h5>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombreAcompaniante" class="row-size-label">
                        Nombres:
                    </label>
                    <input type="text" name="nombreAcompaniante" class="row-size-input form-control-form" value="{{ old('nombreAcompaniante') }}">
                </div>
                <div class="col-md-6">
                    <label for="apellidoAcompaniante" class="row-size-label">
                        Apellidos:
                    </label>
                    <input type="text" name="apellidoAcompaniante" class="row-size-input form-control-form" value="{{ old('apellidoAcompaniante') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="parentesco" class="row-size-label">
                        Parentesco:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="parentescoAcompaniante">
                            <option value=""></option>
                            <option value="MADRE" {{ old('parentescoAcompaniante') == 'MADRE' ? "selected":"" }}>MADRE</option>
                            <option value="PADRE" {{ old('parentescoAcompaniante') == 'PADRE' ? "selected":"" }}>PADRE</option>
                            <option value="ESPOS@" {{ old('parentescoAcompaniante') == 'ESPOS@' ? "selected":"" }}>ESPOS@</option>
                            <option value="NOVI@" {{ old('parentescoAcompaniante') == 'NOVI@' ? "selected":"" }}>NOVI@</option>
                            <option value="HERMAN@" {{ old('parentescoAcompaniante') == 'HERMAN@' ? "selected":"" }}>HERMAN@</option>
                            <option value="PRIM@" {{ old('parentescoAcompaniante') == 'PRIM@' ? "selected":"" }}>PRIM@</option>
                            <option value="AMIG@" {{ old('parentescoAcompaniante') == 'AMIG@' ? "selected":"" }}>AMIG@</option>
                            <option value="TI@" {{ old('parentescoAcompaniante') == 'TI@' ? "selected":"" }}>TI@</option>
                            <option value="HIJ@" {{ old('parentescoAcompaniante') == 'HIJ@' ? "selected":"" }}>HIJ@</option>
                            <option value="ABUEL@" {{ old('parentescoAcompaniante') == 'ABUEL@' ? "selected":"" }}>ABUEL@</option>
                            <option value="CUÑAD@" {{ old('parentescoAcompaniante') == 'CUÑAD@' ? "selected":"" }}>CUÑAD@</option>
                            <option value="EMPRESA" {{ old('parentescoAcompaniante') == 'EMPRESA' ? "selected":"" }}>EMPRESA</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="celularA" class="row-size-label">
                        Teléfono celular:
                    </label>
                    <input type="text" name="celularAcompaniante" class="row-size-input form-control-form" value="{{ old('celularAcompaniante') }}">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
    @endif
@stop
