@extends('layouts')
@section('title')
    <title>Paciente - Buscar</title>
@stop
@section('contenido')

    <div class="container-local">
        <h4 class="title-principal">BUSCAR PACIENTE</h4>
    </div>

    <div class="container-local border-container">
        <form action="{{ route('paciente.findByDocumentOrName') }}" method="get">
            {!! csrf_field() !!}
            <div class="row find-row">
                <div class="col-md-3 col-find-input">
                    <input type="text" name="buscar_paciente" class="form-control find-input" placeholder="No documento o nombre">
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Buscar" class="button-form btn btn-primary find-button">
                </div>
            </div>
        </form>

        <table class="table table-hover table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">N° documento</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Editar</th>
            </tr>
            </thead>
            <tbody>
            @isset($pacientes)
                @forelse($pacientes as $paciente)
                    <tr>
                        <th scope="row" class="count"></th>
                        <td>{{ $paciente->documento }}</td>
                        <td>{{ $paciente->nombre }}</td>
                        <td>{{ $paciente->apellido }}</td>
                        <td><a href="{{ route('paciente.edit', encrypt($paciente->id)) }}">Editar</a></td>
                    </tr>
                @empty
                    <h1>No se encontraron resultados</h1>
                @endforelse
            @endisset
            </tbody>
        </table>
        @isset($pacientes)
            <div class="row">
                <div class="col-md-4 offset-4">
                    {{$pacientes->appends(request()->input())->links()}}
                </div>
            </div>
        @endisset
    </div>
@endsection
