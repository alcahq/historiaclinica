@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Paciente - Editar</title>
@stop
@section('contenido')
    @if(session()->has('infoedit'))
        <div class="container-local">
            <h1>{{ session()->get('infoedit') }}</h1>
        </div>
    @else
    <form method="POST" action="{{ route('paciente.update', encrypt($paciente->id))}}">

        @if (count($errors) > 0)
            <div class="container-local">
                <div class="toast toast-local" data-autohide="false">
                    <div class="toast-header">
                        <strong class="mr-auto">Alerta</strong>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="container-local">
            <h4 class="title-principal">EDITAR DATOS DEL PACIENTE</h4>
        </div>

        <div class="container-local border-container">
            <h5 class="title-sections">DATOS PERSONALES</h5>
            {!! csrf_field() !!}
            @if($paciente->foto != '')
                <div class="row" style="height: 175px">
                    <div class="offset-md-5 col-md-2 container-foto" style="width: 100%">
                        <img id="foto-img" src="data:image/png;base64, {{$paciente->foto}}" class="img-foto" style="transform: rotate(90deg)"/>
                        <input id="foto" type="hidden" name="foto" value="{{$paciente->foto}}">
                    </div>
                </div>

                <div class="row" style="padding-bottom: 2%">
                    <div class="offset-md-5 col-md-2">
                        <input type="button" id="eliminar-foto" class="btn button-form button-delete btn-primary" value="Eliminar foto">
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <label for="tipodocumento" class="row-size-label">Tipo de documento:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="cc" name="tipodocumento" value="CC" {{ $paciente->tipodocumento == 'CC' || old('tipodocumento') == 'CC'  ? "checked":"" }}>
                        <label class="custom-control-label" for="cc">CC</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ti" name="tipodocumento" value="TI" {{ $paciente->tipodocumento == 'TI' || old('tipodocumento') == 'TI'  ? "checked":"" }}>
                        <label class="custom-control-label" for="ti">TI</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="pa" name="tipodocumento" value="PA" {{ $paciente->tipodocumento == 'PA' || old('tipodocumento') == 'PA'  ? "checked":"" }}>
                        <label class="custom-control-label" for="pa">PA</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="ceei" name="tipodocumento" value="CE" {{ $paciente->tipodocumento == 'CE' || old('tipodocumento') == 'CE'  ? "checked":"" }}>
                        <label class="custom-control-label" for="ceei">CE</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="documento" class="row-size-label">N° de documento:</label>
                    <input type="text" name="documento" class="row-size-input form-control-form" value="{{ old('documento') == '' || $paciente->documento == old('documento') ? $paciente->documento : old('documento') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombre" class="row-size-label">Nombres:</label>
                    <input type="text" name="nombre" class="row-size-input form-control-form" value="{{ old('nombre') == '' || $paciente->nombre == old('nombre') ? $paciente->nombre : old('nombre') }}">
                </div>
                <div class="col-md-6">
                    <label for="apellido" class="row-size-label">Apellidos:</label>
                    <input type="text" name="apellido" class="row-size-input form-control-form" value="{{ old('apellido') == '' || $paciente->apellido == old('apellido') ? $paciente->apellido : old('apellido') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="genero" class="row-size-label">Sexo:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="m" name="genero" value="MASCULINO" {{ $paciente->genero == 'MASCULINO' || old('genero') == 'MASCULINO'  ? "checked":"" }}>
                        <label class="custom-control-label" for="m">MASCULINO</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="f" name="genero" value="FEMENINO" {{ $paciente->genero == 'FEMENINO' || old('genero') == 'FEMENINO'  ? "checked":"" }}>
                        <label class="custom-control-label" for="f">FEMENINO</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="fechaNacimiento" class="row-size-label">Fecha de nacimiento:</label>
                    <input type="text" class="row-size-input form-control-form datepicker" name="fechaNacimiento" id="fechaN" autocomplete="off" value="{{ old('fechaNacimiento') == '' || $paciente->fechaNacimiento == old('fechaNacimiento') ? $paciente->fechaNacimiento : old('fechaNacimiento') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="edad" class="row-size-label">Edad:</label>
                    <input type="text" name="edad2" id="edadCalculada" class="row-size-input form-control-form" disabled>
                    <input type="hidden" id="edadCalculada2" name="edad">
                </div>
                <div class="col-md-6">
                    <label for="lugarNacimiento" class="row-size-label">Lugar de nacimiento:</label>
                    <input type="text" name="lugarNacimiento" class="row-size-input form-control-form" value="{{ old('lugarNacimiento') == '' || $paciente->lugarNacimiento == old('lugarNacimiento') ? $paciente->lugarNacimiento : old('lugarNacimiento') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="direccionDomicilio" class="row-size-label" >
                        Dirección de domicilio:
                    </label>
                    <input type="text" name="direccionDomicilio" class="row-size-input form-control-form" value="{{ old('direccionDomicilio') == '' || $paciente->direccionDomicilio == old('direccionDomicilio') ? $paciente->direccionDomicilio : old('direccionDomicilio') }}">
                </div>
                <div class="col-md-6">
                    <label for="ciudad" class="row-size-label">
                        Ciudad:
                    </label>
                    <input type="text" name="ciudadDomicilio" class="row-size-input form-control-form" value="{{ old('ciudadDomicilio') == '' || $paciente->ciudadDomicilio == old('ciudadDomicilio') ? $paciente->ciudadDomicilio : old('ciudadDomicilio') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="direccionDomicilio" class="row-size-label">
                        Teléfono celular:
                    </label>
                    <input type="text" name="celular" class="row-size-input form-control-form" value="{{ old('celular') == '' || $paciente->celular == old('celular') ? $paciente->celular : old('celular') }}">
                </div>
                <div class="col-md-6">
                    <label for="ciudad" class="row-size-label">
                        Teléfono fijo:
                    </label>
                    <input type="text" name="telefono" class="row-size-input form-control-form" value="{{ old('telefono') == '' || $paciente->telefono == old('telefono') ? $paciente->telefono : old('telefono') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="cargo" class="row-size-label">
                        Cargo:
                    </label>
                    <input type="text" name="cargo" class="row-size-input form-control-form" value="{{ old('cargo') == '' || $paciente->cargo == old('cargo') ? $paciente->cargo : old('cargo') }}">
                </div>
                <div class="col-md-6">
                    <label for="antiguedad" class="row-size-label">
                        Antiguedad:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="antiguedad">
                            <option value=""></option>
                            <option value="< 1 año" {{ old('antiguedad') == '< 1 año' || $paciente->antiguedad == '< 1 año' ? "selected":""}}>< 1 año</option>
                            <option value="1 - 5 años" {{ old('antiguedad') == '1 - 5 años' || $paciente->antiguedad == '1 - 5 años' ? "selected":""}}>1 - 5 años</option>
                            <option value="5 - 10 años" {{ old('antiguedad') == '5 - 10 años' || $paciente->antiguedad == '5 - 10 años' ? "selected":""}}>5 - 10 años</option>
                            <option value="> 10 años" {{ old('antiguedad') == '> 10 años' || $paciente->antiguedad == '> 10 años' ? "selected":""}}>> 10 años</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="email" class="row-size-label">
                        Correo electrónico:
                    </label>
                    <input type="text" name="correoElectronico" class="row-size-input form-control-form" value="{{ old('correoElectronico') == '' || $paciente->correoElectronico == old('correoElectronico') ? $paciente->correoElectronico : old('correoElectronico') }}">
                </div>
                <div class="col-md-6">
                    <label for="escolaridad" class="row-size-label">
                        Nivel de escolaridad:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="escolaridad">
                            <option value=""></option>
                            <option value="ANALFABETA" {{ old('escolaridad') == 'ANALFABETA' || $paciente->escolaridad == 'ANALFABETA' ? "selected":""}}>ANALFABETA</option>
                            <option value="PRIMARIA" {{ old('escolaridad') == 'PRIMARIA' || $paciente->escolaridad == 'PRIMARIA' ? "selected":""}}>PRIMARIA</option>
                            <option value="SECUNDARIA" {{ old('escolaridad') == 'SECUNDARIA' || $paciente->escolaridad == 'SECUNDARIA' ? "selected":""}}>SECUNDARIA</option>
                            <option value="TECNICO" {{ old('escolaridad') == 'TECNICO' || $paciente->escolaridad == 'TECNICO' ? "selected":""}}>TÉCNICO</option>
                            <option value="TECNOLOGO" {{ old('escolaridad') == 'TECNOLOGO' || $paciente->escolaridad == 'TECNOLOGO' ? "selected":""}}>TECNÓLOGO</option>
                            <option value="UNIVERSITARIO" {{ old('escolaridad') == 'UNIVERSITARIO' || $paciente->escolaridad == 'UNIVERSITARIO' ? "selected":""}}>UNIVERSITARIO</option>
                            <option value="ESPECIALIZACION" {{ old('escolaridad') == 'ESPECIALIZACION' || $paciente->escolaridad == 'ESPECIALIZACION' ? "selected":""}}>ESPECIALIZACIÓN</option>
                            <option value="MAESTRIA" {{ old('escolaridad') == 'MAESTRIA' || $paciente->escolaridad == 'MAESTRIA' ? "selected":""}}>MAESTRIA</option>
                            <option value="DOCTORADO" {{ old('escolaridad') == 'DOCTORADO' || $paciente->escolaridad == 'DOCTORADO' ? "selected":""}}>DOCTORADO</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="estadoCivil" class="row-size-label">
                        Estado civil:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect2" name="estadoCivil">
                            <option value=""></option>
                                <option value="SOLTERO" {{ old('estadoCivil') == 'SOLTERO' || $paciente->estadoCivil == 'SOLTERO' ? "selected":""}}>SOLTERO</option>
                                <option value="CASADO" {{ old('estadoCivil') == 'CASADO' || $paciente->estadoCivil == 'CASADO' ? "selected":""}}>CASADO</option>
                                <option value="UNIONLIBRE" {{ old('estadoCivil') == 'UNIONLIBRE' || $paciente->estadoCivil == 'UNIONLIBRE' ? "selected":""}}>UNIÓN LIBRE</option>
                                <option value="SEPARADO" {{ old('estadoCivil') == 'SEPARADO' || $paciente->estadoCivil == 'SEPARADO' ? "selected":""}}>SEPARADO</option>
                                <option value="VIUDO" {{ old('estadoCivil') == 'VIUDO' || $paciente->estadoCivil == 'VIUDO' ? "selected":""}}>VIUDO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="eps" class="row-size-label">
                        EPS:
                    </label>
                    <input type="text" name="eps" class="row-size-input form-control-form" value="{{ old('eps') == '' || $paciente->eps == old('eps') ? $paciente->eps : old('eps') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="arl" class="row-size-label">
                        ARL:
                    </label>
                    <input type="text" name="arl" class="row-size-input form-control-form" value="{{ old('arl') == '' || $paciente->arl == old('arl') ? $paciente->arl : old('arl') }}">
                </div>
                <div class="col-md-6">
                    <label for="afp" class="row-size-label">
                        AFP:
                    </label>
                    <input type="text" name="afp" class="row-size-input form-control-form" value="{{ old('afp') == '' || $paciente->afp == old('afp') ? $paciente->afp : old('afp') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="grupoSanguineo" class="row-size-label">
                        Grupo sanguineo:
                    </label>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="A" name="grupoSanguineo" value="A" {{ $paciente->grupoSanguineo == 'A' || old('grupoSanguineo') == 'A'  ? "checked":"" }}>
                            <label class="custom-control-label" for="A">A</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="B" name="grupoSanguineo" value="B" {{ $paciente->grupoSanguineo == 'B' || old('grupoSanguineo') == 'B'  ? "checked":"" }}>
                            <label class="custom-control-label" for="B">B</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="AB" name="grupoSanguineo" value="AB" {{ $paciente->grupoSanguineo == 'AB' || old('grupoSanguineo') == 'AB'  ? "checked":"" }}>
                            <label class="custom-control-label" for="AB">AB</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="O" name="grupoSanguineo" value="O" {{ $paciente->grupoSanguineo == 'O' || old('grupoSanguineo') == 'O'  ? "checked":"" }}>
                            <label class="custom-control-label" for="O">O</label>
                        </div>
                </div>
                <div class="col-md-6">
                    <label for="rh" class="row-size-label">
                        RH:
                    </label>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="P" name="rh" value="POSITIVO" {{ $paciente->rh == 'POSITIVO' || old('rh') == 'POSITIVO'  ? "checked":"" }}>
                            <label class="custom-control-label" for="P">POSITIVO</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="N" name="rh" value="NEGATIVO" {{ $paciente->rh == 'NEGATIVO' || old('rh') == 'NEGATIVO'  ? "checked":"" }}>
                            <label class="custom-control-label" for="N">NEGATIVO</label>
                        </div>
                </div>
            </div>

            <h5 class="title-sections" id="sub-title">INFORMACIÓN DEL ACOMPAÑANTE</h5>

            <div class="row">
                <div class="col-md-6">
                    <label for="nombreAcompaniante" class="row-size-label">
                        Nombres:
                    </label>
                    <input type="text" name="nombreAcompaniante" class="row-size-input form-control-form" value="{{ old('nombreAcompaniante') == '' || $paciente->nombreAcompaniante == old('nombreAcompaniante') ? $paciente->nombreAcompaniante : old('nombreAcompaniante') }}">
                </div>
                <div class="col-md-6">
                    <label for="apellidoAcompaniante" class="row-size-label">
                        Apellidos:
                    </label>
                    <input type="text" name="apellidoAcompaniante" class="row-size-input form-control-form" value="{{ old('apellidoAcompaniante') == '' || $paciente->apellidoAcompaniante == old('apellidoAcompaniante') ? $paciente->apellidoAcompaniante : old('apellidoAcompaniante') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="parentesco" class="row-size-label">
                        Parentesco:
                    </label>
                    <div class="form-group row-size-input div-select-form">
                        <select class="select-form form-control" id="exampleFormControlSelect1" name="parentescoAcompaniante">
                            <option value=""></option>
                            <option value="MADRE" {{ old('parentescoAcompaniante') == 'MADRE' || $paciente->parentescoAcompaniante == 'MADRE' ? "selected":""}}>MADRE</option>
                            <option value="PADRE" {{ old('parentescoAcompaniante') == 'PADRE' || $paciente->parentescoAcompaniante == 'PADRE' ? "selected":""}}>PADRE</option>
                            <option value="ESPOS@" {{ old('parentescoAcompaniante') == 'ESPOS@' || $paciente->parentescoAcompaniante == 'ESPOS@' ? "selected":""}}>ESPOS@</option>
                            <option value="NOVI@" {{ old('parentescoAcompaniante') == 'NOVI@' || $paciente->parentescoAcompaniante == 'NOVI@' ? "selected":""}}>NOVI@</option>
                            <option value="HERMAN@" {{ old('parentescoAcompaniante') == 'HERMAN@' || $paciente->parentescoAcompaniante == 'HERMAN@' ? "selected":""}}>HERMAN@</option>
                            <option value="PRIM@" {{ old('parentescoAcompaniante') == 'PRIM@' || $paciente->parentescoAcompaniante == 'PRIM@' ? "selected":""}}>PRIM@</option>
                            <option value="AMIG@" {{ old('parentescoAcompaniante') == 'AMIG@' || $paciente->parentescoAcompaniante == 'AMIG@' ? "selected":""}}>AMIG@</option>
                            <option value="TI@" {{ old('parentescoAcompaniante') == 'TI@' || $paciente->parentescoAcompaniante == 'TI@' ? "selected":""}}>TI@</option>
                            <option value="HIJ@" {{ old('parentescoAcompaniante') == 'HIJ@' || $paciente->parentescoAcompaniante == 'HIJ@' ? "selected":""}}>HIJ@</option>
                            <option value="ABUEL@" {{ old('parentescoAcompaniante') == 'ABUEL@' || $paciente->parentescoAcompaniante == 'ABUEL@' ? "selected":""}}>ABUEL@</option>
                            <option value="CUÑAD@" {{ old('parentescoAcompaniante') == 'CUÑAD@' || $paciente->parentescoAcompaniante == 'CUÑAD@' ? "selected":""}}>CUÑAD@</option>
                            <option value="EMPRESA" {{ old('parentescoAcompaniante') == 'EMPRESA' || $paciente->parentescoAcompaniante == 'EMPRESA' ? "selected":""}}>EMPRESA</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="celularA" class="row-size-label">
                        Teléfono celular:
                    </label>
                    <input type="text" name="celularAcompaniante" class="row-size-input form-control-form" value="{{ old('celularAcompaniante') == '' || $paciente->celularAcompaniante == old('celularAcompaniante') ? $paciente->celularAcompaniante : old('celularAcompaniante') }}">
                </div>
            </div>

            @if($paciente->firma != '')
                <div class="row" style="padding-top: 2%">
                    <div class="offset-md-6 col-md-6" style="width: 100%">
                        <img id="firma-img" src="data:image/png;base64, {{$paciente->firma}}" class="img-firma"/>
                        <input id="firma" type="hidden" name="firma" value="{{$paciente->firma}}">
                    </div>
                </div>

                <div class="row">
                    <div class="offset-md-6 col-md-2">
                        <input type="button" id="eliminar-firma" class="btn button-form button-delete btn-primary" value="Eliminar firma">
                    </div>
                </div>
            @endif
        </div>
        <input type="hidden" name="attribute" value="{{ isset($paciente) ? encrypt($paciente->id) : old('id') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
    @endif
@stop
