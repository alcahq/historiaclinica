@extends('layouts')
@section('title')
    <title>Empresa - Buscar</title>
@stop
@section('contenido')
    <div class="container-local">
        <h4 class="title-principal">BUSCAR EMPRESA</h4>
    </div>

    <div class="container-local border-container">
        <form method="POST" action="{{ route('empresa.findByNitOrName') }}">
            {!! csrf_field() !!}
            <div class="row find-row">
                <div class="col-md-3 col-find-input">
                    <input type="text" name="buscar_empresa" class="form-control find-input" placeholder="Nit o nombre">
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Buscar" class="button-form btn btn-primary find-button">
                </div>
            </div>
        </form>
        
        <table class="table table-hover table-bordered">
            <thead class="thead-light">
            <tr>
                <th scope = "col">#</th>
                <th scope = "col">Nit</th>
                <th scope = "col">Nombre</th>
                <th scope = "col">Dirección</th>
                <th scope = "col">Teléfono</th>
                <th scope = "col">Editar</th>
            </tr>
            </thead>
            <tbody>
            @isset($empresas)
                @forelse($empresas as $empresa)
                    <tr>
                        <td scope="row" class="count"></td>
                        <td>{{ $empresa->nit }}</td>
                        <td>{{ $empresa->nombre }}</td>
                        <td>{{ $empresa->direccion }}</td>
                        <td>{{ $empresa->telefono }}</td>
                        <td><a href="{{ route('empresa.edit', encrypt($empresa->id)) }}">Editar</a></td>
                    </tr>
                @empty
                    <h1>No se encontraron resultados</h1>
                @endforelse
            @endisset
            </tbody>
        </table>
    </div>
@endsection
