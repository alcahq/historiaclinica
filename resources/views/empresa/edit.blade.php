@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Empresa - Editar</title>
@stop
@section('contenido')
    @if(count($errors) > 0)
        <div class="container-local">
            <div class="toast toast-local" data-autohide="false">
                <div class="toast-header">
                    <strong class="mr-auto">Alerta</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('UpdateCompany'))
        <div class="container-local">
            <h1>{{ session()->get('UpdateCompany') }}</h1>
        </div>
    @else
    <form method="POST" action="{{ route('empresa.update', encrypt($empresa->id)) }}">
        {!! csrf_field() !!}}
        <div class="container-local">
            <h4 class="title-pass">EDITAR DATOS DE EMPRESA</h4>
        </div>

        <div class="container-pass border-container">
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="nit" class="row-size-pass" placeholder="Nit">Nit</label>
                    <input type="text" class="form-control-form row-size-pass" name="nit" value="{{ old('nit') == '' || $empresa->nit == old('nit') ? $empresa->nit : old('nit') }}">
                </div>
            </div>
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="nombre" class="row-size-pass">Nombre</label>
                    <input type="text" class="form-control-form row-size-pass" name="nombre" value="{{ old('nombre') == '' || $empresa->nombre == old('nombre') ? $empresa->nombre : old('nombre') }}">
                </div>
            </div>
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="documento usuario" class="row-size-pass">Persona de contacto</label>
                    <input type="text" class="form-control-form row-size-pass" name="contacto" value="{{ old('contacto') == '' || $empresa->contacto == old('contacto') ? $empresa->contacto : old('contacto') }}">
                </div>
            </div>
            <div class="row padding-bottom-pass">
                <div class="col-md-10 offset-md-1">
                    <label for="direccion" class="row-size-pass">Dirección</label>
                    <input type="text" class="form-control-form row-size-pass" name="direccion" value="{{ old('direccion') == '' || $empresa->direccion == old('direccion') ? $empresa->direccion : old('direccion') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <label for="telefono" class="row-size-pass">Teléfono</label>
                    <input type="text" class="form-control-form row-size-pass" name="telefono" value="{{ old('telefono') == '' || $empresa->telefono == old('telefono') ? $empresa->telefono : old('telefono') }}">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Guardar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
    @endif
@stop
