@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Reportes - Diagnostico</title>
@stop
@section('contenido')
    @if(session()->has('busquedaReporte'))
        <div class="container-local">
            <h1>{{ session()->get('busquedaReporte') }}</h1>
        </div>
    @else
         @if (count($errors) > 0)
        <div class="container-local">
            <div class="toast toast-local" id="toastRerpote" data-autohide="false">
                <div class="toast-header">
                    <strong class="mr-auto">Alerta</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
        <form action="{{ route('historia.generarDiag') }}" method="POST">
            {!! csrf_field() !!}
            <div class="container-local">
                <h4 class="title-pass">GENERAR REPORTE DE DIAGNOSTICO</h4>
            </div>

            <div class="container-pass border-container">
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="empresaActual" class="row-size-pass">Empresa:</label>
                        <div class="form-group row-size-pass div-select-form">
                            <select class="select-form form-control" id="exampleFormControlSelect1" name="empresa_id">
                                <option value=""></option>
                                @foreach($empresas as $empresa)
                                    <option value="{{ $empresa->id }}">{{ $empresa->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="fecha desde" class="row-size-pass">Fecha desde:</label>
                        <input type="text" class="row-size-pass form-control-form datepicker" name="fechaDesde" id="fechaDesde" autocomplete="off" style="padding: 3px">
                    </div>
                </div>
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="fecha hasta" class="srow-size-pass">Fecha hasta:</label>
                        <input type="text" class="row-size-pass form-control-form datepicker" name="fechaHasta" id="fechaHasta" autocomplete="off" style="padding: 3px">
                    </div>
                </div>
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1" style="padding-right: 0">
                        <label for="motivo evaluacion" class="srow-size-pass">Motivo evaluación:</label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="motivoEvaluacion[]" value="INGRESO">
                            <label class="form-check-label" for="inlineCheckbox1">Ingreso</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="motivoEvaluacion[]" value="PERIÓDICO">
                            <label class="form-check-label" for="inlineCheckbox2">Periódico</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="motivoEvaluacion[]" value="RETIRO">
                            <label class="form-check-label" for="inlineCheckbox1">Retiro</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="motivoEvaluacion[]" value="POSTINCAPACIDAD">
                            <label class="form-check-label" for="inlineCheckbox1">Postincapacidad</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2 offset-md-5">
                        <input type="submit" value="Generar" class="button-form btn btn-primary">
                    </div>
                </div>
            </div>
        </form>
    @endif
@stop
