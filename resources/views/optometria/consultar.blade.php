@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
    <script src="{{asset('/js/historiaOptometria.js')}}"></script>
@stop
@section('title')
    <title>Historia Optometria - Consultar</title>
@stop
@section('contenido')
    @if(session()->has('registerHC'))
        <div class="container-local">
            <h1>{{ session()->get('registerHC') }}</h1>
        </div>
    @else
        <div class="container-local border-container">
            <div class="toast toast-local" data-autohide="false" id="toastHO">
                <div class="toast-header">
                    <strong class="mr-auto">Alerta</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 padd-label" style="padding-right: 0">
                    <label for="fecha" class="font-bold">Fecha:</label>
                    <input value="{{ $historia->fecha }}" id="fechaRegistro" name="fecha" class="form-control-form border-white" >
                    <label for="fecha" class="font-bold" hidden="true" style="padding-right: 8px">Paciente:</label>
                    <input class="form-control-form" type="hidden" name="documento" id="documentoBuscado" value="{{ $historia->paciente_cedula }}">
                </div>
                <div class="col-md-6 padd-row-ppl padd-label">
                    <label for="fecha" class="font-bold padd-label width-lbl">Médico:</label>
                    <input value="{{ $historia['medicoapertura']->nombre_completo }}" id="medicoNombre" class="form-control-form border-white size-input-header" style="color: #003594;" disabled>
                    <input value="{{"CC: ".$historia['medicoapertura']->document }}" id="medicoCedula" class="form-control-form border-white size-input-header" disabled style="color: #003594;">
                    <input value="{{auth()->user()->id }}" id="user_id" type="hidden">
                    <label for="fecha" class="font-bold padd-label width-lbl">Nombre:</label>
                    <input class="form-control-form border-white size-input-header" id="nombrePaciente" value="{{ $historia->paciente }}" style="color: #003594;" disabled>
                    <input class="form-control-form border-white size-input-header" id="documentoPaciente" value="{{ "CC: ".$historia->paciente_cedula }}" style="color: #003594; width: 20%" disabled>
                    <input type="text" class="form-control-form border-white" value="{{ "Edad: " . $historia->paciente_edad . " años" }}" id="edadPaciente" style="color: #003594; width: 20%" disabled>
                    <input type="hidden" id="estadoHO" value="{{ $historia->estado }}">
                </div>
                <div class="col-md-3">
                    <div class="container-estado">
                        <div class="row">
                            <div class="col-md-10 offset-1 col-title-estado">
                                <span class="">Estado: {{ $historia->estado }}</span>
                            </div>
                            @if($historia->estado != "Anulada" and auth()->user()->hasRoles(['admin', 'optometra']))
                                <div class="col-md-10 offset-1" id="estado-btn">
                                    <input type="submit" value="Anular" id="anularHO" class="button-form btn btn-primary">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 1%">
                <div class="col-md-2" style="padding-right: 0; padding-top: 6px">
                    <label for="motivo-evaluacion" class="font-bold" style="padding-right: 8px">Motivo de la evaluación:</label>
                </div>
                <div class="col-md-3" style="padding-right: 0; padding-bottom: 1%; padding-left: 0">
                    <div class="form-group size-motivoE div-select-form">
                        <select class="select-form form-control" id="motivoE" name="motivoEvaluacion" style="font-size: 0.8rem">
                            <option></option>
                            <option value="INGRESO" {{ old('motivoEvaluacion') == 'INGRESO' || $historia->motivoevaluacion == 'INGRESO' ? "selected":""}}>INGRESO</option>
                            <option value="PERIÓDICO" {{ old('motivoEvaluacion') == 'PERIÓDICO' || $historia->motivoevaluacion == 'PERIÓDICO' ? "selected":""}}>PERIÓDICO</option>
                            <option value="RETIRO" {{ old('motivoEvaluacion') == 'RETIRO' || $historia->motivoevaluacion == 'RETIRO' ? "selected":""}}>RETIRO</option>
                            <option value="POSTINCAPACIDAD" {{ old('motivoEvaluacion') == 'POSTINCAPACIDAD' || $historia->motivoevaluacion == 'POSTINCAPACIDAD' ? "selected":""}}>POSTINCAPACIDAD</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-7">
                    <label for="cargo evaluar" class="font-bold" style="padding-left: 12px; padding-right: 5%">Cargo a evaluar:</label>
                    <input type="text" name="cargoEvaluar" id="cargoE" class="size-input-cargo form-control-form" value="{{ $historia->cargo }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-1" style="padding-right: 0">
                    <label for="rh" class="font-bold">Otros:</label>
                </div>
                <div class="col-md-11" style="padding-right: 0; padding-left: 0">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Trabajo en alturas" {{ strpos($historia->motivoevaluacion_otro, 'Trabajo en alturas') !== false || old('otros[]') == 'Trabajo en alturas'  ? "checked":"" }}>
                        <label class="form-check-label" for="Trabajo en alturas">Trabajo en alturas</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Manipulación de alimentos" {{ strpos($historia->motivoevaluacion_otro, 'Manipulación de alimentos')!== false || old('otros[]') == 'Manipulación de alimentos'  ? "checked":"" }}>
                        <label class="form-check-label" for="Manipulación de alimentos">Manipulación de alimentos</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Enfasis osteomuscular" {{ strpos($historia->motivoevaluacion_otro, 'Enfasis osteomuscular')!== false || old('otros[]') == 'Enfasis osteomuscular'  ? "checked":"" }}>
                        <label class="form-check-label" for="Enfasis osteomuscular">Enfasis osteomuscular</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="otros[]" value="Reubicación laboral" {{ strpos($historia->motivoevaluacion_otro, 'Reubicación laboral')!== false || old('otros[]') == 'Reubicación laboral'  ? "checked":"" }}>
                        <label class="form-check-label" for="Reubicación laboral">Reubicación laboral</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <label for="empresaActual" class="font-bold" style="margin-top: 4px">Empresa:</label>
                </div>
                <div class="col-md-9" style="padding-left: 0">
                    <input type="text" name="empresa" id="empresaActual" class="size-input-emp form-control-form" autocomplete="off" value="{{ $historia['empresa']->nombre }}" style="padding: 3px 10px">
                </div>
            </div>
        </div>
        <div class="container-local">
            <ul class="nav nav-tabs" id="tabHC" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="antecedentes-tab" data-toggle="tab" href="#antecedentes" role="tab" aria-controls="antecedentes" aria-selected="true">Antecedentes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="sintomas-tab" data-toggle="tab" href="#sintomas" role="tab" aria-controls="sintomas" aria-selected="false">Sintomas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="riesgocargoevaluar-tab" data-toggle="tab" href="#riesgocargoevaluar" role="tab" aria-controls="riesgocargoevaluar" aria-selected="false">Riesgos de cargo a evaluar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="agudezavisual-tab" data-toggle="tab" href="#agudezavisual" role="tab" aria-controls="agudezavisual" aria-selected="true">Agudeza visual</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="recomendaciones-tab" data-toggle="tab" href="#recomendaciones" role="tab" aria-controls="recomendaciones" aria-selected="false">Conducta y recomendaciones</a>
                </li>
            </ul>
            <div class="tab-content border-tabs" id="myTabContent">
                <div class="tab-pane fade show active" id="antecedentes" role="tabpanel" aria-labelledby="antecedentes-tab">
                    <h5 class="title-sections" id="sub-title">ANTECEDENTES PERSONALES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6">

                                <label for="hta" class="width-lbl-antecedentesOpto">Diabetes:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_diabetes" name="antecedentes_diabetes" value="Si" {{ $historia['antecedente']->diabetes == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_diabetes">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_diabetes" name="antecedentes_diabetes" value="No" {{ $historia['antecedente']->diabetes == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_diabetes">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_diabetes_obs" id="antecedentes_diabetes_obs" value="{{ $historia['antecedente']->diabetes_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Problemas cardiacos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_problemascardiacos" name="antecedentes_problemascardiacos" value="Si" {{ $historia['antecedente']->problemascardiacos == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_problemascardiacos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_problemascardiacos" name="antecedentes_problemascardiacos" value="No" {{ $historia['antecedente']->problemascardiacos == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_problemascardiacos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_problemascardiacos_obs" id="antecedentes_problemascardiacos_obs" value="{{ $historia['antecedente']->problemascardiacos_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Alergicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_alergicos" name="antecedentes_alergicos" value="Si" {{ $historia['antecedente']->alergicos == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_alergicos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_alergicos" name="antecedentes_alergicos" value="No" {{ $historia['antecedente']->alergicos == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_alergicos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_alergicos_obs" id="antecedentes_alergicos_obs" value="{{ $historia['antecedente']->alergicos_obs }}">
                            </div>
                            <div class="col-md-6">
                                <label for="hta" class="width-lbl-antecedentesOpto">Hipertensión:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_hipertension" name="antecedentes_hipertension" value="Si" {{ $historia['antecedente']->hipertension == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_hipertension">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_hipertension" name="antecedentes_hipertension" value="No" {{ $historia['antecedente']->hipertension == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_hipertension">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_hipertension_obs" id="antecedentes_hipertension_obs"  value="{{ $historia['antecedente']->hipertension_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Alteraciones de tiroides:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_alteraciontiroides" name="antecedentes_alteraciontiroides" value="Si" {{ $historia['antecedente']->alteraciontiroides == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_alteraciontiroides">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_alteraciontiroides" name="antecedentes_alteraciontiroides" value="No" {{ $historia['antecedente']->alteraciontiroides == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_alteraciontiroides">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_alteraciontiroides_obs" id="antecedentes_alteraciontiroides_obs"  value="{{ $historia['antecedente']->alteraciontiroides_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Otros:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_personales_otros" name="antecedentes_personales_otros" value="Si" {{ $historia['antecedente']->personales_otros == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_personales_otros">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_personales_otros" name="antecedentes_personales_otros" value="No" {{ $historia['antecedente']->personales_otros == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_personales_otros">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_personales_otros_obs" id="antecedentes_personales_otros_obs" value="{{ $historia['antecedente']->personales_otros_obs }}">

                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">ANTECEDENTES OCULARES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="hta" class="width-lbl-antecedentesOpto">Usuarios RX:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_usuariosrx" name="antecedentes_usuariosrx" value="Si" {{ $historia['antecedente']->usuariosrx == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_usuariosrx">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_usuariosrx" name="antecedentes_usuariosrx" value="No" {{ $historia['antecedente']->usuariosrx == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_usuariosrx">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_usuariosrx_obs" id="antecedentes_usuariosrx_obs" value="{{ $historia['antecedente']->usuariosrx_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Glaucoma:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_glaucoma" name="antecedentes_glaucoma" value="Si" {{ $historia['antecedente']->glaucoma == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_glaucoma">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_glaucoma" name="antecedentes_glaucoma" value="No" {{ $historia['antecedente']->glaucoma == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_glaucoma">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_glaucoma_obs" id="antecedentes_glaucoma_obs" value="{{ $historia['antecedente']->glaucoma_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Quirurgicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_quirurgicos" name="antecedentes_quirurgicos" value="Si" {{ $historia['antecedente']->quirurgicos == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_quirurgicos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_quirurgicos" name="antecedentes_quirurgicos" value="No" {{ $historia['antecedente']->quirurgicos == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_quirurgicos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_quirurgicos_obs" id="antecedentes_quirurgicos_obs" value="{{ $historia['antecedente']->quirurgicos_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Rehabilitación visual:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_rehabilitacionvisual" name="antecedentes_rehabilitacionvisual" value="Si" {{ $historia['antecedente']->rehabilitacionvisual == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_rehabilitacionvisual">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_rehabilitacionvisual" name="antecedentes_rehabilitacionvisual" value="No" {{ $historia['antecedente']->rehabilitacionvisual == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_rehabilitacionvisual">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_rehabilitacionvisual_obs" id="antecedentes_rehabilitacionvisual_obs" value="{{ $historia['antecedente']->rehabilitacionvisual_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Trauma:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_trauma" name="antecedentes_trauma" value="Si" {{ $historia['antecedente']->trauma == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_trauma">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_trauma" name="antecedentes_trauma" value="No" {{ $historia['antecedente']->trauma == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_trauma">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_trauma_obs" id="antecedentes_trauma_obs" value="{{ $historia['antecedente']->trauma_obs }}">
                            </div>
                            <div class="col-md-6">
                                <label for="hta" class="width-lbl-antecedentesOpto">Cataratas:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_catarata" name="antecedentes_catarata" value="Si" {{ $historia['antecedente']->catarata == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_catarata">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_catarata" name="antecedentes_catarata" value="No" {{ $historia['antecedente']->catarata == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_catarata">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_catarata_obs" id="antecedentes_catarata_obs" value="{{ $historia['antecedente']->catarata_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Esquirlas:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_esquirlas" name="antecedentes_esquirlas" value="Si" {{ $historia['antecedente']->esquirlas == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_esquirlas">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_esquirlas" name="antecedentes_esquirlas" value="No" {{ $historia['antecedente']->esquirlas == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_esquirlas">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_esquirlas_obs" id="antecedentes_esquirlas_obs" value="{{ $historia['antecedente']->esquirlas_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Quimicos:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_quimicos" name="antecedentes_quimicos" value="Si" {{ $historia['antecedente']->quimicos == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_quimicos">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_quimicos" name="antecedentes_quimicos" value="No" {{ $historia['antecedente']->quimicos == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_quimicos">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_quimicos_obs" id="antecedentes_quimicos_obs" value="{{ $historia['antecedente']->quimicos_obs }}">

                                <label for="hta" class="width-lbl-antecedentesOpto">Otros:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siantecedentes_oculares_otros" name="antecedentes_oculares_otros" value="Si" {{ $historia['antecedente']->oculares_otros == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siantecedentes_oculares_otros">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noantecedentes_oculares_otros" name="antecedentes_oculares_otros" value="No" {{ $historia['antecedente']->oculares_otros == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noantecedentes_oculares_otros">No</label>
                                </div>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="antecedentes_oculares_otros_obs" id="antecedentes_oculares_otros_obs" value="{{ $historia['antecedente']->oculares_otros_obs }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sintomas" class="tab-pane fade">
                    <h5 class="title-sections" id="sub-title">SINTOMAS</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-4 offset-2">
                                <label for="hta" class="width-lbl-sintomasOpto">Visión borrosa lejana:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_visionborrosalejana" name="sintomas_visionborrosalejana" value="Si" {{ $historia['sintoma']->visionborrosalejana == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_visionborrosalejana">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_visionborrosalejana" name="sintomas_visionborrosalejana" value="No" {{ $historia['sintoma']->visionborrosalejana == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_visionborrosalejana">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Visión borrosa cercana:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_visionborrosacercana" name="sintomas_visionborrosacercana" value="Si" {{ $historia['sintoma']->visionborrosacercana == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_visionborrosacercana">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_visionborrosacercana" name="sintomas_visionborrosacercana" value="No" {{ $historia['sintoma']->visionborrosacercana == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_visionborrosacercana">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Fotofobia:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_fotofobia" name="sintomas_fotofobia" value="Si" {{ $historia['sintoma']->fotofobia == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_fotofobia">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_fotofobia" name="sintomas_fotofobia" value="No" {{ $historia['sintoma']->fotofobia == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_fotofobia">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Dolor ocular:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_dolorocular" name="sintomas_dolorocular" value="Si" {{ $historia['sintoma']->dolorocular == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_dolorocular">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_dolorocular" name="sintomas_dolorocular" value="No" {{ $historia['sintoma']->dolorocular == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_dolorocular">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Visión doble:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_visiondoble" name="sintomas_visiondoble" value="Si" {{ $historia['sintoma']->visiondoble == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_visiondoble">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_visiondoble" name="sintomas_visiondoble" value="No" {{ $historia['sintoma']->visiondoble == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_visiondoble">No</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-offset-1">
                                <label for="hta" class="width-lbl-sintomasOpto">Cefalea:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_cefalea" name="sintomas_cefalea" value="Si" {{ $historia['sintoma']->cefalea == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_cefalea">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_cefalea" name="sintomas_cefalea" value="No" {{ $historia['sintoma']->cefalea == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_cefalea">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Enrojecimiento:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_enrojecimiento" name="sintomas_enrojecimiento" value="Si" {{ $historia['sintoma']->enrojecimiento == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_enrojecimiento">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_enrojecimiento" name="sintomas_enrojecimiento" value="No" {{ $historia['sintoma']->enrojecimiento == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_enrojecimiento">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Sueño al leer:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_suenoleer" name="sintomas_suenoleer" value="Si" {{ $historia['sintoma']->suenoleer == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_suenoleer">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_suenoleer" name="sintomas_suenoleer" value="No" {{ $historia['sintoma']->suenoleer == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_suenoleer">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">Ardor:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_ardor" name="sintomas_ardor" value="Si" {{ $historia['sintoma']->ardor == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_ardor">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_ardor" name="sintomas_ardor" value="No" {{ $historia['sintoma']->ardor == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_ardor">No</label>
                                </div>

                                <label for="hta" class="width-lbl-sintomasOpto">No refiere:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="sisintomas_norefiere" name="sintomas_norefiere" value="Si" {{ $historia['sintoma']->norefiere == 'Si' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="sisintomas_norefiere">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="nosintomas_norefiere" name="sintomas_norefiere" value="No" {{ $historia['sintoma']->norefiere == 'No' ? "checked" : "" }}>
                                    <label class="custom-control-label" for="nosintomas_norefiere">No</label>
                                </div>
                            </div>
                            <div class="col-md-12 offset-2">
                                <label for="hta" style="width: 5%">Otros:</label>
                                <input type="text" class="form-control-form" placeholder="Observaciones" name="sintomas_otros" id="sintomas_otros" style="width: 49%" value="{{ $historia['sintoma']->otros }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="riesgocargoevaluar" class="tab-pane fade">
                    <h5 class="title-sections" id="sub-title">RIESGOS DE CARGO A EVALUAR</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-4 offset-2">
                                <label for="hta" class="width-lbl-riesgosOpto">Trauma:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="riesgocargoevaluar_trauma" name="riesgocargoevaluar_trauma" value="Si" {{ $historia['riesgocargoevaluar']->trauma == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="riesgocargoevaluar_trauma">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_trauma" name="riesgocargoevaluar_trauma" value="No" {{ $historia['riesgocargoevaluar']->trauma == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_trauma">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Exposicion material particulado:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_exposicionmaterialparticulado" name="riesgocargoevaluar_exposicionmaterialparticulado" value="Si" {{ $historia['riesgocargoevaluar']->exposicionmaterialparticulado == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_exposicionmaterialparticulado">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_exposicionmaterialparticulado" name="riesgocargoevaluar_exposicionmaterialparticulado" value="No" {{ $historia['riesgocargoevaluar']->exposicionmaterialparticulado == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_exposicionmaterialparticulado">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Iluminación:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_iluminacion" name="riesgocargoevaluar_iluminacion" value="Si" {{ $historia['riesgocargoevaluar']->iluminacion == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_iluminacion">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_iluminacion" name="riesgocargoevaluar_iluminacion" value="No" {{ $historia['riesgocargoevaluar']->iluminacion == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_iluminacion">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Exposicion a videoterminales:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_exposicionvideoterminales" name="riesgocargoevaluar_exposicionvideoterminales" value="Si" {{ $historia['riesgocargoevaluar']->exposicionvideoterminales == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_exposicionvideoterminales">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_exposicionvideoterminales" name="riesgocargoevaluar_exposicionvideoterminales" value="No" {{ $historia['riesgocargoevaluar']->exposicionvideoterminales == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_exposicionvideoterminales">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Exposicion a quimicos solventes:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_exposicionquimicossolventes" name="riesgocargoevaluar_exposicionquimicossolventes" value="Si" {{ $historia['riesgocargoevaluar']->exposicionquimicossolventes == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_exposicionquimicossolventes">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_exposicionquimicossolventes" name="riesgocargoevaluar_exposicionquimicossolventes" value="No" {{ $historia['riesgocargoevaluar']->exposicionquimicossolventes == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_exposicionquimicossolventes">No</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="hta" class="width-lbl-riesgosOpto">Exposicion a gases vapores:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_exposiciongasesvapores" name="riesgocargoevaluar_exposiciongasesvapores" value="Si" {{ $historia['riesgocargoevaluar']->exposiciongasesvapores == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_exposiciongasesvapores">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_exposiciongasesvapores" name="riesgocargoevaluar_exposiciongasesvapores" value="No" {{ $historia['riesgocargoevaluar']->exposiciongasesvapores == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_exposiciongasesvapores">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Radiaciones ionizantes:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_radiacionesionizantes" name="riesgocargoevaluar_radiacionesionizantes" value="Si" {{ $historia['riesgocargoevaluar']->radiacionesionizantes == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_radiacionesionizantes">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_radiacionesionizantes" name="riesgocargoevaluar_radiacionesionizantes" value="No" {{ $historia['riesgocargoevaluar']->radiacionesionizantes == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_radiacionesionizantes">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Radiaciones  no ionizantes:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_radiacionesnoionizantes" name="riesgocargoevaluar_radiacionesnoionizantes" value="Si" {{ $historia['riesgocargoevaluar']->radiacionesnoionizantes == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_radiacionesnoionizantes">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_radiacionesnoionizantes" name="riesgocargoevaluar_radiacionesnoionizantes" value="No" {{ $historia['riesgocargoevaluar']->radiacionesnoionizantes == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_radiacionesnoionizantes">No</label>
                                </div>
                                <label for="hta" class="width-lbl-riesgosOpto">Exposicion a material proyección:</label>
                                <div class="custom-control custom-radio custom-control-inline" style="margin-right: 0.4rem;">
                                    <input type="radio" class="custom-control-input" id="siriesgocargoevaluar_exposicionmaterialproyeccion" name="riesgocargoevaluar_exposicionmaterialproyeccion" value="Si" {{ $historia['riesgocargoevaluar']->exposicionmaterialproyeccion == "Si" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="siriesgocargoevaluar_exposicionmaterialproyeccion">Si</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="noriesgocargoevaluar_exposicionmaterialproyeccion" name="riesgocargoevaluar_exposicionmaterialproyeccion" value="No" {{ $historia['riesgocargoevaluar']->exposicionmaterialproyeccion == "No" ? "checked" : "" }}>
                                    <label class="custom-control-label" for="noriesgocargoevaluar_exposicionmaterialproyeccion">No</label>
                                </div>
                                <label for="hta" class="width-lbl-antecedentesOpto">Otros:</label>
                                <input type="text" class="form-control-form width-input-antecedentesP" placeholder="Observaciones" name="riesgocargoevaluar_otros" id="riesgocargoevaluar_otros" value="{{ $historia['riesgocargoevaluar']->otros }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="agudezavisual" class="tab-pane fade">
                    <h5 class="title-sections" id="sub-title">LEJANA</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <h5 class="font-bold">Sin correción</h5>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_sincorreccion_ojoderecho">Ojo derecho: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" name="lejana_sincorreccion_ojoderecho" id="lejana_sincorreccion_ojoderecho">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->ls_od == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->ls_od== "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->ls_od == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->ls_od == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->ls_od == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->ls_od == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->ls_od == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->ls_od == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->ls_od == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->ls_od == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->ls_od == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->ls_od == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->ls_od == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->ls_od == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->ls_od == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="lejana_sincorreccion_ojoderecho_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->ls_od_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_sincorreccion_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lejana_sincorreccion_ojoizquierdo">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->ls_oi == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->ls_oi == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->ls_oi == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->ls_oi == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->ls_oi == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->ls_oi == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->ls_oi == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->ls_oi == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->ls_oi == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->ls_oi == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->ls_oi == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->ls_oi == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->ls_oi == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->ls_oi == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->ls_oi == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text"  id="lejana_sincorreccion_ojoizquierdo_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->ls_oi_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_sincorreccion_ojobinocular">Binocular: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lejana_sincorreccion_ojobinocular">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->ls_ojobinocular == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->ls_ojobinocular == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->ls_ojobinocular == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->ls_ojobinocular == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->ls_ojobinocular == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->ls_ojobinocular == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->ls_ojobinocular == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->ls_ojobinocular == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->ls_ojobinocular == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->ls_ojobinocular == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->ls_ojobinocular == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->ls_ojobinocular == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->ls_ojobinocular == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->ls_ojobinocular == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->ls_ojobinocular == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="lejana_sincorreccion_ojobinocular_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->ls_ojobinocular_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">

                                <div class="row">
                                    <h5 class="font-bold">Con correción</h5>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_concorreccion_ojoderecho">Ojo derecho: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lejana_concorreccion_ojoderecho">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->lc_od == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->lc_od == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->lc_od == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->lc_od == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->lc_od == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->lc_od == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->lc_od == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->lc_od == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->lc_od == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->lc_od == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->lc_od == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->lc_od == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->lc_od == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->lc_od == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->lc_od == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="lejana_concorreccion_ojoderecho_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->lc_od_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_concorreccion_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lejana_concorreccion_ojoizquierdo">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->lc_oi == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->lc_oi == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->lc_oi == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->lc_oi == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->lc_oi == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->lc_oi == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->lc_oi == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->lc_oi == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->lc_oi == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->lc_oi == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->lc_oi == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->lc_oi == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->lc_oi == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->lc_oi == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->lc_oi == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text"  id="lejana_concorreccion_ojoizquierdo_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->lc_oi_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lejana_concorreccion_ojobinocular">Binocular: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lejana_concorreccion_ojobinocular">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->lc_ojobinocular == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->lc_ojobinocular == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->lc_ojobinocular == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="20/1200" {{ $historia['agudezavisual']->lc_ojobinocular == "20/1200" ? "selected" : "" }}>20/1200</option>
                                                <option value="20/800" {{ $historia['agudezavisual']->lc_ojobinocular == "20/800" ? "selected" : "" }}>20/800</option>
                                                <option value="20/400" {{ $historia['agudezavisual']->lc_ojobinocular == "20/400" ? "selected" : "" }}>20/400</option>
                                                <option value="20/200" {{ $historia['agudezavisual']->lc_ojobinocular == "20/200" ? "selected" : "" }}>20/200</option>
                                                <option value="20/100" {{ $historia['agudezavisual']->lc_ojobinocular == "20/100" ? "selected" : "" }}>20/100</option>
                                                <option value="20/70" {{ $historia['agudezavisual']->lc_ojobinocular == "20/70" ? "selected" : "" }}>20/70</option>
                                                <option value="20/50" {{ $historia['agudezavisual']->lc_ojobinocular == "20/50" ? "selected" : "" }}>20/50</option>
                                                <option value="20/40" {{ $historia['agudezavisual']->lc_ojobinocular == "20/40" ? "selected" : "" }}>20/40</option>
                                                <option value="20/30" {{ $historia['agudezavisual']->lc_ojobinocular == "20/30" ? "selected" : "" }}>20/30</option>
                                                <option value="20/25" {{ $historia['agudezavisual']->lc_ojobinocular == "20/25" ? "selected" : "" }}>20/25</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->lc_ojobinocular == "20/20" ? "selected" : "" }}>20/20</option>
                                                <option value="20/15" {{ $historia['agudezavisual']->lc_ojobinocular == "20/15" ? "selected" : "" }}>20/15</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="lejana_concorreccion_ojobinocular_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->lc_ojobinocular_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">CERCANA</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <h5 class="font-bold">Sin correción</h5>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_sincorreccion_ojoderecho">Ojo derecho: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_sincorreccion_ojoderecho">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cs_od == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cs_od == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cs_od == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cs_od == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cs_od == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cs_od == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cs_od == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cs_od == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50M" {{ $historia['agudezavisual']->cs_od == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cs_od == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cs_od == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cs_od == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cs_od == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_sincorreccion_ojoderecho_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cs_od_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_sincorreccion_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_sincorreccion_ojoizquierdo">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cs_oi == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cs_oi == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cs_oi == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cs_oi == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cs_oi == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cs_oi == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cs_oi == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cs_oi == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50" {{ $historia['agudezavisual']->cs_oi == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cs_oi == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cs_oi == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cs_oi == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cs_oi == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_sincorreccion_ojoizquierdo_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cs_oi_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_sincorreccion_ojobinocular">Ojo binocular: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_sincorreccion_ojobinocular">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cs_ojobinocular == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cs_ojobinocular == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cs_ojobinocular == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cs_ojobinocular == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cs_ojobinocular == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cs_ojobinocular == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cs_ojobinocular == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cs_ojobinocular == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50M" {{ $historia['agudezavisual']->cs_ojobinocular == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cs_ojobinocular == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cs_ojobinocular == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cs_ojobinocular == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cs_ojobinocular == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_sincorreccion_ojobinocular_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cs_ojobinocular_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <h5 class="font-bold">Con correción</h5>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_concorreccion_ojoderecho">Ojo derecho: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_concorreccion_ojoderecho">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cc_od == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cc_od == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cc_od == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cc_od == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cc_od == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cc_od == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cc_od == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cc_od == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50M" {{ $historia['agudezavisual']->cc_od == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cc_od == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cc_od == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cc_od == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cc_od == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_concorreccion_ojoderecho_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cc_od_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_concorreccion_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_concorreccion_ojoizquierdo">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cc_oi == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cc_oi == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cc_oi == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cc_oi == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cc_oi == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cc_oi == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cc_oi == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cc_oi == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50M" {{ $historia['agudezavisual']->cc_oi == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cc_oi == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cc_oi == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cc_oi == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cc_oi == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_concorreccion_ojoizquierdo_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cc_oi_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="cercana_concorreccion_ojobinocular">Ojo binocular: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="cercana_concorreccion_ojobinocular">
                                                <option ></option>
                                                <option value="AMAUROSIS" {{ $historia['agudezavisual']->cc_ojobinocular == "AMAUROSIS" ? "selected" : "" }}>AMAUROSIS</option>
                                                <option value="P.L" {{ $historia['agudezavisual']->cc_ojobinocular == "P.L" ? "selected" : "" }}>P.L</option>
                                                <option value="C.D" {{ $historia['agudezavisual']->cc_ojobinocular == "C.D" ? "selected" : "" }}>C.D</option>
                                                <option value="No Titulo" {{ $historia['agudezavisual']->cc_ojobinocular == "No Titulo" ? "selected" : "" }}>No Titulo</option>
                                                <option value="TITULO" {{ $historia['agudezavisual']->cc_ojobinocular == "TITULO" ? "selected" : "" }}>TITULO</option>
                                                <option value="2M" {{ $historia['agudezavisual']->cc_ojobinocular == "2M" ? "selected" : "" }}>2M</option>
                                                <option value="1.75M" {{ $historia['agudezavisual']->cc_ojobinocular == "1.75M" ? "selected" : "" }}>1.75M</option>
                                                <option value="1.25M" {{ $historia['agudezavisual']->cc_ojobinocular == "1.25M" ? "selected" : "" }}>1.25M</option>
                                                <option value="1.50M" {{ $historia['agudezavisual']->cc_ojobinocular == "1.50M" ? "selected" : "" }}>1.50M</option>
                                                <option value="1.00M" {{ $historia['agudezavisual']->cc_ojobinocular == "1.00M" ? "selected" : "" }}>1.00M</option>
                                                <option value="0.75M" {{ $historia['agudezavisual']->cc_ojobinocular == "0.75M" ? "selected" : "" }}>0.75M</option>
                                                <option value="0.50M" {{ $historia['agudezavisual']->cc_ojobinocular == "0.50M" ? "selected" : "" }}>0.50M</option>
                                                <option value="20/20" {{ $historia['agudezavisual']->cc_ojobinocular == "20/20" ? "selected" : "" }}>20/20</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="cercana_concorreccion_ojobinocular_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia['agudezavisual']->cc_ojobinocular_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">LENSOMETRIA</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row" >
                                    <div class="col-md-3">
                                        <label for="lensometria_ojoderecho">Ojo derecho: </label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" id="lensometria_ojoderecho" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->lensometria_ojoderecho }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="lensometria_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text"  id="lensometria_ojoizquierdo" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->lensometria_ojoizquierdo }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lensometria_ojoderecho">ADD: </label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" id="lensometria_add" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->lensometria_add }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="lensometria_ojoizquierdo">Tipo de lente: </label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="lensometria_tipolente">
                                                <option ></option>
                                                <option value="MONOFOCAL" {{ $historia->lensometria_tipolente == "MONOFOCAL" ? "selected" : "" }}>MONOFOCAL</option>
                                                <option value="BIFOCAL" {{ $historia->lensometria_tipolente == "BIFOCAL" ? "selected" : "" }}>BIFOCAL</option>
                                                <option value="OCUPACIONAL" {{ $historia->lensometria_tipolente == "OCUPACIONAL" ? "selected" : "" }}>OCUPACIONAL</option>
                                                <option value="PROGRESIVO" {{ $historia->lensometria_tipolente == "PROGRESIVO" ? "selected" : "" }}>PROGRESIVO</option>
                                                <option value="DOS PARES" {{ $historia->lensometria_tipolente == "DOS PARES" ? "selected" : "" }}>DOS PARES</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">SEGMENTO ANTERIOR</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_cejas">Cejas: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_cejas">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_cejas == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_cejas == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_cejas_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_cejas_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_pestanas">Pestañas: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_pestanas">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_pestanas == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_pestanas == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="segmentoanterior_pestanas_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_pestanas_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_parpados">Parpados:</label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_parpados">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_parpados == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_parpados == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_parpados_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_parpados_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_viaslagrimales">Vias lagrimales: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_viaslagrimales">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_viaslagrimales == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_viaslagrimales == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="segmentoanterior_viaslagrimales_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_viaslagrimales_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_conjuntiva">Conjuntiva: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_conjuntiva">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_conjuntiva == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_conjuntiva == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_conjuntiva_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_conjuntiva_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_motilidadocular">Motilidad visual: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_motilidadocular">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_motilidadocular == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_motilidadocular == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="segmentoanterior_motilidadocular_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_motilidadocular_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_visioncolor">Visión color: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_visioncolor">
                                                <option></option>
                                                <option value="9/9" {{ $historia->segmentoanterior_visioncolor == "9/9" ? "selected" : "" }}>9/9</option>
                                                <option value="8/9" {{ $historia->segmentoanterior_visioncolor == "8/9" ? "selected" : "" }}>8/9</option>
                                                <option value="7/9" {{ $historia->segmentoanterior_visioncolor == "7/9" ? "selected" : "" }}>7/9</option>
                                                <option value="6/9" {{ $historia->segmentoanterior_visioncolor == "6/9" ? "selected" : "" }}>6/9</option>
                                                <option value="5/9" {{ $historia->segmentoanterior_visioncolor == "5/9" ? "selected" : "" }}>5/9</option>
                                                <option value="4/9" {{ $historia->segmentoanterior_visioncolor == "4/9" ? "selected" : "" }}>4/9</option>
                                                <option value="3/9" {{ $historia->segmentoanterior_visioncolor == "3/9" ? "selected" : "" }}>3/9</option>
                                                <option value="2/9" {{ $historia->segmentoanterior_visioncolor == "2/9" ? "selected" : "" }}>2/9</option>
                                                <option value="1/9" {{ $historia->segmentoanterior_visioncolor == "1/9" ? "selected" : "" }}>1/9</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_visioncolor_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_visioncolor_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_visionprofundidad">Visión profundidad: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_visionprofundidad">
                                                <option></option>
                                                <option value="ALTERADA800" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA800" ? "selected" : "" }}>ALTERADA800</option>
                                                <option value="ALTERADA400" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA400" ? "selected" : "" }}>ALTERADA400</option>
                                                <option value="ALTERADA200" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA200" ? "selected" : "" }}>ALTERADA200</option>
                                                <option value="ALTERADA140" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA140" ? "selected" : "" }}>ALTERADA140</option>
                                                <option value="ALTERADA100" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA100" ? "selected" : "" }}>ALTERADA100</option>
                                                <option value="ALTERADA80" {{ $historia->segmentoanterior_visionprofundidad == "ALTERADA80" ? "selected" : "" }}>ALTERADA80</option>
                                                <option value="NORMAL60" {{ $historia->segmentoanterior_visionprofundidad == "NORMAL60" ? "selected" : "" }}>NORMAL60</option>
                                                <option value="NORMAL50" {{ $historia->segmentoanterior_visionprofundidad == "NORMAL50" ? "selected" : "" }}>NORMAL50</option>
                                                <option value="NORMAL40" {{ $historia->segmentoanterior_visionprofundidad == "NORMAL40" ? "selected" : "" }}>NORMAL40</option>
                                                <option value="NORMAL20" {{ $historia->segmentoanterior_visionprofundidad == "NORMAL20" ? "selected" : "" }}>NORMAL20</option>
                                                <option value="AUSENTE" {{ $historia->segmentoanterior_visionprofundidad == "AUSENTE" ? "selected" : "" }}>AUSENTE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="segmentoanterior_visionprofundidad_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_visionprofundidad_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_cornea">Cornea: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_cornea">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_cornea == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_cornea == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text"  id="segmentoanterior_cornea_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_cornea_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_iris">Iris: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_iris">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_iris == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_iris == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_iris_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_iris_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_pupilas">Pupilas: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_pupilas">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_pupilas == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_pupilas == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_pupilas_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_pupilas_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_esclerotica">Esclerotica: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_esclerotica">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_esclerotica == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_esclerotica == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_esclerotica_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_esclerotica_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_camaraanterior">Camara anterior: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_camaraanterior">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_camaraanterior == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_camaraanterior == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_camaraanterior_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_camaraanterior_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_fondoojoderecho">Fondo de ojo derecho: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_fondoojoderecho">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_fondoojoderecho == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_fondoojoderecho == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_fondoojoderecho_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_fondoojoderecho_obs }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="segmentoanterior_fondoojoizquierdo">Fondo de ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group div-select-form" style="width: 100%">
                                            <select class="select-form form-control" id="segmentoanterior_fondoojoizquierdo">
                                                <option ></option>
                                                <option value="NORMAL" {{ $historia->segmentoanterior_fondoojoizquierdo == "NORMAL" ? "selected" : "" }} >NORMAL</option>
                                                <option value="ANORMAL" {{ $historia->segmentoanterior_fondoojoizquierdo == "ANORMAL" ? "selected" : "" }}>ANORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="segmentoanterior_fondoojoizquierdo_obs" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->segmentoanterior_fondoojoizquierdo_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="title-sections" id="sub-title">RETINOSCOPIA</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="retinoscopia_ojoderecho">Ojo derecho: </label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" id="retinoscopia_ojoderecho" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->retinoscopia_ojoderecho }}">
                                    </div>
                                </div>
                                <div class="row" id="margin-bottom-row">
                                    <div class="col-md-3">
                                        <label for="retinoscopia_ojoizquierdo">Ojo izquierdo: </label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text"  id="retinoscopia_ojoizquierdo" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->retinoscopia_ojoizquierdo }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="retinoscopia_add">ADD: </label>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="text" id="retinoscopia_add" placeholder="Observaciones" class="form-control-form" style="width: 100%" value="{{ $historia->retinoscopia_add }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="recomendaciones" class="tab-pane fade">
                    <h5 class="title-sections" id="sub-title">CONDUCTA Y RECOMENDACIONES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_ergonomiavisual">Ergonomia visual:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_ergonomiavisual" id="recomendacionEVS" value="Si" {{ $historia['recomendacion']->ergonomiavisual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionEVS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_ergonomiavisual" id="recomendacionEVN" value="No" {{ $historia['recomendacion']->ergonomiavisual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionEVN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_ergonomiavisual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->ergonomiavisual_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_pautashigienevisual">Pausas
                                            activas visuales:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_pautashigienevisual" id="recomendacionPHVS" value="Si" {{ $historia['recomendacion']->pautashigienevisual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionPHVS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_pautashigienevisual" id="recomendacionPHVN" value="No" {{ $historia['recomendacion']->pautashigienevisual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionPHVN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_pautashigienevisual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->pautashigienevisual_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_higienevisual">Higiene visual:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_higienevisual" id="recomendacionHVS" value="Si" {{ $historia['recomendacion']->higienevisual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionHVS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_higienevisual" id="recomendacionHVN" value="No" {{ $historia['recomendacion']->higienevisual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionHVN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_higienevisual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->higienevisual_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_correccionopticapermanente">Correción optica permanente:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticapermanente" id="recomendacionROPS" value="Si" {{ $historia['recomendacion']->correccionopticapermanente == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionROPS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticapermanente" id="recomendacionROPN" value="No" {{ $historia['recomendacion']->correccionopticapermanente == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionROPN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_correccionopticapermanente_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->correccionopticapermanente_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_correccionopticavisionprolongada">Corrección optica vision prolongada:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticavisionprolongada" id="recomendacionCOVPS" value="Si" {{ $historia['recomendacion']->correccionopticavisionprolongada == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCOVPS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticavisionprolongada" id="recomendacionCOVPN" value="No" {{ $historia['recomendacion']->correccionopticavisionprolongada == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCOVPN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_correccionopticavisionprolongada_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->correccionopticavisionprolongada_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_correccionopticaactual">Correción optica actual:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticaactual" id="recomendacionCOAS" value="Si" {{ $historia['recomendacion']->correccionopticaactual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCOAS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticaactual" id="recomendacionCOAN" value="No" {{ $historia['recomendacion']->correccionopticaactual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCOAN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_correccionopticaactual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->correccionopticaactual_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_correccionopticavisioncercana">Corrección optica vision cercana:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticavisioncercana" id="recomendacionCOVCS" value="Si" {{ $historia['recomendacion']->correccionopticavisioncercana == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCOVCS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_correccionopticavisioncercana" id="recomendacionCOVCN" value="No" {{ $historia['recomendacion']->correccionopticavisioncercana == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCOVCN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_correccionopticavisioncercana_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->correccionopticavisioncercana_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_usoelementosproteccionvisual">Uso de elem. de proteccion visual:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_usoelementosproteccionvisual" id="recomendacionUEPVS" value="Si" {{ $historia['recomendacion']->usoelementosproteccionvisual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionUEPVS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_usoelementosproteccionvisual" id="recomendacionUEPVN" value="No" {{ $historia['recomendacion']->usoelementosproteccionvisual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionUEPVN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_usoelementosproteccionvisual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->usoelementosproteccionvisual_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_proteccionvisualcorreccionoptica">Proteccion visual correccion optica:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_proteccionvisualcorreccionoptica" id="recomendacionPVCOS" value="Si" {{ $historia['recomendacion']->proteccionvisualcorreccionoptica == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionPVCOS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_proteccionvisualcorreccionoptica" id="recomendacionPVCON" value="No" {{ $historia['recomendacion']->proteccionvisualcorreccionoptica == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionPVCON">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_proteccionvisualcorreccionoptica_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->proteccionvisualcorreccionoptica_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_usogafasfiltrouv">Uso de gafas con filtro UV:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_usogafasfiltrouv" id="recomendacionUGFUVS" value="Si" {{ $historia['recomendacion']->usogafasfiltrouv == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionUGFUVS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_usogafasfiltrouv" id="recomendacionUGFUVN" value="No" {{ $historia['recomendacion']->usogafasfiltrouv == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionUGFUVN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_usogafasfiltrouv_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->usogafasfiltrouv_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_controlanual">Control anual:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controlanual" id="recomendacionCAnuS" value="Si" {{ $historia['recomendacion']->controlanual == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCAnuS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controlanual" id="recomendacionCAnuN" value="No" {{ $historia['recomendacion']->controlanual == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCAnuN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_controlanual_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->controlanual_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_controloptometria">Control optometria:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controloptometria" id="recomendacionCOptoS" value="Si" {{ $historia['recomendacion']->controloptometria == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCOptoS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controloptometria" id="recomendacionCOptoN" value="No" {{ $historia['recomendacion']->controloptometria == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCOptoN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_controloptometria_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->controloptometria_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_controloftalmologia">Control oftalmologia:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controloftalmologia" id="recomendacionCOftaS" value="Si" {{ $historia['recomendacion']->controloftalmologia == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionCOftaS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_controloftalmologia" id="recomendacionCOftaN" value="No" {{ $historia['recomendacion']->controloftalmologia == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionCOftaN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_controloftalmologia_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->controloftalmologia_obs }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_norequiereusocorreccionoptica">No requiere uso correccion optica:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_norequiereusocorreccionoptica" id="recomendacionNRUCOS" value="Si" {{ $historia['recomendacion']->norequiereusocorreccionoptica == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionNRUCOS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_norequiereusocorreccionoptica" id="recomendacionNRUCON" value="No" {{ $historia['recomendacion']->norequiereusocorreccionoptica == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionNRUCON">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_norequiereusocorreccionoptica_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->norequiereusocorreccionoptica_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="form-check-label" for="recomendaciones_otrasconductas">Otras conductas:</label>
                                    </div>
                                    <div class="col-md-2" style="padding: 0">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_otrasconductas" id="recomendacionOCondS" value="Si" {{ $historia['recomendacion']->otrasconductas == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="recomendacionOCondS">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_otrasconductas" id="recomendacionOCondN" value="No" {{ $historia['recomendacion']->otrasconductas == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="recomendacionOCondN">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" id="recomendaciones_otrasconductas_obs" placeholder="Recomendacion" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->otrasconductas_obs }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-check-label" for="recomendaciones_ojoderecho">Diagnóstico ojo derecho:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="recomendaciones_ojoderecho" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->ojoderecho }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="margin-bottom-row">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-check-label" for="recomendaciones_ojoizquierdo">Diagnóstico ojo izquierdo:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="recomendaciones_ojoizquierdo" class="form-control-form" style="width: 100%" value="{{ $historia['recomendacion']->ojoizquierdo }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="recomendaciones_recomendacion">Diagnóstico:</label>
                                <textarea class="form-control-form" style="width: 100%" id="recomendaciones_diagnostico" rows="3">{{ $historia['recomendacion']->diagnostico }}</textarea>
                            </div>
                        </div>
                    </div>

                    <h5 class="title-sections" id="sub-title">RECOMENDACIONES ADICIONALES</h5>
                    <div class="border-container">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="recomendaciones_recomendacion">Recomendaciones</label>
                                <textarea class="form-control-form" style="width: 100%" id="recomendaciones_recomendacion" rows="3">{{ $historia['recomendacion']->recomendacion }}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="col-form-label">Paciente es compatible para realizar la labor:</label>
                                    </div>
                                    <div class="col-md-6" style="padding-top: 1.2%;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_pacientecompatible" id="radioDiabetes1" value="Si" {{ $historia['recomendacion']->pacientecompatible == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="radioDiabetes1">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_pacientecompatible" id="radioDiabetes2" value="No" {{ $historia['recomendacion']->pacientecompatible == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="radioDiabetes2">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="recomendaciones_pacientecompatible_obs">Observaciones:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea class="form-control-form" style="width: 100%" id="recomendaciones_pacientecompatible_obs" rows="3">{{ $historia['recomendacion']->pacientecompatible_obs }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="col-form-label">Requiere nueva valoración:</label>
                                    </div>
                                    <div class="col-md-6" style="padding-top: 1.2%;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_requierevaloracion" id="radioDiabetes1" value="Si" {{ $historia['recomendacion']->requierevaloracion == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="radioDiabetes1">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_requierevaloracion" id="radioDiabetes2" value="No" {{ $historia['recomendacion']->requierevaloracion == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="radioDiabetes2">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="recomendaciones_recomendaciones_requierevaloracion_obs">Observaciones:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea class="form-control-form" style="width: 100%" id="recomendaciones_requierevaloracion_obs" rows="3">{{ $historia['recomendacion']->requierevaloracion_obs }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="col-form-label">Requiere remision a especialista:</label>
                                    </div>
                                    <div class="col-md-6" style="padding-top: 1.2%;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_requiereremision" id="radioDiabetes1" value="Si" {{ $historia['recomendacion']->requiereremision == "Si" ? "checked" : "" }}>
                                            <label class="form-check-label" for="radioDiabetes1">Si</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="recomendaciones_requiereremision" id="radioDiabetes2" value="No" {{ $historia['recomendacion']->requiereremision == "No" ? "checked" : "" }} >
                                            <label class="form-check-label" for="radioDiabetes2">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="Requiere valoracion obs">Observaciones:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea class="form-control-form" style="width: 100%" id="recomendaciones_requiereremision_obs" rows="3">{{ $historia['recomendacion']->requiereremision_obs }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="llave" value="{{ ($historia->id) }}">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Aceptar" class="button-form btn btn-primary" id="aceptarHO">
                </div>
            </div>
        </div>
    @endif
@stop
