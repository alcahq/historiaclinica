@extends('layouts')
@section('js')
    <script src="{{asset('/js/forms.js')}}"></script>
@stop
@section('title')
    <title>Certificado - Opto</title>
@stop
@section('contenido')
    <form action="{{ route('optometria.generarCertificado') }}" method="POST">
        {!! csrf_field() !!}

        <div class="container-local">
            <h4 class="title-pass">GENERAR CERTIFICADO OPTOMETRÍA</h4>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-2 offset-md-5">
                    <input type="submit" value="Generar" class="button-form btn btn-primary">
                </div>
            </div>
        </div>
    </form>
@stop
