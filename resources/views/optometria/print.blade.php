<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Create Word File in Laravel</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> 
    <style>
        @font-face {
            font-family: 'Montserrat';
            src: url('/fonts/Montserrat.otf');
        }
        body{
            font-family: 'Montserrat';
        }
        h1{
            font-size: 12pt;
            text-align: center;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        h2{
            font-size: 11pt;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        label{
            font-size: 10pt;
            font-weight: normal;
        }
        .border{
            border-width: 1px;
            border-color: rgba(0,0,0,0.5);
            border-style: solid;
            border-radius: 5px;
        }
        .row{
            margin-right: 0px;
            margin-left: 0px;
        }

    </style>
  </head>
  <body>
    <div class="container">
        <h1>OPTOMETRIA</h1>
        <div class="row">
            <label><b>Fecha:</b> {{ $ho->fecha }}</label>
        </div>
        <h2>MOTIVO DE EVALUACIÓN</h2>
        <div class="row border">
          <div class="col-md-4"></div>
            <div class="form-group col-md-12">
              <label><b>Tipo de evaluacion:</b> {{ $ho->motivoevaluacion }}</label>
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Number">Phone Number:</label>
              <input type="text" class="form-control" name="number">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
   </div>
  </body>
</html>