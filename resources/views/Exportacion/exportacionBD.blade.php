@extends('layouts')
@section('title')
    <title>Reportes - Exportación base de datos</title>
@stop
@section('contenido')
    @if(session()->has('busquedaReporte'))
        <div class="container-local">
            <h1>{{ session()->get('busquedaReporte') }}</h1>
        </div>
    @else
        @if (count($errors) > 0)
            <div class="container-local">
                <div class="toast toast-local" id="toastRerpote" data-autohide="false">
                    <div class="toast-header">
                        <strong class="mr-auto">Alerta</strong>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <form action="{{ route('historiaClinica.export') }}" method="get">
            {!! csrf_field() !!}

            <div class="container-local">
                <h4 class="title-pass">EXPORTACIÓN BASE DE DATOS</h4>
            </div>

            <div class="container-pass border-container">
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="tipo historia" class="row-size-pass">Seleccione el tipo de historia:</label>
                        <div class="form-group row-size-pass div-select-form">
                            <select class="select-form form-control" id="exampleFormControlSelect1" name="tipo_historia">
                                <option value=""></option>
                                <option value="Historia clínica">Historia clínica</option>
                                <option value="Historia optometría">Historia optometría</option>
                                <option value="Historia audiometría">Historia audiometría</option>
                                <option value="Historia visiometría">Historia visiometría</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="fecha desde" class="row-size-pass">Fecha desde:</label>
                        <input type="text" class="row-size-pass form-control-form datepicker" name="fechaDesde" id="fechaDesde" autocomplete="off" style="padding: 3px">
                    </div>
                </div>
                <div class="row padding-bottom-pass">
                    <div class="col-md-10 offset-md-1">
                        <label for="fecha hasta" class="srow-size-pass">Fecha hasta:</label>
                        <input type="text" class="row-size-pass form-control-form datepicker" name="fechaHasta" id="fechaHasta" autocomplete="off" style="padding: 3px">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-2 offset-md-5">
                        <input type="submit" value="Generar" class="button-form btn btn-primary">
                    </div>
                </div>
            </div>
        </form>
    @endif
    <script>
        $('.toast').toast('show');

        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                language: "es",
                autoclose: true
            });
        })
    </script>
@stop
