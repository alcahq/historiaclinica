$(document).ready(function () {
    //Campo fecha
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });

    //Autocompletar empresa
    $('#empresa_select').easyAutocomplete({
        url: function(phrase) {
            return "/empresa/autocomplete";
        },

        getValue: function(element) {
            return element.nombre;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $("#empresa_select").val();
            return data;
        },

        requestDelay: 10
    });

    //Mostrar campos segun selección de criterio de busqueda
    $('#criterio_select > div >input').on('change', function () {
        if ($('#profesional').is(":checked")){
            $('.fecha').attr('hidden', 'true');
            $('.estado').attr('hidden', 'true');
            $('.empresa').attr('hidden', 'true');
            $('.profesional').removeAttr('hidden');
            $('.buscar').removeAttr('hidden');
        }else if ($('#fecha').is(":checked")){
            $('.profesional').attr('hidden', 'true');
            $('.empresa').attr('hidden', 'true');
            $('.estado').attr('hidden', 'true');
            $('.fecha').removeAttr('hidden');
            $('.buscar').removeAttr('hidden');
        } else if ($('#estado').is(":checked")) {
            $('.fecha').attr('hidden', 'true');
            $('.profesional').attr('hidden', 'true');
            $('.empresa').attr('hidden', 'true');
            $('.estado').removeAttr('hidden');
            $('.buscar').removeAttr('hidden');
        } else if ($('#empresa').is(":checked")){
            $('.fecha').attr('hidden', 'true');
            $('.estado').attr('hidden', 'true');
            $('.profesional').attr('hidden', 'true');
            $('.empresa').removeAttr('hidden');
            $('.buscar').removeAttr('hidden');
        }
    });

    if ($('#profesional').is(":checked")){
        $('.fecha').attr('hidden', 'true');
        $('.estado').attr('hidden', 'true');
        $('.empresa').attr('hidden', 'true');
        $('.profesional').removeAttr('hidden');
        $('.buscar').removeAttr('hidden');
    }else if ($('#fecha').is(":checked")){
        $('.profesional').attr('hidden', 'true');
        $('.empresa').attr('hidden', 'true');
        $('.estado').attr('hidden', 'true');
        $('.fecha').removeAttr('hidden');
        $('.buscar').removeAttr('hidden');
    } else if ($('#estado').is(":checked")) {
        $('.fecha').attr('hidden', 'true');
        $('.profesional').attr('hidden', 'true');
        $('.empresa').attr('hidden', 'true');
        $('.estado').removeAttr('hidden');
        $('.buscar').removeAttr('hidden');
    } else if ($('#empresa').is(":checked")){
        $('.fecha').attr('hidden', 'true');
        $('.estado').attr('hidden', 'true');
        $('.profesional').attr('hidden', 'true');
        $('.empresa').removeAttr('hidden');
        $('.buscar').removeAttr('hidden');
    };
});
