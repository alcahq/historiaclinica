$(document).ready(function () {
    //Alert booststrap
    $('.toast').toast('show');

    //Campo fecha
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
    $('#fechaN').on('changeDate', function (e) {
        var fecha = $('#fechaN').val();
        var edad = Edad(fecha);
        $('#edadCalculada').val(edad + " años");
        $('#edadCalculada2').val(edad);
    });

    var fecha = $('#fechaN').val();
    var edad = Edad(fecha);
    $('#edadCalculada').val(edad + " años");
    $('#edadCalculada2').val(edad);

    $('#fechaN').focusout(function () {
        var fecha = $('#fechaN').val();
        var edad = Edad(fecha);
        $('#edadCalculada').val(edad + " años");
        $('#edadCalculada2').val(edad);
    });

    //Eliminar foto
    $('#eliminar-foto').on("click", function (e) {
        e.preventDefault();
        $('#foto-img').attr('src', "");
        $('#foto').val("");
    });

    //Eliminar firma
    $('#eliminar-firma').on("click", function (e) {
        e.preventDefault();
        $('#firma-img').attr('src', "");
        $('#firma').val("");
    });

    //Eliminar foto
    $('#eliminar-fotoUsuario').on("click", function (e) {
        e.preventDefault();
        $('#foto-img').attr('src', "");
        $('#foto').val("");
    });

    //Eliminar firma
    $('#eliminar-firmaUsuario').on("click", function (e) {
        e.preventDefault();
        $('#firma-img').attr('src', "");
        $('#firma').val("");
    });

});

function Edad(FechaNacimiento) {

    var fechaC = FechaNacimiento.replace(/^(\d{2})-(\d{2})-(\d{4})$/g,'$3-$2-$1');
    var fechaNace = new Date(fechaC);
    var fechaActual = new Date();

    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();

    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);
    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));

    return edad;
}

