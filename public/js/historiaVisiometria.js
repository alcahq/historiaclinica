$(document).ready(function () {

    $('#toastHV').hide();

    //Buscar paciente en HC
    $('#documentoBuscado').on('keyup', function (e) {
        e.preventDefault();
        if (e.keyCode == 13){
            buscarPaciente();
        }
    });

    $('#documentoBuscado').focusout(function (e) {
        e.preventDefault();
        buscarPaciente();
    });

    //Auto completar empresa
    $('#empresaActual').easyAutocomplete({
        url: function(phrase) {
            return "/empresa/autocomplete";
        },

        getValue: function(element) {
            return element.nombre;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $("#empresaActual").val();
            return data;
        },

        list: {
            onChooseEvent: function() {
                $("#empresaFR0" ).val($("#empresaActual").val());

            }
        },
        requestDelay: 10
    });

    //Registrar historia visiometria
    $('#registrarHV').on('click', function (e) {
        e.preventDefault();
        if (confirm("¿Desea registrar la historia de visiometria?")){
            registrarHV();
        }
    });

    //Bloquear campos cuando el estado es cerrado o anulado
    var estado = $('#estadoHO').val();
    if (estado != 'Abierta'){
        console.log(estado);
        $('input').attr('disabled', true);
        $('select').attr('disabled', true);
        $('textarea').attr('disabled', true);
    }
    if(estado != 'Anulada'){
        $('#anularHV').attr("disabled", false);
    };
    $('#aceptarHV').removeAttr("disabled");

    $('#aceptarHV').on('click', function () {
        // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
        location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
    });

    //Anular historia clinica
    $('#anularHV').on('click', function () {
        if ( confirm("¿Desea anular esta historia clinica?")){
            anularHistoriaV();
        };
    });
});

function anularHistoriaV(){
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "id": $('#id').val(),
            'user_id': $('#user_id').val()
        },
        type: 'POST',
        url: '/hv/anular',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
            location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function buscarPaciente() {
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "documento": $('#documentoBuscado').val()
        },
        type: 'POST',
        url: '/paciente/findByDocument',
        dataType: 'json',
        success: function (data) {
            var nombre = data.nombre + " " +  data.apellido;
            var documento = "CC: " + data.documento;
            var edad = "Edad: " + data.edad +" años";
            var cargo = data.cargo;
            console.log(data.edad);
            var genero = data.genero;
            $('#nombrePaciente').val(nombre);
            $('#documentoPaciente').val(documento);
            $('#edadPaciente').val(edad);
            $('#cargoE').val(cargo);
        },
        error: function (data) {
            //console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                console.log(json);
                $.each(json.errors, function (key, value) {
                    $.each(value, function(key1,value1){
                        $(".toast-body").append("<li>"+value1+"</li>");
                    });
                });
                $("#toastHV").removeClass('hide').addClass('show');
                $("#toastHV").show();
            }
        }
    });
}

function registrarHV() {

    //Split cedula medico y paciente
    var cedulaM = $('#medicoCedula').val();
    cedulaM = cedulaM.split(" ")[1];

    var cedulaP = $('#documentoPaciente').val();
    cedulaP = cedulaP.split(" ")[1];

    var otros = [];
    $("input[name='otros[]']:checked").each(function () {
        otros += $(this).val()+",";
    });
    if (otros.length > 0){
        otros = otros.substring(0,otros.length-1);
    }


    var  json = {
        "tipo": 0,
        "documento": $('#documentoBuscado').val(),
        "fecha": $('#fechaRegistro').val(),
        "medicoApertura_id": $('#id_medicoA').val(),
        "paciente": $('#nombrePaciente').val(),
        "paciente_cedula": cedulaP,
        "motivoevaluacion": $('#motivoE option:selected').val(),
        "cargo": $('#cargoE').val(),
        "motivoevaluacion_otros": otros,
        "empresa": $('#empresaActual').val(),
        "antecedentes_alergicos" : $('input[name=antecedentes_alergicos]:checked').val(),
        "antecedentes_alergicos_obs" : $('#antecedentes_alergicos_obs').val(),
        "antecedentes_alteraciontiroides" : $('input[name=antecedentes_alteraciontiroides]:checked').val(),
        "antecedentes_alteraciontiroides_obs" : $('#antecedentes_alteraciontiroides_obs').val(),
        "antecedentes_catarata" : $('input[name=antecedentes_catarata]:checked').val(),
        "antecedentes_catarata_obs" : $('#antecedentes_catarata_obs').val(),
        "antecedentes_diabetes" : $('input[name=antecedentes_diabetes]:checked').val(),
        "antecedentes_diabetes_obs" : $('#antecedentes_diabetes_obs').val(),
        "antecedentes_esquirlas" : $('input[name=antecedentes_esquirlas]:checked').val(),
        "antecedentes_esquirlas_obs" : $('#antecedentes_esquirlas_obs').val(),
        "antecedentes_glaucoma" : $('input[name=antecedentes_glaucoma]:checked').val(),
        "antecedentes_glaucoma_obs" : $('#antecedentes_glaucoma_obs').val(),
        "antecedentes_hipertension" : $('input[name=antecedentes_hipertension]:checked').val(),
        "antecedentes_hipertension_obs" : $('#antecedentes_hipertension_obs').val(),
        "antecedentes_oculares_otros" : $('input[name=antecedentes_oculares_otros]:checked').val(),
        "antecedentes_oculares_otros_obs" : $('#antecedentes_oculares_otros_obs').val(),
        "antecedentes_personales_otros" : $('input[name=antecedentes_personales_otros]:checked').val(),
        "antecedentes_personales_otros_obs" : $('#antecedentes_personales_otros_obs').val(),
        "antecedentes_problemascardiacos" : $('input[name=antecedentes_problemascardiacos]:checked').val(),
        "antecedentes_problemascardiacos_obs" : $('#antecedentes_problemascardiacos_obs').val(),
        "antecedentes_quimicos" : $('input[name=antecedentes_quimicos]:checked').val(),
        "antecedentes_quimicos_obs" : $('#antecedentes_quimicos_obs').val(),
        "antecedentes_quirurgicos" : $('input[name=antecedentes_quirurgicos]:checked').val(),
        "antecedentes_quirurgicos_obs" : $('#antecedentes_quirurgicos_obs').val(),
        "antecedentes_rehabilitacionvisual" : $('input[name=antecedentes_rehabilitacionvisual]:checked').val(),
        "antecedentes_rehabilitacionvisual_obs" : $('#antecedentes_rehabilitacionvisual_obs').val(),
        "antecedentes_trauma" : $('input[name=antecedentes_trauma]:checked').val(),
        "antecedentes_trauma_obs" : $('#antecedentes_trauma_obs').val(),
        "antecedentes_usuariosrx" : $('input[name=antecedentes_usuariosrx]:checked').val(),
        "antecedentes_usuariosrx_obs" : $('#antecedentes_usuariosrx_obs').val(),
        "sintomas_ardor" : $('input[name=sintomas_ardor]:checked').val(),
        "sintomas_cefalea" : $('input[name=sintomas_cefalea]:checked').val(),
        "sintomas_dolorocular" : $('input[name=sintomas_dolorocular]:checked').val(),
        "sintomas_enrojecimiento" : $('input[name=sintomas_enrojecimiento]:checked').val(),
        "sintomas_fotofobia" : $('input[name=sintomas_fotofobia]:checked').val(),
        "sintomas_norefiere" : $('input[name=sintomas_norefiere]:checked').val(),
        "sintomas_otros" : $('#sintomas_otros').val(),
        "sintomas_suenoleer" : $('input[name=sintomas_suenoleer]:checked').val(),
        "sintomas_visionborrosacercana" : $('input[name=sintomas_visionborrosacercana]:checked').val(),
        "sintomas_visionborrosalejana" : $('input[name=sintomas_visionborrosalejana]:checked').val(),
        "sintomas_visiondoble" : $('input[name=sintomas_visiondoble]:checked').val(),
        "riesgocargoevaluar_exposiciongasesvapores" : $('input[name=riesgocargoevaluar_exposiciongasesvapores]:checked').val(),
        "riesgocargoevaluar_exposicionmaterialparticulado" : $('input[name=riesgocargoevaluar_exposicionmaterialparticulado]:checked').val(),
        "riesgocargoevaluar_exposicionmaterialproyeccion" : $('input[name=riesgocargoevaluar_exposicionmaterialproyeccion]:checked').val(),
        "riesgocargoevaluar_exposicionquimicossolventes" : $('input[name=riesgocargoevaluar_exposicionquimicossolventes]:checked').val(),
        "riesgocargoevaluar_exposicionvideoterminales" : $('input[name=riesgocargoevaluar_exposicionvideoterminales]:checked').val(),
        "riesgocargoevaluar_iluminacion" : $('input[name=riesgocargoevaluar_iluminacion]:checked').val(),
        "riesgocargoevaluar_otros" : $('#riesgocargoevaluar_otros').val(),
        "riesgocargoevaluar_radiacionesionizantes" : $('input[name=riesgocargoevaluar_radiacionesionizantes]:checked').val(),
        "riesgocargoevaluar_radiacionesnoionizantes" : $('input[name=riesgocargoevaluar_radiacionesnoionizantes]:checked').val(),
        "riesgocargoevaluar_trauma" : $('input[name=riesgocargoevaluar_trauma]:checked').val(),
        "lejana_concorreccion_binocular" : $('#lejana_concorreccion_binocular option:selected').val(),
        "lejana_concorreccion_binocular_obs" : $('#lejana_concorreccion_binocular_obs').val(),
        "lejana_concorreccion_ojoderecho" : $('#lejana_concorreccion_ojoderecho option:selected').val(),
        "lejana_concorreccion_ojoderecho_obs" : $('#lejana_concorreccion_ojoderecho_obs').val(),
        "lejana_concorreccion_ojoizquierdo" : $('#lejana_concorreccion_ojoizquierdo option:selected').val(),
        "lejana_concorreccion_ojoizquierdo_obs" : $('#lejana_concorreccion_ojoizquierdo_obs').val(),
        "lejana_sincorreccion_binocular" : $('#lejana_sincorreccion_binocular option:selected').val(),
        "lejana_sincorreccion_binocular_obs" : $('#lejana_sincorreccion_binocular_obs').val(),
        "lejana_sincorreccion_ojoderecho" : $('#lejana_sincorreccion_ojoderecho option:selected').val(),
        "lejana_sincorreccion_ojoderecho_obs" : $('#lejana_sincorreccion_ojoderecho_obs').val(),
        "lejana_sincorreccion_ojoizquierdo" : $('#lejana_sincorreccion_ojoizquierdo option:selected').val(),
        "lejana_sincorreccion_ojoizquierdo_obs" : $('#lejana_sincorreccion_ojoizquierdo_obs').val(),
        "cercana_concorreccion_binocular" : $('#cercana_concorreccion_binocular option:selected').val(),
        "cercana_concorreccion_binocular_obs" : $('#cercana_concorreccion_binocular_obs').val(),
        "cercana_concorreccion_ojoderecho" : $('#cercana_concorreccion_ojoderecho option:selected').val(),
        "cercana_concorreccion_ojoderecho_obs" : $('#cercana_concorreccion_ojoderecho_obs').val(),
        "cercana_concorreccion_ojoizquierdo" : $('#cercana_concorreccion_ojoizquierdo option:selected').val(),
        "cercana_concorreccion_ojoizquierdo_obs" : $('#cercana_concorreccion_ojoizquierdo_obs').val(),
        "cercana_sincorreccion_binocular" : $('#cercana_sincorreccion_binocular option:selected').val(),
        "cercana_sincorreccion_binocular_obs" : $('#cercana_sincorreccion_binocular_obs').val(),
        "cercana_sincorreccion_ojoderecho" : $('#cercana_sincorreccion_ojoderecho option:selected').val(),
        "cercana_sincorreccion_ojoderecho_obs" : $('#cercana_sincorreccion_ojoderecho_obs').val(),
        "cercana_sincorreccion_ojoizquierdo" : $('#cercana_sincorreccion_ojoizquierdo option:selected').val(),
        "cercana_sincorreccion_ojoizquierdo_obs" : $('#cercana_sincorreccion_ojoizquierdo_obs').val(),
        "lensometria_add" : $('#lensometria_add').val(),
        "lensometria_ojoderecho" : $('#lensometria_ojoderecho').val(),
        "lensometria_ojoizquierdo" : $('#lensometria_ojoizquierdo').val(),
        "lensometria_tipolente" : $('#lensometria_tipolente option:selected').val(),
        "segmentoanterior_segmentoanterior" : $('#segmentoanterior_segmentoanterior option:selected').val(),
        "segmentoanterior_segmentoanterior_obs" : $('#segmentoanterior_segmentoanterior_obs').val(),
        "segmentoanterior_campovisual" : $('#segmentoanterior_campovisual option:selected').val(),
        "segmentoanterior_campovisual_obs" : $('#segmentoanterior_campovisual_obs').val(),
        "segmentoanterior_motilidadocular" : $('#segmentoanterior_motilidadocular option:selected').val(),
        "segmentoanterior_motilidadocular_obs" : $('#segmentoanterior_motilidadocular_obs').val(),
        "segmentoanterior_visioncolor" : $('#segmentoanterior_visioncolor option:selected').val(),
        "segmentoanterior_visioncolor_obs" : $('#segmentoanterior_visioncolor_obs').val(),
        "segmentoanterior_visionprofundidad" : $('#segmentoanterior_visionprofundidad option:selected').val(),
        "segmentoanterior_visionprofundidad_obs" : $('#segmentoanterior_visionprofundidad_obs').val(),
        "segmentoanterior_impresiondiagnostica_ojoderecho" : $('#segmentoanterior_impresiondiagnostica_ojoderecho option:selected').val(),
        "segmentoanterior_impresiondiagnostica_ojoderecho_obs" : $('#segmentoanterior_impresiondiagnostica_ojoizquierdo_obs').val(),
        "segmentoanterior_impresiondiagnostica_ojoizquierdo" : $('#segmentoanterior_impresiondiagnostica_ojoizquierdo option:selected').val(),
        "segmentoanterior_impresiondiagnostica_ojoizquierdo_obs" : $('#segmentoanterior_impresiondiagnostica_ojoizquierdo_obs').val(),
        "recomendaciones_adicionales" : $('#recomendaciones_adicionales').val(),
        "recomendaciones_controloftalmologia" : $('input[name=recomendaciones_controloftalmologia]:checked').val(),
        "recomendaciones_controloftalmologia_obs" : $('#recomendaciones_controloftalmologia_obs').val(),
        "recomendaciones_controloptometria" : $('input[name=recomendaciones_controloptometria]:checked').val(),
        "recomendaciones_controloptometria_obs" : $('#recomendaciones_controloptometria_obs').val(),
        "recomendaciones_correccionopticaactual" : $('input[name=recomendaciones_correccionopticaactual]:checked').val(),
        "recomendaciones_correccionopticaactual_obs" : $('#recomendaciones_correccionopticaactual_obs').val(),
        "recomendaciones_ergonomiavisual" : $('input[name=recomendaciones_ergonomiavisual]:checked').val(),
        "recomendaciones_ergonomiavisual_obs" : $('#recomendaciones_ergonomiavisual_obs').val(),
        "recomendaciones_higienevisual" : $('input[name=recomendaciones_higienevisual]:checked').val(),
        "recomendaciones_higienevisual_obs" : $('#recomendaciones_higienevisual_obs').val(),
        "recomendaciones_otrasconductas" : $('input[name=recomendaciones_otrasconductas]:checked').val(),
        "recomendaciones_otrasconductas_obs" : $('#recomendaciones_otrasconductas_obs').val(),
        "recomendaciones_pacientecompatible" : $('input[name=recomendaciones_pacientecompatible]:checked').val(),
        "recomendaciones_pacientecompatible_obs" : $('#recomendaciones_pacientecompatible_obs').val(),
        "recomendaciones_pautashigienevisual" : $('input[name=recomendaciones_pautashigienevisual]:checked').val(),
        "recomendaciones_pautashigienevisual_obs" : $('#recomendaciones_pautashigienevisual_obs').val(),
        "recomendaciones_proteccionvisualcorreccionoptica" : $('input[name=recomendaciones_proteccionvisualcorreccionoptica]:checked').val(),
        "recomendaciones_proteccionvisualcorreccionoptica_obs" : $('#recomendaciones_proteccionvisualcorreccionoptica_obs').val(),
        "recomendaciones_requiereremision" : $('input[name=recomendaciones_requiereremision]:checked').val(),
        "recomendaciones_requiereremision_obs" : $('#recomendaciones_requiereremision_obs').val(),
        "recomendaciones_requierevaloracion" : $('input[name=recomendaciones_requierevaloracion]:checked').val(),
        "recomendaciones_requierevaloracion_obs" : $('#recomendaciones_requierevaloracion_obs').val(),
        "recomendaciones_usoelementosproteccionvisual" : $('input[name=recomendaciones_usoelementosproteccionvisual]:checked').val(),
        "recomendaciones_usoelementosproteccionvisual_obs" : $('#recomendaciones_usoelementosproteccionvisual_obs').val(),
        "recomendaciones_usogafasfiltrouv" : $('input[name=recomendaciones_usogafasfiltrouv]:checked').val(),
        "recomendaciones_usogafasfiltrouv_obs" : $('#recomendaciones_usogafasfiltrouv_obs').val(),
    };

    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: json,
        type: 'POST',
        url: '/visiometria/register',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            location.reload(true);
        },
        error: function (data) {
            console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                $.each(json.errors, function (key, value) {
                    $(".toast-body").append("<li>"+value+"</li>");
                });
                $("#toastHV").removeClass('hide').addClass('show');
                $("#toastHV").show();
                $('html, body').animate({
                    scrollTop:$('#toastHV').offset().top
                }, 1000);
            }
        }
    });
}
