
$(document).ready(function () {
    $('#toastHC').hide();

    //Confirmacion de recargar la pagina
    // window.onbeforeunload = function(){
    //     return "Al recargar la página perdera todos los cambios realizados. ¿Desea continuar?";
    // };


    //Copiar cargo y empresa - Inputs
    $("#cargoE").focusout(function () {
        var value = $(this).val();
        $("#cargoActual").val(value);
        $("#cargoFR0").val(value);
    });
    $("#empresaActual").focusout(function () {
        var value = $(this).val();
        $("#empresaFR0").val(value);
    });
    $("#cargoE").keyup(function () {
        var value = $(this).val();
        $("#cargoActual").val(value);
        $("#cargoFR0").val(value);
    });
    $("#empresaActual").keyup(function () {
        var value = $(this).val();
        $("#empresaFR0").val(value);
    });

    // var valueAutoc = $("#empresaActual").val(ui.item.label);
    // $("#empresaFR0").val(valueAutoc);

    //Anadir factor de riesgo
    $("#factorRiesgo").on("click", function (e) {
        e.preventDefault();
        anadirFactoresRiesgo();
    });

    //Eliminar factor de riesgo
    $(document).on("click", '#eliminarFactorRiesgo', function (e) {
        e.preventDefault();
        eliminarFactoresRiesgo($(this));
    });

    //Buscar paciente en HC
    $('#documentoBuscado').on('keyup', function (e) {
        e.preventDefault();
        if (e.keyCode == 13){
            buscarPaciente();
        }
    });

    $('#documentoBuscado').focusout(function (e) {
        e.preventDefault();
        if ($('#documentoBuscado').val() != "") {
            buscarPaciente();
        }
    });

    //Buscar CIE10 por codigo
    $('#codigoCIE10').on('keyup', function (e) {
        e.preventDefault();
        if($(this).val() == ""){
            $("#diagnosticoCIE10").val("");
            $("#diagnosticoCIE10").attr("disabled",false);
        }
        if (e.keyCode == 13){
            buscarCIE10($(this));
        }
    });

    $('#codigoCIE10').focusout(function (e) {
        e.preventDefault();
        if($(this).val() == ""){
            $("#diagnosticoCIE10").val("");
            $("#diagnosticoCIE10").attr("disabled",false);

        }else{
            buscarCIE10($(this));
        }
    });
    $('#diagnosticoCIE10').focusout(function (e) {
        e.preventDefault();
        if($(this).val() == ""){
            $("#codigoCIE10").val("");
            $("#codigoCIE10").attr("disabled",false);

        }
    });

    //Registrar historia clinica
    $('#registrarHC').on("click", function (e) {
        e.preventDefault();
        if ( confirm("¿Desea registrar esta historia clinica?")){
            registrarHC();
        };
    });

    $('#actualizarHC').on("click", function (e) {
        e.preventDefault();
        actualizarHC();
    })

    //Ocultar o mostrar accidentes de trabajo HC
    $('#divCheckAT > div > input').on('change', function () {
        if($('#no').is(":checked")){
            $('#tableAT').hide();
            $('#anadirAT').attr("hidden", "true");
        }else{
            $('#tableAT').show();
            $('#tableAT').removeAttr("hidden");
            $('#anadirAT').removeAttr("hidden");
        }
    });

    //Habilita el campo de otros elementos de proteccion en caso de que este seleccionado el check
    $('.observacionOtro > div > input').on('change', function () {
        if ($('#otrosEP').is(":checked")){
            $('#OtrosLbl').show();
            $('#otrosObs').removeAttr("hidden");
            $('#OtrosLbl').removeAttr("hidden");
        }else{
            $('#otrosObs').attr('hidden', true);
            $('#OtrosLbl').attr('hidden', true);
        }
    });

    //Agregar fila a la tabla accidentes de trabajo
    $('#anadirAT').on("click", function (e) {
        e.preventDefault();
        anadirAccidenteTrabajo();
    });

    //Eliminar fila accidente de trabajo
    $(document).on('click','#eliminarAT', function (e) {
        e.preventDefault();
        eliminarAccidenteTrabajo($(this));
    });

    //Ocultar o mostrar enfermedad profesional HC
    $('#divCheckEP > div > input').on('change', function () {
        if($('#noEP').is(":checked")){
            $('#tableEP').hide();
            $('#anadirEP').attr("hidden", "true");
        }else{
            $('#tableEP').show();
            $('#tableEP').removeAttr("hidden");
            $('#anadirEP').removeAttr("hidden");
        }
    });

    //Agregar fila a la tabla enfermedad profesional
    $('#anadirEP').on("click", function (e) {
        e.preventDefault();
        anadirEnfermedadProfesional();
    });

    //Añadir nota de evolucion
    $('#anadirNota').on("click", function (e) {
       e.preventDefault();
       anadirNotaEvolucion();
    });

    //Eliminar nota de evolucion
    $(document).on('click', '#eliminarNota', function (e) {
        e.preventDefault();
        eliminarNotaEvolucion($(this));
    });

    //Eliminar fila enfermedad profesional
    $(document).on('click','#eliminarEP', function (e) {
        e.preventDefault();
        eliminarEnfermedadProfesional($(this));
    });

    //Ocultar o mostrar manipulacion de alimentos
    $('#divCheckMA > div > input').on('change', function () {
        if($('#noSolicitado').is(":checked")){
            $('#contenido-ma').hide();
        }else{
            $('#contenido-ma').show();
            $('#contenido-ma').removeAttr("hidden");
        }
    });

    //Adicion fila paraclinicos
    $('#anadirPara').on("click", function (e) {
        e.preventDefault();
        anadirParaclinico();
    });

    //Eliminar fila paraclinico
    $(document).on('click','#eliminarPara', function (e) {
        e.preventDefault();
        eliminarParaclinico($(this));
    });

    //Adicion fila impresion diagnostica
    $('#anadirID').on("click", function (e) {
        e.preventDefault();
        var codigo = $('#codigoCIE10').val();
        var diagnostico = $('#diagnosticoCIE10').val();
        if (codigo != "" && diagnostico!= ""){
            anadirImpresionDiagnostica();
            $('#codigoCIE10').val("");
            $('#diagnosticoCIE10').val("");
        }

    });

    //Eliminar fila impresion diagnostica
    $(document).on('click','#eliminarID', function (e) {
        e.preventDefault();
        eliminarImpresionD($(this));
    });

    $('.carousel').carousel({

        interval: false,
    });
    var imc;
    //calcular  indice de masa corporal
    $("#peso").focusout(function () {
        imc = calcularIMC();
        $("#imc").val(imc);
    });
    // $("#imc").val(imc);

    $("#talla").focusout(function () {
        imc = calcularIMC();
        $("#imc").val(imc);
    });
    // $("#imc").val(imc);

    $('#empresaActual').easyAutocomplete({
        url: function(phrase) {
            return "/empresa/autocomplete";
        },

        getValue: function(element) {
            return element.nombre;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $("#empresaActual").val();
            return data;
        },

        list: {
            onChooseEvent: function() {
                $("#empresaFR0" ).val($("#empresaActual").val());

            }
        },
        requestDelay: 10
    });

    $('#diagnosticoCIE10').easyAutocomplete({
        url: function(phrase) {
            return "/hc/autocomplete";
        },

        getValue: function(element) {
            return element.descripcion;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $("#diagnosticoCIE10").val();
            return data;
        },

        list: {
            onSelectItemEvent: function(){
              var selected = $("#diagnosticoCIE10").getSelectedItemData().codigo;
                $("#codigoCIE10" ).val(selected);
                if ($("#codigoCIE10" ).val() != " "){
                    $("#codigoCIE10" ).attr("disabled", true);
                }else{
                    $("#codigoCIE10" ).attr("disabled", false);
                }
            },
        },
        requestDelay: 10
    });

    $('#anularHC').on('click', function () {
        if ( confirm("¿Desea anular esta historia clinica?")){
            anularHistoriaC();
        };
    });

    //Bloquear campos cuando el estado es cerrado o anulado
    var estado = $('#estadoHC').val();
    if (estado != 'Abierta'){
        console.log(estado);
        $('input').attr('disabled', true);
        $('select').attr('disabled', true);
        $('textarea').attr('disabled', true);
    }

    //Componentes habilitados cuando la historia se encuentra en estado cerrada
    $('#consultarHC').removeAttr('disabled');
    $('#crearNota').removeAttr('disabled');
    $('#notaObs').removeAttr('disabled');
    $('#resultado_valoracion > div > div > input').removeAttr('disabled');
    $('#otroPC_nota').removeAttr('disabled');
    $('#restriccionesID_nota').removeAttr('disabled');
    $('#recomendacionesID_nota').removeAttr('disabled');
    $('#nuevaValoracionSi').removeAttr('disabled');
    $('#nuevaValoracionNo').removeAttr('disabled');

    $('#nuevaVChecked > div > input').on('change', function () {
        if ($('#nuevaValoracionSi').is(":checked")){
            $('.hidden-valoracion').show();
            $('.hidden-valoracion').removeAttr("hidden");
        }else{
            $('.hidden-valoracion').attr('hidden', true);
        }
    });

    //Se inhabilita la creacion de una nueva valoración médica
    if($('#valoracion_hc').val() == 1){
        $('#crearNota').attr('hidden', 'true');
        $('#anadirNota').attr('hidden', 'true');
        $('input').attr('disabled', true);
        $('select').attr('disabled', true);
        $('textarea').attr('disabled', true);
        $('#consultarHC').removeAttr('disabled');
    }

    if(estado != 'Anulada'){
        $('#anularHC').attr("disabled", false);
    };

    $('#consultarHC').on('click', function () {
        // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
        location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
    });

    $('#crearNota').on('click', function () {
        if ($('input[name=nuevaValoracionSN]:checked').val() == 'Si'){
            if ( confirm("¿Desea crear la nota de evolución con una nueva valoración?")){
                crearNotaEvolucion();
            }
        } else{
            crearNotaEvolucion();
        }
    });

});

function anadirFactoresRiesgo() {
    i = $(".generalcontainer").length;
    let div = $("#prueba");
    var htmldiv =
        "<div class=\"generalcontainer\" id=\"generalContainer"+i+"\">" +
        "<div class='row'>" +
        "<div class=\"col-md-5\">\n" +
        "<label for=\"empresa\" class=\"size-lbl-efr font-bold\">Empresa:</label>\n" +
        "<input type=\"text\" name=\"empresaFR\" id=\"empresaFR"+i+"\" class=\"form-control-form size-input-efr\">\n" +
        "</div>" +
        "<div class=\"col-md-4\" style=\"padding-left: 0\">\n" +
        "<label for=\"cargo\" class=\"size-lbl-cfr font-bold\">Cargo:</label>\n" +
        "<input type=\"text\" name=\"cargoFR\" id=\"cargoFR"+i+"\" class=\"form-control-form size-input-cfr\">\n" +
        "</div>\n" +
        "<div class=\"col-md-3\" style=\"padding-left: 0; padding-right: 0\">\n" +
        "<label for=\"cargo\" class=\"size-lbl-texp font-bold\">Tiempo de exposición:</label>\n" +
        "<input type=\"text\" name=\"tiempoExp\" id=\"tiempoExp"+i+"\" class=\"form-control-form size-input-texp\">\n" +
        "<div class=\"form-group div-select-form size-row-antigTE\">\n" +
        "<select class=\"select-form form-control\" name=\"antiguedadTE\" id=\"antiguedadTE"+i+"\" style=\"font-size: 0.8rem\">\n" +
        "<option value=\"\"></option>\n" +
        "<option value=\"dias\">Días</option>\n" +
        "<option value=\"meses\">Meses</option>\n" +
        "<option value=\"años\">Años</option>\n" +
        "</select>\n" +
        "</div>" +
        "</div>" +
        "</div>" +
        "<div class=\"row\" style=\"padding-top: 3px\">\n" +
        "                            <div class=\"col-md-5\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-2\" style=\"max-width: 15.8%\">\n" +
        "                                        <label for=\"fisicos\" class=\"font-bold\">Físicos:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-5\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Iluminación\">\n" +
        "                                            <label class=\"form-check-label\" for=\"iluminacion\">Iluminación</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Temperatura alta\">\n" +
        "                                            <label class=\"form-check-label\" for=\"temperatura alta\">Temperatura alta</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Radiaciones ionizantes\">\n" +
        "                                            <label class=\"form-check-label\" for=\"radiaciones ionizantes\">Radiaciones ionizantes</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Vibraciones\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Vibraciones</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-5\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Temperatura baja\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Temperatura baja</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Presión barométrica\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Presión barométrica</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Radiaciones no ionizantes\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Radiaciones no ionizantes</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"fisicos"+i+"[]\" value=\"Ruido\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Ruido</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "\n" +
        "                            <div class=\"col-md-3\" style=\"padding-left: 0\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-4\" style=\"max-width: 30%\">\n" +
        "                                        <label for=\"quimicos\" class=\"font-bold\">Químicos:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-3\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0;\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Gases\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Gases</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Vapores\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Vapores</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Humos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Humos</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-3\" id=\"jornada-tab\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Fibras\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Fibras</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Polvos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Polvos</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"quimicos"+i+"[]\" value=\"Líquidos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Líquidos</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "\n" +
        "                            <div class=\"col-md-3\" style=\"padding-left: 0\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-5\">\n" +
        "                                        <label for=\"fisicos\" class=\"font-bold\">Ergononómicos:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-7\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0;\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"ergonomicos"+i+"[]\"  value=\"Posturas prolongadas\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Posturas prolongadas</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"ergonomicos"+i+"[]\" value=\"Manejo de cargas\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Manejo de cargas</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"ergonomicos"+i+"[]\" value=\"Movimientos repetitivos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Movimientos repetitivos</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"ergonomicos"+i+"[]\" value=\"Video terminales\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Video terminales</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\" style=\"padding-top: 2px\">\n" +
        "                            <div class=\"col-md-3\" style=\"max-width: 22.8%;\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-5\" style=\"max-width: 37%;\">\n" +
        "                                        <label for=\"fisicos\" class=\"font-bold\">Biológicos:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-6\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0;\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"biologicos"+i+"[]\" value=\"Microorganismos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Microorganismos</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"biologicos"+i+"[]\" value=\"Animales\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Animales</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"biologicos"+i+"[]\" value=\"Vegetales\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Vegetales</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "\n" +
        "                            <div class=\"col-md-3\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-5\" style=\"max-width: 39%\">\n" +
        "                                        <label for=\"fisicos\" class=\"font-bold\">Psicosociales:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-7\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0;\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"psicosociales"+i+"[]\" value=\"Relaciones humanas\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Relaciones humanas</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"psicosociales"+i+"[]\" value=\"Gestión\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Gestión</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"psicosociales"+i+"[]\" value=\"Ambiente de trabajo\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Ambiente de trabajo</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "\n" +
        "                            <div class=\"col-md-3\" style=\"max-width: 22.8%;\">\n" +
        "                                <div class=\"row\">\n" +
        "                                    <div class=\"col-md-5\" style=\"max-width: 37%;\">\n" +
        "                                        <label for=\"fisicos\" class=\"font-bold\">Seguridad:</label>\n" +
        "                                    </div>\n" +
        "                                    <div class=\"col-md-6\" id=\"jornada-tab\" style=\"padding-left: 0; padding-right: 0;\">\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"seguridad"+i+"[]\" value=\"Eléctricos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Eléctricos</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"seguridad"+i+"[]\" value=\"Incendio\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox2\">Incendio</label>\n" +
        "                                        </div>\n" +
        "                                        <div class=\"form-check form-check-inline\">\n" +
        "                                            <input class=\"form-check-input\" type=\"checkbox\" name=\"seguridad"+i+"[]\" value=\"Mecánicos\">\n" +
        "                                            <label class=\"form-check-label\" for=\"inlineCheckbox1\">Mecánicos</label>\n" +
        "                                        </div>\n" +
        "                                    </div>\n" +
        "                                </div>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-12\">\n" +
        "                                <label for=\"jornada\" class=\"font-bold\">Otros:</label>\n" +
        "                                <textarea class=\"form-control\" id=\"otrosFactores"+i+"\" rows=\"3\"></textarea>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div class=\"row\" id=\"\" style=\"padding-top: 1%;\">\n" +
    "<div class=\"col-md-6 offset-6\" style=\"text-align: right\">\n" +
    "<a href=\"\" id=\"eliminarFactorRiesgo\" style=\" color: red\">Eliminar factores de riesgo del cargo actual</a>\n" +
    "</div>\n" +
    "</div>\n"+
    "<div class=\"row\">\n" +
    "<div class=\"col-md-12\">\n" +
    "<hr>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>";
    div.before(htmldiv);
}

function eliminarFactoresRiesgo(actual){
    // i = $(".generalcontainer").length-1;
    // var ultimoContenedor = $('#generalContainer'+i);
    // if (i > 0){
    //     ultimoContenedor.remove();
    // }
    $(actual).parent().parent().parent().remove();

}

function buscarPaciente() {
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "documento": $('#documentoBuscado').val()
        },
        type: 'POST',
        url: '/paciente/findByDocument',
        dataType: 'json',
        success: function (data) {
            var nombre = data.nombre + " " +  data.apellido;
            var documento = "CC: " + data.documento;
            var edad = "Edad: " + data.edad +" años";
            var cargo = data.cargo;
            var genero = data.genero;
            $('#nombrePaciente').val(nombre);
            $('#documentoPaciente').val(documento);
            $('#edadPaciente').val(edad);
            $('#cargoE').val(cargo);
            $("#cargoActual").val(cargo);
            $("#cargoFR0").val(cargo);

            if (genero == 'MASCULINO'){
                $('.ocultarAG').attr("hidden", "true");
            }else if (genero == 'FEMENINO') {
                $('.ocultarAG').removeAttr("hidden");
            }

        },
        error: function (data) {
            //console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                console.log(json);
                $.each(json.errors, function (key, value) {
                    $.each(value, function(key1,value1){
                        $(".toast-body").append("<li>"+value1+"</li>");
                    });
                });
                $("#toastHC").removeClass('hide').addClass('show');
                $("#toastHC").show();
            }
        }
    });
}

function anadirAccidenteTrabajo() {
    var fila = "<tr id=\"tr\" class=\"accidenteTR\">\n" +
        "<td><input type=\"text\" class=\"form-control-form input-table\" name=\"fechaAT\" id=\"fechaAT\" autocomplete=\"off\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"empresaAt\" id=\"empresaAT\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"lesionAT\" id=\"lesionAT\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"diasIncapacidadAT\" id=\"diasIncapacidadAT\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"secuelasAT\" id=\"secuelasAT\"></td>\n" +
        "<td class=\"tdImg\" style=\"padding: 7px; text-align: center\"><a href=\"\" id=\"eliminarAT\"><img src=\"/img/delete.png\" alt=\"delete\" style=\"width: 30px\"></a></td>\n" +
        "</tr>";

    $('#tbodyAt').append(fila);
}

function anadirEnfermedadProfesional() {
    var fila = "<tr id=\"tr\" class=\"enfermedadTR\">\n" +
        "<td><input type=\"text\" class=\"form-control-form input-table\" name=\"fechaEP\" id=\"fechaEP"+"\" autocomplete=\"off\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"empresaEP\" id=\"empresaEP"+"\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"diagnosticoEP\" id=\"diagnosticoEP"+"\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"arlEP\" id=\"arlEP"+"\"></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"reubicacionEP\" id=\"reubicacionEP"+"\"></td>\n" +
        "<td class=\"tdImg\" style=\"padding: 7px; text-align: center\"><a href=\"\" id=\"eliminarEP\"><img src=\"/img/delete.png\" alt=\"delete\" style=\"width: 30px\"></a></td>\n" +
        "</tr>";

    $('#tbodyEP').append(fila);
}

function fechaActual() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    return dd + '/' + mm + '/' + yyyy;
}

function anadirNotaEvolucion() {
    var fila = "<tr id=\"tr\" class=\"notaEvolucion nuevo\">\n" +
        "<td><input type=\"text\" class=\"form-control-form input-table\" name=\"fechaNota\" id=\"fechaNota\" value=\""+fechaActual()+"\" autocomplete=\"off\" disabled></td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"observacion_nota\" id=\"observacion_nota\"></td>\n" +
        "<td class=\"tdImg\" style=\"padding: 7px; text-align: center\"><a href=\"\" id=\"eliminarNota\"><img src=\"/img/delete.png\" alt=\"delete\" style=\"width: 30px\"></a></td>\n" +
        "</tr>";
    $('#tbodyNota').append(fila);
}

function eliminarAccidenteTrabajo(actual) {

    $(actual).parent().parent().remove();
}

function eliminarEnfermedadProfesional(actual) {

    $(actual).parent().parent().remove();
}

function eliminarNotaEvolucion(actual) {

    $(actual).parent().parent().remove();
}

function anadirParaclinico() {

    var fila = "<tr id=\"tr\" class=\"paraclinicosTR\">\n" +
        "<td>\n" +
        "<div class=\"form-group div-select-form\" style=\"width: 100%\">\n" +
        "<select class=\"select-form form-control input-table\" id=\"examenesLab\" name=\"examenesLab\" style=\"font-size: 0.8rem; height: 44px\">\n" +
        "<option value=\"\"></option>\n";
        for (let z = 0; z < examen.length; z++) {
            fila += "<option value=\""+examen[z].id+"\">"+examen[z].descripcion+"</option>\n";
        }
        fila += "</select>\n" +
        "</div>\n" +
        "</td>\n" +
        "<td>\n" +
        "<div class=\"form-group div-select-form\" style=\"width: 100%\">\n" +
        "<select class=\"select-form form-control input-table\" id=\"diagnosticoPara\" name=\"diagnoticoPara\" style=\"font-size: 0.8rem; height: 44px\">\n" +
        "<option value=\"\"></option>\n" +
        "<option value=\"Normal\">Normal</option>\n" +
        "<option value=\"Anormal\">Anormal</option>\n" +
        "<option value=\"No examinado\">No examinado</option>\n" +
        "<option value=\"No solicitado\">No solicitado</option>\n" +
        "<option value=\"Pendiente resultado\">Pendiente resultado</option>\n" +
        "</select>\n" +
        "</div>\n" +
        "</td>\n" +
        "<td><input type=\"text\" class=\"input-table\" name=\"observacionesPara\" id=\"observacionesPara\"></td>\n" +
        "<td class=\"tdImg\" style=\"padding: 7px; text-align: center\"><a href=\"\" id=\"eliminarPara\"><img src=\"/img/delete.png\" alt=\"delete\" style=\"width: 30px\"></a></td>\n" +
        "</tr>";
    $('#tbodyPARA').append(fila);
}

function eliminarParaclinico(actual) {

    $(actual).parent().parent().remove();
}

function anadirImpresionDiagnostica() {

    var codigo = $('#codigoCIE10').val();
    var diagnostico = $('#diagnosticoCIE10').val();
    var idCIE10 = $('#id_CIE10');
    var fila = "<tr id=\"tr\" class=\"impresionDTR\">\n" +
        "                            <td>\n" +
        "                                <input type=\"text\" class=\"input-table impresiondiagnostica\" value=\""+codigo+"\" name=\"buscarCIE10\" id=\"buscarCIE10\" disabled>\n" +
        "                            </td>\n" +
        "                            <td>\n" +
        "                                <input type=\"text\" class=\"input-table diagnosticoid autocomplete-paracl\" name=\"diagnosticoID\" value=\""+diagnostico+"\" id=\"diagnosticoID\" style=\"font-size: 0.8rem; height: 44px;border-color: #0000;\n" +
        "                                    border-radius: 0;border-style: none; border-width: 0; box-shadow: none\" disabled>\n" +
        "                                <div class=\"easy-autocomplete-container\" id=\"eac-container-diagnosticoID\"><ul style=\"display: none;\"></ul></div>\n"+
        "                            </td>\n" +
        "                            <td>\n" +
        "                                <div class=\"form-group div-select-form\" style=\"width: 100%\">\n" +
        "                                    <select class=\"select-form form-control input-table\" id=\"sospechaOrigen\" name=\"sospechaOrigen\" style=\"font-size: 0.8rem; height: 44px\">\n" +
        "                                        <option value=\"\"></option>\n" +
        "                                        <option value=\"Común\">Común</option>\n" +
        "                                        <option value=\"Profesional\">Profesional</option>\n" +
        "                                    </select>\n" +
        "                                </div>\n" +
        "                            </td>\n" +
        "                            <td>\n" +
        "                                <div class=\"form-group div-select-form\" style=\"width: 100%\">\n" +
        "                                    <select class=\"select-form form-control input-table \" id=\"tipoDiagnostico\" name=\"tipoDiagnostico\" style=\"font-size: 0.8rem; height: 44px;\">\n" +
        "                                        <option value=\"\"></option>\n" +
        "                                        <option value=\"Impresión diagnóstica\">Impresión diagnóstica</option>\n" +
        "                                        <option value=\"Confirmado nuevo\">Confirmado nuevo</option>\n" +
        "                                        <option value=\"Confirmado repetido\">Confirmado repetido</option>\n" +
        "                                    </select>\n" +
        "                                </div>\n" +
        "                            </td>\n" +
        "                            <td class=\"tdImg\" style=\"padding: 7px; text-align: center\"><a href=\"\" id=\"eliminarID\"><img src=\"/img/delete.png\" alt=\"delete\" style=\"width: 30px\"></a></td>\n" +
        "                        </tr>";
    $('#tbodyImpresionD').append(fila);
}

function eliminarImpresionD(actual){
    $(actual).parent().parent().remove();
}

function calcularIMC(){

    var talla = $('#talla').val();
    var peso = $('#peso').val();

    var IMC = peso/(talla*talla)*10000;

    if (talla == 0){
        return 0;
    }else
    {
        return IMC.toFixed(1);
    }
}

function buscarCIE10() {
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "buscarCIE10": $('#codigoCIE10').val()
        },
        type: 'POST',
        url: '/historiaClinica/buscarCIE10',
        dataType: 'json',
        success: function (data) {

            var descripcion = data.descripcion;
            var id = data.id;

            $('#id_CIE10').val(id);
            if (descripcion != " "){
                $('#diagnosticoCIE10').val(descripcion);
                $('#diagnosticoCIE10').attr("disabled", true);
            }else if($('#codigoCIE10').val() != " "){
                $('#diagnosticoCIE10').removeAttr("disabled");
                $('#diagnosticoCIE10').val("");
            }
        },
        error: function (data) {
            //console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                $.each(json.errors, function (key, value) {
                    $.each(value, function(key1,value1){
                        $(".toast-body").append("<li>"+value1+"</li>");
                    });
                });
                $("#toastHC").removeClass('hide').addClass('show');
                $("#toastHC").show();
            }
        }
    });
}

function registrarHC() {

    //Obtener valores multiples de un checkbox
    var checked = [];
    $("input[name='elementosP[]']:checked").each(function () {
        checked += $(this).val()+",";
    });
    if (checked.length > 0){
        checked = checked.substring(0,checked.length-1);
    }

    var otros = [];
    $("input[name='otros[]']:checked").each(function () {
        otros += $(this).val()+",";
    });
    if (otros.length > 0){
        otros = otros.substring(0,otros.length-1);
    }

    var neuro = [];
    $("input[name='neuro[]']:checked").each(function () {
        neuro += $(this).val()+",";
    });
    if (neuro.length > 0){
        neuro = neuro.substring(0,neuro.length-1);
    }

    var cardio = [];
    $("input[name='cardio[]']:checked").each(function () {
        cardio += ($(this).val()+",");
    });
    if (cardio.length > 0){
        cardio = cardio.substring(0,cardio.length-1);
    }

    var geni = [];
    $("input[name='geni[]']:checked").each(function () {
        geni += $(this).val()+",";
    });
    if (geni.length > 0){
        geni = geni.substring(0,geni.length-1);
    }

    var dige = [];
    $("input[name='digestivo[]']:checked").each(function () {
        dige += $(this).val()+",";
    });
    if (dige.length > 0){
        dige = dige.substring(0,dige.length-1);
    }

    var derma = [];
    $("input[name='derma[]']:checked").each(function () {
        derma += ($(this).val()+",");
    });
    if (derma.length > 0){
        derma = derma.substring(0,derma.length-1);
    }

    var osteo = [];
    $("input[name='osteomuscular[]']:checked").each(function () {
        osteo += $(this).val()+",";
    });
    if (osteo.length > 0){
        osteo = osteo.substring(0,osteo.length-1);
    }

    //Manipulacion de alimentos
    var inspeccion = [];
    $("input[name='inspeccion[]']:checked").each(function () {
        inspeccion += $(this).val()+",";
    });
    if (inspeccion.length > 0){
        inspeccion = inspeccion.substring(0,inspeccion.length-1);
    }

    var lesionMA = [];
    $("input[name='lesionesMA[]']:checked").each(function () {
        lesionMA += $(this).val()+",";
    });
    if (lesionMA.length > 0){
        lesionMA = lesionMA.substring(0,lesionMA.length-1);
    }

    // //Paraclinicos resultados
    // var resultadosEspiro = [];
    // $("input[name='resultadosEspiro[]']:checked").each(function () {
    //     resultadosEspiro += $(this).val()+",";
    // });
    // if (resultadosEspiro.length > 0){
    //     resultadosEspiro = resultadosEspiro.substring(0,resultadosEspiro.length-1);
    // }

    //Resultados
    var resultadosPC = [];
    $("input[name='pruebasComplementarias[]']:checked").each(function () {
        resultadosPC += ($(this).val()+",");
    });
    if (resultadosPC.length > 0){
        resultadosPC = resultadosPC.substring(0,resultadosPC.length-1);
    }

    var habitosSaludables = [];
    $("input[name='habitosSaludables[]']:checked").each(function () {
        habitosSaludables += ($(this).val()+",");
    });
    if (habitosSaludables.length > 0){
        habitosSaludables = habitosSaludables.substring(0,habitosSaludables.length-1);
    }

    var ocupacionales = [];
    $("input[name='ocupacionales[]']:checked").each(function () {
        ocupacionales += ($(this).val()+",");
    });
    if (ocupacionales.length > 0){
        ocupacionales = ocupacionales.substring(0,ocupacionales.length-1);
    }

    var ingresoSVE = [];
    $("input[name='ingresoSVE[]']:checked").each(function () {
        ingresoSVE += ($(this).val()+",");
    });
    if (ingresoSVE.length > 0){
        ingresoSVE = ingresoSVE.substring(0,ingresoSVE.length-1);
    }

    var medicasResult = [];
    $("input[name='medicasresult[]']:checked").each(function () {
        medicasResult += ($(this).val()+",");
    });
    if (medicasResult.length > 0){
        medicasResult = medicasResult.substring(0,medicasResult.length-1);
    }

    var valoracionMedica = [];
    $("input[name='valoracionMedica[]']:checked").each(function () {
        valoracionMedica += ($(this).val()+",");
    });

    //Json object con todos los campos empresa que se agregan
    var i = $('.generalcontainer').length;;
    var factores = [];
    for (a = 0; a < i; a++){

        var fisicos = [];
        $("input[name='fisicos"+a+"[]']:checked").each(function () {
            fisicos += ($(this).val()+",");
        });
        if (fisicos.length > 0){
            fisicos = fisicos.substring(0,fisicos.length-1);
        }

        var quimicos = [];
        $("input[name='quimicos"+a+"[]']:checked").each(function () {
            quimicos += ($(this).val()+",");
        });
        if (quimicos.length > 0){
            quimicos = quimicos.substring(0,quimicos.length-1);
        }

        var ergonomicos = [];
        $("input[name='ergonomicos"+a+"[]']:checked").each(function () {
            ergonomicos += ($(this).val()+",");
        });
        if (ergonomicos.length > 0){
            ergonomicos = ergonomicos.substring(0,ergonomicos.length-1);
        }

        var biologicos = [];
        $("input[name='biologicos"+a+"[]']:checked").each(function () {
            biologicos += ($(this).val()+",");
        });
        if (biologicos.length > 0){
            biologicos = biologicos.substring(0,biologicos.length-1);
        }

        var psicosociales = [];
        $("input[name='psicosociales"+a+"[]']:checked").each(function () {
            psicosociales += ($(this).val()+",");
        });
        if (psicosociales.length > 0){
            psicosociales = psicosociales.substring(0,psicosociales.length-1);
        }

        var seguridad = [];
        $("input[name='seguridad"+a+"[]']:checked").each(function () {
            seguridad += ($(this).val()+",");
        });
        if (seguridad.length > 0){
            seguridad = seguridad.substring(0,seguridad.length-1);
        }

        factores.push({
            "empresa":$("#empresaFR"+a).val(),
            "cargo":$("#cargoFR"+a).val(),
            "tiempoexposicion":$("#tiempoExp"+a).val(),
            "antiguedad":$('#antiguedadTE'+a+' option:selected').val(),
            "fisicos":fisicos,
            "quimicos":quimicos,
            "ergonomicos":ergonomicos,
            "biologicos":biologicos,
            "psicosociales":psicosociales,
            "seguridad":seguridad,
            "otros":$("#otrosFactores"+a).val()
        });
    }

    //Json object con todos los campos de accidentes de trabajo
    var accidentes = [];

    $('.accidenteTR').each(function () {
       accidentes.push({
          "fecha":$(this).find('#fechaAT').val(),
          "empresa":$(this).find('#empresaAT').val(),
          "lesion":$(this).find('#lesionAT').val(),
          "dias_incapacidad":$(this).find('#diasIncapacidadAT').val(),
          "secuelas":$(this).find('#secuelasAT').val(),
       });
    });

    //Json object con todos los campos de enfermedad profesional
    var enfermedades = [];

    $(".enfermedadTR").each(function () {
        enfermedades.push({
            "fecha":$(this).find("#fechaEP").val(),
            "empresa":$(this).find("#empresaEP").val(),
            "diagnostico":$(this).find("#diagnosticoEP").val(),
            "arl":$(this).find("#arlEP").val(),
            "reubicacion":$(this).find("#reubicacionEP").val()
        });
    });


    //Json object con todos los campos de paraclinicos
    var paraclinicos = [];

    $(".paraclinicosTR").each(function () {
        paraclinicos.push({
            "examenLaboratorio_id":$(this).find("#examenesLab option:selected").val(),
            "diagnostico":$(this).find("#diagnosticoPara option:selected").val(),
            "observacion":$(this).find("#observacionesPara").val(),
        });
    });

    //Json object con todos los campos de Impresion diagnostica
    var Idiagnostica = [];

    $(".impresionDTR").each(function () {
        Idiagnostica.push({
            "diagnostico":$(this).find("#diagnosticoID").val(),
            "sospechaorigen":$(this).find("#sospechaOrigen option:selected").val(),
            "tipodiagnostico":$(this).find("#tipoDiagnostico option:selected").val(),
            "codigo":$(this).find("#buscarCIE10").val()
        });
    });

    //Split cedula medico y paciente
    var cedulaM = $('#medicoCedula').val();
    cedulaM = cedulaM.split(" ")[1];

    var cedulaP = $('#documentoPaciente').val();
    cedulaP = cedulaP.split(" ")[1];
    var  json1 = {
        "tipo": 0,
        "documento": $('#documentoBuscado').val(),
        "fecha": $('#fechaRegistro').val(),
        "medicoApertura_id": $('#id_medicoA').val(),
        "paciente": $('#nombrePaciente').val(),
        "paciente_cedula": cedulaP,
        "motivoevaluacion": $('#motivoE option:selected').val(),
        "cargoEvaluar": $('#cargoE').val(),
        "motivoevaluacion_otros": otros,
        "accidente": $('input[name=accidentesSN]:checked').val(),
        "enfermedad": $('input[name=enfermedadSN]:checked').val(),
        "cargos": {
            "fecha": $('#fechaIngreso').val(),
            "jornada": $('input[name=jornadaTrabajo]:checked').val(),
            "antiguedad": $('#antiguedadO').val(),
            "antiguedadMedida": $('#antiguedadM option:selected').val(),
            "area": $('#area').val(),
            "horas": $('#horasT').val(),
            "empresa": $('#empresaActual').val(),
            "elementos": checked,
            "cargo": $('#cargoE').val(),
            'otros_elementos': $('#otrosObs').val()
        },
        "habitos": {
            "alcohol": $('input[name=alcoholSN]:checked').val(),
            "alcohol_frecuencia": $('#frecuenciaA option:selected').val(),
            "alcohol_cantidad": $('#cantidadA').val(),
            "sedentarismo": $('input[name=sedentarismoSN]:checked').val(),
            "sustancias": $('input[name=psicoactivasSN]:checked').val(),
            "sustancias_frecuencia": $('#frecuenciaPSC option:selected').val(),
            "sustancias_cual": $('#cualPSC').val(),
            "tabaquismo": $('input[name=tabaquismoSN]:checked').val(),
            "tabaquismo_cantidad": $('#cantidadCD').val(),
            "tabaquismo_exfumador": $('input[name=exfumador]:checked').val(),
            "tabaquismo_tiempo": $('#tiempoFumador').val(),
            "tabaquismo_tiempoMedida": $('#tiempoFM option:selected').val(),
            "deporte": $('input[name=deporteSN]:checked').val(),
            "deporte_cual": $('#cualD').val(),
            "deporte_frecuencia": $('#frecuenciaD option:selected').val(),
            "deporte_lesion": $('input[name=lesionesD]:checked').val(),
            "deporte_lesion_cual": $('#cualLD').val(),
        },
        "antecedentes": {
            "familiares_hta": $('input[name=hta]:checked').val(),
            "familiares_hta_parentesco": $('#parentescoHTA option:selected').val(),
            "familiares_eap": $('input[name=eap]:checked').val(),
            "familiares_eap_parentesco": $('#parentescoEAP option:selected').val(),
            "familiares_ecv": $('input[name=ecv]:checked').val(),
            "familiares_ecv_parentesco": $('#parentescoECV option:selected').val(),
            "familiares_tbc": $('input[name=tbc]:checked').val(),
            "familiares_tbc_parentesco": $('#parentescoTBC option:selected').val(),
            "familiares_asma": $('input[name=asma]:checked').val(),
            "familiares_asma_parentesco": $('#parentescoAsma option:selected').val(),
            "familiares_alergia": $('input[name=alergias]:checked').val(),
            "familiares_alergia_parentesco": $('#parentescoAlergias option:selected').val(),
            "familiares_artritis": $('input[name=artritis]:checked').val(),
            "familiares_artritis_parentesco": $('#parentescoArtritis option:selected').val(),
            "familiares_varices": $('input[name=varices]:checked').val(),
            "familiares_varices_parentesco": $('#parentescoVarices option:selected').val(),
            "familiares_cancer": $('input[name=cancer]:checked').val(),
            "familiares_cancer_parentesco": $('#parentescoCancer option:selected').val(),
            "familiares_diabetes": $('input[name=diabetes]:checked').val(),
            "familiares_diabetes_parentesco": $('#parentescoDiabetes option:selected').val(),
            "familiares_enfermedadRenal": $('input[name=enfermedadRenal]:checked').val(),
            "familiares_enfermedadRenal_parentesco": $('#parentescoEnfermedadRenal option:selected').val(),
            "familiares_displipidemia": $('input[name=dislipidemia]:checked').val(),
            "familiares_displipidemia_parentesco": $('#parentescoDislipidemia option:selected').val(),
            "familiares_enfermedadCoronaria": $('input[name=enfermedadCoronaria]:checked').val(),
            "familiares_enfermedadCoronaria_parentesco": $('#parentescoEnfermedadCoronaria option:selected').val(),
            "familiares_enfermedadColagenosis": $('input[name=enfermedadColagenosis]:checked').val(),
            "familiares_enfermedadColagenosis_parentesco": $('#parentescoEnfermedadColagenosis option:selected').val(),
            "familiares_enfermedadTiroidea": $('input[name=enfermedadTiroidea]:checked').val(),
            "familiares_enfermedadTiroidea_parentesco": $('#parentescoEnfermedadTiroidea option:selected').val(),
            "familiares_otra": $('input[name=otra]:checked').val(),
            "familiares_otra_parentesco": $('#parentescoOtra option:selected').val(),
            "familiares_otra_observacion": $('#familiaresOtraObs').val(),
            "personales_patologicos": $('input[name=patologicos]:checked').val(),
            "personales_patologicos_parentesco": $('#observacionesPatologico').val(),
            "personales_quirurgicos": $('input[name=quirurgico]:checked').val(),
            "personales_quirurgicos_parentesco": $('#observacionesQuirurgico').val(),
            "personales_traumaticos": $('input[name=traumaticos]:checked').val(),
            "personales_traumaticos_parentesco": $('#observacionesTraumaticos').val(),
            "personales_farmacologicos": $('input[name=farmacologicos]:checked').val(),
            "personales_farmacologicos_parentesco": $('#observacionesFarmacologico').val(),
            "personales_transfusionales": $('input[name=Transfusionales]:checked').val(),
            "personales_transfusionales_parentesco": $('#observacionesTransfusionales').val(),
            "personales_otros": $('input[name=otros]:checked').val(),
            "personales_otros_parentesco": $('#observacionesOtros').val(),
            "inmunologicos_carnetVacunacion": $('input[name=carnetV]:checked').val(),
            "inmunologicos_hepatitisA": $('#hepatitisA option:selected').val(),
            "inmunologicos_hepatitisB": $('#hepatitisB option:selected').val(),
            "inmunologicos_tripleViral": $('#tripleV option:selected').val(),
            "inmunologicos_tetano": $('#tetano option:selected').val(),
            "inmunologicos_varicela": $('#varicelaAI option:selected').val(),
            "inmunologicos_influenza": $('#influenza option:selected').val(),
            "inmunologicos_fiebreAmarilla": $('#fiebreA option:selected').val(),
            "inmunologicos_otros": $('#otrosAI option:selected').val(),
            "inmunologicos_otros_obs": $('#inmunologicosOtrosObs').val(),
            "ginecoobstetricos_menarquia": $('#menarquia').val(),
            "ginecoobstetricos_fechaUltimoPeriodo": $('#fechaUPeriodo').val(),
            "ginecoobstetricos_fechaUltimaCitologia": $('#fechaUC').val(),
            "ginecoobstetricos_resultado": $('#resultado').val(),
            "ginecoobstetricos_ciclos_1": $('#ciclos').val(),
            "ginecoobstetricos_ciclos_2": $('#ciclos2').val(),
            "ginecoobstetricos_fechaUltimoParto": $('#fechaUParto').val(),
            "ginecoobstetricos_metodoPlanificacion": $('#metodoP').val(),
            "ginecoobstetricos_G": $('#obstetricaG').val(),
            "ginecoobstetricos_P": $('#obstetricaP').val(),
            "ginecoobstetricos_A": $('#obstetricaA').val(),
            "ginecoobstetricos_C": $('#obstetricaC').val(),
            "ginecoobstetricos_E": $('#obstetricaE').val(),
        },
        "revisionporsistemas": {
            "neurologico": neuro,
            "cardiovascular": cardio,
            "genitourinario": geni,
            "digestivo": dige,
            "dermatologico": derma,
            "hematologico": $("input[name='hermatologico']:checked").val(),
            "respiratorio": $("input[name='respiratorio']:checked").val(),
            "psiquiatrico": $("input[name='psiquiatrico']:checked").val(),
            "osteomuscular": osteo,
            "observaciones": $('#observacionesRS').val()
        },
        "examenfisico": {
            "estadogeneral": $('#estadoG option:selected').val(),
            "dominancia": $('#dominancia option:selected').val(),
            'tas': $('#tenArt1').val(),
            'tad': $('#tenArt2').val(),
            'fc': $('#fc').val(),
            'fr': $('#fr').val(),
            't': $('#t').val(),
            "perimetroabdominal": $('#pa').val(),
            "talla": $('#talla').val(),
            "peso": $('#peso').val(),
            "imc": $('#imc').val(),
            "cabezacuello": $('#cabezaCuello option:selected').val(),
            "cabezacuello_obs": $('#cabezaCDescripcion').val(),
            "oido": $('#oidoEF option:selected').val(),
            "oido_obs": $('#oidoDescriocion').val(),
            "piel": $('#pielEF option:selected').val(),
            "piel_obs": $('#pielDescripcion').val(),
            "ojos": $('#ojosEF option:selected').val(),
            "ojos_obs": $('#ojosDescripcion').val(),
            "ojos_ojoderecho": $('#ojoDerecho option:selected').val(),
            "ojos_ojoizquierdo": $('#ojoIzquierdo option:selected').val(),
            "torax": $('#toraxEF option:selected').val(),
            "torax_rscs": $('#RsCs option:selected').val(),
            "torax_rscs_obs": $('#rscslDescripcion').val(),
            "torax_rsrs": $('#rsrs').val(),
            "torax_senos": $('#senosEF').val(),
            "abdomen": $('#abdomenEF option:selected').val(),
            "abdomen_obs": $('#abdomenDescriocion').val(),
            "abdomen_hernias": $('#herniaDescripcion').val(),
            "extremidadsuperior": $('#extSup option:selected').val(),
            "extremidadsuperior_descripcion": $('#extSupDescripcion').val(),
            "extremidadsuperior_derechotinel": $('#derechoTin option:selected').val(),
            "extremidadsuperior_izquierdotinel": $('#izquierdoTin option:selected').val(),
            "extremidadsuperior_derechophanel": $('#derechoPhan option:selected').val(),
            "extremidadsuperior_izquierdophanel": $('#izquierdoPhan option:selected').val(),
            "extremidadsuperior_derechofinkelstein": $('#derechoFink option:selected').val(),
            "extremidadsuperior_izquierdofinkelstein": $('#izquierdoFink option:selected').val(),
            "neurologico": $('#neuroEF option:selected').val(),
            "neurologico_obs": $('#neuroDescripcion').val(),
            "neurologico_parescraneanos": $('#paresCraneo option:selected').val(),
            "neurologico_sensibilidad": $('#sensibilidadEF option:selected').val(),
            "neurologico_marchaenlinea": $('#marchaLinea option:selected').val(),
            "neurologico_reflejos": $('#reflejosEF option:selected').val(),
            "neurologico_coordinacionromber": $('#coorRomber option:selected').val(),
            "neurologico_esferamental": $('#esferaMental option:selected').val(),
            "extremidadinferior": $('#extInferior option:selected').val(),
            "extremidadinferior_obs": $('#extInferiorDescripcion').val(),
            "extremidadinferior_varices": $('#varicesEF option:selected').val(),
            "extremidadinferior_varices_obs": $('#varicesDescripcion').val(),
            "extremidadinferior_juanetes": $('#juanetesEF option:selected').val(),
            "extremidadinferior_juanetes_obs": $('#juanetesDescripcion').val(),
            "columna_inspeccion_simetria": $('#simetriaI option:selected').val(),
            "columna_inspeccion_simetria_obs": $('#descripcionSimetria').val(),
            "columna_inspeccion_curvatura": $('#curvaturaP option:selected').val(),
            "columna_inspeccion_curvatura_obs": $('#descripcionCurvatura').val(),
            "columna_palpacion_dolor": $('input[name=dolorEF]:checked').val(),
            "columna_palpacion_dolor_obs": $('#descripcionDolor').val(),
            "columna_palpacion_espasmo": $('input[name=espasmoEF]:checked').val(),
            "columna_palpacion_espasmo_obs": $('#descripcionEspasmo').val(),
            "columna_test_wells": $('#WellsM option:selected').val(),
            "columna_test_wells_obs": $('#descripcionWells').val(),
            "columna_test_shober": $('#ShoberM option:selected').val(),
            "columna_test_shober_obs": $('#descripcionShober').val(),
            "columna_movilidad_flexion": $('#flexionM option:selected').val(),
            "columna_movilidad_flexion_obs": $('#descripcionFlexion').val(),
            "columna_movilidad_extension": $('#extensionM option:selected').val(),
            "columna_movilidad_extension_obs": $('#descripcionExtension').val(),
            "columna_movilidad_flexionlateral": $('#flexionLM option:selected').val(),
            "columna_movilidad_flexionlateral_obs": $('#descripcionFlexionL').val(),
            "columna_movilidad_rotacion": $('#rotacionM option:selected').val(),
            "columna_movilidad_rotacion_obs": $('#descripcionRotacion').val(),
            "columna_marcha_puntas": $('#puntasM option:selected').val(),
            "columna_marcha_puntas_obs": $('#descripcionPuntas').val(),
            "columna_marcha_talones": $('#talonesM option:selected').val(),
            "columna_marcha_talones_obs": $('#descripcionTalones').val(),
            "columna_trofismomuscular_trofismo": $('#trofismoM option:selected').val(),
            "columna_trofismomuscular_trofismo_obs": $('#descripcionTrofismo').val(),
            "columna": $('#columnaEF option:selected').val(),
            "columna_descripcion": $('#descripcionColumna').val(),
            "balancemuscular_cinturaescapular": $('#cinturaEscapular option:selected').val(),
            "balancemuscular_cinturaescapular_obs": $('#observacionesCinEs').val(),
            "balancemuscular_pectoral": $('#pectoralEF option:selected').val(),
            "balancemuscular_pectoral_obs": $('#observacionesPect').val(),
            "balancemuscular_brazo": $('#brazoBM option:selected').val(),
            "balancemuscular_brazo_obs": $('#observacionesBrazo').val(),
            "balancemuscular_antebrazo": $('#antebrazoBM option:selected').val(),
            "balancemuscular_antebrazo_obs": $('#observacionesAnteb').val(),
            "balancemuscular_caderagluteo": $('#caderaGluteo option:selected').val(),
            "balancemuscular_caderagluteo_obs": $('#observacionesCadGlu').val(),
            "balancemuscular_muslos": $('#muslosBM option:selected').val(),
            "balancemuscular_muslos_obs": $('#observacionesMuslos').val(),
            "balancemuscular_piernas": $('#piernasBM option:selected').val(),
            "balancemuscular_piernas_obs": $('#observacionesPiernas').val(),
            "balancemuscular_explicacion": $('#explicacionBM').val(),
            "reflejososteotendinosos_bicipital_derecho": $('#bicipitalD option:selected').val(),
            "reflejososteotendinosos_bicipital_izquierdo": $('#bicipitalI option:selected').val(),
            "reflejososteotendinosos_radial_derecho": $('#radialD option:selected').val(),
            "reflejososteotendinosos_radial_izquierdo": $('#radialI option:selected').val(),
            "reflejososteotendinosos_rotuliano_derecho": $('#rotulianoD option:selected').val(),
            "reflejososteotendinosos_rotuliano_izquierdo": $('#rotulianoI option:selected').val(),
            "reflejososteotendinosos_alquiliano_derecho": $('#aquilianoD option:selected').val(),
            "reflejososteotendinosos_alquiliano_izquierdo": $('#aquilianoI option:selected').val(),
            "fuerza_miembrosuperior_derecho": $('#miembroD option:selected').val(),
            "fuerza_miembrosuperior_izquierda": $('#miembroI option:selected').val(),
            "fuerza_miembroinferior_derecho": $('#miembroInfD option:selected').val(),
            "fuerza_miembroinferior_izquierda": $('#miembroInfI option:selected').val(),
        },
        "manipulacionalimentos": {
            "solicitado": $('input[name=solicitadoSN]:checked').val(),
            "respiratorio_cumple": $('input[name=pregunta]:checked').val(),
            "respiratorio_inspeccion": inspeccion,
            "respiratorio_ausculacion": $('#auscultacion').val(),
            "dermatologico_prurito": $('input[name=pregunta2]:checked').val(),
            "dermatologico_medicamento": $('input[name=pregunta3]:checked').val(),
            "dermatologico_cual": $('#cualMA').val(),
            "dermatologico_lesiones": lesionMA,
            "observacion": $('#observacionesMA').val(),
        },
        "paraclinicosdatos": {
            "optometria": $('#optometriaPara option:selected').val(),
            "optometria_alteracioncorregida": $('input[name=alteracionSN]:checked').val(),
            "optometria_alteracioncorregida_obs": $('#diagnosticoOpto').val(),
            "visiometria": $('#visiometriaPara option:selected').val(),
            "visiometria_alteracioncorregida": $('input[name=alteracionVSN]:checked').val(),
            "visiometria_alteracioncorregida_obs": $('#diagnosticoVisio').val(),
            "espirometria": $('input[name=espiroSN]:checked').val(),
            "espirometria_resultado": $('#espirometriaPara option:selected').val(),
            "espirometria_obs": $('#observacionesEspiro').val(),
            "audiometria": $('input[name=audioSN]:checked').val(),
            "audiometria_resultado": $('input[name=resultadoAudioSN]:checked').val(),
            "audiometria_obs": $('#observacionesAudio').val(),
            "psicometrico": $('#psicometricosPara option:selected').val(),
            "psicometrico_obs": $('#observacionesPsiMe').val(),
            "psicologico": $('#psicologicoPara option:selected').val(),
            "psicologico_obs": $('#observacionesPsi').val(),
            "pruebavestibular": $('#vestibularPara option:selected').val(),
            "pruebavestibular_obs": $('#observacionesVesti').val()
        },
        "resultado":{
            "patologia":$('#patologiaRes option:selected').val(),
            "puedeContinuarConLabor":$('input[name=continuarSN]:checked').val(),
            "resultadoValoracionMedica":valoracionMedica,
            "reubicacionLaboral":$('#reubicacionRes option:selected').val(),
            "motivo": $('#motivoRes').val(),
            "recomendacion_medica": medicasResult,
            "recomendacion_ingresosve": ingresoSVE,
            "recomendacion_ocupacional": ocupacionales,
            "recomendacion_habitos": habitosSaludables,
            "recomendacion_pruebacomplementaria": resultadosPC,
            "recomendacion_pruebacomplementaria_otros": $('#otroPC').val(),
            "restricciones": $('#restriccionesID').val(),
            "recomendacionesAdicionales": $('#recomendacionesID').val(),
        }
    };
    var json = Object.assign(json1);
    json1["factores"] = factores;
    json1["accidentes"] = accidentes;
    json1["enfermedades"] = enfermedades;
    json1["paraclinicos"] = paraclinicos;
    json1["idiagnostica"] = Idiagnostica;

    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: json,
        type: 'POST',
        url: '/historiaClinica/registrar',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            location.reload(true);
        },
        error: function (data) {
            console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                $.each(json.errors, function (key, value) {
                    $(".toast-body").append("<li>"+value+"</li>");
                });
                $("#toastHC").removeClass('hide').addClass('show');
                $("#toastHC").show();
                $('html, body').animate({
                    scrollTop:$('#toastHC').offset().top
                }, 1000);
            }
        }
    });
}

function actualizarHC() {

    //Obtener valores multiples de un checkbox
    var checked = "";
    $("input[name='elementosP[]']:checked").each(function () {
        checked += $(this).val()+",";
    });
    if (checked.length > 0){
        checked = checked.substring(0,checked.length-1);
    }

    var otros = "";
    $("input[name='otros[]']:checked").each(function () {
        otros += $(this).val()+",";
    });
    if (otros.length > 0){
        otros = otros.substring(0,otros.length-1);
    }

    var neuro = "";
    $("input[name='neuro[]']:checked").each(function () {
        neuro += $(this).val()+",";
    });
    if (neuro.length > 0){
        neuro = neuro.substring(0,neuro.length-1);
    }

    var cardio = "";
    $("input[name='cardio[]']:checked").each(function () {
        cardio += ($(this).val()+",");
    });
    if (cardio.length > 0){
        cardio = cardio.substring(0,cardio.length-1);
    }

    var geni = "";
    $("input[name='geni[]']:checked").each(function () {
        geni += $(this).val()+",";
    });
    if (geni.length > 0){
        geni = geni.substring(0,geni.length-1);
    }

    var dige = "";
    $("input[name='digestivo[]']:checked").each(function () {
        dige += $(this).val()+",";
    });
    if (dige.length > 0){
        dige = dige.substring(0,dige.length-1);
    }

    var derma = "";
    $("input[name='derma[]']:checked").each(function () {
        derma += ($(this).val()+",");
    });
    if (derma.length > 0){
        derma = derma.substring(0,derma.length-1);
    }

    var osteo = "";
    $("input[name='osteomuscular[]']:checked").each(function () {
        osteo += $(this).val()+",";
    });
    if (osteo.length > 0){
        osteo = osteo.substring(0,osteo.length-1);
    }

    //Manipulacion de alimentos
    var inspeccion = "";
    $("input[name='inspeccion[]']:checked").each(function () {
        inspeccion += $(this).val()+",";
    });
    if (inspeccion.length > 0){
        inspeccion = inspeccion.substring(0,inspeccion.length-1);
    }

    var lesionMA = "";
    $("input[name='lesionesMA[]']:checked").each(function () {
        lesionMA += $(this).val()+",";
    });
    if (lesionMA.length > 0){
        lesionMA = lesionMA.substring(0,lesionMA.length-1);
    }

    // //Paraclinicos resultados
    // var resultadosEspiro = "";
    // $("input[name='resultadosEspiro[]']:checked").each(function () {
    //     resultadosEspiro += $(this).val()+",";
    // });
    // if (resultadosEspiro.length > 0){
    //     resultadosEspiro = resultadosEspiro.substring(0,resultadosEspiro.length-1);
    // }

    //Resultados
    var resultadosPC = "";
    $("input[name='pruebasComplementarias[]']:checked").each(function () {
        resultadosPC += ($(this).val()+",");
    });
    if (resultadosPC.length > 0){
        resultadosPC = resultadosPC.substring(0,resultadosPC.length-1);
    }

    var habitosSaludables = "";
    $("input[name='habitosSaludables[]']:checked").each(function () {
        habitosSaludables += ($(this).val()+",");
    });
    if (habitosSaludables.length > 0){
        habitosSaludables = habitosSaludables.substring(0,habitosSaludables.length-1);
    }

    var ocupacionales = "";
    $("input[name='ocupacionales[]']:checked").each(function () {
        ocupacionales += ($(this).val()+",");
    });
    if (ocupacionales.length > 0){
        ocupacionales = ocupacionales.substring(0,ocupacionales.length-1);
    }

    var ingresoSVE = "";
    $("input[name='ingresoSVE[]']:checked").each(function () {
        ingresoSVE += ($(this).val()+",");
    });
    if (ingresoSVE.length > 0){
        ingresoSVE = ingresoSVE.substring(0,ingresoSVE.length-1);
    }

    var medicasResult = "";
    $("input[name='medicasresult[]']:checked").each(function () {
        medicasResult += ($(this).val()+",");
    });
    if (medicasResult.length > 0){
        medicasResult = medicasResult.substring(0,medicasResult.length-1);
    }

    var valoracionMedica = "";
    $("input[name='valoracionMedica[]']:checked").each(function () {
        valoracionMedica += ($(this).val()+",");
    });

    //Json object con todos los campos empresa que se agregan
    var i = $('.generalcontainer').length;
    var factores = [];
    for (a = 0; a < i; a++){
        console.log(i);

        var fisicos = "";
        $("input[name='fisicos"+a+"[]']:checked").each(function () {
            fisicos += ($(this).val()+",");
        });
        if (fisicos.length > 0){
            fisicos = fisicos.substring(0,fisicos.length-1);
        }

        var quimicos = "";
        $("input[name='quimicos"+a+"[]']:checked").each(function () {
            quimicos += ($(this).val()+",");
        });
        if (quimicos.length > 0){
            quimicos = quimicos.substring(0,quimicos.length-1);
        }

        var ergonomicos = "";
        $("input[name='ergonomicos"+a+"[]']:checked").each(function () {
            ergonomicos += ($(this).val()+",");
        });
        if (ergonomicos.length > 0){
            ergonomicos = ergonomicos.substring(0,ergonomicos.length-1);
        }

        var biologicos = "";
        $("input[name='biologicos"+a+"[]']:checked").each(function () {
            biologicos += ($(this).val()+",");
        });
        if (biologicos.length > 0){
            biologicos = biologicos.substring(0,biologicos.length-1);
        }

        var psicosociales = "";
        $("input[name='psicosociales"+a+"[]']:checked").each(function () {
            psicosociales += ($(this).val()+",");
        });
        if (psicosociales.length > 0){
            psicosociales = psicosociales.substring(0,psicosociales.length-1);
        }

        var seguridad = "";
        $("input[name='seguridad"+a+"[]']:checked").each(function () {
            seguridad += ($(this).val()+",");
        });
        if (seguridad.length > 0){
            seguridad = seguridad.substring(0,seguridad.length-1);
        }

        factores.push({
            "empresa":$("#empresaFR"+a).val(),
            "cargo":$("#cargoFR"+a).val(),
            "tiempoexposicion":$("#tiempoExp"+a).val(),
            "antiguedad":$('#antiguedadTE'+a+' option:selected').val(),
            "fisicos":fisicos,
            "quimicos":quimicos,
            "ergonomicos":ergonomicos,
            "biologicos":biologicos,
            "psicosociales":psicosociales,
            "seguridad":seguridad,
            "otros":$("#otrosFactores"+a).val()
        });
    }

    //Json object con todos los campos de accidentes de trabajo
    var accidentes = [];

    $('.accidenteTR').each(function () {
       accidentes.push({
          "fecha":$(this).find('#fechaAT').val(),
          "empresa":$(this).find('#empresaAT').val(),
          "lesion":$(this).find('#lesionAT').val(),
          "dias_incapacidad":$(this).find('#diasIncapacidadAT').val(),
          "secuelas":$(this).find('#secuelasAT').val(),
       });
    });

    //Json object con todos los campos de enfermedad profesional
    var enfermedades = [];

    $(".enfermedadTR").each(function () {
        enfermedades.push({
            "fecha":$(this).find("#fechaEP").val(),
            "empresa":$(this).find("#empresaEP").val(),
            "diagnostico":$(this).find("#diagnosticoEP").val(),
            "arl":$(this).find("#arlEP").val(),
            "reubicacion":$(this).find("#reubicacionEP").val()
        });
    });


    //Json object con todos los campos de paraclinicos
    var paraclinicos = [];

    $(".paraclinicosTR").each(function () {
        paraclinicos.push({
            "examenLaboratorio_id":$(this).find("#examenesLab option:selected").val(),
            "diagnostico":$(this).find("#diagnosticoPara option:selected").val(),
            "observacion":$(this).find("#observacionesPara").val(),
        });
    });

    //Json object con todos los campos de Impresion diagnostica
    var Idiagnostica = [];

    $(".impresionDTR").each(function () {
        Idiagnostica.push({
            "diagnostico":$(this).find("#diagnosticoID").val(),
            "sospechaorigen":$(this).find("#sospechaOrigen option:selected").val(),
            "tipodiagnostico":$(this).find("#tipoDiagnostico option:selected").val(),
            "codigo":$(this).find("#buscarCIE10").val()
        });
    });

    //Split cedula medico y paciente
    var cedulaM = $('#medicoCedula').val();
    cedulaM = cedulaM.split(" ")[1];

    var cedulaP = $('#documentoPaciente').val();
    cedulaP = cedulaP.split(" ")[1];
    var  json1 = {
        "id": $('#llave').val(),
        "tipo": 0,
        "documento": $('#documentoBuscado').val(),
        "fecha": $('#fechaRegistro').val(),
        "medicoApertura_id": $('#id_medicoA').val(),
        "paciente": $('#nombrePaciente').val(),
        "paciente_cedula": cedulaP,
        "motivoevaluacion": $('#motivoE option:selected').val(),
        "cargoEvaluar": $('#cargoE').val(),
        "motivoevaluacion_otros": otros,
        "accidente": $('input[name=accidentesSN]:checked').val(),
        "enfermedad": $('input[name=enfermedadSN]:checked').val(),
        "cargos": {
            "fecha": $('#fechaIngreso').val(),
            "jornada": $('input[name=jornadaTrabajo]:checked').val(),
            "antiguedad": $('#antiguedadO').val(),
            "antiguedadMedida": $('#antiguedadM option:selected').val(),
            "area": $('#area').val(),
            "horas": $('#horasT').val(),
            "empresa": $('#empresaActual').val(),
            "elementos": checked,
            "cargo": $('#cargoE').val(),
            'otros_elementos': $('#otrosObs').val()
        },
        "habitos": {
            "alcohol": $('input[name=alcoholSN]:checked').val(),
            "alcohol_frecuencia": $('#frecuenciaA option:selected').val(),
            "alcohol_cantidad": $('#cantidadA').val(),
            "sedentarismo": $('input[name=sedentarismoSN]:checked').val(),
            "sustancias": $('input[name=psicoactivasSN]:checked').val(),
            "sustancias_frecuencia": $('#frecuenciaPSC option:selected').val(),
            "sustancias_cual": $('#cualPSC').val(),
            "tabaquismo": $('input[name=tabaquismoSN]:checked').val(),
            "tabaquismo_cantidad": $('#cantidadCD').val(),
            "tabaquismo_exfumador": $('input[name=exfumador]:checked').val(),
            "tabaquismo_tiempo": $('#tiempoFumador').val(),
            "tabaquismo_tiempoMedida": $('#tiempoFM option:selected').val(),
            "deporte": $('input[name=deporteSN]:checked').val(),
            "deporte_cual": $('#cualD').val(),
            "deporte_frecuencia": $('#frecuenciaD option:selected').val(),
            "deporte_lesion": $('input[name=lesionesD]:checked').val(),
            "deporte_lesion_cual": $('#cualLD').val(),
        },
        "antecedentes": {
            "familiares_hta": $('input[name=hta]:checked').val(),
            "familiares_hta_parentesco": $('#parentescoHTA option:selected').val(),
            "familiares_eap": $('input[name=eap]:checked').val(),
            "familiares_eap_parentesco": $('#parentescoEAP option:selected').val(),
            "familiares_ecv": $('input[name=ecv]:checked').val(),
            "familiares_ecv_parentesco": $('#parentescoECV option:selected').val(),
            "familiares_tbc": $('input[name=tbc]:checked').val(),
            "familiares_tbc_parentesco": $('#parentescoTBC option:selected').val(),
            "familiares_asma": $('input[name=asma]:checked').val(),
            "familiares_asma_parentesco": $('#parentescoAsma option:selected').val(),
            "familiares_alergia": $('input[name=alergias]:checked').val(),
            "familiares_alergia_parentesco": $('#parentescoAlergias option:selected').val(),
            "familiares_artritis": $('input[name=artritis]:checked').val(),
            "familiares_artritis_parentesco": $('#parentescoArtritis option:selected').val(),
            "familiares_varices": $('input[name=varices]:checked').val(),
            "familiares_varices_parentesco": $('#parentescoVarices option:selected').val(),
            "familiares_cancer": $('input[name=cancer]:checked').val(),
            "familiares_cancer_parentesco": $('#parentescoCancer option:selected').val(),
            "familiares_diabetes": $('input[name=diabetes]:checked').val(),
            "familiares_diabetes_parentesco": $('#parentescoDiabetes option:selected').val(),
            "familiares_enfermedadRenal": $('input[name=enfermedadRenal]:checked').val(),
            "familiares_enfermedadRenal_parentesco": $('#parentescoEnfermedadRenal option:selected').val(),
            "familiares_displipidemia": $('input[name=dislipidemia]:checked').val(),
            "familiares_displipidemia_parentesco": $('#parentescoDislipidemia option:selected').val(),
            "familiares_enfermedadCoronaria": $('input[name=enfermedadCoronaria]:checked').val(),
            "familiares_enfermedadCoronaria_parentesco": $('#parentescoEnfermedadCoronaria option:selected').val(),
            "familiares_enfermedadColagenosis": $('input[name=enfermedadColagenosis]:checked').val(),
            "familiares_enfermedadColagenosis_parentesco": $('#parentescoEnfermedadColagenosis option:selected').val(),
            "familiares_enfermedadTiroidea": $('input[name=enfermedadTiroidea]:checked').val(),
            "familiares_enfermedadTiroidea_parentesco": $('#parentescoEnfermedadTiroidea option:selected').val(),
            "familiares_otra": $('input[name=otra]:checked').val(),
            "familiares_otra_parentesco": $('#parentescoOtra option:selected').val(),
            "familiares_otra_observacion": $('#familiaresOtraObs').val(),
            "personales_patologicos": $('input[name=patologicos]:checked').val(),
            "personales_patologicos_parentesco": $('#observacionesPatologico').val(),
            "personales_quirurgicos": $('input[name=quirurgico]:checked').val(),
            "personales_quirurgicos_parentesco": $('#observacionesQuirurgico').val(),
            "personales_traumaticos": $('input[name=traumaticos]:checked').val(),
            "personales_traumaticos_parentesco": $('#observacionesTraumaticos').val(),
            "personales_farmacologicos": $('input[name=farmacologicos]:checked').val(),
            "personales_farmacologicos_parentesco": $('#observacionesFarmacologico').val(),
            "personales_transfusionales": $('input[name=Transfusionales]:checked').val(),
            "personales_transfusionales_parentesco": $('#observacionesTransfusionales').val(),
            "personales_otros": $('input[name=otros]:checked').val(),
            "personales_otros_parentesco": $('#observacionesOtros').val(),
            "inmunologicos_carnetVacunacion": $('input[name=carnetV]:checked').val(),
            "inmunologicos_hepatitisA": $('#hepatitisA option:selected').val(),
            "inmunologicos_hepatitisB": $('#hepatitisB option:selected').val(),
            "inmunologicos_tripleViral": $('#tripleV option:selected').val(),
            "inmunologicos_tetano": $('#tetano option:selected').val(),
            "inmunologicos_varicela": $('#varicelaAI option:selected').val(),
            "inmunologicos_influenza": $('#influenza option:selected').val(),
            "inmunologicos_fiebreAmarilla": $('#fiebreA option:selected').val(),
            "inmunologicos_otros": $('#otrosAI option:selected').val(),
            "inmunologicos_otros_obs": $('#inmunologicosOtrosObs').val(),
            "ginecoobstetricos_menarquia": $('#menarquia').val(),
            "ginecoobstetricos_fechaUltimoPeriodo": $('#fechaUPeriodo').val(),
            "ginecoobstetricos_fechaUltimaCitologia": $('#fechaUC').val(),
            "ginecoobstetricos_resultado": $('#resultado').val(),
            "ginecoobstetricos_ciclos_1": $('#ciclos').val(),
            "ginecoobstetricos_ciclos_2": $('#ciclos2').val(),
            "ginecoobstetricos_fechaUltimoParto": $('#fechaUParto').val(),
            "ginecoobstetricos_metodoPlanificacion": $('#metodoP').val(),
            "ginecoobstetricos_G": $('#obstetricaG').val(),
            "ginecoobstetricos_P": $('#obstetricaP').val(),
            "ginecoobstetricos_A": $('#obstetricaA').val(),
            "ginecoobstetricos_C": $('#obstetricaC').val(),
            "ginecoobstetricos_E": $('#obstetricaE').val(),
        },
        "revisionporsistemas": {
            "neurologico": neuro,
            "cardiovascular": cardio,
            "genitourinario": geni,
            "digestivo": dige,
            "dermatologico": derma,
            "hematologico": $("input[name='hermatologico']:checked").val(),
            "respiratorio": $("input[name='respiratorio']:checked").val(),
            "psiquiatrico": $("input[name='psiquiatrico']:checked").val(),
            "osteomuscular": osteo,
            "observaciones": $('#observacionesRS').val()
        },
        "examenfisico": {
            "estadogeneral": $('#estadoG option:selected').val(),
            "dominancia": $('#dominancia option:selected').val(),
            'tas': $('#tenArt1').val(),
            'tad': $('#tenArt2').val(),
            'fc': $('#fc').val(),
            'fr': $('#fr').val(),
            't': $('#t').val(),
            "perimetroabdominal": $('#pa').val(),
            "talla": $('#talla').val(),
            "peso": $('#peso').val(),
            "imc": $('#imc').val(),
            "cabezacuello": $('#cabezaCuello option:selected').val(),
            "cabezacuello_obs": $('#cabezaCDescripcion').val(),
            "oido": $('#oidoEF option:selected').val(),
            "oido_obs": $('#oidoDescriocion').val(),
            "piel": $('#pielEF option:selected').val(),
            "piel_obs": $('#pielDescripcion').val(),
            "ojos": $('#ojosEF option:selected').val(),
            "ojos_obs": $('#ojosDescripcion').val(),
            "ojos_ojoderecho": $('#ojoDerecho option:selected').val(),
            "ojos_ojoizquierdo": $('#ojoIzquierdo option:selected').val(),
            "torax": $('#toraxEF option:selected').val(),
            "torax_rscs": $('#RsCs option:selected').val(),
            "torax_rscs_obs": $('#rscslDescripcion').val(),
            "torax_rsrs": $('#rsrs').val(),
            "torax_senos": $('#senosEF').val(),
            "abdomen": $('#abdomenEF option:selected').val(),
            "abdomen_obs": $('#abdomenDescriocion').val(),
            "abdomen_hernias": $('#herniaDescripcion').val(),
            "extremidadsuperior": $('#extSup option:selected').val(),
            "extremidadsuperior_descripcion": $('#extSupDescripcion').val(),
            "extremidadsuperior_derechotinel": $('#derechoTin option:selected').val(),
            "extremidadsuperior_izquierdotinel": $('#izquierdoTin option:selected').val(),
            "extremidadsuperior_derechophanel": $('#derechoPhan option:selected').val(),
            "extremidadsuperior_izquierdophanel": $('#izquierdoPhan option:selected').val(),
            "extremidadsuperior_derechofinkelstein": $('#derechoFink option:selected').val(),
            "extremidadsuperior_izquierdofinkelstein": $('#izquierdoFink option:selected').val(),
            "neurologico": $('#neuroEF option:selected').val(),
            "neurologico_obs": $('#neuroDescripcion').val(),
            "neurologico_parescraneanos": $('#paresCraneo option:selected').val(),
            "neurologico_sensibilidad": $('#sensibilidadEF option:selected').val(),
            "neurologico_marchaenlinea": $('#marchaLinea option:selected').val(),
            "neurologico_reflejos": $('#reflejosEF option:selected').val(),
            "neurologico_coordinacionromber": $('#coorRomber option:selected').val(),
            "neurologico_esferamental": $('#esferaMental option:selected').val(),
            "extremidadinferior": $('#extInferior option:selected').val(),
            "extremidadinferior_obs": $('#extInferiorDescripcion').val(),
            "extremidadinferior_varices": $('#varicesEF option:selected').val(),
            "extremidadinferior_varices_obs": $('#varicesDescripcion').val(),
            "extremidadinferior_juanetes": $('#juanetesEF option:selected').val(),
            "extremidadinferior_juanetes_obs": $('#juanetesDescripcion').val(),
            "columna_inspeccion_simetria": $('#simetriaI option:selected').val(),
            "columna_inspeccion_simetria_obs": $('#descripcionSimetria').val(),
            "columna_inspeccion_curvatura": $('#curvaturaP option:selected').val(),
            "columna_inspeccion_curvatura_obs": $('#descripcionCurvatura').val(),
            "columna_palpacion_dolor": $('input[name=dolorEF]:checked').val(),
            "columna_palpacion_dolor_obs": $('#descripcionDolor').val(),
            "columna_palpacion_espasmo": $('input[name=espasmoEF]:checked').val(),
            "columna_palpacion_espasmo_obs": $('#descripcionEspasmo').val(),
            "columna_test_wells": $('#WellsM option:selected').val(),
            "columna_test_wells_obs": $('#descripcionWells').val(),
            "columna_test_shober": $('#ShoberM option:selected').val(),
            "columna_test_shober_obs": $('#descripcionShober').val(),
            "columna_movilidad_flexion": $('#flexionM option:selected').val(),
            "columna_movilidad_flexion_obs": $('#descripcionFlexion').val(),
            "columna_movilidad_extension": $('#extensionM option:selected').val(),
            "columna_movilidad_extension_obs": $('#descripcionExtension').val(),
            "columna_movilidad_flexionlateral": $('#flexionLM option:selected').val(),
            "columna_movilidad_flexionlateral_obs": $('#descripcionFlexionL').val(),
            "columna_movilidad_rotacion": $('#rotacionM option:selected').val(),
            "columna_movilidad_rotacion_obs": $('#descripcionRotacion').val(),
            "columna_marcha_puntas": $('#puntasM option:selected').val(),
            "columna_marcha_puntas_obs": $('#descripcionPuntas').val(),
            "columna_marcha_talones": $('#talonesM option:selected').val(),
            "columna_marcha_talones_obs": $('#descripcionTalones').val(),
            "columna_trofismomuscular_trofismo": $('#trofismoM option:selected').val(),
            "columna_trofismomuscular_trofismo_obs": $('#descripcionTrofismo').val(),
            "columna": $('#columnaEF option:selected').val(),
            "columna_descripcion": $('#descripcionColumna').val(),
            "balancemuscular_cinturaescapular": $('#cinturaEscapular option:selected').val(),
            "balancemuscular_cinturaescapular_obs": $('#observacionesCinEs').val(),
            "balancemuscular_pectoral": $('#pectoralEF option:selected').val(),
            "balancemuscular_pectoral_obs": $('#observacionesPect').val(),
            "balancemuscular_brazo": $('#brazoBM option:selected').val(),
            "balancemuscular_brazo_obs": $('#observacionesBrazo').val(),
            "balancemuscular_antebrazo": $('#antebrazoBM option:selected').val(),
            "balancemuscular_antebrazo_obs": $('#observacionesAnteb').val(),
            "balancemuscular_caderagluteo": $('#caderaGluteo option:selected').val(),
            "balancemuscular_caderagluteo_obs": $('#observacionesCadGlu').val(),
            "balancemuscular_muslos": $('#muslosBM option:selected').val(),
            "balancemuscular_muslos_obs": $('#observacionesMuslos').val(),
            "balancemuscular_piernas": $('#piernasBM option:selected').val(),
            "balancemuscular_piernas_obs": $('#observacionesPiernas').val(),
            "balancemuscular_explicacion": $('#explicacionBM').val(),
            "reflejososteotendinosos_bicipital_derecho": $('#bicipitalD option:selected').val(),
            "reflejososteotendinosos_bicipital_izquierdo": $('#bicipitalI option:selected').val(),
            "reflejososteotendinosos_radial_derecho": $('#radialD option:selected').val(),
            "reflejososteotendinosos_radial_izquierdo": $('#radialI option:selected').val(),
            "reflejososteotendinosos_rotuliano_derecho": $('#rotulianoD option:selected').val(),
            "reflejososteotendinosos_rotuliano_izquierdo": $('#rotulianoI option:selected').val(),
            "reflejososteotendinosos_alquiliano_derecho": $('#aquilianoD option:selected').val(),
            "reflejososteotendinosos_alquiliano_izquierdo": $('#aquilianoI option:selected').val(),
            "fuerza_miembrosuperior_derecho": $('#miembroD option:selected').val(),
            "fuerza_miembrosuperior_izquierda": $('#miembroI option:selected').val(),
            "fuerza_miembroinferior_derecho": $('#miembroInfD option:selected').val(),
            "fuerza_miembroinferior_izquierda": $('#miembroInfI option:selected').val(),
        },
        "manipulacionalimentos": {
            "solicitado": $('input[name=solicitadoSN]:checked').val() == null ? "" : $('input[name=solicitadoSN]:checked').val(),
            "respiratorio_cumple": $('input[name=pregunta]:checked').val() == null ? "" : $('input[name=pregunta]:checked').val(),
            "respiratorio_inspeccion": inspeccion,
            "respiratorio_ausculacion": $('#auscultacion').val(),
            "dermatologico_prurito": $('input[name=pregunta2]:checked').val() == null ? "" : $('input[name=pregunta2]:checked').val(),
            "dermatologico_medicamento": $('input[name=pregunta3]:checked').val() == null ? "" : $('input[name=pregunta3]:checked').val(),
            "dermatologico_cual": $('#cualMA').val(),
            "dermatologico_lesiones": lesionMA,
            "observacion": $('#observacionesMA').val(),
        },
        "paraclinicosdatos": {
            "optometria": $('#optometriaPara option:selected').val(),
            "optometria_alteracioncorregida": $('input[name=alteracionSN]:checked').val(),
            "optometria_alteracioncorregida_obs": $('#diagnosticoOpto').val(),
            "visiometria": $('#visiometriaPara option:selected').val(),
            "visiometria_alteracioncorregida": $('input[name=alteracionVSN]:checked').val(),
            "visiometria_alteracioncorregida_obs": $('#diagnosticoVisio').val(),
            "espirometria": $('input[name=espiroSN]:checked').val(),
            "espirometria_resultado": $('#espirometriaPara option:selected').val(),
            "espirometria_obs": $('#observacionesEspiro').val(),
            "audiometria": $('input[name=audioSN]:checked').val(),
            "audiometria_resultado": $('input[name=resultadoAudioSN]:checked').val() == null ? "" : $('input[name=resultadoAudioSN]:checked').val(),
            "audiometria_obs": $('#observacionesAudio').val(),
            "psicometrico": $('#psicometricosPara option:selected').val(),
            "psicometrico_obs": $('#observacionesPsiMe').val(),
            "psicologico": $('#psicologicoPara option:selected').val(),
            "psicologico_obs": $('#observacionesPsi').val(),
            "pruebavestibular": $('#vestibularPara option:selected').val(),
            "pruebavestibular_obs": $('#observacionesVesti').val()
        },
        "resultado":{
            "patologia":$('#patologiaRes option:selected').val(),
            "puedeContinuarConLabor":$('input[name=continuarSN]:checked').val(),
            "resultadoValoracionMedica":valoracionMedica,
            "reubicacionLaboral":$('#reubicacionRes option:selected').val(),
            "motivo": $('#motivoRes').val(),
            "recomendacion_medica": medicasResult,
            "recomendacion_ingresosve": ingresoSVE,
            "recomendacion_ocupacional": ocupacionales,
            "recomendacion_habitos": habitosSaludables,
            "recomendacion_pruebacomplementaria": resultadosPC,
            "recomendacion_pruebacomplementaria_otros": $('#otroPC').val(),
            "restricciones": $('#restriccionesID').val(),
            "recomendacionesAdicionales": $('#recomendacionesID').val(),
        }
    };
    var json = Object.assign(json1);
    json1["factores"] = factores;
    json1["accidentes"] = accidentes;
    json1["enfermedades"] = enfermedades;
    json1["paraclinicos"] = paraclinicos;
    json1["idiagnostica"] = Idiagnostica;
    console.log((json));

    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: json,
        type: 'POST',
        url: '/hc/update/'+json['id'],
        dataType: 'json',
        success: function (data) {
            if ( confirm("¿Desea cerrar esta historia clinica?")){
                cerrarHistoriaC(json['id']);
            };
            alert(data.mensaje);
            // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
            location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
        },
        error: function (data) {
            console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                $.each(json.errors, function (key, value) {
                    $(".toast-body").append("<li>"+value+"</li>");
                });
                $("#toastHC").removeClass('hide').addClass('show');
                $("#toastHC").show();
                $('html, body').animate({
                    scrollTop:$('#toastHC').offset().top
                }, 1000);
            }
        }
    });
}

function crearNotaEvolucion(){

    var valoracionMedica = [];
    $("input[name='valoracionMedica_nota[]']:checked").each(function () {
        valoracionMedica += ($(this).val()+",");
    });

    var medicasResult = [];
    $("input[name='medicasresult_nota[]']:checked").each(function () {
        medicasResult += ($(this).val()+",");
    });
    if (medicasResult.length > 0){
        medicasResult = medicasResult.substring(0,medicasResult.length-1);
    }

    var ingresoSVE = [];
    $("input[name='ingresoSVE_nota[]']:checked").each(function () {
        ingresoSVE += ($(this).val()+",");
    });
    if (ingresoSVE.length > 0){
        ingresoSVE = ingresoSVE.substring(0,ingresoSVE.length-1);
    }

    var ocupacionales = [];
    $("input[name='ocupacionales_nota[]']:checked").each(function () {
        ocupacionales += ($(this).val()+",");
    });
    if (ocupacionales.length > 0){
        ocupacionales = ocupacionales.substring(0,ocupacionales.length-1);
    }

    var habitosSaludables = [];
    $("input[name='habitosSaludables_nota[]']:checked").each(function () {
        habitosSaludables += ($(this).val()+",");
    });
    if (habitosSaludables.length > 0){
        habitosSaludables = habitosSaludables.substring(0,habitosSaludables.length-1);
    }

    var resultadosPC = [];
    $("input[name='pruebasComplementarias_nota[]']:checked").each(function () {
        resultadosPC += ($(this).val()+",");
    });
    if (resultadosPC.length > 0){
        resultadosPC = resultadosPC.substring(0,resultadosPC.length-1);
    }

    var notas = [];

    $('.nuevo').each(function () {
        notas.push({
            "fecha_nota":$(this).find('#fechaNota').val(),
            "observacion_nota":$(this).find('#observacion_nota').val(),
            "historiaclinica_id": $('#llave').val()
        });
    });

    var json = {
        "historiaclinica_id": $('#llave').val(),
        "nota_valoracion_medica": valoracionMedica,
        "nota_recomendacion_medica": medicasResult,
        "nota_recomendacion_ingreso": ingresoSVE,
        "nota_recomendacion_ocupacional": ocupacionales,
        "nota_recomendacion_habito": habitosSaludables,
        "nota_complementaria": resultadosPC,
        "nota_complementaria_otro": $('#otroPC_nota').val(),
        "nota_restriccion": $('#restriccionesID_nota').val(),
        "nota_recomendacion_adicional": $('#recomendacionesID_nota').val(),
        "nuevaValoracionSN": $('input[name=nuevaValoracionSN]:checked').val(),
        "notas": notas
    };

    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: json,
        type: 'POST',
        url: '/hc/crearNota',
        dataType: 'json',
        success: function (data) {
            if (data.tipo == 0 || data.tipo == 1 || data.tipo == 2){
                alert(data.mensaje);
                location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
            }else{
                alert(data.mensaje);
                console.log(data);
            }
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function cerrarHistoriaC(id){
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            'medicoCierre': $('#id_medicoC').val()
        },
        type: 'POST',
        url: '/hc/cerrar/'+id,
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
            location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function anularHistoriaC(){
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "llave": $('#llave').val(),
            'medicoCierre': $('#id_medicoC').val()
        },
        type: 'POST',
        url: '/hc/anular',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
            location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
        },
        error: function (data) {
            console.log(data);
        },
    });
}

// function crearNotaEvolucion(id){
//     $.ajax({
//         headers: {
//             'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
//         },
//         data: {
//             "llave": $('#llave').val(),
//             "fechaNota": $('#fechaNota').val(),
//             "notaObservacion": $('#notaObs').val()
//         },
//         type: 'POST',
//         url: '/hc/crearNota',
//         dataType: 'json',
//         success: function (data) {
//             alert(data.mensaje);
//         },
//         error: function (data) {
//             console.log(data);
//         },
//     });
// }
