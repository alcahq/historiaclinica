$(document).ready(function () {

    $('#toastHA').hide();

    //Buscar paciente en HC
    $('#documentoBuscado').on('keyup', function (e) {
        e.preventDefault();
        if (e.keyCode == 13){
            buscarPaciente();
        }
    });

    $('#documentoBuscado').focusout(function (e) {
        e.preventDefault();
        buscarPaciente();
    });

    //Auto completar empresa
    $('#empresaActual').easyAutocomplete({
        url: function(phrase) {
            return "/empresa/autocomplete";
        },

        getValue: function(element) {
            return element.nombre;
        },

        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },

        preparePostData: function(data) {
            data.phrase = $("#empresaActual").val();
            return data;
        },

        list: {
            onChooseEvent: function() {
                $("#empresaFR0" ).val($("#empresaActual").val());

            }
        },
        requestDelay: 10
    });


    $('#registrarHA').on('click', function (e) {
        e.preventDefault();
        if (confirm("¿Desea registrar la historia de audiometria?")){
            registrarHA();
        }
    });

    //Bloquear campos cuando el estado es cerrado o anulado
    var estado = $('#estadoHA').val();
    if (estado != 'Abierta'){
        console.log(estado);
        $('input').attr('disabled', true);
        $('select').attr('disabled', true);
        $('textarea').attr('disabled', true);
    }
    if(estado != 'Anulada' && estado != null){
        $('#anularHA').attr("disabled", false);
    };
    $('#aceptarHA').removeAttr("disabled");

    $('#aceptarHA').on('click', function () {
        // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
        location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
    });

    //Anular historia clinica
    $('#anularHA').on('click', function () {
        if ( confirm("¿Desea anular esta historia clinica?")){
            anularHistoriaA();
        };
    });

    $('.OD').focusout(function () {
       updateChartOD();
    });

    $('.OI').focusout(function () {
        updateChartOI();
    });

    //GRAFICAS
    var myChartOD;
    var myChartOI;

    graficaOidoDerecho();

    graficaOidoIzquierdo();

    calcularPTA();

});

function anularHistoriaA(){
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "id": $('#id').val(),
            'user_id': $('#user_id').val()
        },
        type: 'POST',
        url: '/ha/anular',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            // location.href = "http://190.145.137.44:8004/usuario/buscarHistorias";
            location.href = "http://"+window.location.host+"/usuario/buscarHistorias";
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function graficaOidoDerecho() {

    var OD250 = $('#OD250').val();
    var OD500 = $('#OD500').val();
    var OD1000 = $('#OD1000').val();
    var OD2000 = $('#OD2000').val();
    var OD3000 = $('#OD3000').val();
    var OD4000 = $('#OD4000').val();
    var OD6000 = $('#OD6000').val();
    var OD8000 = $('#OD8000').val();

    if (OD250 == ""){
        OD250 = 110;
    }
    if (OD500 == ""){
        OD500 = 110;
    }
    if (OD1000 == ""){
        OD1000 = 110;
    }
    if (OD2000 == ""){
        OD2000 = 110;
    }
    if (OD3000 == ""){
        OD3000 = 110;
    }
    if (OD4000 == ""){
        OD4000 = 110;
    }
    if (OD6000 == ""){
        OD6000 = 110;
    }
    if (OD8000 == ""){
        OD8000 = 110;
    }
    myChartOD = new Chart($('#myChart'), {
            type: 'line',
            data: {
                labels: [250, 500, 1000, 2000, 3000, 4000, 6000, 8000],
                datasets: [{
                    label: 'OIDO DERECHO',
                    data: [OD250, OD500, OD1000, OD2000, OD3000, OD4000, OD6000, OD8000],
                    borderWidth: 1,
                    pointBorderColor: 'orange',
                    pointBackgroundColor: 'rgba(255,150,0,0.5)',
                    pointRadius: 6,
                    pointHoverRadius: 5,
                    pointHitRadius: 30,
                    pointBorderWidth: 4,
                    pointStyle: 'circle'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            reverse:true,
                            min:-10,
                            max:100

                        }
                    }],
                    xAxes: [{
                        offset:true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                elements:{
                    line:{
                        tension:0,
                        backgroundColor: 'transparent',
                        borderColor: 'orange'
                    }
                }
            }
});
}

function graficaOidoIzquierdo() {

    var OI250 = $('#OI250').val();
    var OI500 = $('#OI500').val();
    var OI1000 = $('#OI1000').val();
    var OI2000 = $('#OI2000').val();
    var OI3000 = $('#OI3000').val();
    var OI4000 = $('#OI4000').val();
    var OI6000 = $('#OI6000').val();
    var OI8000 = $('#OI8000').val();

    if (OI250 == ""){
        OI250 = 110;
    }
    if (OI500 == ""){
        OI500 = 110;
    }
    if (OI1000 == ""){
        OI1000 = 110;
    }
    if (OI2000 == ""){
        OI2000 = 110;
    }
    if (OI3000 == ""){
        OI3000 = 110;
    }
    if (OI4000 == ""){
        OI4000 = 110;
    }
    if (OI6000 == ""){
        OI6000 = 110;
    }
    if (OI8000 == ""){
        OI8000 = 110;
    }

    myChartOI = new Chart($('#myChartOI'), {
        type: 'line',
        data: {
            labels: [250, 500, 1000, 2000, 3000, 4000, 6000, 8000],
            datasets: [{
                label: 'OIDO IZQUIERDO',
                data: [OI250, OI500, OI1000, OI2000, OI3000, OI4000, OI6000, OI8000],
                borderWidth: 1,
                pointBorderColor: 'blue',
                pointBackgroundColor: 'rgba(0,53,148,0.5)',
                pointRadius: 6,
                pointHoverRadius: 5,
                pointHitRadius: 30,
                pointBorderWidth: 4,
                pointStyle: 'circle'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        reverse:true,
                        min:-10,
                        max:100

                    }
                }],
                xAxes: [{
                    offset:true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            elements:{
                line:{
                    tension:0,
                    backgroundColor: 'transparent',
                    borderColor: 'blue'
                }
            }
        }
    });

}

function addDataOD(chart, data) {
    chart.data.datasets.forEach((dataset) => {
        dataset.data = (data);
    });
    chart.update();
}

function addDataOI(chart, data) {
    chart.data.datasets.forEach((dataset) => {
        dataset.data = (data);
    });
    chart.update();
}

function updateChartOD() {
    var OD250 = ($('#OD250').val());
    var OD500 = ($('#OD500').val());
    var OD1000 = ($('#OD1000').val());
    var OD2000 = ($('#OD2000').val());
    var OD3000 = ($('#OD3000').val());
    var OD4000 = ($('#OD4000').val());
    var OD6000 = ($('#OD6000').val());
    var OD8000 = ($('#OD8000').val());
    if (OD250 == ""){
        OD250 = 110;
    }
    if (OD500 == ""){
        OD500 = 110;
    }
    if (OD1000 == ""){
        OD1000 = 110;
    }
    if (OD2000 == ""){
        OD2000 = 110;
    }
    if (OD3000 == ""){
        OD3000 = 110;
    }
    if (OD4000 == ""){
        OD4000 = 110;
    }
    if (OD6000 == ""){
        OD6000 = 110;
    }
    if (OD8000 == ""){
        OD8000 = 110;
    }
    var data = [OD250,OD500,OD1000,OD2000,OD3000,OD4000,OD6000,OD8000];
    addDataOD(myChartOD,data);

    $('#ptaOD').val(parseFloat(parseFloat(OD500)+parseFloat(OD1000)+parseFloat(OD2000)+parseFloat(OD3000))/4);
}

function updateChartOI() {
    var OI250 = ($('#OI250').val());
    var OI500 = ($('#OI500').val());
    var OI1000 = ($('#OI1000').val());
    var OI2000 = ($('#OI2000').val());
    var OI3000 = ($('#OI3000').val());
    var OI4000 = ($('#OI4000').val());
    var OI6000 = ($('#OI6000').val());
    var OI8000 = ($('#OI8000').val());
    if (OI250 == ""){
        OI250 = 110;
    }
    if (OI500 == ""){
        OI500 = 110;
    }
    if (OI1000 == ""){
        OI1000 = 110;
    }
    if (OI2000 == ""){
        OI2000 = 110;
    }
    if (OI3000 == ""){
        OI3000 = 110;
    }
    if (OI4000 == ""){
        OI4000 = 110;
    }
    if (OI6000 == ""){
        OI6000 = 110;
    }
    if (OI8000 == ""){
        OI8000 = 110;
    }
    var data = [OI250,OI500,OI1000,OI2000,OI3000,OI4000,OI6000,OI8000];
    addDataOI(myChartOI,data);

    $('#ptaOI').val(parseFloat(parseFloat(OI500)+parseFloat(OI1000)+parseFloat(OI2000)+parseFloat(OI3000))/4);
}

function calcularPTA() {
    var OD250 = ($('#OD250').val());
    var OD500 = ($('#OD500').val());
    var OD1000 = ($('#OD1000').val());
    var OD2000 = ($('#OD2000').val());
    var OD3000 = ($('#OD3000').val());
    var OD4000 = ($('#OD4000').val());
    var OD6000 = ($('#OD6000').val());
    var OD8000 = ($('#OD8000').val());

    var OI250 = ($('#OI250').val());
    var OI500 = ($('#OI500').val());
    var OI1000 = ($('#OI1000').val());
    var OI2000 = ($('#OI2000').val());
    var OI3000 = ($('#OI3000').val());
    var OI4000 = ($('#OI4000').val());
    var OI6000 = ($('#OI6000').val());
    var OI8000 = ($('#OI8000').val());

    $('#ptaOD').val(parseFloat(parseFloat(OD500)+parseFloat(OD1000)+parseFloat(OD2000)+parseFloat(OD3000))/4);
    $('#ptaOI').val(parseFloat(parseFloat(OI500)+parseFloat(OI1000)+parseFloat(OI2000)+parseFloat(OI3000))/4);
}

function buscarPaciente() {
    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "documento": $('#documentoBuscado').val()
        },
        type: 'POST',
        url: '/paciente/findByDocument',
        dataType: 'json',
        success: function (data) {
            var nombre = data.nombre + " " +  data.apellido;
            var documento = "CC: " + data.documento;
            var edad = "Edad: " + data.edad +" años";
            var cargo = data.cargo;
            console.log(data.edad);
            var genero = data.genero;
            $('#nombrePaciente').val(nombre);
            $('#documentoPaciente').val(documento);
            $('#edadPaciente').val(edad);
            $('#cargoE').val(cargo);

        },
        error: function (data) {
            //console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                console.log(json);
                $.each(json.errors, function (key, value) {
                    $.each(value, function(key1,value1){
                        $(".toast-body").append("<li>"+value1+"</li>");
                    });
                });
                $("#toastHA").removeClass('hide').addClass('show');
                $("#toastHA").show();
            }
        }
    });
}

function registrarHA() {

    //Split cedula medico y paciente
    var cedulaM = $('#medicoCedula').val();
    cedulaM = cedulaM.split(" ")[1];

    var cedulaP = $('#documentoPaciente').val();
    cedulaP = cedulaP.split(" ")[1];

    var otros = [];
    $("input[name='otros[]']:checked").each(function () {
        otros += $(this).val()+",";
    });

    var tiempoExp = [];
    $("input[name='tiempoExp[]']:checked").each(function () {
        tiempoExp += $(this).val()+",";
    });

    var PANormal = [];
    $("input[name='PANormal[]']:checked").each(function () {
        PANormal += $(this).val()+",";
    });

    var PAArtesia = [];
    $("input[name='PAArtesia[]']:checked").each(function () {
        PAArtesia += $(this).val()+",";
    });

    var PAAgenesia = [];
    $("input[name='PAAgenesia[]']:checked").each(function () {
        PAAgenesia += $(this).val()+",";
    });

    var PACicatriz = [];
    $("input[name='PACicatriz[]']:checked").each(function () {
        PACicatriz += $(this).val()+",";
    });

    var PACicatriz = [];
    $("input[name='PACicatriz[]']:checked").each(function () {
        PACicatriz += $(this).val()+",";
    });

    var MTNormal = [];
    $("input[name='MTNormal[]']:checked").each(function () {
        MTNormal += $(this).val()+",";
    });

    var MTPerforada = [];
    $("input[name='MTPerforada[]']:checked").each(function () {
        MTPerforada += $(this).val()+",";
    });

    var MThiperemica = [];
    $("input[name='MThiperemica[]']:checked").each(function () {
        MThiperemica += $(this).val()+",";
    });

    var MTPlaca = [];
    $("input[name='MTPlaca[]']:checked").each(function () {
        MTPlaca += $(this).val()+",";
    });

    var MTOpaca = [];
    $("input[name='MTOpaca[]']:checked").each(function () {
        MTOpaca += $(this).val()+",";
    });

    var MTAbultada = [];
    $("input[name='MTAbultada[]']:checked").each(function () {
        MTAbultada += $(this).val()+",";
    });

    var MTNoVisu = [];
    $("input[name='MTNoVisu[]']:checked").each(function () {
        MTNoVisu += $(this).val()+",";
    });

    var CAENormal = [];
    $("input[name='CAENormal[]']:checked").each(function () {
        CAENormal += $(this).val()+",";
    });

    var CAETapon = [];
    $("input[name='CAETapon[]']:checked").each(function () {
        CAETapon += $(this).val()+",";
    });

    var CAETaponT = [];
    $("input[name='CAETaponT[]']:checked").each(function () {
        CAETaponT += $(this).val()+",";
    });

    var recomendaciones = [];
    $("input[name='recomendaciones[]']:checked").each(function () {
        recomendaciones += $(this).val()+",";
    });

    var charoi = $('#myChartOI').get(0).toDataURL();
    charoi = charoi.substring(22,charoi.length);

    var charod = $('#myChart').get(0).toDataURL();
    charod = charod.substring(22,charod.length);

    var  json = {
        "tipo": 0,
        "documento": $('#documentoBuscado').val(),
        "fecha": $('#fechaRegistro').val(),
        "medicoApertura_id": $('#id_medicoA').val(),
        "medicoCierre_id": $('#id_medicoA').val(),
        "motivoevaluacion": $('#motivoE option:selected').val(),
        "cargoEvaluar": $('#cargoE').val(),
        "motivoevaluacion_otros": otros,
        "empresa": $('#empresaActual').val(),
        "audiogramaOI":charoi,
        "audiogramaOD":charod,
        "antecedentes":{
            "ocupacionales_horario": $('#ocupacionales_horario').val(),
            "ocupacionales_ocupacionanterior": $('#ocupacionales_ocupacionanterior').val(),
            "ocupacionales_ocupacionactual": $('#ocupacionales_ocupacionactual').val(),
            "ocupacionales_ocupacionanterior_tiempo": $('#ocupacionales_ocupacionanterior_tiempo').val(),
            "ocupacionales_utilizaproteccion_tipo": $('#ocupacionales_utilizaproteccion_tipo').val(),
            "ocupacionales_jornadalaboral": $('#ocupacionales_jornadalaboral').val(),
            "ocupacionales_utilizaproteccion_tiempo": $('#ocupacionales_utilizaproteccion_tiempo').val(),
            "ocupacionales_laboresdesempena": $('#ocupacionales_laboresdesempena').val(),
            "ocupacionales_tiempoexposicion": tiempoExp,
            "ocupacionales_utilizaproteccion": $('input[name=ocupacionales_utilizaproteccion]:checked').val(),
            "ocupacionales_ocupacionactual_tiempo": $('#ocupacionales_ocupacionactual_tiempo').val(),
            "familiares_otologicos": $('#familiares_otologicos').val(),
            "otologicos_otorrea": $('input[name=otologicos_otorrea]:checked').val(),
            "otologicos_cuales": $('#otologicos_cuales').val(),
            "otologicos_prurito": $('input[name=otologicos_prurito]:checked').val(),
            "otologicos_otitis": $('input[name=otologicos_otitis]:checked').val(),
            "otologicos_otros": $('input[name=otologicos_otros]:checked').val(),
            "otologicos_vertigo": $('input[name=otologicos_vertigo]:checked').val(),
            "otologicos_sensacionoido": $('input[name=otologicos_sensacionoido]:checked').val(),
            "otologicos_otalgia": $('input[name=otologicos_otalgia]:checked').val(),
            "otologicos_tinitus": $('input[name=otologicos_tinitus]:checked').val(),

            "patologico_cuales": $('#patologico_cuales').val(),
            "patologico_hipertension": $('input[name=patologico_hipertension]:checked').val(),
            "patologico_rinitis": $('input[name=patologico_rinitis]:checked').val(),
            "patologico_sarampion": $('input[name=patologico_sarampion]:checked').val(),
            "patologico_parotiditis": $('input[name=patologico_parotiditis]:checked').val(),
            "patologico_diabetes": $('input[name=patologico_diabetes]:checked').val(),
            "patologico_otros": $('input[name=patologico_otros]:checked').val(),
            "patologico_rubeola": $('input[name=patologico_rubeola]:checked').val(),

            "extralaboral_tejo": $('input[name=extralaboral_tejo]:checked').val(),
            "extralaboral_moto": $('input[name=extralaboral_moto]:checked').val(),
            "extralaboral_cuales": $('#extralaboral_cuales').val(),
            "extralaboral_musica": $('input[name=extralaboral_musica]:checked').val(),
            "extralaboral_serviciomilitar": $('input[name=extralaboral_serviciomilitar]:checked').val(),
            "extralaboral_otros": $('input[name=extralaboral_otros]:checked').val(),
            "extralaboral_audifonos": $('input[name=extralaboral_audifonos]:checked').val(),

            "toxicosnervio_industriales": $('input[name=toxicosnervio_industriales]:checked').val(),
            "toxicosnervio_farmacos": $('input[name=toxicosnervio_farmacos]:checked').val(),

            "quirurgicos_cirugiaoido": $('input[name=quirurgicos_cirugiaoido]:checked').val(),
            "quirurgicos_timpanoplastia": $('input[name=quirurgicos_timpanoplastia]:checked').val(),
            "quirurgicos_cuales": $('#quirurgicos_cuales').val(),
            "quirurgicos_otros": $('input[name=quirurgicos_otros]:checked').val(),

            "traumaticos_cuales": $('#traumaticos_cuales').val(),
            "traumaticos_craneo": $('input[name=traumaticos_craneo]:checked').val(),
            "traumaticos_acustico": $('input[name=traumaticos_acustico]:checked').val(),
            "traumaticos_otros": $('input[name=traumaticos_otros]:checked').val(),
        },
        "otoscopia":{
            "membranatimpanica_otros": $('#membranatimpanica_otros').val(),
            "cae_tapontotal": CAETaponT,
            "cae_taponparcial": CAETapon,
            "cae_normal": CAENormal,
            "pabellonauricular_otros": $('#pabellonauricular_otros').val(),
            "membranatimpanica_hiperemica": MThiperemica,
            "cae_otros": $('#cae_otros').val(),
            "pabellonauricular_atresia": PAArtesia,
            "pabellonauricular_normal": PANormal,
            "membranatimpanica_perforada": MTPerforada,
            "pabellonauricular_cicatriz": PACicatriz,
            "membranatimpanica_normal": MTNormal,
            "membranatimpanica_placacalcarea": MTPlaca,
            "membranatimpanica_abultada": MTAbultada,
            "pabellonauricular_agenesia": PAAgenesia,
            "membranatimpanica_nosevisualiza": MTNoVisu,
            "membranatimpanica_opaca": MTOpaca,
        },
        "audiograma":[
            {
                "oidoderecho":$('#OD250').val(),
                "oidoizquierdo":$('#OI250').val(),
                "nivel_id":1
            },
            {
                "oidoderecho":$('#OD500').val(),
                "nivel_id":2,
                "oidoizquierdo":$('#OI500').val()
            },
            {
                "nivel_id":3,
                "oidoderecho":$('#OD1000').val(),
                "oidoizquierdo":$('#OI1000').val()
            },
            {
                "nivel_id":4,
                "oidoizquierdo":$('#OI2000').val(),
                "oidoderecho":$('#OD2000').val()
            },
            {
                "oidoderecho":$('#OD3000').val(),
                "oidoizquierdo":$('#OI3000').val(),
                "nivel_id":5
            },
            {
                "oidoizquierdo":$('#OI4000').val(),
                "nivel_id":6,
                "oidoderecho":$('#OD4000').val()
            },
            {
                "nivel_id":7,
                "oidoderecho":$('#OD6000').val(),
                "oidoizquierdo":$('#OI6000').val()
            },
            {
                "nivel_id":8,
                "oidoderecho":$('#OD8000').val(),
                "oidoizquierdo":$('#OI8000').val()
            }
        ],
        "recomendaciones":{
            "impresiondiagnostica":$('#impresiondiagnostica').val(),
            "pacientecompatible_obs":$('#pacientecompatible_obs').val(),
            "recomendaciones": recomendaciones,
            "requiereremision":$('input[name=requiereremision]:checked').val(),
            "requierevaloracion_obs":$('#requierevaloracion_obs').val(),
            "requiereremision_obs":$('#requiereremision_obs').val(),
            "pacientecompatible": $('input[name=pacientecompatible]:checked').val(),
            "requierevaloracion": $('input[name=requierevaloracion]:checked').val()
        },
    };

    $.ajax({
        headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        },
        data: json,
        type: 'POST',
        url: '/audiometria/register',
        dataType: 'json',
        success: function (data) {
            alert(data.mensaje);
            location.reload(true);
        },
        error: function (data) {
            console.log(data);
        },
        statusCode: {
            422:function (data) {
                var json = (JSON.parse(data.responseText));
                $('.toast-body').empty();
                $.each(json.errors, function (key, value) {
                    $(".toast-body").append("<li>"+value+"</li>");
                });
                $("#toastHA").removeClass('hide').addClass('show');
                $("#toastHA").show();
                $('html, body').animate({
                    scrollTop:$('#toastHA').offset().top
                }, 1000);
            }
        }
    });
}
